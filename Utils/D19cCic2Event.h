/*

        \file                          Event.h
        \brief                         Event handling from DAQ
        \author                        Nicolas PIERRE
        \version                       1.0
        \date                                  10/07/14
        Support :                      mail to : nicolas.pierre@icloud.com

 */

#ifndef __D19cCic2Event_H__
#define __D19cCic2Event_H__

#include "Event.h"
#include "Local2SHit.h"
#include <iterator>
#include <numeric>

namespace Ph2_HwInterface
{
using HybridData    = std::pair<std::pair<uint16_t, uint16_t>, std::vector<uint32_t>>;
using RawHybridData = std::pair<std::pair<uint16_t, uint16_t>, std::vector<std::bitset<RAW_L1_CBC>>>;

using EventList    = std::vector<HybridData>;
using RawEventList = std::vector<RawHybridData>;
/*!
 * \class CicEvent
 * \brief Event container to manipulate event flux from the Cbc2
 */
class D19cCic2Event : public Event
{
  public:
    /*!
     * \brief Constructor of the Event Class
     * \param pBoard : Board to work with
     * \param pNbCbc
     * \param pEventBuf : the pointer to the raw Event buffer of this Event
     */
    D19cCic2Event(const Ph2_HwDescription::BeBoard* pBoard, const std::vector<uint32_t>& list, bool pWith8CBC3, bool pWithTLU = false);
    /*!
     * \brief Copy Constructor of the Event Class
     */
    // CicEvent ( const Event& pEvent );
    /*!
     * \brief Destructor of the Event Class
     */
    ~D19cCic2Event()
    {
        fEventHitList.clear();
        fEventStubList.clear();
        // fEventMap.clear();
        // fEventDataList.clear();
    }

    /*!
     * \brief Set an Event to the Event map
     * \param pEvent : Event to set
     * \return Aknowledgement of the Event setting (1/0)
     */
    void Set(const Ph2_HwDescription::BeBoard* pBoard, const std::vector<uint32_t>& list) override;
    /*!
     * \brief Set an Event to the Event map
     * \param pEvent : Event to set
     * \return Aknowledgement of the Event setting (1/0)
     */
    void SetEvent(const Ph2_HwDescription::BeBoard* pBoard, uint32_t pNbCbc, const std::vector<uint32_t>& list) override;

    /*!
     * \brief Get the Cbc Event counter
     * \return Cbc Event counter
     */
    uint32_t GetEventCountCBC() const override { return fEventCountCBC; }

    // private members of cbc3 events only
    uint32_t getBeBoardId() const { return fBeId; }
    uint8_t  GetFWType() const { return fBeFWType; }
    uint32_t GetCbcDataType() const { return fCBCDataType; }
    uint32_t GetNCbc() const { return fNCbc; }
    uint32_t GetEventDataSize() const { return fEventDataSize; }
    uint32_t GetBeStatus() const { return fBeStatus; }
    /*!
     * \brief Convert Data to Hex string
     * \return Data string in hex
     */
    std::string HexString() const override;
    /*!
     * \brief Function to get bit string in hexadecimal format for CBC data
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return Data Bit string in Hex
     */
    std::string DataHexString(uint8_t pHybridId, uint8_t pCbcId) const override;

    /*!
     * \brief Function to get Error bit
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \param i : Error bit number i
     * \return Error bit
     */
    bool Error(uint8_t pHybridId, uint8_t pCbcId, uint32_t i) const override;
    /*!
     * \brief Function to get all Error bits
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return Error bit
     */
    uint32_t Error(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Function to get pipeline address
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return Pipeline address
     */
    uint32_t PipelineAddress(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Function to get a CBC pixel bit data
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \param i : pixel bit data number i
     * \return Data Bit
     */
    bool DataBit(uint8_t pHybridId, uint8_t pCbcId, uint32_t i) const override;
    /*!
     * \brief Function to get bit string of CBC data
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return Data Bit string
     */
    std::string DataBitString(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Function to get bit vector of CBC data
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return Data Bit vector
     */
    std::vector<bool> DataBitVector(uint8_t pHybridId, uint8_t pCbcId) const override;
    std::vector<bool> DataBitVector(uint8_t pHybridId, uint8_t pCbcId, const std::vector<uint8_t>& channelList) const override;
    /*!
     * \brief Function to get GLIB flag string
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return Glib flag string
     */
    std::string GlibFlagString(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Function to get Stub bit
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return stub bit?
     */
    std::string StubBitString(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Function to get Stub bit
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return stub bit?
     */
    bool StubBit(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Get a vector of Stubs - will be empty for Cbc2
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     */
    std::vector<Stub> StubVector(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Function to count the Hits in this event
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return number of hits
     */
    uint32_t GetNHits(uint8_t pHybridId, uint8_t pCbcId) const override;
    /*!
     * \brief Function to get a sparsified hit vector
     * \param pHybridId : Hybrid Id
     * \param pCbcId : Cbc Id
     * \return vector with hit channels
     */
    std::vector<uint64_t> GetHitsLocalCoord(Ph2_HwDescription::ReadoutChip* pChip);
    std::vector<uint32_t> GetHits(uint8_t pHybridId, uint8_t pCbcId) const override;
    std::vector<Cluster>  clusterize(uint8_t pHybridId) const;

    std::vector<Cluster>  getClusters(uint8_t pHybridId, uint8_t pCbcId) const override;
    uint8_t               GetNStripClusters(uint8_t pHybridId) const;
    uint8_t               GetNPixelClusters(uint8_t pHybridId) const;
    std::vector<SCluster> GetStripClusters(uint8_t pHybridId, uint8_t pMPAId) const;
    std::vector<PCluster> GetPixelClusters(uint8_t pHybridId, uint8_t pMPAId) const;

    void fillChipDataContainer(ChipDataContainer* chipContainer, const std::shared_ptr<ChannelGroupBase> testChannelGroup, uint16_t hybridId) override;

    void     print(std::ostream& out) const override;
    uint16_t L1Status(uint8_t pHybridId) const;
    uint32_t L1Id(uint8_t pHybridId, uint8_t pReadoutChipId) const;
    uint32_t BxId(uint8_t pHybridId) const override;
    uint16_t Status(uint8_t pHybridId) const;

    std::bitset<NMPACHANNELS> decodePClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const;
    std::bitset<NSSACHANNELS> decodeSClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const;
    std::bitset<NCHANNELS>    decodeClusters(uint8_t pHybridId, uint8_t pReadoutChipId) const;
    std::bitset<RAW_L1_CBC>   getRawL1Word(uint8_t pHybridId, uint8_t pReadoutChipId) const;
    size_t                    getHybridIndex(const uint8_t pHybridId) const
    {
        // first find feIndex
        auto cHybridIterator = std::find(fHybridIds.begin(), fHybridIds.end(), pHybridId);
        if(cHybridIterator != fHybridIds.end()) { return std::distance(fHybridIds.begin(), cHybridIterator); }
        else
        {
            LOG(INFO) << BOLDRED << "HybridId " << +pHybridId << " not found in D19cCic2Event .. check xml!" << RESET;
            throw std::runtime_error(std::string("HybridId not found in D19cCic2Event .. check xml!"));
        }
    }
    size_t getChipIndex(const uint8_t pHybridIndex, const uint8_t pChipId) const
    {
        // first find feIndex
        auto cChipIterator = std::find(fChipIds[pHybridIndex].begin(), fChipIds[pHybridIndex].end(), pChipId);
        if(cChipIterator != fChipIds[pHybridIndex].end()) { return std::distance(fChipIds[pHybridIndex].begin(), cChipIterator); }
        else
            throw std::runtime_error(std::string("ChipId not found in D19cCic2Event .. check xml!"));
    }

    void set8CBC3(bool pIs8CBC3) { fIs8CBC3 = pIs8CBC3; }

  private:
    // figure out how to switch between various hybrid types here
    std::vector<uint8_t> fFeMapping2S{0, 1, 2, 3, 7, 6, 5, 4};   // Index Hybrid Hybrid Id , Value CIC Hybrid Id
    std::vector<uint8_t> fFeMapping8BC3{3, 2, 1, 0, 4, 5, 6, 7}; // Index CIC Hybrid Id , Value Hybrid Hybrid Id - double check this!
    std::vector<uint8_t> fFeMappingPSR{6, 7, 3, 2, 1, 0, 4, 5};  //  Index Hybrid Hybrid Id , Value CIC Hybrid Id
    std::vector<uint8_t> fFeMappingPSL{1, 0, 4, 5, 6, 7, 3, 2};  // Index hybrid Hybrid Id , Value CIC Hybrid Id

    std::vector<uint8_t>              fFeMapping; //{3, 2, 1, 0, 4, 5, 6, 7}; // FE --> FE CIC
    std::vector<uint8_t>              fHybridIds;
    std::vector<uint8_t>              fHybridIdsCic;
    std::vector<std::vector<uint8_t>> fChipIds;
    std::vector<uint8_t>              fNStripClusters;
    std::vector<uint8_t>              fNPxlClusters;

    uint8_t      fTLUenabled   = 0;
    bool         fIs2S         = true;
    bool         fIs8CBC3      = false;
    bool         fIsSparsified = true;
    EventList    fEventHitList;
    RawEventList fEventRawList;
    EventList    fEventStubList;
    // mapped id
    // takes chip id on the hybrid
    // returns chip id in the CIC
    uint8_t getChipIdMapped(uint8_t pHybridId, uint8_t pReadoutChipId) const
    {
        pReadoutChipId = pReadoutChipId % 8;
        // assign front-end mapping
        std::vector<uint8_t> cHybridMapping = (fIs2S) ? fFeMapping2S : fFeMappingPSR;
        if(!fIs2S) cHybridMapping = (pHybridId % 2 == 0) ? fFeMappingPSR : fFeMappingPSL;
        if(fIs8CBC3 && fIs2S) cHybridMapping = fFeMapping8BC3;

        // if( fIs2S && cHybridIds.size() == 0 ) return 0;
        // else if( fIs2S ) return (cHybridIds.size() - 1) - std::distance(cHybridIds.begin(), std::find(cHybridIds.begin(), cHybridIds.end(), pReadoutChipId));
        // else  return std::distance(cHybridIds.begin(), std::find(cHybridIds.begin(), cHybridIds.end(), pReadoutChipId));
        // if(fIs2S)
        // {
        //     return (7 - std::distance(cHybridMapping.begin(), std::find(cHybridMapping.begin(), cHybridMapping.end(), pReadoutChipId)));
        // }
        // else
        return cHybridMapping[pReadoutChipId]; // std::distance(cHybridMapping.begin(), std::find(cHybridMapping.begin(), cHybridMapping.end(), pReadoutChipId));
    }

    std::vector<Cluster> formClusters(std::vector<uint32_t> pHits, int pSensorId) const
    {
        std::vector<Cluster> cClusters;
        if(pHits.size() != 0)
        {
            auto cFirstHit = pHits[0];
            std::transform(pHits.begin(), pHits.end(), pHits.begin(), [cFirstHit](int c) { return c -= cFirstHit; });
            std::vector<int> cDifference(pHits.size());
            std::adjacent_difference(pHits.begin(), pHits.end(), cDifference.begin()); // difference between consecutive elements
            auto cIter  = cDifference.begin();
            auto cStart = cDifference.begin();
            do {
                cIter = std::find_if(cIter, cDifference.end(), [](int i) { return (i > 1); });
                Cluster cCluster;
                cCluster.fSensor       = pSensorId;
                cCluster.fFirstStrip   = pHits[std::distance(cDifference.begin(), cStart)];
                cCluster.fClusterWidth = pHits[std::distance(cDifference.begin(), cIter - 1)] - cCluster.fFirstStrip + 1;
                cCluster.fFirstStrip += cFirstHit;
                cClusters.push_back(cCluster);
                cStart = cIter;
                cIter += 1;
            } while(cStart < cDifference.end());
        }
        return cClusters;
    }
    std::bitset<NCHANNELS> hitsFromClusters(uint8_t pHybridId, uint8_t pReadoutChipId);

    // templated decoding function
    // split stream of data
    template <std::size_t N>
    std::bitset<N> decodeCicClusters(uint8_t pHybridId, uint8_t pReadoutChipId)
    {
        auto&          cClusterWords = fEventHitList[getHybridIndex(pHybridId)].second;
        std::bitset<N> cBitSet(0);
        // size_t                 cClusterId = 0;
        // for( auto cCluster : cClusterWords )
        // {
        // uint8_t cChipId = (cCluster & ((0x7) << (0+4+3+7))) >> (0+4+3+7);
        // auto  cChipIdMapped = this->getChipIdMapped(pHybridId, pReadoutChipId);
        // if(cChipId == cChipIdMapped)
        // {
        //     //figure out if it is an S or a P cluster
        //     uint8_t cFlag = ( cCluster & (0x1 << 31) ) >> 31 ;
        //     if( cFlag == 0 ) // s cluster
        //     {
        //         SCluster cSCluster;
        //         cSCluster.fAddress = (cCluster & ((0x7F) << (0+1+3))) >> (0+1+3);
        //         cSCluster.fWidth = (cCluster & ((0x7) << (0+1))) >> (0+1);
        //         cSCluster.fMip = (cCluster & ((0x1) << 0)) >> 0;
        //         //cSClusters.push_back(cSCluster);
        //         LOG(DEBUG) << BOLDRED << "S-cluster, address : " << unsigned(cSCluster.fAddress)<<","<<unsigned(cSCluster.fWidth)<<","<< unsigned(cSCluster.fMip)<< RESET;
        //     }
        //     else
        //     {
        //         PCluster aPCluster;
        //         aPCluster.fAddress = (cCluster & ((0x7F) << (0+4+3))) >> (0+4+3);
        //         aPCluster.fWidth = (cCluster & ((0x7) << (0+4))) >> (0+4);
        //         aPCluster.fZpos = (cCluster & ((0xF) << 0)) >> 0;
        //         //cPClusters.push_back(aPCluster);
        //         LOG(DEBUG) << BOLDGREEN << "P-cluster, address : " << unsigned(aPCluster.fAddress)<<","<<unsigned(aPCluster.fWidth)<<","<< unsigned(aPCluster.fZpos)<< RESET;
        //     }
        //     cClusterId++;
        // }
        // }
        return cBitSet;
    }

    // L1 Id from chip
    void       printL1Header(std::ostream& os, uint8_t pHybridId, uint8_t pCbcId) const;
    SLinkEvent GetSLinkEvent(Ph2_HwDescription::BeBoard* pBoard) const override;
};
} // namespace Ph2_HwInterface
#endif
