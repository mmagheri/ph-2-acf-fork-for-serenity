#include "../Utils/D19cPSEventAS.h"
#include "../HWDescription/Definition.h"
#include "../Utils/ChannelGroupHandler.h"
#include "../Utils/DataContainer.h"
#include "../Utils/EmptyContainer.h"
#include "../Utils/Occupancy.h"
#include <algorithm>
#include <numeric>

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
D19cPSEventAS::D19cPSEventAS(const BeBoard* pBoard, const std::vector<uint32_t>& list)
{
    fEventDataVector.clear();
    fHybridIds.clear();
    fChipIds.clear();
    fCounterData.clear();
    // assuming that FEIds aren't shared between links
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            fHybridIds.push_back(cHybrid->getId());
            HybridCounterData cHybridCounterData;
            cHybridCounterData.clear();
            std::vector<uint8_t> cChipIds(0);
            cChipIds.clear();
            for(auto cChip: *cHybrid)
            {
                ChipCounterData cChipData;
                cChipData.clear();
                cHybridCounterData.push_back(cChipData);
                cChipIds.push_back(cChip->getId());
            } // chip
            fCounterData.push_back(cHybridCounterData);
            fChipIds.push_back(cChipIds);
        } // hybrids
    }     // opticalGroup
    this->Set(pBoard, list);
}
void D19cPSEventAS::Set(const BeBoard* pBoard, const std::vector<uint32_t>& pData)
{
    LOG(DEBUG) << BOLDYELLOW << "Setting event for Async PS counters " << RESET;
    auto cDataIterator  = pData.begin();
    auto cFrontEndTypes = pBoard->connectedFrontEndTypes();
    do {
        uint32_t cId = *cDataIterator;
        // uint32_t cId   = (pBoard->getId() << (4 + 1 + 1 + 4)) | (pGroup->getId() << (4 + 1 + 1)) | (pHybrid->getId() << (4 + 1)) | (pChip->getId() % 8 << (1)) | cType;
        uint32_t cBoardFromId        = (cId >> (4 + 1 + 1 + 4));
        uint32_t cOpticalGroupFromId = (cId >> (4 + 1 + 1)) & 0xF;
        uint32_t cHybridFromId       = (cId >> (4 + 1)) & 0x1;
        uint32_t cChipFromId         = (cId >> (1)) & 0xF;
        uint32_t cTypeFromId         = (cId & 0x1);
        size_t   cOffset             = (cTypeFromId == 1) ? (NMPACHANNELS) / 2 : NSSACHANNELS / 2;
        if(pBoard->getId() != cBoardFromId) break;
        LOG(DEBUG) << BOLDYELLOW << "D19cPSEventAS::Set Id from data vector is " << std::bitset<32>(cId) << " - board is " << cBoardFromId << " OpticalGroupId is " << cOpticalGroupFromId
                   << " HybridId is " << cHybridFromId << " TypeFromId is " << cTypeFromId << " ChipFromId is " << cChipFromId << " offset is " << cOffset << RESET;
        for(auto cOpticalGroup: *pBoard)
        {
            if(cOpticalGroupFromId != cOpticalGroup->getId()) continue;
            for(auto cHybrid: *cOpticalGroup)
            {
                if(cHybridFromId != cHybrid->getId()) continue;
                uint8_t cHybridIndex = getHybridIndex(cHybrid->getId());
                LOG(DEBUG) << BOLDYELLOW << "Data from Hybrid#" << +cHybrid->getId() << " index#" << +cHybridIndex << RESET;
                for(auto cChip: *cHybrid)
                {
                    uint8_t cChipIndex = getChipIndex(cHybridIndex, cChip->getId());
                    if(cChipFromId != cChip->getId() % 8) continue;
                    LOG(DEBUG) << BOLDYELLOW << "Data from chip#" << +cChipFromId << RESET;
                    if(cTypeFromId == 1 && (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)) continue;
                    if(cTypeFromId == 0 && (cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)) continue;

                    fCounterData[cHybridIndex][cChipIndex].clear();
                    size_t cDataOffset = 1;
                    for(uint16_t cChnl = 0; cChnl < cChip->size(); cChnl += 2)
                    {
                        // each 32-bit word hold information from two counters
                        if(cDataOffset < 10)
                        {
                            uint32_t cValue = *(cDataIterator + cDataOffset);
                            LOG(DEBUG) << BOLDYELLOW << std::bitset<32>(cValue) << RESET;
                        }
                        for(int cOffset = 0; cOffset < 2; cOffset++)
                        {
                            auto cCounterValue = (*(cDataIterator + cDataOffset) & (0x7FFF << 15 * cOffset)) >> 15 * cOffset;
                            if(cChnl < 10) LOG(DEBUG) << BOLDYELLOW << "Chnl#" << cChnl + cOffset << "\t" << (cCounterValue) << RESET;
                            fCounterData[cHybridIndex][cChipIndex].push_back(cCounterValue);
                        }
                        cDataOffset++;
                    } // channels
                    auto cMax  = std::max_element(fCounterData[cHybridIndex][cChipIndex].begin(), fCounterData[cHybridIndex][cChipIndex].end());
                    auto cMean = std::accumulate(fCounterData[cHybridIndex][cChipIndex].begin(), fCounterData[cHybridIndex][cChipIndex].end(), 0.0) / fCounterData[cHybridIndex][cChipIndex].size();
                    LOG(DEBUG) << BOLDYELLOW << "D19cPSEventAS::Set...type from data vector is " << cTypeFromId << "  ChipId from data vector is " << cChipFromId << BOLDGREEN
                               << "... Filling data vector with counter information from chip in position#" << +cChipIndex << " - has " << fCounterData[cHybridIndex][cChipIndex].size()
                               << " elements. Max is " << *cMax << " mean is " << cMean << RESET;
                } // chips
            }     // hybrids
        }         // optical groups
        cDataIterator += (1 + cOffset);
    } while(cDataIterator < pData.end());
}
// required by event but not sure if makes sense for AS
void D19cPSEventAS::fillChipDataContainer(ChipDataContainer* chipContainer, const std::shared_ptr<ChannelGroupBase> testChannelGroup, uint16_t hybridId)
{
    uint8_t cHybridIndex = getHybridIndex(hybridId);
    uint8_t cChipIndex   = getChipIndex(cHybridIndex, chipContainer->getId());
    LOG(DEBUG) << BOLDYELLOW << "HybridIndex " << +cHybridIndex << " ChipIndex " << +cChipIndex << " -- " << fCounterData.at(cHybridIndex).at(cChipIndex).size() << RESET;
    std::vector<uint32_t> cHits = GetHits(hybridId, chipContainer->getId());
    LOG(DEBUG) << BOLDYELLOW << "HybridIndex " << +cHybridIndex << " ChipIndex " << +cChipIndex << " -- " << cHits.size() << RESET;
    float            cOcc  = 0;
    size_t           cChnl = 0;
    std::vector<int> cCounts;
    for(auto cHit: cHits)
    {
        if(testChannelGroup == nullptr) break;
        if(testChannelGroup->isChannelEnabled(cChnl))
        {
            if(cChnl < 10) LOG(DEBUG) << BOLDYELLOW << "Chnl#" << cChnl << "\t" << cHit << RESET;
            uint32_t cRow = cChnl % testChannelGroup->getNumberOfRows();
            uint32_t cCol;
            if(testChannelGroup->getNumberOfCols() == 0)
                cCol = 0;
            else
                cCol = cChnl / testChannelGroup->getNumberOfRows();

            // if( cChnl < 10 ) LOG (INFO) << BOLDYELLOW << "Chnl#" << cChnl << "\t" << cHit  << "Row " << cRow << " Col" << cCol << RESET;
            cCounts.push_back(cHit);
            chipContainer->getChannel<Occupancy>(cRow, cCol).fOccupancy += cHit;
            cOcc += cHit;
        }
        cChnl++;
    }
    auto cMax  = std::max_element(cCounts.begin(), cCounts.end());
    auto cMean = std::accumulate(cCounts.begin(), cCounts.end(), 0.0) / cCounts.size();
    LOG(DEBUG) << BOLDYELLOW << "D19cPSEventAS::fillChipDataContainer..  Chip#" << +chipContainer->getId() << "\t.. Hits from #" << +cCounts.size() << " enabled channels."
               << " Max is " << *cMax << " mean is " << cMean << " <chip occupancy> is " << cOcc / cCounts.size() << RESET;
}

void D19cPSEventAS::SetEvent(const BeBoard* pBoard, uint32_t pNMPA, const std::vector<uint32_t>& list)
{
    std::cout << "MPAASEV" << std::endl;

    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            uint32_t nc = 0;
            for(auto cChip: *cHybrid)
            {
                fEventDataVector[encodeVectorIndex(cHybrid->getId(), cChip->getId(), pNMPA)] = std::vector<uint32_t>(list.begin() + nc * 1920, list.begin() + (nc + 1) * 1920);
                // std::cout<<fEventDataVector[encodeVectorIndex (cHybrid->getId(), cChip->getId(), pNMPA)
                // ][5]<<std::endl;

                nc += 1;
            }
        }
    }
}

uint32_t D19cPSEventAS::GetNHits(uint8_t pHybridId, uint8_t pChipId) const
{
    uint8_t cHybridIndex = getHybridIndex(pHybridId);
    uint8_t cChipIndex   = getChipIndex(cHybridIndex, pChipId);
    auto&   cHitVector   = fCounterData.at(cHybridIndex).at(cChipIndex);
    return std::accumulate(cHitVector.begin(), cHitVector.end(), 0);
    // const std::vector<uint32_t> &hitVector = fEventDataVector.at(encodeVectorIndex(pHybridId, pMPAId,fNMPA));
    // return std::accumulate(hitVector.begin()+1, hitVector.end(), 0);
}
std::vector<uint32_t> D19cPSEventAS::GetHits(uint8_t pHybridId, uint8_t pChipId) const
{
    uint8_t cHybridIndex = getHybridIndex(pHybridId);
    uint8_t cChipIndex   = getChipIndex(cHybridIndex, pChipId);
    return fCounterData.at(cHybridIndex).at(cChipIndex);
    // const std::vector<uint32_t> &hitVector = fEventDataVector.at(encodeVectorIndex(pHybridId, pMPAId,fNMPA));
    // LOG (INFO) << BOLDBLUE << hitVector[0] << RESET;
    // return hitVector;
}

} // namespace Ph2_HwInterface
