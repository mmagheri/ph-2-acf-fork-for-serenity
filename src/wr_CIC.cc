#include <cstring>
#include <fstream>
#include <ios>

#include "../Utils/argvparser.h"
#include "../tools/Tool.h"
#include "../HWInterface/DTCFWInterface.h"
#include "../HWInterface/FastCommandInterface.h"
#include "../HWInterface/DTCFastCommandInterface.h"
#include "../HWInterface/DTCLinkInterface.h"
#include "../HWInterface/DTCL1ReadoutInterface.h"
#include "../HWInterface/RegManager.h"
#include "OTModuleStartUp.h"

#include "emp/logger/logger.hpp"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;

using namespace std;

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[])
{
	LOG(INFO) << "WR_CIC: Starting..." << RESET;

	// configure the logger
	el::Configurations conf("settings/logger.conf");
	el::Loggers::reconfigureAllLoggers(conf);
	//emp::logger::configure("EMP");
	//emp::logger::setLevel("trace");
	//emp::logger::setLevel("info");

	ArgvParser cmd;

	// init
 	cmd.setIntroductoryDescription("DAQPATH CIC Test Application. Implementation of DAQPATH infinite loop with CIC-like data.");
	// error codes
	cmd.addErrorCode(0, "Success");
	cmd.addErrorCode(1, "Error");
	// options
	cmd.setHelpOption("h", "help", "Print this help page");
	cmd.defineOption("file", "Hw Description File . Default value: settings/Serenity_example.xml", ArgvParser::OptionRequiresValue);
	cmd.defineOptionAlternative("file", "f");
	cmd.defineOption("reconfigure", "Reset and reconfigure modules.", ArgvParser::NoOptionAttribute);
	cmd.defineOption("setThreshold", "Set threshold value", ArgvParser::OptionRequiresValue);
	cmd.defineOption("nEvents", "Select the number of events to read. Default: 10", ArgvParser::OptionRequiresValue);
	cmd.defineOptionAlternative("nEvents", "e");
	cmd.defineOption("raw", "Save the data into a .raw file using the Ph2ACF format. Argument: name of the output .raw file in the ./Results directory", ArgvParser::OptionRequiresValue);
	cmd.defineOption("chMask", "Channel mask in exadecimal. 1: channel skipped 0: channel kept. Default: 0xfffffcff", ArgvParser::OptionRequiresValue);
	cmd.defineOptionAlternative("chMask", "m");
	cmd.defineOption("daqHEnable", "Enable header in DAQPATH. Default: Yes", ArgvParser::OptionRequiresValue);
	cmd.defineOption("linkId", "Select the linkId. Default: 2", ArgvParser::OptionRequiresValue);


	int result = cmd.parse(argc, argv);
	if(result != ArgvParser::NoParserError) {
		LOG(INFO) << cmd.parseErrorDescription(result);
		exit(1);
	}

	// now query the parsing results
	std::string cHWFile      = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/Serenity_example.xml";
	uint32_t    cNEvents     = (cmd.foundOption("nEvents")) ? convertAnyInt(cmd.optionValue("nEvents").c_str()) : 10;
	long        cChMask      = (cmd.foundOption("chMask")) ? stol(cmd.optionValue("chMask") ,NULL,16) : stol("fffffcff", NULL, 16);
	int         daqHEnable     = (cmd.foundOption("daqHEnable")) ? stoi(cmd.optionValue("daqHEnable"), NULL, 10) : 1;

	bool        cReconfigure = (cmd.foundOption("reconfigure")) ? true : false;
	unsigned    cThreshold   = (cmd.foundOption("setThreshold")) ? convertAnyInt(cmd.optionValue("setThreshold").c_str()) : -1;
	unsigned    cChanId      = (cmd.foundOption("linkId")) ? convertAnyInt(cmd.optionValue("linkId").c_str()) : 2;

	Tool cTool;

	cTool.setResultsDirectory("Results");
	//Initialize HW and Settings
	cTool.InitializeHw(cHWFile, cout);
	LOG(INFO) << BOLDRED << "Hardware initialised" << RESET;
	uhal::setLogLevelTo(uhal::WarningLevel());
	cTool.InitializeSettings(cHWFile, cout);
	LOG(INFO) << BOLDRED << "Settings initialised" << RESET;
	//cTool.ConfigureHw();

	BeBoardInterface* cBeBoardInterface = cTool.fBeBoardInterface;

	OTModuleStartUp cModuleStartUp;
	LOG(INFO) << BOLDRED << "----- Starting Module ----" << RESET;
	LOG(INFO) << BOLDRED << "----- Setting cold start ----" << RESET;
    cModuleStartUp.setColdStart(1);
	LOG(INFO) << BOLDRED << "----- Inherit from tool ----" << RESET;
    cModuleStartUp.Inherit(&cTool);
	LOG(INFO) << BOLDRED << "----- Start ----" << RESET;
    cModuleStartUp.Start(0);
	LOG(INFO) << BOLDRED << "----- WaitForRunToBeCompleted ----" << RESET;
    cModuleStartUp.waitForRunToBeCompleted();
	LOG(INFO) << BOLDRED << "----- DumpConfigFiles ----" << RESET;

    cModuleStartUp.dumpConfigFiles();


    for (auto cBoard : *cTool.fDetectorContainer) {
		cBeBoardInterface->setBoard(cBoard->getId());
		DTCFWInterface *cFWInterface = static_cast<DTCFWInterface*>(cBeBoardInterface->getFirmwareInterface());
		
		cFWInterface->ConfigureBoard(cBoard);
		DTCL1ReadoutInterface* cL1ReadoutInterface = static_cast<DTCL1ReadoutInterface*>(cFWInterface->getL1ReadoutInterface());
		//Try to factorize this in L1ReadoutInterface - now it gives a segfault
		//cL1ReadoutInterface->SetChannelMask(cChMask);
		//cL1ReadoutInterface->EnableDaqPath();
		//cL1ReadoutInterface->EnableReadingFromOutputFIFO();
		//cL1ReadoutInterface->EnableDaqpathEventHeader(daqHEnable);
		//cL1ReadoutInterface->ResetReadout();
		cL1ReadoutInterface->ReadEvents(cBoard);
    }
	cTool.dumpConfigFiles();
	return 0;
}
