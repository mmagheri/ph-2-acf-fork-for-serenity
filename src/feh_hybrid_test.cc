#ifndef __EMP__
#include "2STestCardControl.h"
#include "BackendAlignmentOT.h"
#include "CheckCbcNeighbors.h"
#include "CicFEAlignment.h"
#include "CicInputTest.h"
#include "OTModuleStartUp.h"
#include "OpenFinder.h"
#include "PSTestCardControl.h"
#include "PedeNoise.h"
#include "PedestalEqualization.h"
#include "ShortFinder.h"
#include "Utils/Timer.h"
#include "Utils/Utilities.h"
#include "Utils/argvparser.h"
#include <cstring>

#if defined(__USE_ROOT__)
#include "TApplication.h"
#include "TROOT.h"
#endif

#if defined(__TCUSB__)
#include "USB_a.h"
#endif

#define __NAMEDPIPE__

#ifdef __NAMEDPIPE__
#include "gui_logger.h"
#endif

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;
INITIALIZE_EASYLOGGINGPP

#define CHIPSLAVE 4
sig_atomic_t killProcess  = 0;
sig_atomic_t runCompleted = 0;

void interruptHandler(int handler) { killProcess = 1; }

void killProcessFunction(Tool* theTool)
{
    while(1)
    {
        usleep(250000);
        if(killProcess || runCompleted) break;
    }
    if(killProcess)
    {
        theTool->Destroy();
        abort();
    }
}

int main(int argc, char* argv[])
{
    // configure the logger
    el::Configurations conf(std::string(std::getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("CMS Ph2_ACF  Commissioning tool to perform the following procedures:\n-Timing / "
                                   "Latency scan\n-Threshold Scan\n-Stub Latency Scan");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");
    // options
    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("file", "Hw Description File . Default value: settings/Commission_2CBC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");

    cmd.defineOption("skipAlignment", "Skip alignment : cic inputs [CIC] or back0end [BE] or both [all]", ArgvParser::OptionRequiresValue);

    cmd.defineOption("cicInTest", "Check CIC inputs");
    cmd.defineOption("powerUp", "Power-up hybrid");
    cmd.defineOption("reconfigure", "Configure hybrid");

    cmd.defineOption("tuneOffsets", "tune offsets on readout chips connected to CIC.");
    cmd.defineOptionAlternative("tuneOffsets", "t");

    cmd.defineOption("measurePedeNoise", "measure pedestal and noise on readout chips connected to CIC.");
    cmd.defineOptionAlternative("measurePedeNoise", "m");

    cmd.defineOption("checkSharedStubs", "Check stubs at boundary between chips", ArgvParser::NoOptionAttribute);

    cmd.defineOption("findShorts", "look for shorts", ArgvParser::NoOptionAttribute);
    cmd.defineOption("findOpens", "perform latency scan with antenna on UIB", ArgvParser::NoOptionAttribute);
    cmd.defineOption("hybridId", "Serial Number of front-end hybrid. Default value: xxxx", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);

    // general
    cmd.defineOption("batch", "Run the application in batch mode", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("batch", "b");

    cmd.defineOption("antennaValue", "Antenna value for test.", ArgvParser::OptionRequiresValue);
    int result = cmd.parse(argc, argv);
    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    // now query the parsing results
    std::string cHWFile    = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/Commissioning.xml";
    bool        batchMode  = (cmd.foundOption("batch")) ? true : false;
    std::string cDirectory = (cmd.foundOption("output")) ? cmd.optionValue("output") : "Results/";
    std::string cHybridId  = (cmd.foundOption("hybridId")) ? cmd.optionValue("hybridId") : "xxxx";
    std::string cSkip      = (cmd.foundOption("skipAlignment")) ? cmd.optionValue("skipAlignment") : "";
    // start time
    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    std::stringstream outp;
    outp.str(std::string());
    outp << "Results/FEH_PS_" << cHybridId << "_" << startTimeUTC_us;
    cDirectory = outp.str();
    outp.str(std::string());
    std::string cResultfile = "Hybrid";
    Tool        cTool;
    Timer       cGlobalTimer;
    cGlobalTimer.start();

    std::thread softKillThread(killProcessFunction, &cTool);
    softKillThread.detach();

    struct sigaction act;
    act.sa_handler = interruptHandler;
    sigaction(SIGINT, &act, NULL);

#if defined(__USE_ROOT__)
    TApplication cApp("Root Application", &argc, argv);
    if(batchMode)
        gROOT->SetBatch(true);
    else
        TQObject::Connect("TCanvas", "Closed()", "TApplication", &cApp, "Terminate()");
#endif

    cTool.InitializeHw(cHWFile, outp);
    cTool.InitializeSettings(cHWFile, outp);
    LOG(INFO) << outp.str();

#if defined(__TCUSB__)
    auto              cTCUSBbus    = cTool.findValueInSettings<double>("HybridTCUSBBus", 0);
    auto              cTCUSBdevice = cTool.findValueInSettings<double>("HybridTCUSBDevice", 0);
    TestCardControl2S cController;
    cController.Initialise(cTCUSBbus, cTCUSBdevice);
    if(cmd.foundOption("powerUp"))
    {
        cController.SetDefaultHybridVoltage();
        cController.ReadHybridVoltage("Hybrid1V25");
    }
    // cController.SelectCIC(true);
#endif

    cTool.CreateResultDirectory(cDirectory, false, false);
#if defined(__USE_ROOT__)
    cTool.InitResultFile(cResultfile);
#endif
    OTModuleStartUp cModuleStartUp;
    if(cmd.foundOption("reconfigure"))
    {
        cModuleStartUp.setColdStart(cmd.foundOption("powerUp") ? 1 : 0);
        cModuleStartUp.Inherit(&cTool);
        cModuleStartUp.Start(startTimeUTC_us);
        cModuleStartUp.waitForRunToBeCompleted();
    }
#if defined(__USE_ROOT__)
    cTool.AddMetadata();
#endif
    bool cSkipBeAlignment  = (cmd.foundOption("skipAlignment")) && (cSkip.find("BE") != std::string::npos);
    bool cSkipCicAlignment = (cmd.foundOption("skipAlignment")) && (cSkip.find("CIC") != std::string::npos);
    bool cSkipBoth         = (cmd.foundOption("skipAlignment")) && (cSkip.find("all") != std::string::npos);
    if(!cSkipBoth && !cSkipBeAlignment)
    {
        BackendAlignmentOT cAligner;
        cAligner.Inherit(&cTool);
        cAligner.Start(startTimeUTC_us);
        cAligner.waitForRunToBeCompleted();
        // cAligner.Reset();
    }
    if(!cSkipBoth && !cSkipCicAlignment)
    {
        // align FEs - CIC
        CicFEAlignment cCicAligner;
        cCicAligner.Inherit(&cTool);
        cCicAligner.Start(startTimeUTC_us);
        cCicAligner.waitForRunToBeCompleted();
        // cCicAligner.Reset();
    }
    if(cmd.foundOption("checkSharedStubs"))
    {
        LOG(INFO) << BOLDMAGENTA << "Checking stubs across CBC neighbors" << RESET;

        CheckCbcNeighbors cCheckCbcNeighbors;
        cCheckCbcNeighbors.Inherit(&cTool);
        cCheckCbcNeighbors.Start(startTimeUTC_us);
        cCheckCbcNeighbors.waitForRunToBeCompleted();
    }
    // check CIC sparsification
    /*for(auto cBoard: *cTool.fDetectorContainer)
    {
        cBoard->setSparsification(false);
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                cTool.fCicInterface->SetSparsification(cCic,false);
                auto cFECnfg = cTool.fCicInterface->ReadChipReg(cCic,"FE_CONFIG");
                auto cFEEnable = cTool.fCicInterface->ReadChipReg(cCic,"FE_ENABLE");
                LOG (INFO) << BOLDYELLOW << " FECnfg " << std::bitset<8>(cFECnfg)
                    << " FE_ENABLE " << std::bitset<8>(cFEEnable)
                    << RESET;
            }
        }
    }*/
    /*for(auto cBoard: *cTool.fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    cTool.fReadoutChipInterface->WriteChipReg(cChip,"Threshold",cChip->getId());
                    // auto cTh = cTool.fReadoutChipInterface->ReadChipReg(cChip,"Threshold");
                    // LOG (INFO) << "Chip#" << +cChip->getId() << " : " << cTh << RESET;
                }
            }
        }
    }
    for( int cId=0; cId < 8; cId++)
    {
        LOG (INFO) << BOLDYELLOW << "ROC#" << +cId << RESET;
        for(auto cBoard: *cTool.fDetectorContainer)
        {
            uint32_t cSparsified = cBoard->getSparsification(); // this is set in the file parser .. so check using that
            LOG(INFO) << BOLDYELLOW << "Sparisfication set to " << +cSparsified << RESET;
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        cTool.fReadoutChipInterface->WriteChipReg(cChip,"Threshold",1023);
                    }
                    for(auto cChip: *cHybrid)
                    {
                        if( cChip->getId() == cId ){
                            LOG (INFO) << BOLDYELLOW << "Setting threshold on ROC#" << +cChip->getId() << " to 0 "<< RESET;
                            cTool.fReadoutChipInterface->WriteChipReg(cChip,"Threshold",0);
                        }
                    }
                }
            }
            cTool.ReadNEvents(cBoard, 10);
        }
        const std::vector<Event*>& cEvents = cTool.GetEvents ();
        for(auto& cEvent: cEvents)
        {
            if( cEvent->GetEventCount() > 0 ) continue;
            for(auto cBoard: *cTool.fDetectorContainer)
            {
                for(auto cOpticalGroup: *cBoard)
                {
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        for(auto cChip: *cHybrid)
                        {
                            auto     cHits   = cEvent->GetHits(cHybrid->getId(), cChip->getId());
                            LOG (INFO) << BOLDYELLOW << "Chip#" << +cChip->getId()
                                << " " << cHits.size() << " hits."
                                << RESET;
                        }
                    }
                }
            }
        }
    }
    */
    // equalize thresholds on readout chips
    if(cmd.foundOption("tuneOffsets"))
    {
        bool                 cAllChan = true;
        PedestalEqualization cPedestalEqualization;
        cPedestalEqualization.Inherit(&cTool);
        cPedestalEqualization.Initialise(cAllChan, true);
        cPedestalEqualization.FindVplus();
        cPedestalEqualization.FindOffsets();
        cPedestalEqualization.Reset();
        // second parameter disables stub logic on CBC3
        cPedestalEqualization.writeObjects();
        cPedestalEqualization.dumpConfigFiles();
        cPedestalEqualization.resetPointers();
    }
    // measure noise on FE chips
    if(cmd.foundOption("measurePedeNoise"))
    {
        bool cAllChan = true;
        LOG(INFO) << BOLDMAGENTA << "Measuring pedestal and noise" << RESET;
        PedeNoise cPedeNoise;
        cPedeNoise.Inherit(&cTool);
        cPedeNoise.Initialise(cAllChan, true); // canvases etc. for fast calibration
        cPedeNoise.measureNoise();
        cPedeNoise.Validate(5.);
        cPedeNoise.writeObjects();
        cPedeNoise.dumpConfigFiles();
        cPedeNoise.Reset();
    }

    if(cmd.foundOption("cicInTest"))
    {
        CicInputTester cCicInputTester(0, 0, 0xAA);
        cCicInputTester.Inherit(&cTool);
        cCicInputTester.Initialise();
        cCicInputTester.Start(startTimeUTC_us);
        cCicInputTester.waitForRunToBeCompleted();
        cCicInputTester.Stop();
        // cCicInputTester.writeObjects();
        // cCicInputTester.dumpConfigFiles();
    }
    if(cmd.foundOption("findOpens"))
    {
#if defined(__TCUSB__)
        OpenFinder cOpenFinder;
        cOpenFinder.Inherit(&cTool);
        cOpenFinder.ConfigureInjection();
        cOpenFinder.Initialise();
        cOpenFinder.FindOpens2S();
        cOpenFinder.Print();
#endif
    }
    if(cmd.foundOption("findShorts"))
    {
        ShortFinder cShortFinder;
        cShortFinder.Inherit(&cTool);
        cShortFinder.Initialise();
        cShortFinder.Start(startTimeUTC_us);
        cShortFinder.waitForRunToBeCompleted();
    }

    cTool.SaveResults();
#if defined(__USE_ROOT__)
    cTool.WriteRootFile();
    cTool.CloseResultFile();
#endif
    cTool.Destroy();
    signal(SIGINT, SIG_DFL);
    runCompleted = 1;
    if(!batchMode) cApp.Run();
    cGlobalTimer.stop();
    cGlobalTimer.show("Total execution time: ");
    return 0;
}
#endif
