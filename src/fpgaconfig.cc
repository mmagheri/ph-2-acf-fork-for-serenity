#include <cstdlib>
#include <inttypes.h>
#include <string>
#include <vector>

#include "../Utils/ConsoleColor.h"
#include "../Utils/Utilities.h"
#include "../Utils/argvparser.h"
#include "../Utils/easylogging++.h"
#include "../miniDAQ/MiddlewareStateMachine.h"

using namespace CommandLineProcessing;

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[])
{
    auto* baseDirChar_p = std::getenv("PH2ACF_BASE_DIR");
    if(baseDirChar_p == nullptr)
    {
        LOG(ERROR) << "Error, the environment variable PH2ACF_BASE_DIR is not initialized (hint: source setup.sh)";
        exit(EXIT_FAILURE);
    }

    el::Configurations conf(std::string(baseDirChar_p) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    cmd.setIntroductoryDescription("CMS Ph2_ACF  Data acquisition test and Data dump");

    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");

    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("list", "Print the list of available firmware images on SD card (works only with CTA boards)");
    cmd.defineOptionAlternative("list", "l");

    cmd.defineOption("delete", "Delete a firmware image on SD card (works only with CTA boards)", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("delete", "d");

    cmd.defineOption("file", "Local FPGA Bitstream file (*.mcs format for GLIB or *.bit/*.bin format for CTA boards)", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");

    cmd.defineOption("download", "Download an FPGA configuration from SD card to file (only for CTA boards)", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("download", "o");

    cmd.defineOption("config", "Hw Description File . Default value: settings/HWDescription_2CBC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("config", "c");

    cmd.defineOption("image", "Without -f: load image from SD card to FPGA\nWith    -f: name of image written to SD card\n-f specifies the source filename", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("image", "i");

    cmd.defineOption("board", "In case of multiple boards in the same file, specify board Id", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("board", "b");

    cmd.defineOption("downloadLatest", "Download latest image from website", ArgvParser::OptionRequiresValue);

    int result = cmd.parse(argc, argv);

    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(EXIT_FAILURE);
    }

    std::string configurationFile = (cmd.foundOption("config")) ? cmd.optionValue("config") : "settings/HWDescription_2CBC.xml";
    uint16_t    boardId           = (cmd.foundOption("board")) ? convertAnyInt(cmd.optionValue("board").c_str()) : 0;

    MiddlewareStateMachine theMiddlewareStateMachine;

    std::string cFWFile;
    if(cmd.foundOption("file")) cFWFile = cmd.optionValue("file");
    // std::string              strImage("1");

    if(cmd.foundOption("delete"))
    {
        std::string firmwareName = cmd.optionValue("delete");
        LOG(INFO) << BOLDBLUE << "Deleting " << firmwareName << " from SD card..." << RESET;
        theMiddlewareStateMachine.deleteFirmwareFromSDcard(configurationFile, firmwareName, boardId);
        LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
        exit(EXIT_SUCCESS);
    }

    if(cmd.foundOption("downloadLatest"))
    {
        std::string cImage         = (cmd.foundOption("downloadLatest")) ? cmd.optionValue("downloadLatest") : "";
        std::string cWebsiteURL    = "https://udtc-ot-firmware.web.cern.ch/latest/";
        std::string cFMCL12        = "";
        std::string cFMCL8         = "";
        std::string cModuleType    = "";
        std::string cImageName     = "";
        std::string cInputFileName = "";
        std::string cCmd           = "curl -O " + cWebsiteURL;
        if(cImage == "2S_8Modules_CIC2")
        {
            LOG(INFO) << BOLDYELLOW << "Will download image from website for 8 2S-module readout CIC2" << RESET;
            cModuleType = "2s_8m_cic2";
            cFMCL12     = "l12octa";
            cFMCL8      = "l8dio5";
            cImageName  = "2S_8Modules_default.bin";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + "/";             // + "_withbend_nostubdebug_notlu";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin";          // "_withbend_nostubdebug_notlu.bin";
            cInputFileName = cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin"; // + "_withbend_nostubdebug_notlu.bin";
        }
        else if(cImage == "2S_12Modules_CIC2")
        {
            LOG(INFO) << BOLDYELLOW << "Will download image from website for 12 2S-module readout CIC2" << RESET;
            cModuleType = "2s_12m_cic2";
            cFMCL12     = "l12octa";
            cFMCL8      = "l8quad";
            cImageName  = "2S_12Modules_default.bin";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + "/";             // + "_withbend_nostubdebug_notlu";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin";          // "_withbend_nostubdebug_notlu.bin";
            cInputFileName = cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin"; // + "_withbend_nostubdebug_notlu.bin";
        }
        else if(cImage == "2S_8Modules_CIC2")
        {
            LOG(INFO) << BOLDYELLOW << "Will download image from website for 8 2S-module readout CIC1" << RESET;
            cModuleType = "2s_8m_cic2";
            cFMCL12     = "l12octa";
            cFMCL8      = "l8dio5";
            cImageName  = "2S_8Modules_CIC2_default.bin";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + "/";             // + "_withbend_nostubdebug_notlu";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin";          // "_withbend_nostubdebug_notlu.bin";
            cInputFileName = cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin"; // + "_withbend_nostubdebug_notlu.bin";
        }
        else if(cImage == "2S_8Modules_CIC1")
        {
            LOG(INFO) << BOLDYELLOW << "Will download image from website for 8 2S-module readout CIC1" << RESET;
            cModuleType = "2s_8m_cic1";
            cFMCL12     = "l12octa";
            cFMCL8      = "l8dio5";
            cImageName  = "2S_8Modules_CIC1_default.bin";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + "/";             // + "_withbend_nostubdebug_notlu";
            cCmd += cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin";          // "_withbend_nostubdebug_notlu.bin";
            cInputFileName = cModuleType + "_" + cFMCL12 + "_" + cFMCL8 + ".bin"; // + "_withbend_nostubdebug_notlu.bin";
        }
        else if(cImage == "2S_FEH")
        {
            LOG(INFO) << BOLDYELLOW << "Will download image from website for 2S-FEH production testing" << RESET;
            cModuleType = "2s_feh_cic2";
            cFMCL12     = "";
            cFMCL8      = "";
            cImageName  = "2S_feh_cic2_default.bin";
            cCmd += cModuleType + "/";
            cCmd += cModuleType + ".bin";
            cInputFileName = cModuleType + ".bin";
        }
        else if(cImage == "PS_FEH_CICmode")
        {
            LOG(INFO) << BOLDYELLOW << "Will download image from website for PS-FEH production testing [Pogo-CIC mode]" << RESET;
            cModuleType = "ps_feh_5g_cic2";
            cFMCL12     = "";
            cFMCL8      = "";
            cImageName  = "ps_feh_5g_cic2_default.bin";
            cCmd += cModuleType + "/";
            cCmd += cModuleType + ".bin";
            cInputFileName = cModuleType + ".bin";
        }
        LOG(INFO) << cCmd.c_str() << RESET;
        system((const char*)cCmd.c_str());

        std::vector<std::string> firmwareList = theMiddlewareStateMachine.getFirmwareList(configurationFile, boardId);
        LOG(INFO) << firmwareList.size() << " firmware images on SD card:";
        bool cDelete = false;
        for(auto& name: firmwareList)
        {
            if(name == cImageName) cDelete = true;
        }
        if(cDelete)
        {
            LOG(INFO) << BOLDBLUE << "Deleting " << cImageName << " from SD card..." << RESET;
            theMiddlewareStateMachine.deleteFirmwareFromSDcard(configurationFile, cImageName, boardId);
            LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
        }

        // upload new image
        LOG(INFO) << BOLDBLUE << "Uploading " << cInputFileName << " on SD card with name " << cImageName << "..." << RESET;
        theMiddlewareStateMachine.uploadFirmwareOnSDcard(configurationFile, cImageName, cInputFileName, boardId);
        LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;

        // check if image is there
        firmwareList.clear();
        firmwareList = theMiddlewareStateMachine.getFirmwareList(configurationFile, boardId);
        LOG(INFO) << firmwareList.size() << " firmware images on SD card:";
        bool cLoad = false;
        for(auto& name: firmwareList)
        {
            if(name == cImageName) cLoad = true;
        }

        if(cLoad)
        {
            // load new image
            LOG(INFO) << BOLDBLUE << "Loading " << cImageName << " into the FPGA..." << RESET;
            theMiddlewareStateMachine.loadFirmwareInFPGA(configurationFile, cImageName, boardId);
            LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
        }
    }

    if(cmd.foundOption("image"))
    {
        std::string firmwareName = cmd.optionValue("image");
        if(cmd.foundOption("file") && !cmd.foundOption("download"))
        {
            std::string inputFileName = cmd.optionValue("file");
            LOG(INFO) << BOLDBLUE << "Uploading " << inputFileName << " on SD card with name " << firmwareName << "..." << RESET;
            theMiddlewareStateMachine.uploadFirmwareOnSDcard(configurationFile, firmwareName, inputFileName, boardId);
            LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
            exit(EXIT_SUCCESS);
        }
        else if(cmd.foundOption("download") && !cmd.foundOption("file"))
        {
            std::string outputFileName = cmd.optionValue("download");
            LOG(INFO) << BOLDBLUE << "Downloading " << firmwareName << " on SD card into " << outputFileName << "..." << RESET;
            theMiddlewareStateMachine.downloadFirmwareFromSDcard(configurationFile, firmwareName, outputFileName, boardId);
            LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
            exit(EXIT_SUCCESS);
        }
        else if(!cmd.foundOption("file") && !cmd.foundOption("download"))
        {
            LOG(INFO) << BOLDBLUE << "Loading " << firmwareName << " into the FPGA..." << RESET;
            theMiddlewareStateMachine.loadFirmwareInFPGA(configurationFile, firmwareName, boardId);
            LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
        }
        else
        {
            LOG(ERROR) << BOLDRED << "Error: -f and -o options cannot be specified at the same time" << RESET;
            exit(EXIT_FAILURE);
        }
    }
    // else
    // {
    //     LOG(ERROR) << "Error, no FW image specified";
    //     exit(EXIT_FAILURE);
    // }

    if(cmd.foundOption("list"))
    {
        std::vector<std::string> firmwareList = theMiddlewareStateMachine.getFirmwareList(configurationFile, boardId);
        LOG(INFO) << firmwareList.size() << " firmware images on SD card:";
        for(auto& name: firmwareList) LOG(INFO) << " - " << name;
        exit(EXIT_SUCCESS);
    }
}
