#include <cstring>
#include <fstream>
#include <ios>

#include "../Utils/argvparser.h"
#include "../tools/Tool.h"
#include "../HWInterface/DTCFWInterface.h"
#include "../HWInterface/FastCommandInterface.h"
#include "../HWInterface/DTCLinkInterface.h"

#include "emp/logger/logger.hpp"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;

using namespace std;

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[])
{
	LOG(INFO) << "WR_CIC_NO_RESET: Starting..." << RESET;
	// configure the logger
	el::Configurations conf("settings/logger.conf");
	el::Loggers::reconfigureAllLoggers(conf);
	//emp::logger::configure("EMP");
	//emp::logger::setLevel("trace");
	//emp::logger::setLevel("info");

	ArgvParser cmd;

	// init
 	cmd.setIntroductoryDescription("DAQPATH CIC Test Application. Implementation of DAQPATH infinite loop with CIC-like data.");
	// error codes
	cmd.addErrorCode(0, "Success");
	cmd.addErrorCode(1, "Error");
	// options
	cmd.setHelpOption("h", "help", "Print this help page");
	cmd.defineOption("file", "Hw Description File . Default value: settings/Serenity5HWDescription2SLpGBT.xml", ArgvParser::OptionRequiresValue);
	cmd.defineOptionAlternative("file", "f");
	cmd.defineOption("reconfigure", "Reset and reconfigure modules.", ArgvParser::NoOptionAttribute);
	cmd.defineOption("setThreshold", "Set threshold value", ArgvParser::OptionRequiresValue);
	cmd.defineOption("nEvents", "Select the number of events to read. Default: 10", ArgvParser::OptionRequiresValue);
	cmd.defineOptionAlternative("nEvents", "e");
	cmd.defineOption("raw", "Save the data into a .raw file using the Ph2ACF format. Argument: name of the output .raw file in the ./Results directory", ArgvParser::OptionRequiresValue);
	cmd.defineOption("chMask", "Channel mask in exadecimal. 1: channel skipped 0: channel kept. Default: 0xff00", ArgvParser::OptionRequiresValue);
	cmd.defineOptionAlternative("chMask", "m");
	cmd.defineOption("chEnable", "Enable header in DAQPATH. Default: Yes", ArgvParser::OptionRequiresValue);
	cmd.defineOptionAlternative("chEnable", "H");
	cmd.defineOption("linkId", "Select the linkId. Default: 2", ArgvParser::OptionRequiresValue);


	int result = cmd.parse(argc, argv);
	if(result != ArgvParser::NoParserError) {
		LOG(INFO) << cmd.parseErrorDescription(result);
		exit(1);
	}

	// now query the parsing results
	std::string cHWFile      = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/Serenity5HWDescription2SLpGBT.xml";
	uint32_t    cNEvents     = (cmd.foundOption("nEvents")) ? convertAnyInt(cmd.optionValue("nEvents").c_str()) : 10;
	long        cChMask      = (cmd.foundOption("chMask")) ? stol(cmd.optionValue("chMask") ,NULL,16) : stol("ff00", NULL, 16);
	int         cHEnable     = (cmd.foundOption("chEnable")) ? stoi(cmd.optionValue("chEnable"), NULL, 10) : 1;

	bool        cReconfigure = (cmd.foundOption("reconfigure")) ? true : false;
	unsigned    cThreshold   = (cmd.foundOption("setThreshold")) ? convertAnyInt(cmd.optionValue("setThreshold").c_str()) : -1;
	unsigned    cChanId      = (cmd.foundOption("linkId")) ? convertAnyInt(cmd.optionValue("linkId").c_str()) : 2;

	Tool cTool;


	//Initialize HW and Settings
	cTool.InitializeHw(cHWFile, cout);
	LOG(INFO) << BOLDRED << "Hardware initialised" << RESET;
	uhal::setLogLevelTo(uhal::WarningLevel());
	cTool.InitializeSettings(cHWFile, cout);
	LOG(INFO) << BOLDRED << "Settings initialised" << RESET;
	cTool.ConfigureHw();

	BeBoardInterface* cBeBoardInterface = cTool.fBeBoardInterface;

    for (auto cBoard : *cTool.fDetectorContainer) {

      cBeBoardInterface->setBoard(cBoard->getId());
      DTCFWInterface *cFWInterface = static_cast<DTCFWInterface*>(cBeBoardInterface->getFirmwareInterface());
	  auto fComInterfaceSerenity = (cFWInterface->getFastCommandInterface());
	  fComInterfaceSerenity->SendGlobalReSync();
      cFWInterface->SetChannelMask(cChMask);
      cFWInterface->EnableDaqpathEventHeader(cHEnable);
      cFWInterface->ResetDaqPath();
	  std::vector<uint32_t> cData;
	  LOG(INFO) << BOLDRED << "Reading 10 event" << RESET;
	  cFWInterface->ReadNEvents(cBoard, 10, cData);
	  LOG(INFO) << BOLDRED << "Reading 10 event done" << RESET;
    }


	if(cmd.foundOption("raw")) cTool.closeFileHandler();


return 0;
}
