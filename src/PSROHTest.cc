
#include "../Utils/Timer.h"
#include "../Utils/Utilities.h"
#include "../Utils/argvparser.h"
#include "ProductionToolsOT/BackendAlignmentOT.h"
#include "Tool.h"

#include "PSROHTester.h"

#ifdef __USE_ROOT__
#include "TApplication.h"
#include "TROOT.h"
#endif

#define __NAMEDPIPE__

#ifdef __NAMEDPIPE__
#include "gui_logger.h"
#endif

#include <cstring>
#include <fstream>
#include <inttypes.h>

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;

using namespace std;
INITIALIZE_EASYLOGGINGPP

#define CHIPSLAVE 4
sig_atomic_t killProcess  = 0;
sig_atomic_t runCompleted = 0;

void interruptHandler(int handler) { killProcess = 1; }

void killProcessFunction(Tool* theTool)
{
    while(1)
    {
        usleep(250000);
        if(killProcess || runCompleted) break;
    }
    if(killProcess)
    {
        theTool->SaveResults();
        theTool->WriteRootFile();
        theTool->CloseResultFile();
        theTool->Destroy();
        abort();
    }
}

int main(int argc, char* argv[])
{
#if defined(__TCUSB__) && defined(__ROH_USB__) && defined(__USE_ROOT__)
    // configure the logger
    el::Configurations conf(std::string(std::getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("CMS Ph2_ACF  Data acquisition test and Data dump");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");
    // options
    cmd.setHelpOption("h", "help", "Print this help page");
    cmd.defineOption("file", "Hw Description file. Default value: settings/D19CDescription_ROH_EFC7.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");
    // Load pattern
    cmd.defineOption("test-internal-pattern", "Internally Generated LpGBT Pattern", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequires*/);
    cmd.defineOptionAlternative("test-internal-pattern", "ip");
    cmd.defineOption("test-external-pattern", "Externlly Generated LpGBT Pattern using the Data Player for Control FC7", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequires*/);
    cmd.defineOptionAlternative("test-external-pattern", "ep");

    cmd.defineOption("cic-pattern", "Externally Generated LpGBT Pattern using CIC output", ArgvParser::NoOptionAttribute /*| ArgvParser::OptionRequires*/);
    cmd.defineOptionAlternative("cic-pattern", "cp");

    // Test Reset lines
    cmd.defineOption("test-reset", "Test Reset lines");
    cmd.defineOptionAlternative("test-reset", "r");
    // test I2C Masters
    cmd.defineOption("test-i2c", "Test I2C LpGBT Masters on ROH", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequires*/);
    cmd.defineOptionAlternative("test-i2c", "i2c");
    // test ADC channels
    cmd.defineOption("test-adc", "Test LpGBT ADCs on ROH");
    cmd.defineOptionAlternative("test-adc", "a");
    // run Eye Opening Monitor
    cmd.defineOption("test-eom", "Run Eye Opening Monitor test");
    cmd.defineOptionAlternative("test-eom", "eom");
    //
    cmd.defineOption("eq-attenuation", "EQ attenuation for eye opening measurement", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("eq-attenuation", "eqa");
    // run Bit Error Test
    cmd.defineOption("test-ber", "Run Bit Error Rate test");
    cmd.defineOptionAlternative("test-ber", "ber");
    cmd.defineOption("ber-pattern", "Define pattern to be used for Bit Error Rate test", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequires*/);
    cmd.defineOptionAlternative("ber-pattern", "bp");
    // clock test
    cmd.defineOption("test-clock", "Run clock tests", ArgvParser::NoOptionAttribute);
    // fast command test
    cmd.defineOption("test-fcmd", "Scope fast commands [de-serialized]");
    cmd.defineOption("fcmd-pattern", "Injected pattern (simulates FCMD) on the DownLink", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequires*/);
    cmd.defineOptionAlternative("fcmd-pattern", "fp");
    //
    cmd.defineOption("fmcd-test", "Run fast command tests", ArgvParser::NoOptionAttribute);
    cmd.defineOption("fcmd-test-start-pattern", "Fast command FSM test start pattern", ArgvParser::OptionRequiresValue);
    cmd.defineOption("fcmd-test-userfile", "User file with fastcommands for testing", ArgvParser::OptionRequiresValue);
    // FCMD check in BRAM
    cmd.defineOption("bramfcmd-check", "Access to written data in BRAM", ArgvParser::OptionRequiresValue);
    // Write reference patterns to BRAM
    cmd.defineOption("bramreffcmd-write", "Write reference patterns to BRAM", ArgvParser::OptionRequiresValue);
    // convert user file to fw format
    cmd.defineOption("convert-userfile", "Convert user defined file to fw compliant format", ArgvParser::OptionRequiresValue);
    // read single ref FCMD BRAM addr
    cmd.defineOption("read-ref-bram", "Read single ref FCMD BRAM address", ArgvParser::OptionRequiresValue);
    // read single check FCMD BRAM addr
    cmd.defineOption("read-check-bram", "Read single check FCMD BRAM address", ArgvParser::OptionRequiresValue);
    // Flush check BRAM
    cmd.defineOption("clear-check-bram", "", ArgvParser::NoOptionAttribute);
    // Flush ref BRAM
    cmd.defineOption("clear-ref-bram", "", ArgvParser::NoOptionAttribute);
    // debug
    cmd.defineOption("debug", "Run debug", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("debug", "d");
    // Test VTRx+ registers
    cmd.defineOption("test-vtrx", "Test testVTRx+ slow control");
    cmd.defineOptionAlternative("testVTRx+", "v");
    // Check ROM
    cmd.defineOption("test-rom", "Test ROM loading [only lpGBT-v1]");
    // Measure Current-Voltages on test card
    cmd.defineOption("measure-input-iv", "Measure input currents and voltages on test card");
    cmd.defineOptionAlternative("measure-input-iv", "iv");
    // general
    cmd.defineOption("batch", "Run the application in batch mode", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("batch", "b");

    //
    cmd.defineOption("hybridId", "Name or serial number of ROH", ArgvParser::OptionRequiresValue);

    //
    cmd.defineOption("output", "Output directory. Default: Results/", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("output", "o");

    //
    cmd.defineOption("USBBus", "USB device bus number", ArgvParser::OptionRequiresValue);
    cmd.defineOption("USBDev", "USB device device number", ArgvParser::OptionRequiresValue);
    cmd.defineOption("useGui",
                     "Support for running the test from the gui for hybrids testing. The named pipe for communication needs to be passed as the last parameter. Default: false",
                     ArgvParser::NoOptionAttribute);

    int result = cmd.parse(argc, argv);
    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    std::string cHWFile    = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/D19CDescription_ROH_OFC7.xml";
    bool        batchMode  = (cmd.foundOption("batch")) ? true : false;
    std::string cDirectory = (cmd.foundOption("output")) ? cmd.optionValue("output") : "Results/";
    std::string cHybridId  = (cmd.foundOption("hybridId")) ? cmd.optionValue("hybridId") : "xxxx";
    bool        cDebug     = (cmd.foundOption("debug"));
    // Test to perform
    bool        cFCMDTest             = (cmd.foundOption("fmcd-test")) ? true : false;
    std::string cFCMDTestStartPattern = (cmd.foundOption("fcmd-test-start-pattern")) ? cmd.optionValue("fcmd-test-start-pattern") : "11000001";
    std::string cFCMDTestUserFileName = (cmd.foundOption("fcmd-test-userfile")) ? cmd.optionValue("fcmd-test-userfile") : "fcmd_file.txt";
    std::string cBRAMFCMDLine         = (cmd.foundOption("bramfcmd-check")) ? cmd.optionValue("bramfcmd-check") : "fe_for_ps_roh_fcmd_SSA_l_check";
    std::string cBRAMFCMDFileName     = (cmd.foundOption("bramreffcmd-write")) ? cmd.optionValue("bramreffcmd-write") : "fcmd_file.txt";
    std::string cConvertUserFileName  = (cmd.foundOption("convert-userfile")) ? cmd.optionValue("convert-userfile") : "fcmd_file.txt";
    std::string cRefBRAMAddr          = (cmd.foundOption("read-ref-bram")) ? cmd.optionValue("read-ref-bram") : "0";
    std::string cCheckBRAMAddr        = (cmd.foundOption("read-check-bram")) ? cmd.optionValue("read-check-bram") : "0";
    bool        cMeasureInputIV       = cmd.foundOption("measure-input-iv");

    // To use from the GUI
    uint32_t cUsbBus = (cmd.foundOption("USBBus")) ? (uint32_t)(std::stoi(cmd.optionValue("USBBus"))) : 0; // Default option?
    uint8_t  cUsbDev = (cmd.foundOption("USBDev")) ? (uint32_t)(std::stoi(cmd.optionValue("USBDev"))) : 0; // Default option?
    bool     cGui    = (cmd.foundOption("useGui"));

    std::stringstream cDirName;
    cDirName << cDirectory << "PS_ROH_" << cHybridId.str();
    cDirectory = cDirName.str();
    // cDirectory += Form("PS_ROH_%s", cHybridId.c_str());

#ifdef __USE_ROOT__
    TApplication cApp("Root Application", &argc, argv);
    if(batchMode)
        gROOT->SetBatch(true);
    else
        TQObject::Connect("TCanvas", "Closed()", "TApplication", &cApp, "Terminate()");
#endif
    std::string cResultfile = "Hybrid";
    // Timer t;

    if(cGui)
    {
        // Initialize gui communication with named pipe
        gui::init(argv[argc - 1]);

        gui::status("Initializing test");
        gui::progress(0 / 10.0);
    }

    // Initialize and Configure Back-End (Optical) FC7
    Tool cTool;

    std::stringstream outp;
    LOG(INFO) << BOLDYELLOW << "Initializing FC7" << RESET;
    cTool.InitializeHw(cHWFile, outp);
    cTool.InitializeSettings(cHWFile, outp);
    LOG(INFO) << outp.str();
    outp.str("");
    cTool.CreateResultDirectory(cDirectory);
    cTool.InitResultFile(cResultfile);
    cTool.bookSummaryTree();

    if(cmd.foundOption("USBBus") && cmd.foundOption("USBDev")) { TC_PSROH cTC_PSROH(cUsbBus, cUsbDev); }

    // Initilaise PSROH tester
    PSROHTester cPSROHTester;
    cPSROHTester.Inherit(&cTool);

    uint8_t cExternalPattern = (cmd.foundOption("test-external-pattern")) ? convertAnyInt(cmd.optionValue("test-external-pattern").c_str()) : 0;
    cPSROHTester.LpGBTInjectULExternalPattern(true, cExternalPattern);

    // Initialize BackEnd & Control LpGBT Tester
    LOG(INFO) << BOLDYELLOW << "Configuring FC7" << RESET;
    if(cMeasureInputIV) cPSROHTester.MeasureInputIV("BEFORE_CONFIG");
    LOG(INFO) << BOLDMAGENTA << " ------------------------------------------- " << RESET;
    cTool.ConfigureHw();

    // Initialise tester
    cPSROHTester.Initialise();

    if(cMeasureInputIV) cPSROHTester.MeasureInputIV("AFTER_CONFIG");

    /***************/
    /* TEST UPLINK */
    /***************/
    if(cmd.foundOption("test-internal-pattern") || cmd.foundOption("test-external-pattern"))
    {
        if(cGui)
        {
            gui::message("");
            gui::status("Testing uplink");
            gui::progress(1 / 10.0);

            gui::data("ResultsDirectory", cPSROHTester.getDirectoryName().c_str());
        }
        /* INTERNALLY GENERATED PATTERN */
        if(cmd.foundOption("test-internal-pattern"))
        {
            uint8_t  cInternalPattern8  = (cmd.foundOption("test-internal-pattern")) ? convertAnyInt(cmd.optionValue("test-internal-pattern").c_str()) : 0;
            uint32_t cInternalPattern32 = cInternalPattern8 << 24 | cInternalPattern8 << 16 | cInternalPattern8 << 8 | cInternalPattern8 << 0;
            cPSROHTester.LpGBTInjectULInternalPattern(cInternalPattern32);
            cPSROHTester.LpGBTCheckULPattern(false, cInternalPattern8);
        }
        /* EXTERNALLY GENERATED PATTERN */
        else if(cmd.foundOption("test-external-pattern"))
        {
            uint8_t cExternalPattern = (cmd.foundOption("test-external-pattern")) ? convertAnyInt(cmd.optionValue("test-external-pattern").c_str()) : 0;
            // cPSROHTester.LpGBTInjectULExternalPattern(true, cExternalPattern);
            bool cStatus = cPSROHTester.LpGBTCheckULPattern(true, cExternalPattern);
#ifdef __USE_ROOT__
            cTool.fillSummaryTree("status_CicOutTest", (cStatus) ? 1 : 0);
#endif
            cPSROHTester.LpGBTInjectULExternalPattern(false, cExternalPattern);
            if(cStatus) { LOG(INFO) << BOLDGREEN << "CIC_Out test passed." << RESET; }
            else
            {
                LOG(INFO) << BOLDRED << "CIC_Out test failed." << RESET;
            }
            if(cGui)
            {
                gui::message("CIC OUT test finished");
                gui::status("Finished testing uplink");
                gui::progress(3 / 10.0);
            }
        }
    }
    /****************************/
    /* TEST RESET LINES (GPIOs) */
    /****************************/
    if(cmd.foundOption("test-reset"))
    {
        if(cGui)
        {
            gui::message("CIC OUT test finished");
            gui::status("Testing reset lines");
            gui::progress(3.5 / 10.0);
        }
        bool cStatus = cPSROHTester.LpGBTTestResetLines();
#ifdef __USE_ROOT__
        cTool.fillSummaryTree("status_ResetTest", (cStatus) ? 1 : 0);
#endif
        if(cStatus) { LOG(INFO) << BOLDGREEN << "Reset test passed." << RESET; }
        else
        {
            LOG(INFO) << BOLDRED << "Reset test failed." << RESET;
        }
        cStatus = cPSROHTester.LpGBTTestGPILines();
#ifdef __USE_ROOT__
        cTool.fillSummaryTree("status_PowerGoodTest", (cStatus) ? 1 : 0);
#endif
        if(cStatus) { LOG(INFO) << BOLDGREEN << "Power Good test passed." << RESET; }
        else
        {
            LOG(INFO) << BOLDRED << "Power Good test failed." << RESET;
        }
    }

    // Test VTRx+ slow control

    if(cmd.foundOption("test-vtrx"))
    {
        if(cGui)
        {
            gui::message("Reset lines test finished");
            gui::status("Testing VTRX+ slow control lines");
            gui::progress(4.5 / 10.0);
        }
        bool cStatus = cPSROHTester.LpGBTTestVTRx();
#ifdef __USE_ROOT__
        cTool.fillSummaryTree("status_vtrxplusslowcontrol", (cStatus) ? 1 : 0);
#endif
        if(cStatus)
            LOG(INFO) << BOLDBLUE << "VTRx+ slow control test passed." << RESET;
        else
            LOG(INFO) << BOLDRED << "VTRx+ slow control test failed." << RESET;
    }

    /********************/
    /* TEST I2C MASTERS */
    /********************/
    if(cmd.foundOption("test-i2c"))
    {
        if(cGui)
        {
            gui::message("VTRX+ test finished");
            gui::status("Testing I2C Masters on the lpGBT");
            gui::progress(5.5 / 10.0);
        }
        int                  pNTries  = convertAnyInt(cmd.optionValue("test-i2c").c_str());
        std::vector<uint8_t> cMasters = {0, 2};
        bool                 cStatus  = cPSROHTester.LpGBTTestI2CMaster(cMasters, pNTries);
#ifdef __USE_ROOT__
        cTool.fillSummaryTree("status_i2cmasters", (cStatus) ? 1 : 0);
#endif
        if(cStatus)
            LOG(INFO) << BOLDBLUE << "I2C test " << BOLDGREEN << " passed" << RESET;
        else
        {
            LOG(INFO) << BOLDBLUE << "I2C test " << BOLDRED << " failed" << RESET;
        }
    }

    /**********************************/
    /* TEST ANALOG-DIGITAL-CONVERTERS */
    /**********************************/
    if(cmd.foundOption("test-adc"))
    {
        if(cGui)
        {
            gui::message("I2C Masters test finished");
            gui::status("Testing ADC lines on the lpGBT");
            gui::progress(6.5 / 10.0);
        }
        cPSROHTester.LpGBTTestFixedADCs();

        std::vector<std::string> cADCs = {"ADC0", "ADC1", "ADC3"};
        cPSROHTester.LpGBTTestADC(cADCs, 0, 1000, 20);
    }

    /********************/
    /* TEST EYE OPENING */
    /********************/
    if(cmd.foundOption("test-eom"))
    {
        if(cGui)
        {
            gui::message("ADC test finished");
            gui::status("Measuring eye opening");
            gui::progress(7.5 / 10.0);
        }
        uint8_t cEQAttenuation = cmd.foundOption("eq-attenuation") ? convertAnyInt(cmd.optionValue("eq-attenuation").c_str()) : 3;
        cPSROHTester.LpGBTRunEyeOpeningMonitor(7, cEQAttenuation);
    }

    /***********************/
    /* TEST BIT ERROR RATE */
    /***********************/
    if(cmd.foundOption("test-ber"))
    {
        uint32_t cBERTPattern32 = cmd.foundOption("ber-pattern") ? convertAnyInt(cmd.optionValue("ber-pattern").c_str()) : 0x00000000;
        // FIXME still hard coded
        uint8_t cCoarseSource = 1, cFineSource = 4, cMeasTime = 5;
        cPSROHTester.LpGBTRunBitErrorRateTest(cCoarseSource, cFineSource, cMeasTime, cBERTPattern32);
    }

    /***************/
    /* TEST CLOCKS */
    /***************/
    if(cmd.foundOption("test-clock"))
    {
        if(cGui)
        {
            gui::message("Eye opening monitoring finished");
            gui::status("Testing clock lines");
            gui::progress(8.5 / 10.0);
        }
        LOG(INFO) << BOLDBLUE << "Clock test" << RESET;
        bool cStatus = cPSROHTester.LpGBTCheckClocks();
        if(cStatus)
            LOG(INFO) << BOLDBLUE << "Clock test " << BOLDGREEN << " passed" << RESET;
        else
        {
            LOG(INFO) << BOLDBLUE << "Clock test " << BOLDRED << " failed" << RESET;
        }
#ifdef __USE_ROOT__
        cTool.fillSummaryTree("status_clocktest", (cStatus) ? 1 : 0);
#endif
    }

    /*********************/
    /* TEST FAST COMMAND */
    /*********************/
    if(cmd.foundOption("test-fcmd"))
    {
        if(cGui)
        {
            gui::message("Clock line test finished");
            gui::status("Testing FCMD lines");
            gui::progress(9.5 / 10.0);
        }
        if(cmd.foundOption("fcmd-pattern"))
        {
            int     cFmcdCounter = 0, cFcmdTries = 100;
            uint8_t cFCMDPattern = (cmd.foundOption("fcmd-pattern")) ? convertAnyInt(cmd.optionValue("fcmd-pattern").c_str()) : 0;
            LOG(INFO) << BOLDBLUE << "FCMD pattern test" << RESET;
            cPSROHTester.LpGBTInjectDLInternalPattern(cFCMDPattern);
            for(int i = 0; i < cFcmdTries; i++)
            {
                if(!cPSROHTester.LpGBTFastCommandChecker(cFCMDPattern)) cFmcdCounter += 1;
            }
            LOG(INFO) << BOLDRED << "FCMD pattern test failed " << +cFmcdCounter << " times" << RESET;
#ifdef __USE_ROOT__
            cTool.fillSummaryTree("fcmd_tries", cFcmdTries);
            cTool.fillSummaryTree("fcmd_failures", cFmcdCounter);
#endif
        }
        else
        {
            if(cPSROHTester.FastCommandScope())
                LOG(INFO) << BOLDBLUE << "FCMD test " << BOLDGREEN << "passed" << RESET;
            else
                LOG(INFO) << BOLDBLUE << "FDMC test " << BOLDRED << "failed" << RESET;
        }
    }

    if(cDebug)
    {
        LOG(INFO) << "Start debugging" << RESET;
        cPSROHTester.PSROHInputsDebug();
    }

    if(cFCMDTest && !cFCMDTestStartPattern.empty() && !cFCMDTestUserFileName.empty())
    {
        LOG(INFO) << BOLDBLUE << "Fast command test" << RESET;
        cPSROHTester.CheckFastCommands(cFCMDTestStartPattern, cFCMDTestUserFileName);
    }

    if(cmd.foundOption("bramfcmd-check") && !cBRAMFCMDLine.empty())
    {
        LOG(INFO) << BOLDBLUE << "Access to written data in BRAM" << RESET;
        cPSROHTester.CheckFastCommandsBRAM(cBRAMFCMDLine);
    }

    if(cmd.foundOption("bramreffcmd-write") && !cBRAMFCMDFileName.empty())
    {
        LOG(INFO) << BOLDBLUE << "Write reference patterns to BRAM" << RESET;
        cPSROHTester.WritePatternToBRAM(cBRAMFCMDFileName);
    }

    if(cmd.foundOption("convert-userfile") && !cConvertUserFileName.empty())
    {
        LOG(INFO) << BOLDBLUE << "Convert user file to fw compliant format" << RESET;
        cPSROHTester.UserFCMDTranslate(cConvertUserFileName);
    }

    if(cmd.foundOption("read-ref-bram"))
    {
        int cAddr = std::atoi(cRefBRAMAddr.c_str());
        LOG(INFO) << BOLDBLUE << "Read single ref FCMD BRAM address: " << cmd.optionValue("read-ref-bram") << RESET;
        cPSROHTester.ReadRefAddrBRAM(cAddr);
    }

    if(cmd.foundOption("read-check-bram"))
    {
        int cAddr = std::atoi(cCheckBRAMAddr.c_str());
        LOG(INFO) << BOLDBLUE << "Read single check FCMD BRAM address: " << cmd.optionValue("read-check-bram") << RESET;
        cPSROHTester.ReadCheckAddrBRAM(cAddr);
    }

    if(cmd.foundOption("clear-ref-bram"))
    {
        LOG(INFO) << BOLDBLUE << "Flushing ref BRAM!" << RESET;
        cPSROHTester.ClearBRAM(std::string("ref"));
    }

    if(cmd.foundOption("clear-check-bram"))
    {
        LOG(INFO) << BOLDBLUE << "Flushing check BRAM!" << RESET;
        cPSROHTester.ClearBRAM(std::string("test"));
    }

    if(cGui)
    {
        gui::message("FCMD lines test finished");
        gui::status("Test finished");
        gui::progress(10.0 / 10.0);
    }
    // Save Result File
    cTool.SaveResults();
    cTool.WriteRootFile();
    cTool.CloseResultFile();
    // Destroy Tools
    cTool.Destroy();
    runCompleted = 1;

#ifdef __USE_ROOT__
    if(!batchMode) cApp.Run();
#endif
#endif
    return 0;
}
