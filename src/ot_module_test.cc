#include <cstring>

#include "BeamTestCheck.h"
#include "CBCPulseShape.h"
#include "CicFEAlignment.h"
#include "D19cDebugFWInterface.h"
#include "LatencyScan.h"
#include "LinkAlignmentOT.h"
#include "LinkTestOT.h"
#include "VTRxController.h"
#ifndef __EMP__
#include "BackendAlignmentOT.h"
#include "CheckCbcNeighbors.h"
#include "CicInputTest.h"
#include "MemoryCheck2S.h"
#include "SFPMonitoringTool.h"
#include "ShortFinder.h"
#endif
#include "OTCMNoise.h"
#include "OTModuleStartUp.h"
#include "OTTemperature.h"
#include "PSAlignment.h"
#include "PedeNoise.h"
#include "PedestalEqualization.h"
#include "RegisterTester.h"
#include "Utils/Timer.h"
#include "Utils/Utilities.h"
#include "Utils/argvparser.h"
#include "boost/format.hpp"
#include "HWInterface/DTCL1ReadoutInterface.h"
#ifdef __POWERSUPPLY__
// Libraries
#include "DeviceHandler.h"
#include "PowerSupply.h"
#include "PowerSupplyChannel.h"
#endif

#ifdef __USE_ROOT__
#include "TApplication.h"
#include "TROOT.h"
#endif

#define __NAMEDPIPE__

#ifdef __NAMEDPIPE__
#include "gui_logger.h"
#endif

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;
using namespace CommandLineProcessing;
INITIALIZE_EASYLOGGINGPP

#define CHIPSLAVE 4

sig_atomic_t killProcess  = 0;
sig_atomic_t runCompleted = 0;

void interruptHandler(int handler) { killProcess = 1; }

void killProcessFunction(Tool* theTool)
{
    while(1)
    {
        usleep(250000);
        if(killProcess || runCompleted) break;
    }
    if(killProcess)
    {
        theTool->Destroy();
        abort();
    }
}

uint16_t returnRunNumber(std::string cFileName)
{
    std::string   cLine;
    int           cRunNumber = -1;
    std::ifstream cStream(cFileName);
    if(cStream.is_open())
    {
        while(std::getline(cStream, cLine))
        {
            std::istringstream cIStream(cLine);
            cIStream >> cRunNumber;
            // LOG(INFO) << BOLDMAGENTA << cRunNumber << RESET;
        }
    }
    return (uint16_t)(cRunNumber + 1);
}

std::vector<uint8_t> getArgs(std::string pArgsStr)
{
    std::vector<uint8_t> cSides;
    std::stringstream    cArgsSS(pArgsStr);
    int                  i;
    while(cArgsSS >> i)
    {
        cSides.push_back(i);
        if(cArgsSS.peek() == ',') cArgsSS.ignore();
    };
    return cSides;
}

int main(int argc, char* argv[])
{
    // configure the logger
    el::Configurations conf(std::string(std::getenv("PH2ACF_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("CMS Ph2_ACF  Commissioning tool to perform the following procedures:\n-Timing / "
                                   "Latency scan\n-Threshold Scan\n-Stub Latency Scan");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");
    // options
    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("file", "Hw Description File . Default value: settings/Commission_2CBC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");

    cmd.defineOption("tuneOffsets", "tune offsets on readout chips connected to CIC.");
    cmd.defineOptionAlternative("tuneOffsets", "t");
    cmd.defineOption("linkTest", "Check data coming over link....", ArgvParser::OptionRequiresValue);
    cmd.defineOption("measurePedeNoise", "measure pedestal and noise on readout chips connected to CIC.");
    cmd.defineOptionAlternative("measurePedeNoise", "m");
    cmd.defineOption("setThresholdsToPedestal", "reset chip thresholds to pedestal values after measuring the noise.");

    cmd.defineOption("cmNoise", "measure common mode noise");
    cmd.defineOption("checkSFP", "Read-back monitorable from SFP [channel#,monitorable] - Valid monitorables are Temperature,Voltage,BiasCurrent,TxPower,RxPower", ArgvParser::OptionRequiresValue);
    cmd.defineOption("VTRxTest", "Read-back monitorable from SFP [channel#,monitorable] - Valid monitorables are Temperature,Voltage,BiasCurrent,TxPower,RxPower", ArgvParser::OptionRequiresValue);

    cmd.defineOption("read", "Read data from a raw file.  ", ArgvParser::OptionRequiresValue);
    cmd.defineOption("save", "Save the data to a raw file.  ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("skipAlignment", "Skip the back-end alignment step ", ArgvParser::OptionRequiresValue);
    // general
    cmd.defineOption("batch", "Run the application in batch mode", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("batch", "b");

    cmd.defineOption("allChan", "Do pedestal and noise measurement using all channels? Default: false", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("allChan", "a");

    cmd.defineOption("startUp", "Power up module in correct configuration");
    cmd.defineOption("reconfigure", "Reconfigure Hardware");

    cmd.defineOption("reload", "Reload settings files and board registers");
    cmd.defineOption("realign", "Re-align module [SSA-MPA] and/or [BE]");
    cmd.defineOption("realignStubs", "Re-align stubs");

    cmd.defineOption("moduleId", "Serial Number of module . Default value: xxxx", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOption("checkData", "Compare injected hits and stubs with output [please provide a comma seperated list of chips to check]", ArgvParser::OptionRequiresValue);
    cmd.defineOption("alignPS", "Perform SSA-MPA alignment steps", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkClusters", "Check CIC2 sparsification... ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("psDataTest", "....", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkSLink", "Check S-link ... data saved to file ", ArgvParser::OptionRequiresValue);
    cmd.defineOption("checkStubs", "Check Stubs... ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkReadData", "Check ReadData method... ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkAsync", "Check Async readout methods [PS objects only]... ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkReadNEvents", "Check ReadNEvents method... ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("noiseInjection", "Check noise injection...", ArgvParser::NoOptionAttribute);
    cmd.defineOption("calibrateADC", "Calibrate ADC on lpGBT....", ArgvParser::NoOptionAttribute);
    cmd.defineOption("readIDs", "Read chip ids....", ArgvParser::NoOptionAttribute);
    cmd.defineOption("memCheck", "Check memories of the following CBCs", ArgvParser::NoOptionAttribute);
    cmd.defineOption("completeDataCheck", "Complete data check for the following CBCs", ArgvParser::OptionRequiresValue);
    cmd.defineOption("VTRxTest", "....", ArgvParser::NoOptionAttribute);

    cmd.defineOption("pageToTest", "Page to test", ArgvParser::OptionRequiresValue);
    cmd.defineOption("registerTestWrite", "Test I2C registers on Chips", ArgvParser::OptionRequiresValue);
    cmd.defineOption("registerTestWriteAndToggle", "Test I2C registers on Chips", ArgvParser::OptionRequiresValue);
    cmd.defineOption("registerTestRead", "Test I2C registers on Chips", ArgvParser::OptionRequiresValue);
    cmd.defineOption("registerTestReadAndToggle", "Test I2C registers on Chips", ArgvParser::OptionRequiresValue);
    cmd.defineOption("sortOrder", "Sort order for CBC registers  : 0 - no sort other than page; 1 - page then increasing addresss; 2 - page then decreasing addresss", ArgvParser::OptionRequiresValue);
    cmd.defineOption("bitToFlip", "Bit to flip when testing register write", ArgvParser::OptionRequiresValue);
    cmd.defineOption("testAttempts", "Number of attempts", ArgvParser::OptionRequiresValue);
    cmd.defineOption("returnToDefPage", "Return to Def Page", ArgvParser::OptionRequiresValue);
    cmd.defineOption("manualScan", "Manual scan of threshold", ArgvParser::NoOptionAttribute);
    cmd.defineOption("injectionTest", "Manual scan of threshold", ArgvParser::OptionRequiresValue);
    //
    cmd.defineOption("DataMonitor", "Data monitor", ArgvParser::OptionRequiresValue);
    cmd.defineOption("TestPulseCheck", "Test pulse check - inject with TP and perform latency scan", ArgvParser::NoOptionAttribute);
    cmd.defineOption("ExternalCheck", "External trigger check - run with external triggers", ArgvParser::NoOptionAttribute);
    cmd.defineOption("TLUCheck", "External trigger check - run with external triggers", ArgvParser::NoOptionAttribute);
    cmd.defineOption("InternalCheck", "External trigger check - run with external triggers", ArgvParser::NoOptionAttribute);
    cmd.defineOption("continuousReadout", "Readout triggers as they come : argument to provide is how often to poll the readout [in us]", ArgvParser::OptionRequiresValue);
    cmd.defineOption("scanLatencies", "Scan L1+Stub Latencies ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("scanL1", "Scan L1 Latency ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("scanStubs", "Scan Stub Latency ", ArgvParser::NoOptionAttribute);
    cmd.defineOption("limitTriggers", "Only accept exactly the correct number of triggers", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkCICAlignment", "Manually scan CIC input aligner", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkLink", "Check that I can receive constant pattern from link", ArgvParser::OptionRequiresValue);
    //
    cmd.defineOption("readTemperatures", "Read temperature sensors available on module [lpGBT internal; sensor thermistory]", ArgvParser::OptionRequiresValue);
    cmd.defineOption("readMonitors", "Read internal monitors on lpGBT [lpGBT internal; sensor thermistory]", ArgvParser::OptionRequiresValue);
    cmd.defineOption("pulseShape", "Scan the threshold and fit for signal Vcth", ArgvParser::NoOptionAttribute);
    cmd.defineOption("checkSharedStubs", "Check stubs at boundary between chips", ArgvParser::NoOptionAttribute);
    //DaqPath specific options
	cmd.defineOption("daqPathChMask", "Channel mask in exadecimal. 1: channel skipped 0: channel kept. Default: 0xffffff3f", ArgvParser::OptionRequiresValue);
	cmd.defineOption("daqPathHEnable", "Enable header in DAQPATH. Default: Yes", ArgvParser::OptionRequiresValue);

	cmd.defineOption("nEvents", "set nEvents for testDTC", ArgvParser::OptionRequiresValue);
	cmd.defineOption("setThreshold", "set threshold for testDTC", ArgvParser::OptionRequiresValue);
	
    cmd.defineOption("testDTC", "testDTC", ArgvParser::NoOptionAttribute);



    int result = cmd.parse(argc, argv);

    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    // now query the parsing results
    std::string cHWFile          = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/Commissioning.xml";
    bool        batchMode        = (cmd.foundOption("batch")) ? true : false;
    bool        cSaveToFile      = cmd.foundOption("save");
    std::string cSkip            = (cmd.foundOption("skipAlignment")) ? cmd.optionValue("skipAlignment") : "";
    std::string cInjectionSource = (cmd.foundOption("injectionTest")) ? cmd.optionValue("injectionTest") : "digital";
    std::string cSrcLnkTst       = (cmd.foundOption("linkTest")) ? cmd.optionValue("linkTest") : "lpGBT";
    std::string cModuleId        = (cmd.foundOption("moduleId")) ? cmd.optionValue("moduleId") : "ModuleOT";
    std::string cDirectory       = (cmd.foundOption("output")) ? cmd.optionValue("output") : "Results/";
    bool        cPulseShape      = (cmd.foundOption("pulseShape")) ? true : false;

    std::string        cChMask      = (cmd.foundOption("daqPathChMask")) ? cmd.optionValue("daqPathChMask") : "ffffff3f";
	int         daqHEnable     = (cmd.foundOption("daqPathHEnable")) ? std::stoi(cmd.optionValue("daqPathHEnable"), NULL, 10) : 1;

    bool testDTC = (cmd.foundOption("testDTC")) ? true : false;
    int nEvents = (cmd.foundOption("nEvents")) ? std::stoi(cmd.optionValue("nEvents"), NULL, 10) : 4;
    int setThreshold = (cmd.foundOption("setThreshold")) ? std::stoi(cmd.optionValue("setThreshold"), NULL, 10) : 1000;

    uint16_t          cRunNumber = 666;
    std::stringstream cOutput;
    if(!cmd.foundOption("read"))
    {
        std::ofstream cRunLog;
        cRunNumber = returnRunNumber("RunNumbers.dat");
        cRunLog.open("RunNumbers.dat", std::fstream::app);
        cRunLog << cRunNumber << "\n";
        cRunLog.close();
        LOG(INFO) << BOLDBLUE << "Run number is " << +cRunNumber << RESET;
        cOutput << "OT_ModuleTest_" << cModuleId.c_str() << "_Run" << cRunNumber;
    }
    else
    {
        std::string       cRawFileName = cmd.foundOption("read") ? cmd.optionValue("read") : "";
        std::stringstream cOutput;
        cOutput << "Raw_" << cRawFileName.substr(0, cRawFileName.find(".raw")).c_str();
    }
    cDirectory += cOutput.str();
    LOG(INFO) << BOLDYELLOW << cDirectory << RESET;
    // #if defined(__USE_ROOT__)
    //     TApplication cApp("Root Application", &argc, argv);

    //     if(batchMode)
    //         gROOT->SetBatch(true);
    //     else
    //         TQObject::Connect("TCanvas", "Closed()", "TApplication", &cApp, "Terminate()");
    // #endif
    std::string cResultfile = "Hybrid";
    Timer       t;
    Timer       cGlobalTimer;
    cGlobalTimer.start();

    std::stringstream outp;
    Tool              cTool;

    std::thread softKillThread(killProcessFunction, &cTool);
    softKillThread.detach();

    struct sigaction act;
    act.sa_handler = interruptHandler;
    sigaction(SIGINT, &act, NULL);

    if(cSaveToFile)
    {
        char cRawFileName[80];
        std::snprintf(cRawFileName, sizeof(cRawFileName), "Run%.05d.raw", cRunNumber);
        std::string cRawFile = cRawFileName;
        cTool.addFileHandler(cRawFile, 'w');
        LOG(INFO) << BOLDBLUE << "Writing Binary Rawdata to:   " << cRawFile;
    }

    cTool.setResultsDirectory(cDirectory);
    cTool.setResultsFileName("ModuleTest");
    if(cmd.foundOption("read")) RegManager::enableParser();
    cTool.InitializeHw(cHWFile, outp);
    cTool.InitializeSettings(cHWFile, outp);
    LOG(INFO) << outp.str();

    bool boardIsDTC = false;
    for(const auto cBoard: *cTool.fDetectorContainer) {
        if(cBoard->getBoardType() == BoardType::DTC) {
            boardIsDTC = true;
            break;
        }
    }
    if(!cmd.foundOption("read") && (cmd.foundOption("startUp") || cmd.foundOption("reconfigure")))
    {
        LOG(INFO) << " ---- Starting up modules -----" << RESET;
        uint8_t         cColdStart = cmd.foundOption("startUp") ? 1 : 0;
        OTModuleStartUp cModuleStartUp;
        cModuleStartUp.setColdStart(cColdStart);
        if(boardIsDTC) {
            DTCFWInterface* cDtcFwInterface = static_cast<DTCFWInterface*>(cTool.fBeBoardInterface->getFirmwareInterface());
            DTCL1ReadoutInterface* cL1readoutInterface = static_cast<DTCL1ReadoutInterface*>(cDtcFwInterface->getL1ReadoutInterface());
            cL1readoutInterface->EnableDaqpathEventHeader(daqHEnable);
            cL1readoutInterface->SetDaqpathChannelMask(std::stol(cChMask, NULL, 16));
        }
        cModuleStartUp.Inherit(&cTool);
        cModuleStartUp.Start(0);
        cModuleStartUp.waitForRunToBeCompleted();
        cModuleStartUp.dumpConfigFiles();
        LOG(INFO) << "---- Module startup done. ----" << RESET;
    }
    else {
        if(boardIsDTC) {
            for(const auto cBoard: *cTool.fDetectorContainer) {

                DTCFWInterface *cDtcFwInterface = static_cast<DTCFWInterface*>(cTool.fBeBoardInterface->getFirmwareInterface());
                DTCL1ReadoutInterface* cL1readoutInterface = static_cast<DTCL1ReadoutInterface*>(cDtcFwInterface->getL1ReadoutInterface());
                DTCFastCommandInterface* cFastCommandInterface = static_cast<DTCFastCommandInterface*>(cDtcFwInterface->getFastCommandInterface());
                cL1readoutInterface->EnableDaqpathOutputReadout();
                cL1readoutInterface->EnableDaqpathEventHeader(1);
                cL1readoutInterface->SetDaqpathChannelMask(std::stol(cChMask, NULL, 16));
                cDtcFwInterface->Reset(cBoard);
                cL1readoutInterface->DumpFifo();
            }
        }
    }


    cTool.CreateResultDirectory(cDirectory, false, false);
    #ifdef __USE_ROOT__
    cTool.InitResultFile("ModuleTest");
    cTool.AddMetadata();
    #endif

    //Setup CH mask and daqpath stuff
   
   if(testDTC) {
        for(const auto cBoard: *cTool.fDetectorContainer) {
            cTool.setSameDacBeBoard(cBoard, "Threshold", setThreshold);

            DTCFWInterface *cDtcFwInterface = static_cast<DTCFWInterface*>(cTool.fBeBoardInterface->getFirmwareInterface());
            DTCL1ReadoutInterface* cL1readoutInterface = static_cast<DTCL1ReadoutInterface*>(cDtcFwInterface->getL1ReadoutInterface());
            DTCFastCommandInterface* cFastCommandInterface = static_cast<DTCFastCommandInterface*>(cDtcFwInterface->getFastCommandInterface());
            cL1readoutInterface->EnableDaqpathOutputReadout();
            cL1readoutInterface->EnableDaqpathEventHeader(1);
            cL1readoutInterface->SetDaqpathChannelMask(std::stol(cChMask, NULL, 16));
            cDtcFwInterface->Reset(cBoard);
            
            cTool.ReadNEvents(cBoard, nEvents);
            return 1;
        }
   }

    if(cmd.foundOption("readTemperatures"))
    {
        LOG(INFO) << BOLDBLUE << "Reading internal monitors from lpGBT-ADCs.." << RESET;
        auto          cGain = (cmd.foundOption("readMonitors")) ? convertAnyInt(cmd.optionValue("readMonitors").c_str()) : 0;
        OTTemperature cTemperatureReader;
        cTemperatureReader.Inherit(&cTool);
        cTemperatureReader.SetGain(cGain);
        cTemperatureReader.Start(cRunNumber);
        cTemperatureReader.waitForRunToBeCompleted();
        LOG(INFO) << "READ TMP done." << RESET;

    }

    if(cmd.foundOption("VTRxTest"))
    {
        LOG(INFO) << BOLDBLUE << "---- VTRx test ----" << RESET;
        std::string              cOptions = (cmd.foundOption("VTRxTest")) ? cmd.optionValue("VTRxTest") : "0x06,0x26";
        std::vector<std::string> cTokens;
        tokenize(cOptions, cTokens, ",");

        VTRxController cVTRxController;
        cVTRxController.Inherit(&cTool);
        cVTRxController.Start(cRunNumber);
        cVTRxController.waitForRunToBeCompleted();
        for(auto cBoard: *cTool.fDetectorContainer)
        {
            for(auto cOpticalGroup: *cBoard)
            {
                LOG(INFO) << BOLDYELLOW << "Write to " << cTokens[0] << "\t" << cTokens[1] << RESET;
                // cVTRxController.RegisterWrite(cOpticalGroup, cTokens[0] , (uint8_t)std::atoi(cTokens[1].c_str()) );
            }
        }
        LOG(INFO) << BOLDBLUE << "---- VTRx test done ----" << RESET;

    }
    #ifndef __EMP__
    if(cmd.foundOption("checkSFP"))
    {
        std::string              cOptions = (cmd.foundOption("checkSFP")) ? cmd.optionValue("checkSFP") : "0,Temperature";
        std::vector<std::string> cTokens;
        tokenize(cOptions, cTokens, ",");
        SFPMonitoringTool cSPMonitor(cHWFile);
        auto              cValue = cSPMonitor.GetMonitorable(cTokens[1], "l12", atoi(cTokens[0].c_str()));
        LOG(INFO) << BOLDYELLOW << "checkSFP " << cOptions << " : " << cValue.first << " ± " << cValue.second << RESET;
    }
#endif

    // read chip ids
    if(cmd.foundOption("readIDs"))
    {
        for(const auto cBoard: *cTool.fDetectorContainer)
        {
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        auto cFusedId = cTool.fReadoutChipInterface->ReadChipFuseID(cChip);
                        LOG(INFO) << BOLDMAGENTA << "Hybrid#" << +cChip->getId() << " CBC#" << +cChip->getId() << " Fused Id is " << +cFusedId << RESET;
                    }
                }
            }
        }
    }

    if(cmd.foundOption("registerTestWrite"))
    {
        uint8_t cNregistersToCheck = (cmd.foundOption("registerTestWrite")) ? convertAnyInt(cmd.optionValue("registerTestWrite").c_str()) : 1;
        // 0 - no sorting other than page; 1 - page then increasing addresss ; 2 - page then decreasing address
        uint8_t  cSortOder  = (cmd.foundOption("sortOrder")) ? convertAnyInt(cmd.optionValue("sortOrder").c_str()) : 0;
        uint8_t  cBitToFlip = (cmd.foundOption("bitToFlip")) ? convertAnyInt(cmd.optionValue("bitToFlip").c_str()) : 0;
        uint32_t cAttempts  = (cmd.foundOption("testAttempts")) ? convertAnyInt(cmd.optionValue("testAttempts").c_str()) : 10;
        uint8_t  cPage      = (cmd.foundOption("pageToTest")) ? convertAnyInt(cmd.optionValue("pageToTest").c_str()) : 1;

        RegisterTester cRegTester;
        cRegTester.Inherit(&cTool);
        cRegTester.SetSortOrder(cSortOder);
        cRegTester.SetBitToFlip(cBitToFlip);
        cRegTester.Initialise();
        for(size_t cAttempt = 0; cAttempt < cAttempts; cAttempt++)
        {
            LOG(INFO) << BOLDBLUE << "Read - test#" << +cAttempt << RESET;
            cRegTester.CheckWriteRegisters(cPage, cNregistersToCheck);
        }
        cRegTester.writeObjects();
    }

    if(cmd.foundOption("registerTestWriteAndToggle"))
    {
        uint8_t cNregistersToCheck = (cmd.foundOption("registerTestWriteAndToggle")) ? convertAnyInt(cmd.optionValue("registerTestWriteAndToggle").c_str()) : 1;
        // 0 - no sorting other than page; 1 - page then increasing addresss ; 2 - page then decreasing address
        uint8_t  cSortOder        = (cmd.foundOption("sortOrder")) ? convertAnyInt(cmd.optionValue("sortOrder").c_str()) : 0;
        uint8_t  cBitToFlip       = (cmd.foundOption("bitToFlip")) ? convertAnyInt(cmd.optionValue("bitToFlip").c_str()) : 0;
        uint8_t  cReturnToDefPage = (cmd.foundOption("returnToDefPage")) ? convertAnyInt(cmd.optionValue("returnToDefPage").c_str()) : 1;
        uint32_t cAttempts        = (cmd.foundOption("testAttempts")) ? convertAnyInt(cmd.optionValue("testAttempts").c_str()) : 10;
        uint8_t  cPage            = (cmd.foundOption("pageToTest")) ? convertAnyInt(cmd.optionValue("pageToTest").c_str()) : 1;

        LOG(INFO) << BOLDBLUE << "Will run register test " << cAttempts << " times..." << RESET;
        RegisterTester cRegTester;
        cRegTester.Inherit(&cTool);
        cRegTester.SetSortOrder(cSortOder);
        cRegTester.SetBitToFlip(cBitToFlip);
        cRegTester.SetReturnToDefPage(cReturnToDefPage);
        cRegTester.Initialise();
        for(size_t cAttempt = 0; cAttempt < cAttempts; cAttempt++)
        {
            LOG(INFO) << BOLDBLUE << "Page switch with read - test#" << +cAttempt << RESET;
            cRegTester.CheckPageSwitchWrite(cPage, cNregistersToCheck);
        }
        cRegTester.writeObjects();
    }

    if(cmd.foundOption("registerTestRead"))
    {
        uint8_t cNregistersToCheck = (cmd.foundOption("registerTestRead")) ? convertAnyInt(cmd.optionValue("registerTestRead").c_str()) : 1;
        // 0 - no sorting other than page; 1 - page then increasing addresss ; 2 - page then decreasing address
        uint8_t  cSortOder = (cmd.foundOption("sortOrder")) ? convertAnyInt(cmd.optionValue("sortOrder").c_str()) : 0;
        uint32_t cAttempts = (cmd.foundOption("testAttempts")) ? convertAnyInt(cmd.optionValue("testAttempts").c_str()) : 10;
        uint8_t  cPage     = (cmd.foundOption("pageToTest")) ? convertAnyInt(cmd.optionValue("pageToTest").c_str()) : 1;

        RegisterTester cRegTester;
        cRegTester.Inherit(&cTool);
        cRegTester.SetSortOrder(cSortOder);
        cRegTester.Initialise();
        for(size_t cAttempt = 0; cAttempt < cAttempts; cAttempt++)
        {
            LOG(INFO) << BOLDBLUE << "Read - test#" << +cAttempt << RESET;
            cRegTester.CheckReadRegisters(cPage, cNregistersToCheck);
        }
        cRegTester.writeObjects();
    }

    if(cmd.foundOption("registerTestReadAndToggle"))
    {
        uint8_t cNregistersToCheck = (cmd.foundOption("registerTestReadAndToggle")) ? convertAnyInt(cmd.optionValue("registerTestReadAndToggle").c_str()) : 1;
        // 0 - no sorting other than page; 1 - page then increasing addresss ; 2 - page then decreasing address
        uint8_t  cSortOder        = (cmd.foundOption("sortOrder")) ? convertAnyInt(cmd.optionValue("sortOrder").c_str()) : 0;
        uint8_t  cReturnToDefPage = (cmd.foundOption("returnToDefPage")) ? convertAnyInt(cmd.optionValue("returnToDefPage").c_str()) : 1;
        uint32_t cAttempts        = (cmd.foundOption("testAttempts")) ? convertAnyInt(cmd.optionValue("testAttempts").c_str()) : 10;
        uint8_t  cPage            = (cmd.foundOption("pageToTest")) ? convertAnyInt(cmd.optionValue("pageToTest").c_str()) : 1;

        RegisterTester cRegTester;
        cRegTester.Inherit(&cTool);
        cRegTester.SetSortOrder(cSortOder);
        cRegTester.SetReturnToDefPage(cReturnToDefPage);
        cRegTester.Initialise();
        for(size_t cAttempt = 0; cAttempt < cAttempts; cAttempt++)
        {
            LOG(INFO) << BOLDBLUE << "Page switch with read - test#" << +cAttempt << RESET;
            cRegTester.CheckPageSwitchRead(cPage, cNregistersToCheck);
        }
        cRegTester.writeObjects();
    }

    if(!cmd.foundOption("read") && cmd.foundOption("realignStubs"))
    {
        PrintConfig cCnfg;
        cCnfg.fVerbose    = 1;
        cCnfg.fPrintEvery = 1;

        BeamTestCheck cBeamTestCheck;
        cBeamTestCheck.Inherit(&cTool);
        cBeamTestCheck.Initialise();
        cBeamTestCheck.ConfigureScans(true, true);
        cBeamTestCheck.ConfigurePrintout(cCnfg);
        cBeamTestCheck.AlignStubPackage();
        cBeamTestCheck.Reset();
    }

    // align CIC-lpGBT-BE
    if(!cmd.foundOption("read") && (cmd.foundOption("startUp") && (!cmd.foundOption("skipAlignment")) || cmd.foundOption("realign")))
    {
        LOG(INFO) << BOLDBLUE << "---- ALIGN CIC-lpGBT - BE ----" << RESET;
        t.start();
        // align FEs - CIC
        // first map MPA outputs for PS module
        PSAlignment cPSAlignment;
        cPSAlignment.Inherit(&cTool);
        cPSAlignment.Initialise();
        cPSAlignment.MapMPAOutputs();
        cPSAlignment.ConfigureDefaultAlignmentParameters();

        // // then run FE-CI alignment
        CicFEAlignment cCicAligner;
        cCicAligner.Inherit(&cTool);
        cCicAligner.Start(cRunNumber);
        cCicAligner.waitForRunToBeCompleted();

        // align lpGBT-CIC
        LinkAlignmentOT cLinkAlignment;
        cLinkAlignment.Inherit(&cTool);
        cLinkAlignment.Start(cRunNumber);
        cLinkAlignment.waitForRunToBeCompleted();

#ifndef __EMP__
        // align data in the backend CIC -- uDTC
        BackendAlignmentOT cBackendAligner;
        cBackendAligner.Inherit(&cTool);
        cBackendAligner.Start(cRunNumber);
        cBackendAligner.waitForRunToBeCompleted();
#endif
        t.stop();
        t.show("Time to prepare for data taking :");
        LOG(INFO) << BOLDBLUE << "---- ALIGN CIC-lpGBT - BE DONE ----" << RESET;

    }
    if(!cmd.foundOption("read") && cmd.foundOption("realign"))
    {
    }

    if(!cmd.foundOption("read") && cmd.foundOption("checkCICAlignment"))
    {
        // align FEs - CIC
        CicFEAlignment cCicAligner;
        cCicAligner.Inherit(&cTool);
        cCicAligner.Initialise();
        cCicAligner.InputLineScan();
        cCicAligner.Reset();
    }

    // equalize thresholds on readout chips
    if(cmd.foundOption("tuneOffsets") && !cmd.foundOption("read"))
    {
        LOG(INFO) << BOLDBLUE << "---- TUNING OFFSETS ----" << RESET;
        bool cAllChan = (cmd.foundOption("allChan")) ? true : false;
        t.start();
        // now create a PedestalEqualization object
        PedestalEqualization cPedestalEqualization;
        cPedestalEqualization.Inherit(&cTool);
        cPedestalEqualization.Initialise(cAllChan, true);
        cPedestalEqualization.FindVplus();
        cPedestalEqualization.FindOffsets();
        cPedestalEqualization.Reset();
        // second parameter disables stub logic on CBC3
        cPedestalEqualization.writeObjects();
        cPedestalEqualization.dumpConfigFiles();
        cPedestalEqualization.resetPointers();
        t.stop();
        t.show("Time to tune the front-ends on the system: ");
        // // reset
        // cTool.fDetectorContainer->resetReadoutChipQueryFunction();
        LOG(INFO) << BOLDBLUE << "---- TUNING OFFSETS DONE ----" << RESET;
    }

    if(!cmd.foundOption("read") && cmd.foundOption("checkLink"))
    {
        uint8_t         cPattern = (cmd.foundOption("checkLink")) ? convertAnyInt(cmd.optionValue("checkLink").c_str()) : 0xEA;
        LinkAlignmentOT cLinkAlignment;
        cLinkAlignment.Inherit(&cTool);
        cLinkAlignment.Initialise();
        cLinkAlignment.CheckLpgbtOutputs(cPattern);
        cLinkAlignment.Reset();
    }
    // measure noise on FE chips
    if(cmd.foundOption("measurePedeNoise") && !cmd.foundOption("read"))
    {
        for(const auto cBoard: *cTool.fDetectorContainer) {
            //cTool.setSameDacBeBoard(cBoard, "Threshold", setThreshold);

            DTCFWInterface *cDtcFwInterface = static_cast<DTCFWInterface*>(cTool.fBeBoardInterface->getFirmwareInterface());
            DTCL1ReadoutInterface* cL1readoutInterface = static_cast<DTCL1ReadoutInterface*>(cDtcFwInterface->getL1ReadoutInterface());
            DTCFastCommandInterface* cFastCommandInterface = static_cast<DTCFastCommandInterface*>(cDtcFwInterface->getFastCommandInterface());
            cL1readoutInterface->EnableDaqpathOutputReadout();
            cL1readoutInterface->EnableDaqpathEventHeader(1);
            cL1readoutInterface->SetDaqpathChannelMask(std::stol(cChMask, NULL, 16));
            cDtcFwInterface->Reset(cBoard);
            
            //cTool.ReadNEvents(cBoard, nEvents);
            //return 1;
        }

        LOG(INFO) << BOLDBLUE << "---- MEASURING PEDESTAL AND NOISE ---- " << RESET;
        bool cAllChan = (cmd.foundOption("allChan")) ? true : false;
        LOG(INFO) << BOLDMAGENTA << "Measuring pedestal and noise" << RESET;
        t.start();
        // if this is true, I need to create an object of type PedeNoise from the members of Calibration
        // tool provides an Inherit(Tool* pTool) for this purpose
        PedeNoise cPedeNoise;
        cPedeNoise.Inherit(&cTool);
        cPedeNoise.Initialise(cAllChan, true); // canvases etc. for fast calibration
        cPedeNoise.measureNoise();
        cPedeNoise.Validate();
        if(cmd.foundOption("setThresholdsToPedestal")) cPedeNoise.setThresholdtoNSigma(0.);
        cPedeNoise.writeObjects();
        cPedeNoise.dumpConfigFiles();
        cPedeNoise.Reset();
        t.stop();
        t.show("Time to Scan Pedestals and Noise");
        LOG(INFO) << BOLDBLUE << "---- MEASURING PEDESTAL AND NOISE DONE ----" << RESET;
    }

    if(cmd.foundOption("cmNoise") && !cmd.foundOption("read"))
    {
        LOG(INFO) << "---- OT_MODULE_TEST:: Measuring CM Noise ----" << RESET;
        OTCMNoise cTester;
        cTester.Inherit(&cTool);
        cTester.Initialize();
        LOG(INFO) << "OT_MODULE_TEST:: Setting thresholds" << RESET;
        cTester.SetThresholds();

        LOG(INFO) << "---- OT_MODULE_TEST:: Taking measurements ----" << RESET;
        cTester.TakeData();
    }

// inject hits and stubs using mask and compare input against output
#ifndef __EMP__
    if(cmd.foundOption("memCheck") && !cmd.foundOption("read"))
    {
        MemoryCheck2S cMemoryChecker;
        cMemoryChecker.Inherit(&cTool);
        cMemoryChecker.Initialise();

        // configure reference voltage
        cMemoryChecker.ConfigureVref();
        cMemoryChecker.MonitorTemperature();
        cMemoryChecker.MonitorInputVoltage();
        // find pedestal and set threshold
        if(cmd.foundOption("completeDataCheck"))
        {
            std::string          cArgsStr      = cmd.optionValue("completeDataCheck");
            std::vector<uint8_t> cChipsToCheck = getArgs(cArgsStr);
            cMemoryChecker.EvaluatePedeNoise(10); // find pedestal + noise
            cMemoryChecker.SetThreshold(-2.0);    // set threshold to 3 sigma away from pedestal

            int cTriggerGap = cTool.findValueInSettings<double>("TriggerSeparation", 500);
            cMemoryChecker.DataCheck(cChipsToCheck, cTriggerGap);
        }

        cMemoryChecker.MemoryCheck2SRaw(true);  // all ones
        cMemoryChecker.MemoryCheck2SRaw(false); // all zeros

        cMemoryChecker.MonitorAnalogue();
        cMemoryChecker.SaveOptimalTaps();
        cMemoryChecker.writeObjects();
        cMemoryChecker.resetPointers();
    }
#endif
    uint8_t cScanL1    = (cmd.foundOption("scanL1") || cmd.foundOption("scanLatencies")) ? 1 : 0;
    uint8_t cScanStubs = (cmd.foundOption("scanStubs") || cmd.foundOption("scanLatencies")) ? 1 : 0;
    if(!cmd.foundOption("read") && cmd.foundOption("TestPulseCheck"))
    {
        std::ofstream cGoodRuns;
        cGoodRuns.open("GoodRunNumbers.dat", std::fstream::app);
        cGoodRuns << cRunNumber << "\n";
        cGoodRuns.close();

        PrintConfig cCng;
        cCng.fVerbose    = 1;
        cCng.fPrintEvery = 1;

        BeamTestCheck cBeamTestCheck;
        cBeamTestCheck.Inherit(&cTool);
        cBeamTestCheck.Initialise();

        cBeamTestCheck.ConfigureScans(cScanL1, cScanStubs);
        cBeamTestCheck.ConfigurePrintout(cCng);
        cBeamTestCheck.CheckWithTP();
        cBeamTestCheck.writeObjects();
        // cBeamTestCheck.Reset();
    }

    if(!cmd.foundOption("read") && cmd.foundOption("ExternalCheck"))
    {
        std::ofstream cGoodRuns;
        cGoodRuns.open("GoodRunNumbers.dat", std::fstream::app);
        cGoodRuns << cRunNumber << "\n";
        cGoodRuns.close();

        BeamTestCheck cBeamTestCheck;
        cBeamTestCheck.Inherit(&cTool);
        cBeamTestCheck.Initialise();
        cBeamTestCheck.ConfigureScans(cScanL1, cScanStubs);
        // check with TP
        cBeamTestCheck.CheckWithExternal();
        cBeamTestCheck.writeObjects();
        cBeamTestCheck.Reset();
    }

    if(!cmd.foundOption("read") && cmd.foundOption("InternalCheck"))
    {
        std::ofstream cGoodRuns;
        cGoodRuns.open("GoodRunNumbers.dat", std::fstream::app);
        cGoodRuns << cRunNumber << "\n";
        cGoodRuns.close();

        BeamTestCheck cBeamTestCheck;
        cBeamTestCheck.Inherit(&cTool);
        cBeamTestCheck.Initialise();
        cBeamTestCheck.ConfigureScans(cScanL1, cScanStubs);
        cBeamTestCheck.CheckWithInternal();
        // cBeamTestCheck.Start(cRunNumber);
        // cBeamTestCheck.waitForRunToBeCompleted();
        cBeamTestCheck.Reset();
        cBeamTestCheck.dumpConfigFiles();
        // cBeamTestCheck.Initialise();
        // PrintConfig cCng;
        // cCng.fVerbose    = 1;
        // cCng.fPrintEvery = 10000;
        // cBeamTestCheck.ConfigurePrintout(cCng);
        // cBeamTestCheck.CheckWithInternal();
        // cBeamTestCheck.writeObjects();
        // cBeamTestCheck.Reset();
    }

    if(!cmd.foundOption("read") && cmd.foundOption("TLUCheck"))
    {
        std::ofstream cGoodRuns;
        cGoodRuns.open("GoodRunNumbers.dat", std::fstream::app);
        cGoodRuns << cRunNumber << "\n";
        cGoodRuns.close();

        BeamTestCheck cBeamTestCheck;
        cBeamTestCheck.Inherit(&cTool);
        cBeamTestCheck.Initialise();
        cBeamTestCheck.ConfigureScans(cScanL1, cScanStubs);
        cBeamTestCheck.CheckWithTLU();
        cBeamTestCheck.writeObjects();
        cBeamTestCheck.Reset();
    }

    if(!cmd.foundOption("read") && cmd.foundOption("DataMonitor"))
    {
        std::ofstream cGoodRuns;
        cGoodRuns.open("GoodRunNumbers.dat", std::fstream::app);
        cGoodRuns << cRunNumber << "\n";
        cGoodRuns.close();

        uint8_t       cDisableFEs = (cmd.foundOption("DataMonitor")) ? convertAnyInt(cmd.optionValue("DataMonitor").c_str()) : 0;
        BeamTestCheck cBeamTestCheck;
        cBeamTestCheck.Inherit(&cTool);
        cBeamTestCheck.Initialise();
        if(cDisableFEs == 1) cBeamTestCheck.DisableAllFEs();

        // uint8_t cContinuousReadout = cmd.foundOption("continuousReadout") ? 1 : 0;
        // int     cReadoutPause      = (cmd.foundOption("continuousReadout")) ? convertAnyInt(cmd.optionValue("continuousReadout").c_str()) : 10;
        // cBeamTestCheck.SetReadoutPause(cReadoutPause);

        if(cmd.foundOption("TLUCheck")) cBeamTestCheck.CheckWithTLU();
        if(cmd.foundOption("ExternalCheck")) cBeamTestCheck.CheckWithExternal();
        if(cmd.foundOption("TestPulseCheck")) cBeamTestCheck.CheckWithTP();

        cBeamTestCheck.writeObjects();
        cBeamTestCheck.Reset();
    }

    if(cmd.foundOption("read"))
    {
        std::string   cRawFileName = cmd.foundOption("read") ? cmd.optionValue("read") : "";
        BeamTestCheck cBeamTestCheck;
        cBeamTestCheck.SetReadoutMode(1);
        cBeamTestCheck.Inherit(&cTool);
        cBeamTestCheck.Initialise();
        PrintConfig cCng;
        cCng.fVerbose    = 0;
        cCng.fPrintEvery = 1;
        cBeamTestCheck.ConfigurePrintout(cCng);
        cBeamTestCheck.SaveHitDataTree();
        cBeamTestCheck.ReadDataFromFile(cRawFileName);
        // cBeamTestCheck.ValidateRaw();
        cBeamTestCheck.writeObjects();
        // cBeamTestCheck.Reset();
    }
    // if(!cmd.foundOption("read")) { cTool.dumpConfigFiles(); }

    if(cPulseShape)
    {
        std::cout << "I am in" << std::endl;
        Timer t;
        t.start();
        CBCPulseShape cCBCPulseShape;
        cCBCPulseShape.Inherit(&cTool);
        cCBCPulseShape.Initialise();
        cCBCPulseShape.runCBCPulseShape();
        cCBCPulseShape.writeObjects();
        t.stop();
        t.show("Time for pulseShape plot measurement");
        t.reset();
    }

#ifndef __EMP__
    if(!cmd.foundOption("read") && cmd.foundOption("checkSharedStubs"))
    {
        LOG(INFO) << BOLDMAGENTA << "Checking stubs across CBC neighbors" << RESET;
        t.start();

        // MemoryCheck2S cMemoryChecker;
        // cMemoryChecker.Inherit(&cTool);
        // cMemoryChecker.Initialise();
        // cMemoryChecker.EvaluatePedeNoise(10); // find pedestal + noise
        // cMemoryChecker.SetThreshold(-2.0);    // set threshold to 3 sigma away from pedestal

        CheckCbcNeighbors cCheckCbcNeighbors;
        cCheckCbcNeighbors.Inherit(&cTool);
        cCheckCbcNeighbors.Start(0);
        cCheckCbcNeighbors.waitForRunToBeCompleted();

        t.stop();
        t.show("Time to check stubs on shared channels");
    }
#endif

    cTool.SaveResults();
#if defined(__USE_ROOT__)
    cTool.WriteRootFile();
    cTool.CloseResultFile();
#endif
    cTool.Destroy();
    signal(SIGINT, SIG_DFL);
    runCompleted = 1;
    // #if defined(__USE_ROOT__)
    //     if(!batchMode) cApp.Run();
    // #endif
    cGlobalTimer.stop();
    cGlobalTimer.show("Total execution time: ");

    return 0;
}
