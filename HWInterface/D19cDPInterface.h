#ifndef _D19cDPInterface_H__
#define _D19cDPInterface_H__

#include "../HWDescription/BeBoard.h"
#include "../Utils/Utilities.h"
#include "../Utils/easylogging++.h"
#include "RegManager.h"
#include <string>

namespace Ph2_HwInterface
{
class D19cDPInterface : public RegManager
{
  public: // constructors
    D19cDPInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    D19cDPInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~D19cDPInterface();

  public: //
    // start and stop
    void StartSyncPlaying();
    bool Start();
    bool Stop();
    //
    bool   IsRunning();
    size_t CheckNPatterns();
    // configure pattern played by DataPlayer
    void                 Configure(uint32_t pPattern, uint16_t pFrequency = 320);
    void                 ConfigureLine(uint16_t pPattern, uint8_t pLine);
    void                 ConfigureLineBRAM(uint8_t pLine, std::vector<uint8_t> pPatterns);
    void                 ConfigurePatternAllLines(std::map<uint8_t, uint8_t> pPattern);
    void                 ConfigurePatternAllLines(uint8_t pPattern);
    void                 SetType(uint8_t pType) { fType = pType; }
    std::vector<uint8_t> GetPatternOnLines()
    {
        fPatterns.clear();
        for(size_t cLine = 0; cLine < 6; cLine++) fPatterns.push_back(fPatternMap[cLine]);
        return fPatterns;
    }

  protected:
    uint8_t                    fType{0};
    uint32_t                   fWait_us{1};
    bool                       fEmulatorRunning{0};
    bool                       fEmulatorConfigured{0};
    std::vector<uint8_t>       fPatterns;
    std::map<uint8_t, uint8_t> fPatternMap;
};
} // namespace Ph2_HwInterface
#endif
