#ifndef __DTCL1ReadoutInterface_H__
#define __DTCL1ReadoutInterface_H__

#include "L1ReadoutInterface.h"

namespace DTCL1EvntEncoder
{
// ################
// # Event header #
// ################
const uint16_t EVT_HEADER = 0xFFFF;

const uint16_t IWORD_L1_HEADER = 4;
const uint16_t SBIT_L1_HEADER  = 28;
const uint16_t SBIT_L1_STATUS  = 24;
const uint16_t SBIT_HYBRID_ID  = 16;
const uint16_t SBIT_CHIP_ID    = 12;

// ################
// # Event status #
// ################
const uint16_t GOOD           = 0x0000; // Event status Good
const uint16_t EMPTY          = 0x0002; // Event status Empty event
const uint16_t BADHEADER      = 0x0004; // Bad header
const uint8_t  GOODL1HEADER   = 0x0A;
const uint8_t  GOODStubHEADER = 0x05;
const uint16_t BADL1HEADER    = 0x0006; // Bad L1 header
const uint16_t BADSTUBHEADER  = 0x0008; // Bad Stub header
const uint16_t NODECODER = 0xFFFF; // Event decoding not implemented

const uint16_t CLUSTER_2S   = 14;
const uint16_t SCLUSTER_PS  = 14;
const uint16_t PCLUSTER_PS  = 17;
const uint16_t SCLUSTER_MPA = 0;
const uint16_t PCLUSTER_MPA = 0;
const uint16_t HITS_2S      = 274;
const uint16_t HITS_SSA     = 120;
const uint16_t HITS_CBC     = 254;
} // namespace DTCL1EvntEncoder

// Thread-safe queue
#ifndef __TSQueue_H__
#define __TSQueue_H__
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
template <typename T>
class TSQueue
{
  private:
    // Underlying queue
    std::queue<T>  m_queue;
    std::vector<T> m_vector;

    // mutex for thread synchronization
    std::mutex m_mutex;

    // Condition variable for signaling
    std::condition_variable m_cond;
    size_t                  m_loc{0};

  public:
    // Pushes an element to the queue
    void push(T item)
    {
        // Acquire lock
        std::unique_lock<std::mutex> lock(m_mutex);

        // Add item
        m_queue.push(item);

        // Notify one thread that
        // is waiting
        m_cond.notify_one();
    }

    // Pops an element off the queue
    T pop()
    {
        // acquire lock
        std::unique_lock<std::mutex> lock(m_mutex);

        // wait until queue is not empty
        m_cond.wait(lock, [this]() { return !m_queue.empty(); });

        // retrieve item
        T item = m_queue.front();
        m_queue.pop();

        // return item
        return item;
    }

    size_t size()
    {
        // acquire lock
        // std::unique_lock<std::mutex> lock(m_mutex);
        // acquire lock
        return m_queue.size();
        // return m_vector.size();
    }
};
#endif

using DTCReadoutEvent = std::vector<uint32_t>;
namespace Ph2_HwInterface
{
class DTCL1ReadoutInterface : public L1ReadoutInterface
{
  public:
    DTCL1ReadoutInterface(const std::string& puHalConfigFileName, const std::string& pBoardId);
    DTCL1ReadoutInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    DTCL1ReadoutInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~DTCL1ReadoutInterface();

  public:
    void FillData() override;
    bool WaitForReadout() override;
    bool WaitForNTriggers() override;
    bool ReadEvents(const Ph2_HwDescription::BeBoard* pBoard) override;
    bool PollReadoutData(const Ph2_HwDescription::BeBoard* pBoard, bool pWait = false) override;
    bool ResetReadout() override;
    bool CheckBuffers() override;

    void                      SetWait(uint32_t pWait_us) { fWait_us = pWait_us; }
    void                      StartReadout();
    void                      StopReadout();
    TSQueue<DTCReadoutEvent> fEventQueue;
    // function to read from DDR3 + count events
    void PollReadoutTh();
    void EventCountingTh();
    bool ReadoutFinished() { return fReadoutCompleted; }
    void FeChannelFifoStatus(int pChan);
    //Daqpath functions
    void EnableDaqpathEventHeader(int pHEnable);
    void SetDaqpathChannelMask(uint32_t pMask);
    void EnableDaqPath();
    void EnableDaqpathOutputReadout();
    bool DumpFifo();

  private:
    bool WaitForData();
    bool CheckReadoutReq();
    bool CheckForWordsInReadout();
    void CountFwEvents();
    uint32_t ReadDaqpathIdNw(unsigned int pOutId = 0);
    uint32_t ReadDaqpathDwh(unsigned int pOutId = 0);
    uint32_t ReadDaqpathDwl(unsigned int  pOutId = 0);
    bool IsDaqpathEventHeaderEnabled();
    void ResetDaqPath();
    bool SetupForReadout();
    bool CheckStatusDaqpathOutput(unsigned int pOutId = 0);

    uint32_t          fWait_us{100};
    uint32_t          fReadoutAttempts{0};
    uint32_t          fDDR3Offset{0};
    uint8_t           fWaitForReadoutReq{0};
    TSQueue<uint32_t> fReadoutQueue;
    bool              fReadoutValid;
    bool              fReadoutCompleted;
    bool              fReceivedAllEvents;
    size_t            fEventCounter;
    size_t            fRequestedEvents{100};
    std::thread       fPollingThread;
    std::thread       fEventCountThread;
    int               fReceivedEvents = 0;
};
} // namespace Ph2_HwInterface
#endif
