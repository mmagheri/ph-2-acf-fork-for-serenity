#ifndef _D19cMuxBackplaneFWInterface_H__
#define __D19cMuxBackplaneFWInterface_H__

#include "BeBoardFWInterface.h"
#include <string>

namespace Ph2_HwInterface
{
class D19cMuxBackplaneFWInterface : public RegManager
{
  public:
    D19cMuxBackplaneFWInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    D19cMuxBackplaneFWInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    D19cMuxBackplaneFWInterface(RegManager&& theRegManager);
    ~D19cMuxBackplaneFWInterface();

  public:
    void     DisconnectMultiplexingSetup(uint8_t pWait_ms = 100);
    uint32_t ScanMultiplexingSetup(uint8_t pWait_ms = 100);
    void     ConfigureMultiplexingSetup(int BackplaneNum, int CardNum, uint8_t pWait_ms = 100);

  private:
    uint32_t fWait_us{100};
};
} // namespace Ph2_HwInterface
#endif
