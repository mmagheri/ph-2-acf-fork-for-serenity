/*!

        \file                           DTCFWInterface.h
        \brief                          DTCFWInterface init/config of the FC7 and its Chip's
        \author                         G. Auzinger, K. Uchida, M. Haranko
        \version            1.0
        \date                           24.03.2017
        Support :                       mail to : georg.auzinger@SPAMNOT.cern.ch
                                                  mykyta.haranko@SPAMNOT.cern.ch

 */
#if defined(__EMP__)
#include "DTCFWInterface.h"
#include <algorithm>
#include <chrono>
#include <time.h>
#include <uhal/uhal.hpp>
// #pragma GCC diagnostic ignored "-Wpedantic"

#include "D19cL1ReadoutInterface.h"
#include "D19cTriggerInterface.h"
#include "DTCFastCommandInterface.h"
#include "DTCLinkInterface.h"
#include "DTCOpticalInterface.h"
#include "DTCL1ReadoutInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
DTCFWInterface::DTCFWInterface(const char* puHalConfigFileName, const char* pBoardId) : BeBoardFWInterface(puHalConfigFileName, pBoardId)
{
    setFileHandler(nullptr);
    InitializeInterfaces();
}

DTCFWInterface::DTCFWInterface(const char* puHalConfigFileName, uint32_t pBoardId, FileHandler* pFileHandler) : BeBoardFWInterface(puHalConfigFileName, pBoardId), fFileHandler(pFileHandler)
{
    setFileHandler(fFileHandler);
    InitializeInterfaces();
}

DTCFWInterface::DTCFWInterface(const char* pId, const char* pUri, const char* pAddressTable) : BeBoardFWInterface(pId, pUri, pAddressTable), fFileHandler(nullptr)
{
    setFileHandler(fFileHandler);
    InitializeInterfaces();
}

DTCFWInterface::DTCFWInterface(const char* pId, const char* pUri, const char* pAddressTable, FileHandler* pFileHandler) : BeBoardFWInterface(pId, pUri, pAddressTable), fFileHandler(pFileHandler)
{
    setFileHandler(fFileHandler);
    InitializeInterfaces();
}

void DTCFWInterface::setFileHandler(FileHandler* pHandler)
{
    if(pHandler != nullptr)
    {
        fFileHandler = pHandler;
        fSaveToFile  = true;
    }
    else
        LOG(INFO) << "Error, can not set NULL FileHandler" << RESET;
}

uint32_t DTCFWInterface::getBoardInfo()
{
    // firmware info
    LOG(INFO) << GREEN << "============================" << RESET;
    LOG(INFO) << BOLDGREEN << "General Firmware Info" << RESET;
    return 0;
}

void DTCFWInterface::InitializeInterfaces()
{
    fController = new emp::Controller(*fBoard);
    LOG(INFO) << BOLDRED << "emp::Controller id = " << fController->id() << ".." << RESET;
    fController->identify();

    if(fTriggerInterface == nullptr) { LOG(INFO) << BOLDYELLOW << "Created DTCTriggerInterface ..." << RESET; }
    if(fFastCommandInterface == nullptr)
    {
        LOG(INFO) << BOLDYELLOW << "Created DTCFastCommandInterface ..." << RESET;
        fFastCommandInterface = new DTCFastCommandInterface(this->getUhalCnfg(), this->getId());
        //fFastCommandInterface->InitialiseTCDS();
    }
    if(fLinkInterface == nullptr)
    {
        LOG(INFO) << BOLDBLUE << "Optical readout . initializing link control interface" << RESET;
        fLinkInterface = new DTCLinkInterface(this->getUhalCnfg(), this->getId());
        static_cast<DTCLinkInterface*>(fLinkInterface)->LinkEMPcontroller(fController);        
    }
    if(fFEConfigurationInterface == nullptr)
    {
        Configuration cConfiguration;
        LOG(INFO) << BOLDBLUE << "Optical readout . initializing Optical interface for FE configuration" << RESET;
        fFEConfigurationInterface   = new DTCOpticalInterface(this->getUhalCnfg(), this->getId());
        cConfiguration.fRetryIC     = true;
        cConfiguration.fMaxRetryIC  = 10;
        cConfiguration.fRetryI2C    = true;
        cConfiguration.fMaxRetryI2C = 10;
        cConfiguration.fRetryFE     = true;
        cConfiguration.fMaxRetryFE  = 10;
        fFEConfigurationInterface->Configure(cConfiguration);
        static_cast<DTCOpticalInterface*>(fFEConfigurationInterface)->LinkEMPcontroller(fController);
    }
    if(fL1ReadoutInterface == nullptr)
    {
        LOG(INFO) << BOLDBLUE << "initializing L1 readout interface" << RESET;
        fL1ReadoutInterface = new DTCL1ReadoutInterface(this->getUhalCnfg(), this->getId());
        fL1ReadoutInterface->LinkFastCommandInterface(fFastCommandInterface);
    }
    // configure L1 readout interface
    // this depends on the event type
}

void DTCFWInterface::ConfigureInterfaces(const BeBoard* pBoard)
{
    if(!pBoard->isOptical())
    {
        LOG(ERROR) << BOLDRED << "Electrical readout.. with DTC... not possible " << RESET;
        throw Exception("Electrical readout.. with DTC... not possible...");
    }
}

void DTCFWInterface::ConfigureBoard(const BeBoard* pBoard)
{
    if(!pBoard->isOptical())
    {
        LOG(ERROR) << BOLDRED << "Electrical readout.. with DTC... not possible " << RESET;
        throw Exception("Electrical readout.. with DTC... not possible...");
    }

    ConfigureInterfaces(pBoard);

    bool cWithlpGBT = false;
    for(auto cOpticalGroup: *pBoard)
    {
        auto& clpGBT = cOpticalGroup->flpGBT;
        if(clpGBT != nullptr) {
            cWithlpGBT = true;
            LOG(INFO) << BOLDBLUE << "DTCFWInterface::ConfigureBoard optical group = " << +(cOpticalGroup->getOpticalGroupId()) <<  " lpGBT version = " << +(clpGBT->getVersion()) << RESET;

            unsigned q = cOpticalGroup->getOpticalGroupId() / 4;
            unsigned c = cOpticalGroup->getOpticalGroupId() % 4;
            fController->getDatapath().selectRegion(q);
            fController->getLpGbtMGTs().rxpolarity(c, clpGBT->getRxHSLPolarity());
        }
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid) { fIs2S = fIs2S || (cChip->getFrontEndType() == FrontEndType::CBC3); }
        }
    }

    // reset links
    bool cSkip = (pBoard->getLinkReset() == 0);
    if(!cSkip)
    {
        LOG(INFO) << BOLDMAGENTA << "Resetting lpGBT-FPGA core on BeBoard#" << +pBoard->getId() << RESET;
        static_cast<DTCLinkInterface*>(fLinkInterface)->LinkEMPcontroller(fController);
        fLinkInterface->GeneralLinkReset(pBoard);
    }

    // initialize TCDS
    static_cast<DTCFastCommandInterface*>(fFastCommandInterface)->InitialiseTCDS(pBoard);

    // adding an ReSync to align CBC L1A counters
    this->ChipReSync();
	//DTCL1ReadoutInterface* tmpL1readoutInterface = static_cast<DTCL1ReadoutInterface*>(fL1ReadoutInterface);

}

void DTCFWInterface::Start()
{
    if(fTriggerInterface != nullptr) fTriggerInterface->Start();
}

void DTCFWInterface::Stop()
{
    if(fTriggerInterface != nullptr) fTriggerInterface->Stop();
}

void DTCFWInterface::Pause()
{
    if(fTriggerInterface != nullptr) fTriggerInterface->Pause();
}

void DTCFWInterface::Resume()
{
    if(fTriggerInterface != nullptr) fTriggerInterface->Resume();
}

uint32_t DTCFWInterface::ReadData(BeBoard* pBoard, bool pBreakTrigger, std::vector<uint32_t>& pData, bool pWait)
{
    pData.clear();
    uint32_t cNEvents = 0;
    //LOG(INFO) << BOLDYELLOW << "DTCFWInterface::ReadData L1ReadoutInterface " << fL1ReadoutInterface << RESET;
    if(fL1ReadoutInterface == nullptr)
    {
        throw Exception("DTCFWInterface::ReadData - L1ReadoutInterface is a nullptr..");
    }

    if(fL1ReadoutInterface->PollReadoutData(pBoard, pWait))
    {
        pData    = fL1ReadoutInterface->getData();
        cNEvents = fL1ReadoutInterface->getNReadoutEvents();
    }
    else
    {
        // if triggers are still running throw an exception
        if(fTriggerInterface->GetTriggerState() == 1)
        {
            LOG(INFO) << BOLDRED << "Failed to poll readout-data from BeBoard" << RESET;
            throw Exception("Failed to poll readout-data from BeBoard");
        }
        return cNEvents;
    }

    if(fSaveToFile && pData.size() > 0) fFileHandler->setData(pData);
    // update local event counter
    fEventCounter += cNEvents;
    // need to return the number of events read
    return cNEvents;
}

void DTCFWInterface::ReadNEvents(BeBoard* pBoard, uint32_t pNEvents, std::vector<uint32_t>& pData, bool pWait)
{
    //Reset(pBoard);
    pData.clear();
    if(fL1ReadoutInterface == nullptr){
        LOG(INFO) << BOLDRED << "L1ReadoutInterface is a nullptr.." << RESET;
        throw Exception("DTCFWInterface::ReadNEvents - L1ReadoutInterface is a nullptr");
    }
    fL1ReadoutInterface->setNEvents(pNEvents);
    if(fL1ReadoutInterface->ReadEvents(pBoard)) pData = fL1ReadoutInterface->getData();
    else
    {
        LOG(INFO) << BOLDRED << "Failed to ReadNEvents" << RESET;
        throw Exception("DTCFWInterface::ReadNEvents Failed to ReadNEvents....");
    }
    //LOG(INFO) << BOLDYELLOW << "pData has: " << pData.size() << " words" << RESET;
    if(fSaveToFile) {
        if(fFileHandler != nullptr) fFileHandler->setData(pData);
        else throw Exception("DTCFWInterface::ReadNEvents FileHandler is a nullptr");
    }
}

void DTCFWInterface::ChipReSync()
{
    std::vector<FastCommand> cFastCmds;
    FastCommand              cFastCmd;
    cFastCmd.resync_en = 1;
    cFastCmd.bc0_en    = 0; //(cWithCIC && fIs2S) ? 1 : 0;
    cFastCmds.push_back(cFastCmd);
    fFastCommandInterface->SendGlobalCustomFastCommands(cFastCmds);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

void DTCFWInterface::ChipTestPulse() { fFastCommandInterface->SendGlobalCalPulse(); }

void DTCFWInterface::ChipTrigger() { fFastCommandInterface->SendGlobalL1A(); }

// ##########################################
// # Read/Write registers with CPB I2C functions #
// #########################################
uint8_t DTCFWInterface::SingleRegisterRead(Chip* pChip, ChipRegItem& pItem)
{
    if(pItem.fControlReg == 1) return 0;

    std::lock_guard<std::recursive_mutex> theGuard(fMutex); // Fabio:: I  do not like this lock
    uint8_t                               cValue = 0;
    // LOG (INFO) << BOLDYELLOW << "DTCFWInterface::SingleRegisterRead " << pItem.fAddress << RESET;
    if(fFEConfigurationInterface->SingleRead(pChip, pItem))
    {
        cValue = pItem.fValue;
        // update map
        auto cRegisterMap = pChip->getRegMap();
        auto cIterator    = find_if(cRegisterMap.begin(), cRegisterMap.end(), [&pItem](const ChipRegPair& obj) { return obj.second.fAddress == pItem.fAddress && obj.second.fPage == pItem.fPage; });
        if(cIterator != cRegisterMap.end())
        {
            auto cPreviousValue = cIterator->second.fValue;
            pChip->setReg(cIterator->first, pItem.fValue);
            pItem = pChip->getRegItem(cIterator->first);
            LOG(DEBUG) << BOLDGREEN << " DTCFWInterface::SingleRegisterRead successful read from 0x" << std::hex << +pItem.fValue << std::dec << " to " << cIterator->first
                       << "\t.. value in register is now 0x" << std::hex << +pChip->getReg(cIterator->first) << std::dec << " it was 0x" << std::hex << +cPreviousValue << std::dec << RESET;
        }
        else if(pItem.fStatusReg == 0x00)
            LOG(INFO) << BOLDRED << "DTCFWInterface::SingleRegisterRead Register 0x" << std::hex << +pItem.fAddress << " not in register map " << RESET;
    }
    else
        LOG(ERROR) << BOLDRED << "DTCFWInterface::SingleRegisterRead Register 0x" << std::hex << +pItem.fAddress << " FAILED " << RESET;
    return cValue;
}

bool DTCFWInterface::SingleRegisterWrite(Chip* pChip, ChipRegItem& pItem, bool pVerify)
{
    std::lock_guard<std::recursive_mutex> theGuard(fMutex); // Fabio:: I  do not like this lock
    if(pVerify && pItem.fControlReg == 0) return SingleRegisterWriteRead(pChip, pItem);

    auto cRegisterMap = pChip->getRegMap();
    auto cIterator    = find_if(cRegisterMap.begin(), cRegisterMap.end(), [&pItem](const ChipRegPair& obj) { return obj.second.fAddress == pItem.fAddress && obj.second.fPage == pItem.fPage; });
    if(cIterator != cRegisterMap.end())
    {
        if(fFEConfigurationInterface->SingleWrite(pChip, pItem))
        {
            // update map
            auto cPreviousValue = cIterator->second.fValue;
            pChip->setReg(cIterator->first, pItem.fValue);
            LOG(DEBUG) << BOLDGREEN << " DTCFWInterface::SingleRegisterWrite successful write of 0x" << std::hex << +pItem.fValue << std::dec << " to " << cIterator->first
                       << "\t.. value in register is now 0x" << std::hex << +pChip->getReg(cIterator->first) << std::dec << " it was 0x" << std::hex << +cPreviousValue << std::dec << RESET;
            pItem = pChip->getRegItem(cIterator->first);
        }
        else
            LOG(ERROR) << BOLDRED << "DTCFWInterface::SingleRegisterWrite FAILEd to write to Register " << cIterator->first << RESET;
        return true;
    }
    else
        LOG(INFO) << BOLDRED << "DTCFWInterface::SingleRegisterWrite Could not find register address in register map " << RESET;
    return false;
}

bool DTCFWInterface::SingleRegisterWriteRead(Chip* pChip, ChipRegItem& pItem)
{
    if(pItem.fControlReg == 1)
    {
        LOG(INFO) << BOLDYELLOW << "DTCFWInterface::SingleRegisterWriteRead Control register..." << RESET;
        return false;
    }
    std::lock_guard<std::recursive_mutex> theGuard(fMutex); // Fabio:: I  do not like this lock
    auto                                  cRegisterMap = pChip->getRegMap();
    auto cIterator = find_if(cRegisterMap.begin(), cRegisterMap.end(), [&pItem](const ChipRegPair& obj) { return obj.second.fAddress == pItem.fAddress && obj.second.fPage == pItem.fPage; });
    if(cIterator != cRegisterMap.end())
    {
        auto cPreviousValue = cIterator->second.fValue;
        LOG(DEBUG) << BOLDYELLOW << " DTCFWInterface::SingleRegisterWriteRead  write of 0x" << std::hex << +cIterator->second.fValue << std::dec << " to " << cIterator->first << RESET;
        if(fFEConfigurationInterface->SingleWriteRead(pChip, pItem))
        {
            // update map
            pChip->setReg(cIterator->first, pItem.fValue);
            LOG(DEBUG) << BOLDGREEN << " DTCFWInterface::SingleRegisterWriteRead successful write of 0x" << std::hex << +pItem.fValue << std::dec << " to " << cIterator->first
                       << "\t.. value in register is now 0x" << std::hex << +pChip->getReg(cIterator->first) << std::dec << " it was 0x" << std::hex << +cPreviousValue << std::dec << RESET;
            pItem = pChip->getRegItem(cIterator->first);
            return true;
        }
        else
            LOG(ERROR) << BOLDRED << "DTCFWInterface::SingleRegisterWriteRead FAILED to write to Register " << cIterator->first << RESET;
    }
    else
    {
        LOG(INFO) << BOLDRED << "DTCFWInterface::SingleRegisterWriteRead Could not find register address " << std::hex << +pItem.fAddress << std::dec << " in register map " << RESET;
    }
    return false;
}

std::vector<uint8_t> DTCFWInterface::MultiRegisterRead(Chip* pChip, std::vector<ChipRegItem>& pItems)
{
    std::vector<uint8_t> cValues(0);
    if(pItems.size() == 0) return cValues;

    if(fFEConfigurationInterface->MultiRead(pChip, pItems))
    {
        // update map
        auto cRegisterMap = pChip->getRegMap();
        for(auto cItem: pItems)
        {
            auto cIterator = find_if(cRegisterMap.begin(), cRegisterMap.end(), [&cItem](const ChipRegPair& obj) { return obj.second.fAddress == cItem.fAddress && obj.second.fPage == cItem.fPage; });
            if(cIterator == cRegisterMap.end() && cItem.fStatusReg == 0x0)
                LOG(INFO) << BOLDRED << "Could not find " << cIterator->first << " addresss 0x" << std::hex << cItem.fAddress << std::dec << RESET;
            else
            {
                // LOG (INFO) << BOLDGREEN << "Found " << cIterator->first << " addresss 0x" << std::hex << cItem.fAddress << std::dec << RESET;
                if(cItem.fStatusReg == 0x00)
                {
                    pChip->setReg(cIterator->first, cItem.fValue);
                    cValues.push_back(pChip->getReg(cIterator->first));
                    LOG(DEBUG) << BOLDYELLOW << "DTCFWInterface::MultiRegisterRead Register " << cIterator->first << " 0x" << std::hex << +cItem.fAddress << std::dec << " set to 0x" << std::hex
                               << +cValues.at(cValues.size() - 1) << std::dec << RESET;
                }
                else
                {
                    cValues.push_back(cItem.fValue);
                }
            } // update map
        }
    }
    else
        LOG(ERROR) << BOLDRED << "DTCFWInterface::MultiRegisterRead Register FAILED " << RESET;
    return cValues;
}

bool DTCFWInterface::MultiRegisterWrite(Chip* pChip, std::vector<ChipRegItem>& pItems, bool pVerify)
{
    if(pItems.size() == 0) return true;

    std::lock_guard<std::recursive_mutex> theGuard(fMutex); // Fabio:: I  do not like this lock
    if(pVerify) return MultiRegisterWriteRead(pChip, pItems);

    if(fFEConfigurationInterface->MultiWrite(pChip, pItems))
    {
        LOG(DEBUG) << BOLDGREEN << "DTCFWInterface::MultiRegisterWrite successful write to " << pItems.size() << " registers" << RESET;
        // update map
        auto cRegisterMap = pChip->getRegMap();
        for(auto& cItem: pItems)
        {
            auto cIterator = find_if(cRegisterMap.begin(), cRegisterMap.end(), [&cItem](const ChipRegPair& obj) { return obj.second.fAddress == cItem.fAddress && obj.second.fPage == cItem.fPage; });
            if(cIterator != cRegisterMap.end()) // if item is in the map
            {
                auto cPreviousValue = cIterator->second.fValue;
                pChip->setReg(cIterator->first, cItem.fValue);
                LOG(DEBUG) << BOLDGREEN << " DTCFWInterface::MultiRegisterWrite successful write of 0x" << std::hex << +cItem.fValue << std::dec << " to " << cIterator->first
                           << "\t.. value in register is now 0x" << std::hex << +pChip->getReg(cIterator->first) << std::dec << " it was 0x" << std::hex << +cPreviousValue << std::dec << RESET;
                cItem = pChip->getRegItem(cIterator->first);
            }
            else
                LOG(INFO) << BOLDRED << "DTCFWInterface::MultiRegisterWrite Register 0x" << std::hex << +cItem.fAddress << std::dec << " not in register map " << RESET;
        }
        return true;
    }
    else
        LOG(ERROR) << BOLDRED << "DTCFWInterface::MultiRegisterWrite FAILED" << RESET;
    return false;
}

bool DTCFWInterface::MultiRegisterWriteRead(Chip* pChip, std::vector<ChipRegItem>& pItems)
{
    if(pItems.size() == 0) return true;

    std::lock_guard<std::recursive_mutex> theGuard(fMutex); // Fabio:: I  do not like this lock
    if(fFEConfigurationInterface->MultiWriteRead(pChip, pItems))
    {
        auto cRegisterMap = pChip->getRegMap();
        for(auto cItem: pItems)
        {
            auto cIterator = find_if(cRegisterMap.begin(), cRegisterMap.end(), [&cItem](const ChipRegPair& obj) { return obj.second.fAddress == cItem.fAddress && obj.second.fPage == cItem.fPage; });
            if(cIterator != cRegisterMap.end())
            {
                auto cPreviousValue = cIterator->second.fValue;
                pChip->setReg(cIterator->first, cItem.fValue);
                LOG(DEBUG) << BOLDGREEN << " DTCFWInterface::MultiRegisterWriteRead successful write of 0x" << std::hex << +cItem.fValue << std::dec << " to " << cIterator->first
                           << "\t.. value in register is now 0x" << std::hex << +pChip->getReg(cIterator->first) << std::dec << " it was 0x" << std::hex << +cPreviousValue << std::dec << RESET;
            }
            else
                LOG(INFO) << BOLDRED << "DTCFWInterface::SingleRegisterWriteRead Could not find register address in register map " << RESET;
        }
        return true;
    } // update map
    else
        LOG(ERROR) << BOLDRED << "DTCFWInterface::MultiRegisterWriteRead FAILED to write to " << pItems.size() << " registers." << RESET;
    return false;
}


void DTCFWInterface::ResetL1DataExtractor(unsigned pChanId) 
{
    bool status = WriteReg("payload.fe.chan_sel", pChanId);
    if(status) LOG(ERROR) << BOLDRED << "ERROR! Cannot write on payload.fe.chan_sel" << RESET;

    status = WriteReg("payload.fe_chan.l1_daq.ctrl0.reset", 1);
    if(status) LOG(ERROR) << BOLDRED << "ERROR! Cannot write on payload.fe_chan.l1_daq.ctrl0.reset" << RESET;

    status = WriteReg("payload.fe_chan.l1_daq.ctrl1.reset", 1);
    if(status) LOG(ERROR) << BOLDRED << "ERROR! Cannot write on payload.fe_chan.l1_daq.ctrl1.reset" << RESET;

    status = WriteReg("payload.fe_chan.l1_daq.ctrl0.reset", 0);
    if(status) LOG(ERROR) << BOLDRED << "ERROR! Cannot write on payload.fe_chan.l1_daq.ctrl0.reset" << RESET;

    status = WriteReg("payload.fe_chan.l1_daq.ctrl1.reset", 0);
    if(status) LOG(ERROR) << BOLDRED << "ERROR! Cannot write on payload.fe_chan.l1_daq.ctrl1.reset" << RESET;

    uint32_t status0 = ReadReg("payload.fe_chan.l1_daq.status0");
    LOG(INFO) << YELLOW << "DAQ status: {:08x}" << status0 << RESET;
    uint32_t status1 = ReadReg("payload.fe_chan.l1_daq.status1");
    LOG(INFO) << YELLOW << "DAQ status: {:08x}" << status1 << RESET;
}

void DTCFWInterface::Reset(Ph2_HwDescription::BeBoard* pBoard)//, unsigned pChanId) 
{
    static_cast<DTCFastCommandInterface*>(fFastCommandInterface)->InitialiseTCDS(pBoard);
    static_cast<DTCFastCommandInterface*>(fFastCommandInterface)->SendLocalReSync(0);
    for(int i=0; i<4; i++)
    {
        WriteReg("payload.fe.chan_sel", 3);
        WriteReg("payload.fe_chan.l1_daq.ctrl0.reset", 1);
        WriteReg("payload.fe_chan.l1_daq.ctrl1.reset", 1);
        WriteReg("payload.fe_chan.l1_daq.ctrl0.reset", 0);
        WriteReg("payload.fe_chan.l1_daq.ctrl1.reset", 0);
        WriteReg("payload.be_daq.daqpath_ext_ctrl.C0", 16); // reset be daqpath
        WriteReg("payload.be_daq.daqpath_ext_ctrl.C0", 1); // reset be daqpath
    }
    static_cast<DTCL1ReadoutInterface*>(fL1ReadoutInterface)->DumpFifo();
}

} // namespace Ph2_HwInterface
#endif
