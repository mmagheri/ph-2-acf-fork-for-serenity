#include "D19cDPInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
D19cDPInterface::D19cDPInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : RegManager(pId, pUri, pAddressTable) {}
D19cDPInterface::D19cDPInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : RegManager(puHalConfigFileName, pBoardId)
{
    LOG(INFO) << BOLDYELLOW << "D19cDPInterface::D19cDPInterface Constructor" << RESET;
}
D19cDPInterface::~D19cDPInterface() {}

bool D19cDPInterface::IsRunning()
{
    std::string cRegName = (fType == 0) ? "fc7_daq_stat.physical_interface_block.fe_data_player.stat_feh_data_player" : "fc7_daq_stat.physical_interface_block.fe_data_player.stat_roh_data_player";
    fEmulatorRunning     = (this->ReadReg(cRegName) == 1);
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    if(fEmulatorRunning) { LOG(INFO) << BOLDGREEN << " Data Player [RUNNING]" << RESET; }
    else
    {
        LOG(INFO) << BOLDRED << " Data Player [STOPPED]" << RESET;
    }
    return fEmulatorRunning;
}
void D19cDPInterface::StartSyncPlaying()
{
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    bool cIsRunning = this->IsRunning();
    if(cIsRunning)
    {
        LOG(INFO) << BOLDGREEN << " Data Player [RUNNING] .. going to stop" << RESET;
        this->WriteReg("fc7_daq_ctrl.fast_command_block.control.stop_generic", 0x1);
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    }
    this->WriteReg("fc7_daq_ctrl.fast_command_block.control.start_generic", 0x1);
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
}

bool D19cDPInterface::Start()
{
    this->WriteReg("fc7_daq_ctrl.physical_interface_block.fe_data_player.start_data_player", 0x01);
    LOG(INFO) << BOLDBLUE << "FE data player for PS ROH started." << RESET;
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    return this->IsRunning();
}
bool D19cDPInterface::Stop()
{
    this->WriteReg("fc7_daq_ctrl.physical_interface_block.fe_data_player.stop_data_player", 0x01);
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    LOG(INFO) << BOLDBLUE << "FE data player for PS ROH stopped." << RESET;
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    return !(this->IsRunning());
}
void D19cDPInterface::ConfigurePatternAllLines(uint8_t pPattern)
{
    LOG(INFO) << BOLDYELLOW << "D19cDPInterface::ConfigurePatternAllLines Pattern to configure on all lines is " << std::bitset<8>(pPattern) << RESET;
    std::vector<uint8_t> cPatterns;
    cPatterns.clear();
    cPatterns.push_back(pPattern);
    for(uint8_t cLineId = 0; cLineId < 6; cLineId++)
    {
        fPatternMap[cLineId] = cPatterns[0];
        ConfigureLineBRAM(cLineId, cPatterns);
    }
}
void D19cDPInterface::ConfigurePatternAllLines(std::map<uint8_t, uint8_t> pPattern)
{
    for(uint8_t cLineId = 0; cLineId < 6; cLineId++)
    {
        std::vector<uint8_t> cPatterns;
        cPatterns.clear();
        cPatterns.push_back(pPattern[cLineId]);
        fPatternMap[cLineId] = cPatterns[0];
        ConfigureLineBRAM(cLineId, cPatterns);
    }
}
void D19cDPInterface::ConfigureLineBRAM(uint8_t pLine, std::vector<uint8_t> pPatterns)
{
    if(pPatterns.size() == 1)
        LOG(INFO) << BOLDYELLOW << "Configure CIC data player BRAM for line " << +pLine << " to play a single pattern " << std::bitset<8>(pPatterns[0]) << " continuously..." << RESET;
    else
        LOG(INFO) << BOLDYELLOW << "Configure CIC data player BRAM for line " << +pLine << " to play patterns from BRAM." << RESET;

    // write the same pattern to all BRAMs
    // first - configure pattern
    std::stringstream cRegNamePtrn;
    uint8_t           cBaseIndex = (pLine / 2) * 2;
    cRegNamePtrn << "fc7_daq_cnfg.physical_interface_block.mpa_to_cic_data_player_line";
    cRegNamePtrn << "_" << +cBaseIndex << "_" << +(cBaseIndex + 1);
    cRegNamePtrn << ".line" << +(pLine) << "_pattern_7_to_0";
    // now configure how many clock cycles to play this pattern for
    // i.e. write patterns to the BRAM
    // but always make sure address 0 has 0x00 in it
    for(size_t cIndx = 0; cIndx <= pPatterns.size(); cIndx++)
    {
        if(cIndx == 0)
            this->WriteReg(cRegNamePtrn.str(), 0x00); // always use address 0 for 0x00 pattern
        else
            this->WriteReg(cRegNamePtrn.str(), pPatterns[cIndx - 1]);
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        if(cIndx > 0) LOG(DEBUG) << BOLDYELLOW << "Playing... " << std::bitset<8>(pPatterns[cIndx - 1]) << " for " << +cIndx << "(th) clock cycles." << RESET;
        // select address
        std::stringstream cRegNameAddr;
        cRegNameAddr << "fc7_daq_cnfg.physical_interface_block.mpa_to_cic_data_player_address_BRAM_line";
        cRegNameAddr << "_" << +cBaseIndex << "_" << +(cBaseIndex + 1);
        cRegNameAddr << ".line" << +pLine << "_address";
        this->WriteReg(cRegNameAddr.str(), cIndx);
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        // write enable
        this->WriteReg("fc7_daq_ctrl.physical_interface_block.mpa_to_cic_data_player_BRAM.write_enable", (1 << pLine));
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        this->WriteReg("fc7_daq_ctrl.physical_interface_block.mpa_to_cic_data_player_BRAM.write_enable", 0x00);
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    }
    // also write this number of patterns to fcmd generic
    // player
    this->WriteReg("fc7_daq_cnfg.fast_command_block.generic_fcmd.number_of_cmds_to_play", pPatterns.size() - 1);
    this->WriteReg("fc7_daq_cnfg.fast_command_block.generic_fcmd.number_of_repetitions", 0x1);
}

size_t D19cDPInterface::CheckNPatterns()
{
    auto cInput = this->ReadReg("fc7_daq_stat.physical_interface_block.mpa_to_cic_cntr");
    LOG(INFO) << BOLDYELLOW << " Data Player sent : " << +cInput << " patterns." << RESET;
    return cInput;
}

} // namespace Ph2_HwInterface