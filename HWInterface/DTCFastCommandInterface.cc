#if defined(__EMP__)

#include "DTCFastCommandInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
DTCFastCommandInterface::DTCFastCommandInterface(const std::string& puHalConfigFileName, const std::string& pBoardId) : FastCommandInterface(puHalConfigFileName, pBoardId)
{
    fChanIds.clear();
}
DTCFastCommandInterface::DTCFastCommandInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : FastCommandInterface(pId, pUri, pAddressTable)
{
    fChanIds.clear();
}
DTCFastCommandInterface::DTCFastCommandInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : FastCommandInterface(puHalConfigFileName, pBoardId) { fChanIds.clear(); }
DTCFastCommandInterface::~DTCFastCommandInterface() {}

void DTCFastCommandInterface::InitialiseTCDS(const BeBoard* pBoard)
{
    fChanIds.clear();
    for(int i=0;i<4;i++){fChanIds.push_back(i); InitialiseLocalTCDS(i);}
}

void DTCFastCommandInterface::InitialiseLocalTCDS(unsigned pChanId)
{
    LOG(INFO) << BOLDYELLOW << "Initializing local TCDS on Channel#" << pChanId << RESET;
    WriteReg("payload.fe.chan_sel", pChanId);
    // initialise with config file settings eventually
    LocalResetPatternGen(pChanId);
    LocalSourceSelect(pChanId, 1);      // 0=global, 1=local
    LocalDelayFastCommand(pChanId, 0);  // 0->7 bx delay on output
    LocalRepeatFastCommand(pChanId, 0); // repeat 1->1023 times, 0=infinite repeat (if set by pattern gen)
    LocalInternalBC0sEnable(pChanId);
}

void DTCFastCommandInterface::SendGlobalReSync(uint8_t pDuration)
{
    SendLocalReSync(fChanIds[0]);
}
void DTCFastCommandInterface::SendGlobalCalPulse(uint8_t pDuration)
{
    for(auto pChanId: fChanIds) SendLocalCalPulse(pChanId);
}
void DTCFastCommandInterface::SendGlobalL1A(uint8_t pDuration)
{   
    //SendLocalL1A send a trigger to EVERY channel always ... so just choose a channel you need
    SendLocalL1A(fChanIds[0]);
}

void DTCFastCommandInterface::SendGlobalRepetitiveL1A(unsigned pNtriggers)
{   
    SendLocalRepetitiveL1As(fChanIds[0], pNtriggers);
}

void DTCFastCommandInterface::SendGlobalCounterReset(uint8_t pDuration)
{
    for(auto pChanId: fChanIds) SendLocalCounterReset(pChanId);
}
void DTCFastCommandInterface::SendGlobalCounterResetResync(uint8_t pDuration)
{
    FastCommand cFastCommand;
    cFastCommand.bc0_en    = 1;
    cFastCommand.resync_en = 1;
    std::vector<FastCommand> cFastCommands{cFastCommand};
    SendGlobalCustomFastCommands(cFastCommands);
}

void DTCFastCommandInterface::SendGlobalCounterResetL1A(uint8_t pDuration)
{
    FastCommand cFastCommand;
    cFastCommand.bc0_en = 1;
    cFastCommand.l1a_en = 1;
    std::vector<FastCommand> cFastCommands{cFastCommand};
    SendGlobalCustomFastCommands(cFastCommands);
}
void DTCFastCommandInterface::SendGlobalCounterResetCalPulse(uint8_t pDuration)
{
    FastCommand cFastCommand;
    cFastCommand.bc0_en       = 1;
    cFastCommand.cal_pulse_en = 1;
    std::vector<FastCommand> cFastCommands{cFastCommand};
    SendGlobalCustomFastCommands(cFastCommands);
}
void DTCFastCommandInterface::SendGlobalCustomFastCommands(std::vector<FastCommand>& pFastCmd)
{
    for(int i=0;i<4;i++) {SendLocalCustomFastCommands(i, pFastCmd);}

}
void DTCFastCommandInterface::SendLocalReSync(unsigned pChanId)
{
    FastCommand cmd;
    cmd.resync_en = true;
    cmd.bc0_en    = true;
    std::vector<FastCommand> fastCmd{cmd};

    SendLocalCustomFastCommands(pChanId, fastCmd, false);
}

void DTCFastCommandInterface::SendLocalCalPulse(unsigned pChanId)
{
    FastCommand cmd;
    cmd.cal_pulse_en = true;
    std::vector<FastCommand> fastCmd{cmd};

    SendLocalCustomFastCommands(pChanId, fastCmd, false);
}

void DTCFastCommandInterface::SendLocalL1A(unsigned pChanId)
{
    FastCommand cmd;
    cmd.l1a_en = true;
    std::vector<FastCommand> fastCmd{cmd};

    SendLocalCustomFastCommands(pChanId, fastCmd, false);
}

void DTCFastCommandInterface::SendLocalCounterReset(unsigned pChanId)
{
    FastCommand cmd;
    cmd.bc0_en = true;
    std::vector<FastCommand> fastCmd{cmd};

    SendLocalCustomFastCommands(pChanId, fastCmd, false);
}

void DTCFastCommandInterface::SendLocalRepetitiveL1As(unsigned pChanId, unsigned pNtriggers)
{
    //LOG(INFO) << "Sending trigger to chan: " << pChanId << RESET;
    FastCommand cmd;
    cmd.l1a_en  = true;
    cmd.bx_wait = 400;
    //cmd.bx_wait = 1000;
    std::vector<FastCommand> fastCmd{cmd};

    LocalRepeatFastCommand(pChanId, pNtriggers);
    SendLocalCustomFastCommands(pChanId, fastCmd, true);
}

void DTCFastCommandInterface::SendLocalCustomFastCommands(unsigned pChanId, std::vector<FastCommand>& pFastCmd, bool repeat)
{
    WriteReg("payload.fe.chan_sel", pChanId);

    LocalResetPatternGen(pChanId);
    std::vector<unsigned> patterns;

    for(auto fast_cmd: pFastCmd)
    {
        if(fast_cmd.bx_wait > 1023)
        {
            fast_cmd.bx_wait = 1023;
            LOG(WARNING) << BOLDRED << "Cannot wait for more than 1023 BX after sending a fast command : Setting to 1023 BX" << RESET;
        }
        // load fast command pattern - by default the generator is instructed to move to the next command word
        patterns.push_back((1 << 14) | (fast_cmd.bx_wait << 4) | (fast_cmd.resync_en << 3) | (fast_cmd.l1a_en << 2) | (fast_cmd.cal_pulse_en << 1) | fast_cmd.bc0_en);
    }

    // modify last command word to instruct generator to stop, or repeat sequence from the first word
    if(!patterns.empty())
    {
        unsigned last_cmd = patterns.back() & 0x3fff;
        if(!repeat) last_cmd = last_cmd | (2 << 14);
        patterns.back() = last_cmd;
    }

    LocalLoadPatternGen(pChanId, patterns);
    LocalStartPatternGen(pChanId);
    PrintLocalCounters(pChanId);
}

void DTCFastCommandInterface::PrintLocalCounters(unsigned pChanId)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    uint32_t n_resync_en    = ReadReg("payload.fe_chan.fast_cmd.status.fast_resets");
    uint32_t n_l1a_en       = ReadReg("payload.fe_chan.fast_cmd.status.l1a_trigs");
    uint32_t n_cal_pulse_en = ReadReg("payload.fe_chan.fast_cmd.status.cal_pulses");
    uint32_t n_bc0_en       = ReadReg("payload.fe_chan.fast_cmd.status.counter_resets");

    //LOG(INFO) << BOLDBLUE << "Fast commands sent from local TCDS on channel " << pChanId << " :: FastReset " << n_resync_en << " :: L1A " << n_l1a_en << " :: CalPulse " << n_cal_pulse_en << " :: CntReset " << n_bc0_en << RESET;
}

void DTCFastCommandInterface::LocalSourceSelect(unsigned pChanId, unsigned pSource)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    if(pSource > 1) { LOG(ERROR) << BOLDRED << "Invalid Fast Command local source select option" << RESET; }
    else
    {
        WriteReg("payload.fe_chan.fast_cmd.ctrl.source", pSource);
    }
}

void DTCFastCommandInterface::LocalDelayFastCommand(unsigned pChanId, unsigned pDelay)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    if(pDelay > 7)
    {
        pDelay = 7;
        LOG(WARNING) << BOLDRED << "Cannot delay local fast commands by more than 7 BX : Setting to 7 BX" << RESET;
    }
    WriteReg("payload.fe_chan.fast_cmd.ctrl.delay", pDelay);
}

void DTCFastCommandInterface::LocalRepeatFastCommand(unsigned pChanId, unsigned pRepeat)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    if(pRepeat > 1023)
    {
        pRepeat = 1023;
        LOG(WARNING) << BOLDRED << "Cannot repeat fast command pattern more than 1023 times : Setting to 1023" << RESET;
    }
    WriteReg("payload.fe_chan.fast_cmd.ctrl.repeat", pRepeat);
}

void DTCFastCommandInterface::LocalInternalBC0sEnable(unsigned pChanId)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    WriteReg("payload.fe_chan.fast_cmd.ctrl.bc0", 0x0);
}

void DTCFastCommandInterface::LocalInternalBC0sDisable(unsigned pChanId)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    WriteReg("payload.fe_chan.fast_cmd.ctrl.bc0", 0x1);
}

void DTCFastCommandInterface::LocalStartPatternGen(unsigned pChanId)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    WriteReg("payload.fe_chan.fast_cmd.ctrl.run", 0x1);
}

void DTCFastCommandInterface::LocalResetPatternGen(unsigned pChanId)
{
    WriteReg("payload.fe.chan_sel", pChanId);
    WriteReg("payload.fe_chan.fast_cmd.ctrl.run", 0x0);
}

void DTCFastCommandInterface::LocalLoadPatternGen(unsigned pChanId, std::vector<unsigned>& pPatterns)
{
    WriteReg("payload.fe.chan_sel", pChanId);

    int i = 0;
    for(auto pattern: pPatterns)
    {
        i++;
        std::string reg_name = "payload.fe_chan.fast_cmd.pgen_buf_" + std::to_string(i);
        if(i <= 6) WriteReg(reg_name, pattern);
    }
}

} // namespace Ph2_HwInterface
#endif
