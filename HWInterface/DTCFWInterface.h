/*!

\file                           DTCFWInterface.h
\brief                          DTCFWInterface init/config of the FC7 and its Chip's
\author                         G. Auzinger, K. Uchida, M. Haranko
        \version            1.0
        \date                           24.03.2017
        Support :                       mail to : georg.auzinger@SPAMNOT.cern.ch
                                                  mykyta.haranko@SPAMNOT.cern.ch
                                                  younes.otarid@SPAMNOT.cern.ch

*/

#ifndef _DTCFWINTERFACE_H__
#define _DTCFWINTERFACE_H__

#include "../Utils/easylogging++.h"
#include "BeBoardFWInterface.h"

#if defined(__EMP__)

#include "emp/Controller.hpp"
#include "emp/LpGbtMGTRegionNode.hpp"
#include "emp/DatapathNode.hpp"
#include <limits.h>
#include <map>
#include <mutex>
#include <numeric>
#include <stdint.h>
#include <string>
#include <uhal/uhal.hpp>
#include <vector>

/*!
 * \namespace Ph2_HwInterface
 * \brief Namespace regrouping all the interfaces to the hardware
 */
namespace Ph2_HwInterface
{
class L1ReadoutInterface;
class FEConfigurationInterface;
class TriggerInterface;
class FastCommandInterface;
class LinkInterface;

/*
 * \brief init/config of the Fc7 and its Chip's
 */
class DTCFWInterface : public BeBoardFWInterface
{
  private:
    // std::recursive_mutex                     fMutex;
    FEConfigurationInterface* fFEConfigurationInterface{nullptr};
    L1ReadoutInterface*       fL1ReadoutInterface{nullptr};
    TriggerInterface*         fTriggerInterface{nullptr};
    FastCommandInterface*     fFastCommandInterface{nullptr};
    LinkInterface*            fLinkInterface{nullptr};

    FileHandler* fFileHandler;

    // number of chips and hybrids defined in firmware (compiled for)
    std::map<uint8_t, uint8_t> fRxPolarity;
    std::map<uint8_t, uint8_t> fTxPolarity;
    // event counter
    uint32_t fEventCounter = 0;

  public:
    /*!
     *
     * \brief Constructor of the Cbc3Fc7FWInterface class
     * \param puHalConfigFileName : path of the uHal Config File
     * \param pBoardId
     */
    DTCFWInterface(const char* puHalConfigFileName, const char* pBoardId);
    DTCFWInterface(const char* puHalConfigFileName, uint32_t pBoardId, FileHandler* pFileHandler);
    /*!
     *
     * \brief Constructor of the Cbc3Fc7FWInterface class
     * \param pId : ID string
     * \param pUri: URI string
     * \param pAddressTable: address tabel string
     */

    DTCFWInterface(const char* pId, const char* pUri, const char* pAddressTable);
    DTCFWInterface(const char* pId, const char* pUri, const char* pAddressTable, FileHandler* pFileHandler);
    void setFileHandler(FileHandler* pHandler);

    void                      printReadoutInterface() { LOG(INFO) << BOLDYELLOW << "DTCFWInterface::ReadNEvent L1ReadoutInterface " << fL1ReadoutInterface << RESET; }
    TriggerInterface*         getTriggerInterface() { return fTriggerInterface; }
    L1ReadoutInterface*       getL1ReadoutInterface() { return fL1ReadoutInterface; }
    FastCommandInterface*     getFastCommandInterface() { return fFastCommandInterface; }
    FEConfigurationInterface* getFEConfigurationInterface() { return fFEConfigurationInterface; }
    LinkInterface*            getLinkInterface() { return fLinkInterface; }
    emp::Controller*          getEMPController() { return fController; }

    //
    void ConfigureInterfaces(const Ph2_HwDescription::BeBoard* pBoard);
    void InitializeInterfaces();
    
    /*!
     *
     * \brief Destructor of the Cbc3Fc7FWInterface class
     */

    ~DTCFWInterface()
    {
        if(fFileHandler) delete fFileHandler;
    }

    ///////////////////////////////////////////////////////
    //      DTC Methods                                //
    /////////////////////////////////////////////////////

    uint32_t getBoardInfo();

    BoardType getBoardType() const { return BoardType::DTC; }
    /*!
     * \brief Configure the board with its Config File
     * \param pBoard
     */
    void ConfigureBoard(const Ph2_HwDescription::BeBoard* pBoard) override;
    /*!
     * \brief Detect the right Hybrid Id to write the right registers (not working with the latest Firmware)
     */

    void     ResetEventCounter() { fEventCounter = 0; }
    uint32_t GetEventCounter() { return fEventCounter; }
    /*!
     * \brief Status of triggers
     */
    /*!
     * \brief Start a DAQ
     */
    void Start() override;
    /*!
     * \brief Stop a DAQ
     */
    void Stop() override;
    /*!
     * \brief Pause a DAQ
     */
    void Pause() override;
    /*!
     * \brief Unpause a DAQ
     */
    void Resume() override;

    /*!
     * \brief Read data from DAQ
     * \param pBreakTrigger : if true, enable the break trigger
     * \return fNpackets: the number of packets read
     */
    uint32_t ReadData(Ph2_HwDescription::BeBoard* pBoard, bool pBreakTrigger, std::vector<uint32_t>& pData, bool pWait = true) override;

    /*!
     * \brief Read data for pNEvents
     * \param pBoard : the pointer to the BeBoard
     * \param pNEvents :  the 1 indexed number of Events to read - this will set the packet size to this value -1
     */

    void ReadNEvents(Ph2_HwDescription::BeBoard* pBoard, uint32_t pNEvents, std::vector<uint32_t>& pData, bool pWait = true);
    void SetOptoLinkVersion(uint8_t version) override {}

    void Reset(Ph2_HwDescription::BeBoard* pBoard);//, unsigned pChanId);

  private:
    emp::Controller* fController;
    uint32_t         fReadoutAttempts = 0;
    uint16_t         fWait_us         = 10000; // 10 ms
    uint8_t          daqPathHeaderEnable = 1;
    std::string      daqPathChMask = "fffffcff";

    // 2S or PS readout
    bool fIs2S = true;

  public:
    void ChipReSync() override;

    void ChipReset() override {}

    void ChipTrigger() override;

    void ChipTestPulse() override;

    void setRxPolarity(uint8_t pLinkId, uint8_t pPolarity = 1) { fRxPolarity.insert({pLinkId, pPolarity}); };
    void setTxPolarity(uint8_t pLinkId, uint8_t pPolarity = 1) { fTxPolarity.insert({pLinkId, pPolarity}); };

    // Generic FE configuration functions
    // single register functions
    // Register write
    bool SingleRegisterWrite(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem, bool pVerify = true) override;
    bool MultiRegisterWrite(Ph2_HwDescription::Chip* pChip, std::vector<Ph2_HwDescription::ChipRegItem>& pItem, bool pVerify = true) override;
    // Register write + read-back
    bool SingleRegisterWriteRead(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem) override;
    bool MultiRegisterWriteRead(Ph2_HwDescription::Chip* pChip, std::vector<Ph2_HwDescription::ChipRegItem>& pItem) override;
    // Register read
    uint8_t              SingleRegisterRead(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem) override;
    std::vector<uint8_t> MultiRegisterRead(Ph2_HwDescription::Chip* pChip, std::vector<Ph2_HwDescription::ChipRegItem>& pItem) override;

    // ############################
    // # Read/Write Optical Group #
    // ############################
    // ##############################
    // # Pseudo Random Bit Sequence #
    // ##############################
    double RunBERtest(bool given_time, double frames_or_time, uint16_t hybrid_id, uint16_t chip_id, uint8_t frontendSpeed) override { return 0; };
    // Functions for standard uDTC
    void     selectLink(const uint8_t pLinkId = 0, uint32_t cWait_ms = 100) override{};
    void     StatusOptoLink(uint32_t& txStatus, uint32_t& rxStatus, uint32_t& mgtStatus) override {}
    void     ResetOptoLink() override{};
    bool     WriteOptoLinkRegister(const Ph2_HwDescription::Chip* pChip, const uint32_t pAddress, const uint32_t pData, const bool pVerifLoop = false) override { return true; };
    uint32_t ReadOptoLinkRegister(const Ph2_HwDescription::Chip* pChip, const uint32_t pAddress) override { return 0; };
    void ResetL1DataExtractor(unsigned pChanId);

    
};
} // namespace Ph2_HwInterface

#endif

#endif
