#if defined(__EMP__)

#include "DTCLinkInterface.h"

#include "emp/DatapathNode.hpp"
#include "emp/GbtMGTRegionNode.hpp"
#include "emp/LpGbtMGTRegionNode.hpp"
#include "emp/SCCNode.hpp"
using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
// ##########################################
// # Constructors #
// #########################################

DTCLinkInterface::DTCLinkInterface(const std::string& puHalConfigFileName, const std::string& pBoardId) : LinkInterface(puHalConfigFileName, pBoardId)
{
    LOG(INFO) << BOLDYELLOW << "DTCLinkInterface::DTCLinkInterface Constructor" << RESET;
    fConfiguration.fResetWait_ms = 2000;
    fConfiguration.fReTry        = 1;
    fConfiguration.fMaxAttempts  = 10;
    //TODO: Link emp controller in the constructor?
}
DTCLinkInterface::DTCLinkInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : LinkInterface(pId, pUri, pAddressTable)
{
    fConfiguration.fResetWait_ms = 2000;
    fConfiguration.fReTry        = 1;
    fConfiguration.fMaxAttempts  = 10;
}

DTCLinkInterface::DTCLinkInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : LinkInterface(puHalConfigFileName, pBoardId)
{
    fConfiguration.fResetWait_ms = 2000;
    fConfiguration.fReTry        = 1;
    fConfiguration.fMaxAttempts  = 10;
}

DTCLinkInterface::~DTCLinkInterface() {}

void DTCLinkInterface::ResetLinks() {}
bool DTCLinkInterface::GetLinkStatus(uint8_t pLinkId) { return true; }
void DTCLinkInterface::GeneralLinkReset(const BeBoard* pBoard)
{
    bool   cAllLocked   = false;
    size_t cMaxAttempts = fConfiguration.fReTry ? fConfiguration.fMaxAttempts : 1;
    size_t cAttempts    = 0;
    do {
        cAllLocked = true;
        LOG(INFO) << BOLDMAGENTA << "DTCLinkInterface::GeneralLinkReset Resetting lpGBT-FPGA core on BeBoard#" << +pBoard->getId() << " [Attempt#" << cAttempts << "]" << RESET;
        LOG(INFO) << "Identifying lpGBT-FPGA core..." << RESET;
        if(fController != nullptr) 
        {
            fController->identify();
        }
        else
        {
            LOG(INFO) << BOLDRED << "DTCLinkInterface::GeneralLinkReset fController is nullptr" << RESET;
            throw std::runtime_error("DTCLinkInterface::GeneralLinkReset fController is nullptr");
        }
        //fController->identify();
        LOG(INFO) << "Controller identified as:" << RESET;
        LOG(INFO) << BOLDRED << "emp::Controller id = " << fController->id() << ".." << fController->versionInfo() << RESET;
        for(auto cOpticalReadout: *pBoard)
        {
            auto pLinkId = cOpticalReadout->getId();

            // reset downlink
            LOG(INFO) << BOLDBLUE << "Resetting lpGBT tx of Link " << +pLinkId << "..." << RESET;
            resetLpGbtTx(pLinkId);

            cAllLocked = cAllLocked && LpGbtDownlinkRdy(pLinkId);
            if(cAllLocked)
            {
                // reset uplink
                LOG(INFO) << BOLDBLUE << "Resetting lpGBT rx of Link " << +pLinkId << "..." << RESET;
                resetLpGbtRx(cOpticalReadout->getId());

                cAllLocked = cAllLocked && LpGbtUplinkRdy(pLinkId);
                if(cAllLocked)
                {
                    LOG(INFO) << BOLDBLUE << "Resetting slow command control of Link " << +pLinkId << "..." << RESET;
                    resetSCC(cOpticalReadout->getId());
                }
            }
        }
    } while(cAttempts < cMaxAttempts && !cAllLocked);

    if(!cAllLocked)
    {
        LOG(ERROR) << BOLDRED << "Failed to lock all links after a general reset" << RESET;
        throw Exception("Failed to lock all links after a general reset");
    }
    else
        LOG(INFO) << BOLDGREEN << "General link reset SUCCESSFUL" << RESET;
}

void DTCLinkInterface::resetLpGbtTx(uint8_t pLinkId)
{
    SetQuad(pLinkId / 4);
    unsigned ch(-1);
    if(LPGBTFPGA == 2.1) ch = pLinkId % 4;
    fController->getLpGbtMGTs().resetTx(ch);
}

void DTCLinkInterface::resetLpGbtRx(uint8_t pLinkId)
{
    SetQuad(pLinkId / 4);
    unsigned ch(-1);
    if(LPGBTFPGA == 2.1) ch = pLinkId % 4;
    fController->getLpGbtMGTs().resetRx(ch);
}

bool DTCLinkInterface::LpGbtDownlinkRdy(uint8_t pLinkId)
{
    SetQuad(pLinkId / 4);
    if(fController->getLpGbtMGTs().getChannelStatus(pLinkId % 4).downlinkReady)
        return true;
    else
        return false;
}

bool DTCLinkInterface::LpGbtUplinkRdy(uint8_t pLinkId)
{
    SetQuad(pLinkId / 4);
    if(fController->getLpGbtMGTs().getChannelStatus(pLinkId % 4).uplinkReady)
        return true;
    else
        return false;
}

void DTCLinkInterface::resetSCC(unsigned pLinkId)
{
    fController->getDatapath().selectLink(pLinkId);
    fController->getSCC().reset();
}

bool DTCLinkInterface::resetGbtLink(uint8_t pLinkId)
{
    SetQuad(pLinkId / 4);
    int  ch          = pLinkId % 4;
    bool cGBTxLocked = fController->getGbtMGTs().resetRx(ch);

    LOG(INFO) << BOLDBLUE << "GBT Link Status..." << RESET;
    fController->getGbtMGTs().dumpStatus(ch, std::cout);

    return cGBTxLocked;
}

void DTCLinkInterface::SetQuad(uint32_t q) { fController->getDatapath().selectRegion(q); }
void DTCLinkInterface::SetChan(uint32_t c) { fController->getDatapath().selectChannel(c); }
} // namespace Ph2_HwInterface
#endif