#include "D19cL1ReadoutInterface.h"
#include "Utils/FileHandler.h"
using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
D19cL1ReadoutInterface::D19cL1ReadoutInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : L1ReadoutInterface(pId, pUri, pAddressTable) {}
D19cL1ReadoutInterface::D19cL1ReadoutInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : L1ReadoutInterface(puHalConfigFileName, pBoardId) {}
D19cL1ReadoutInterface::~D19cL1ReadoutInterface() {}

bool D19cL1ReadoutInterface::ResetReadout()
{
    LOG(DEBUG) << BOLDBLUE << "D19cL1ReadoutInterface Resetting readout..." << RESET;
    WriteReg("fc7_daq_ctrl.readout_block.control.readout_reset", 0x1);
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    WriteReg("fc7_daq_ctrl.readout_block.control.readout_reset", 0x0);
    std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));

    LOG(DEBUG) << BOLDBLUE << "Reseting DDR3 " << RESET;
    fDDR3Offset          = 0;
    auto cDDR3Calibrated = (ReadReg("fc7_daq_stat.ddr3_block.init_calib_done") == 1);
    while(!cDDR3Calibrated)
    {
        LOG(DEBUG) << "Waiting for DDR3 to finish initial calibration";
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        cDDR3Calibrated = (ReadReg("fc7_daq_stat.ddr3_block.init_calib_done") == 1);
    }
    return cDDR3Calibrated;
}

void D19cL1ReadoutInterface::CountFwEvents()
{
    fNReadoutEvents = 0;
    if(fData.size() == 0) return;

    std::vector<uint32_t> cValidData(0);
    cValidData.clear();
    auto   cEventIterator = fData.begin();
    bool   cFoundEmpty    = false;
    size_t cOffset        = 0;
    size_t cCorr          = 0;
    do {
        // check event header
        uint32_t cFirstWord = *cEventIterator;
        uint32_t cHeader    = ((0xFFFF << 16) & cFirstWord) >> 16;
        if(cHeader != 0xFFFF)
        {
            if(!cFoundEmpty && cFirstWord == 0)
            {
                cFoundEmpty = true;
                cCorr       = cOffset; // how far from start
            }
            LOG(DEBUG) << BOLDMAGENTA << "Event header " << std::bitset<16>(cHeader) << " not EXPECTED" << RESET;
            cEventIterator = fData.end();
        }
        else
        {
            uint32_t cEventSize = (0x0000FFFF & (*cEventIterator)) * 4; // event size is given in 128 bit words
            // uint32_t cDummyCount = (0xFF & (*(cEventIterator + 1))) * 4;
            // LOG(DEBUG) << BOLDMAGENTA << "Valid event header .. copying over "
            //           << " event is made up of " << +cEventSize << " 32 bit words "
            //           << " of which " << +cDummyCount << " are dummy words." << RESET;
            // for(size_t cIndx = 0; cIndx < cEventSize; cIndx++) LOG(DEBUG) << BOLDYELLOW << "\t..." << std::bitset<32>(*(cEventIterator + cIndx)) << RESET;
            std::copy(fData.begin() + cOffset, fData.begin() + cOffset + cEventSize, std::back_inserter(cValidData));
            cEventIterator += cEventSize;
            cOffset += cEventSize;
            fNReadoutEvents++;
        }
    } while(cEventIterator < fData.end());
    cCorr = (cFoundEmpty) ? fData.size() - cCorr : cCorr;
    fData.clear();
    // LOG (DEBUG) << BOLDMAGENTA << "Original data vector has " << cOriginalDataSize
    //     << " 32-bit words..  found empty word in position "
    //     << cCorr
    //     << " from end of vector"
    //     << RESET;
    // adjust offset to first empty slot in DDR3
    if(cFoundEmpty)
    {
        LOG(DEBUG) << BOLDMAGENTA << "Found an empty event .. all 0s .. resetting DDR3 offset" << RESET;
        // fDDR3Offset = (cFoundEmpty) ? cFoundEmpty - cOffset : fDDR3Offset;
    }
    if(cValidData.size() == 0) return;

    std::move(cValidData.begin(), cValidData.end(), std::back_inserter(fData));
}

bool D19cL1ReadoutInterface::WaitForReadout()
{
    bool cFailed    = true;
    auto cNWords    = ReadReg("fc7_daq_stat.readout_block.general.words_cnt");
    auto cStartTime = std::chrono::high_resolution_clock::now(), cEndTime = cStartTime;
    auto cDuration = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();
    if(!fWaitForReadoutReq) // wait until words in the readout have stopped inreasing
    {
        LOG(DEBUG) << BOLDMAGENTA << "D19cL1ReadoutInterface::WaitForReadout Now checking words from the FC7" << RESET;
        uint32_t cNWordsPrev    = cNWords;
        bool     cStopIncrement = false;
        do {
            std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
            cNWords        = ReadReg("fc7_daq_stat.readout_block.general.words_cnt");
            cStopIncrement = (cNWords == cNWordsPrev);
            cEndTime       = std::chrono::high_resolution_clock::now();
            cDuration      = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();

        } while(!cStopIncrement && cDuration < fTimeout_us);
        cFailed = (cNWords == 0);
        if(cFailed)
            LOG(ERROR) << BOLDRED << "D19cL1ReadoutInterface::WaitForReadout no words in the readout ..[ReadoutAttempt#" << fReadoutAttempt << "]" << RESET;
        else
            LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::WaitForReadout have " << cNWords << " words in the readout  " << RESET;
    }
    else // send triggers until the readout request flag is '1'
    {
        cFailed = !CheckReadoutReq();
        if(cFailed) LOG(ERROR) << BOLDRED << "D19cL1ReadoutInterface::WaitForReadout readout request not fullfilled ..[ReadoutAttempt#" << fReadoutAttempt << "]" << RESET;
    }
    return !cFailed;
}
bool D19cL1ReadoutInterface::WaitForNTriggers()
{
    // fTriggerInterface->ResetTriggerFSM();

    // wait for trigger state machine to send all triggers
    auto cTriggerSource = this->ReadReg("fc7_daq_cnfg.fast_command_block.trigger_source"); // trigger source
    LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::WaitForNTriggers After resetting trigger FSM.. trigger source is " << cTriggerSource << RESET;
    LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::WaitForNTriggers Running Trigger FSM ..." << RESET;
    bool cSuccess = fTriggerInterface->RunTriggerFSM();
    if(!cSuccess) LOG(ERROR) << BOLDRED << "D19cL1ReadoutInterface timed-out while waiting for trigger FSM...[ReadoutAttempt#" << fReadoutAttempt << "]" << RESET;
    return cSuccess;
}

bool D19cL1ReadoutInterface::WaitForData() { return true; }
bool D19cL1ReadoutInterface::CheckReadoutReq()
{
    uint32_t cIterations = 0;
    auto     cStartTime = std::chrono::high_resolution_clock::now(), cEndTime = cStartTime;
    auto     cDuration   = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();
    auto     cReadoutReq = ReadReg("fc7_daq_stat.readout_block.general.readout_req");
    do {
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        cReadoutReq = ReadReg("fc7_daq_stat.readout_block.general.readout_req");
        LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::CheckReadoutReq ReadoutReq is " << +cReadoutReq << RESET;
        cEndTime  = std::chrono::high_resolution_clock::now();
        cDuration = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();
        cIterations++;
    } while(cReadoutReq == 0 && cDuration < fTimeout_us);
    if(cReadoutReq == 0) { LOG(DEBUG) << BOLDRED << "Readout request 0 [i.e words missing in the readout] ...[ReadoutAttempt#" << fReadoutAttempt << "]" << RESET; }
    else
        LOG(DEBUG) << BOLDGREEN << "ReadoutReq fullfilled.... " << RESET;
    return (cReadoutReq == 1);
}

bool D19cL1ReadoutInterface::CheckBuffers()
{
    // read all the words
    if(ReadReg("fc7_daq_stat.ddr3_block.is_ddr3_type"))
    {
        // readout_req high when buffer is almost full
        uint32_t cReadoutReq = ReadReg("fc7_daq_stat.readout_block.general.readout_req");
        return (cReadoutReq == 1); // DD3 almost full ? check this!!
    }
    return false;
}

bool D19cL1ReadoutInterface::CheckForWordsInReadout()
{
    uint32_t cIterations = 0;
    auto     cStartTime = std::chrono::high_resolution_clock::now(), cEndTime = cStartTime;
    auto     cDuration = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();
    auto     cNWords   = ReadReg("fc7_daq_stat.readout_block.general.words_cnt");
    do {
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        cNWords = ReadReg("fc7_daq_stat.readout_block.general.words_cnt");
        LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::CheckForWordsInReadout words_cnt is " << +cNWords << RESET;
        cEndTime  = std::chrono::high_resolution_clock::now();
        cDuration = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();
        cIterations++;
    } while(cNWords == 0 && cDuration < fTimeout_us);
    if(cNWords == 0) { LOG(DEBUG) << BOLDRED << "No words in the readout .... " << RESET; }
    else
        LOG(DEBUG) << BOLDGREEN << "Found words in the readout ... " << RESET;
    return (cNWords > 0);
}
void D19cL1ReadoutInterface::FillData()
{
    fHandshake = ReadReg("fc7_daq_cnfg.readout_block.global.data_handshake_enable");
    fData.clear();
    std::stringstream cMsg;
    cMsg << "Current offset in DDR3 " << fDDR3Offset;
    uint32_t cNWords = ReadReg("fc7_daq_stat.readout_block.general.words_cnt");
    cMsg << " Register showing words in the readout " << cNWords;
    if(cNWords == 0)
    {
        // LOG (DEBUG) << BOLDRED << cMsg.str() << RESET;
        return;
    }

    fData = ReadBlockRegOffset("fc7_daq_ddr3", cNWords, fDDR3Offset);
    fDDR3Offset += fData.size();
    // LOG (INFO) << BOLDGREEN << cMsg.str() << RESET;
}
void D19cL1ReadoutInterface::StartReadout()
{
    fPollingThread    = std::thread(&D19cL1ReadoutInterface::PollReadoutTh, this);
    fEventCountThread = std::thread(&D19cL1ReadoutInterface::EventCountingTh, this);
    // StopReadout();
}
void D19cL1ReadoutInterface::StopReadout()
{
    LOG(INFO) << BOLDBLUE << "D19cL1ReadoutInterface::StopReadout" << RESET;
    fPollingThread.join();    // pauses until first finishes
    fEventCountThread.join(); // pauses until first finishes
}
void D19cL1ReadoutInterface::EventCountingTh()
{
    fReceivedAllEvents = false;
    fReadoutCompleted  = false;
    fEventCounter      = 0;
    bool cMissedEvent  = false;
    do {
        auto cEventWrd = fReadoutQueue.pop(); // front();
        if(cEventWrd == 0xBADDBADD)
        {
            LOG(INFO) << BOLDYELLOW << "Reached end of valid words from DDR3" << RESET;
            break;
        }
        if(cEventWrd == 0xFFFFFFFF) { continue; }

        uint32_t cHeader = (0xFFFF0000 & cEventWrd) >> 16;
        if(cHeader != 0xFFFF) { continue; }
        uint32_t cEventSize = (0x0000FFFF & cEventWrd) * 4; // event size is given in 128 bit words
        if(cEventSize == 0) continue;
        std::stringstream cOut;
        cOut << "Event#" << fEventCounter << " : " << std::bitset<32>(cEventWrd) << "\t" << cEventSize << " 32 bit words";
        // now have the full event
        D19cReadoutEvent cEvent;
        cEvent.push_back(cEventWrd);
        for(uint32_t i = 1; i < cEventSize; i++)
        {
            cEventWrd = fReadoutQueue.pop();
            cEvent.push_back(cEventWrd);
            if(i == 2)
            {
                uint32_t cEventIdFW = (0xFFFFF & cEventWrd);
                cOut << "\tEventIdFw " << cEventIdFW; // << "\t... " << fReadoutQueue.size() << " entries in the queue";
                // 24 bit counter for event in fc7.. it will roll over
                if(cEventIdFW != (fEventCounter & 0xFFFFF))
                {
                    LOG(WARNING) << BOLDRED << cOut.str() << RESET;
                    cMissedEvent = true;
                }
            }
        }
        if(cMissedEvent) continue;
        fEventQueue.push(cEvent);
        if(fSaveRaw) fFileHandler->setData(cEvent);
        if(fEventCounter % 10000 == 0) LOG(INFO) << BOLDYELLOW << cOut.str() << RESET;
        fEventCounter++;
    } while(fEventCounter < fNEvents && fReadoutValid && !cMissedEvent);
    LOG(INFO) << BOLDYELLOW << "Retreived " << fEventCounter << " events from DDR3 - event queue has " << fEventQueue.size() << " events." << RESET;
    fReceivedAllEvents = (fEventCounter >= fNEvents);
    fReadoutCompleted  = true;
}
// poll board from data
// one thread per BeBoard connected to this computer
void D19cL1ReadoutInterface::PollReadoutTh()
{
    LOG(INFO) << BOLDYELLOW << "D19cL1ReadoutInterface::PollReadoutTh Starting continuous readout thread" << RESET;

    // this is a single thread which fills the readout data queue
    size_t   cNReads     = 0;
    uint32_t cMaxReadout = 100000; // in 32 bit words
    uint32_t cOffsets    = 0;
    uint32_t cNWrds      = 0;
    fReadoutValid        = true;

    // wait for triggers to start
    while(fTriggerInterface->GetTriggerState() == 0) {}

    do {
        // std::this_thread::sleep_for(std::chrono::microseconds(fThreadWait));
        uint32_t cWordCounter = ReadReg("fc7_daq_stat.readout_block.general.words_cnt");
        if(cWordCounter == 0)
        {
            // LOG (INFO) << BOLDGREEN << " NO WORDS IN THE READOUT " << RESET;
            // if( cTriggerInterface->GetTriggerState() == 0 ) break;
            // else continue;
            continue;
        }
        try
        {
            fReadoutValid = true;
            cWordCounter  = std::min(cWordCounter, cMaxReadout);
            auto cData    = ReadBlockRegOffset("fc7_daq_ddr3", cWordCounter, cOffsets);
            for(const auto& e: cData) fReadoutQueue.push(e);
            // if(cNReads%10000 == 0)  LOG (INFO) << BOLDMAGENTA << "Read#" << cNReads << " from DDR3..." << cWordCounter << "\t" << cOffsets*32*1e-9 <<  " Gb" << RESET;
            cOffsets += cWordCounter;
        }
        catch(...)
        {
            fReadoutValid        = false;
            cOffsets             = 0;
            std::exception_ptr p = std::current_exception();
            // auto cData = ReadBlockRegOffset("fc7_daq_ddr3",5,0);
            LOG(INFO) << BOLDRED
                      << "Exception thrown "
                      // << " after reading out " << fEvenCounter[cBoard->getId()] << " events : "
                      // << " total word count " << cNWrds[cBoard->getId()]
                      << " DDR3 offset " << cOffsets * 32 * 1e-9 << " Gb " << fEventCounter << "\t" << (p ? p.__cxa_exception_type()->name() : "null") << " will reset DDR3 counter back to start .. "
                      << RESET;
            // for(auto cWord : cData ) LOG (INFO) << BOLDBLUE << std::bitset<32>(cWord) << RESET;
            // push done word to queue
            fReadoutQueue.push(0xBADDBADD);
        }
        cNReads++;
    } while(fReadoutValid && !fReadoutCompleted && fTriggerInterface->GetTriggerState() != 0);
    // make sure to set the readout flag to false
    fReadoutValid = false;
    LOG(INFO) << BOLDYELLOW << "End of thread that polls data from the DDR3 on the FC7" << RESET;
}

bool D19cL1ReadoutInterface::PollReadoutData(const BeBoard* pBoard, bool pWait)
{
    fHandshake = ReadReg("fc7_daq_cnfg.readout_block.global.data_handshake_enable");
    LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::PollReadoutData " << RESET;
    fData.clear();
    // here check if the trigger state machine is running
    if(fTriggerInterface->GetTriggerState() == 0) // 0, idle - 1 running
    {
        LOG(DEBUG) << BOLDRED << "Triggers not running.. no data to read " << RESET;
        return false;
    }

    bool cSuccess = !pWait;
    if(pWait)
    {
        if(fTriggerInterface->WaitForNTriggers(1)) // wait until at least 1 trigger has been sent
        {
            cSuccess = CheckForWordsInReadout();
        }
    }

    if(cSuccess)
    {
        FillData();
        // CountFwEvents();
        LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::PollReadoutData " << fData.size() << " valid 32 bit words .. which are " << +fNReadoutEvents << " events." << RESET;
    }
    // size_t cCounter=0;
    // auto cIter = fData.begin();
    // do{
    //     size_t cIncrement=1;
    //     auto& cWord = *cIter;
    //     auto cEventHeader = ( cWord & (0xFFFF << 16) ) >> 16;
    //     auto cBlockSize   = ( cWord & (0xFFFF << 0) ) >> 0;
    //     if( cEventHeader != 0xFFFF ) cIncrement = 1;
    //     else cIncrement = cBlockSize*4;
    //     LOG (DEBUG) << BOLDYELLOW << "Entry#" << cCounter << " : " << std::bitset<32>(cWord)
    //         << "Header " << std::bitset<16>(cEventHeader)
    //         << " Blk Size " << cBlockSize
    //         << RESET;
    //     // now look in the event
    //     if( cBlockSize >= 6 )
    //     {
    //         auto cL1Header = ( *(cIter+4) & (0xF << 28) ) >> 28;
    //         auto cErrorCode = ( *(cIter+4) & (0xF << 24) ) >> 24;
    //         auto cHybridId = ( *(cIter+4) & (0xFF << 16) ) >> 16;
    //         auto cL1ASize = ( *(cIter+4) & (0xFFF << 0) ) >> 0;
    //         auto cTimeout = ( *(cIter+5) & (0xFFF << 0) ) >> 0;
    //         auto cL1Counter = ( *(cIter+6) & (0x1FF << 14) ) >> 14;
    //         std::stringstream cOut; cOut << "\t" << std::bitset<4>(cL1Header) << "\tHybrid#" << +cHybridId << " TO " << +cTimeout << " L1 " << +cL1Counter;
    //         if( cErrorCode == 0 && cL1ASize !=0 ) LOG (DEBUG) << BOLDGREEN << cOut.str() << RESET;
    //         else LOG (DEBUG) << BOLDRED  << cOut.str() << RESET;
    //     }
    //     // if( cL1Header != 10 ) cIncrement=1;
    //     // auto cErrorCode = ( cWord & (0xF << 24) ) >> 24;
    //     // auto cHybridId = ( cWord & (0xFF << 16) ) >> 16;
    //     // auto cL1ASize = ( cWord & (0xFFF << 0) ) >> 0;
    //     // if( cL1ASize == 0 ) cIncrement=1;
    //     // else cIncrement = cL1ASize*4;
    //     cIter += cIncrement;
    //     cCounter += cIncrement;
    // }while(cIter != fData.end() );
    // for(size_t cCounter=0; cCounter< fData.size(); cCounter++)
    // {
    //     auto& cWord = fData[cCounter];
    //     auto cL1Header = ( cWord & (0xF << 28) ) >> 28;
    //     if( cL1Header != 10 ) continue;
    //     auto cErrorCode = ( cWord & (0xF << 24) ) >> 24;
    //     auto cHybridId = ( cWord & (0xFF << 16) ) >> 16;
    //     auto cL1ASize = ( cWord & (0xFFF << 0) ) >> 0;
    //     if( cL1ASize == 0 ) continue;

    //     // LOG (DEBUG) << BOLDYELLOW << std::bitset<32>(cWord) << RESET;
    //     if( cErrorCode == 0 ) LOG (DEBUG) << BOLDGREEN << "Entry#" << cCounter << " Hybrid#" << +cHybridId << "\t" << std::bitset<32>(cWord) << RESET;
    //     else LOG (DEBUG) << BOLDRED << "Entry#" << cCounter << " Hybrid#" << +cHybridId << "\t" << std::bitset<32>(cWord) << RESET;
    // }
    return cSuccess;
}
bool D19cL1ReadoutInterface::ReadEvents(const BeBoard* pBoard)
{
    fHandshake = ReadReg("fc7_daq_cnfg.readout_block.global.data_handshake_enable");
    LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::ReadEvents " << fNEvents << RESET;
    fReadoutAttempt = 0;
    bool cSuccess   = true;
    do {
        // clear internal data vector
        fData.clear();
        // configure readout
        fHandshake                  = 1;
        auto cOriginalHandshakeMode = ReadReg("fc7_daq_cnfg.readout_block.global.data_handshake_enable");
        auto cOriginalPackNbr       = ReadReg("fc7_daq_cnfg.readout_block.packet_nbr");
        WriteReg("fc7_daq_cnfg.readout_block.global.data_handshake_enable", fHandshake);
        auto cHandshake = ReadReg("fc7_daq_cnfg.readout_block.global.data_handshake_enable");
        // write number of triggers to accept
        // in the handshake mode offset is cleared after each handshake
        auto cMultiplicity = ReadReg("fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity");
        fNEvents           = fNEvents * (cMultiplicity + 1);
        fTriggerInterface->SetNTriggersToAccept(fNEvents);
        WriteReg("fc7_daq_cnfg.readout_block.packet_nbr", fNEvents - 1);
        LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::ReadEvents asking for " << fNEvents << " events handshake mode is currently " << cHandshake << RESET;

        // reset readout
        ResetReadout();
        cSuccess = WaitForNTriggers();
        cSuccess = cSuccess && WaitForReadout();
        cSuccess = cSuccess && CheckReadoutReq();
        if(cSuccess) // fill data vector + count FW events
        {
            FillData();
            CountFwEvents();
            // LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::ReadEvent " << fData.size() << " valid 32 bit words .. which are " << +fNReadoutEvents << " events." << RESET;
        }
        else
            LOG(WARNING) << BOLDRED << "WARNING Readout Nevents failed.." << RESET;
        WriteReg("fc7_daq_cnfg.readout_block.global.data_handshake_enable", cOriginalHandshakeMode);
        WriteReg("fc7_daq_cnfg.readout_block.packet_nbr", cOriginalPackNbr);
        fReadoutAttempt++;
    } while(fReadoutAttempt < fMaxAttempts && !cSuccess);
    // if( fData.size() == 0) fData = ReadBlockRegOffset("fc7_daq_ddr3", 1000 , fDDR3Offset);
    // for(size_t cCounter=0; cCounter< fData.size(); cCounter++)
    // {
    //     auto& cWord = fData[cCounter];
    //     auto cL1Header = ( cWord & (0xF << 28) ) >> 28;
    //     if( cL1Header != 10 ) continue;
    //     auto cErrorCode = ( cWord & (0xF << 24) ) >> 24;
    //     auto cHybridId = ( cWord & (0xFF << 16) ) >> 16;
    //     auto cL1ASize = ( cWord & (0xFFF << 0) ) >> 0;
    //     if( cL1ASize == 0 ) continue;

    //     // LOG (DEBUG) << BOLDYELLOW << std::bitset<32>(cWord) << RESET;
    //     if( cErrorCode == 0 && cL1ASize !=0 ) LOG (DEBUG) << BOLDGREEN << "Entry#" << cCounter << " Hybrid#" << +cHybridId << "\t" << std::bitset<32>(cWord) << RESET;
    //     else LOG (DEBUG) << BOLDRED << "Entry#" << cCounter << " Hybrid#" << +cHybridId << "\t" << std::bitset<32>(cWord) << RESET;
    // }
    if(fReadoutAttempt > 1) LOG(DEBUG) << BOLDYELLOW << "D19cL1ReadoutInterface::ReadEvents.. took " << (fReadoutAttempt) << " attempts to readout L1 data." << RESET;
    return (fData.size() > 0);
}

} // namespace Ph2_HwInterface
