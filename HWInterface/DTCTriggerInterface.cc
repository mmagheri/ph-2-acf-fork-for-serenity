#if defined(__EMP__)

#include "DTCTriggerInterface.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
DTCTriggerInterface::DTCTriggerInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : TriggerInterface(pId, pUri, pAddressTable)
{
    LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::DTCTriggerInterface Constructor" << RESET;
}
DTCTriggerInterface::DTCTriggerInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : TriggerInterface(puHalConfigFileName, pBoardId)
{
    LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::DTCTriggerInterface Constructor" << RESET;
}
DTCTriggerInterface::~DTCTriggerInterface() {}

bool DTCTriggerInterface::SendNTriggers(uint32_t pNTriggers) 
{
    LOG(INFO) << BOLDYELLOW << "DTCTriggerInterface::SendNTriggers" << RESET;
    if(fFastCommandInterface == nullptr) {
        LOG(ERROR) << BOLDRED << "DTCTriggerInterface::SendNTriggers: FastCommandInterface not initialized" << RESET;
        return false;
    }
    for(int i=0; i<pNTriggers; i++) {
        fFastCommandInterface->SendGlobalL1A();
    }
    return true;
};

} // namespace Ph2_HwInterface
#endif