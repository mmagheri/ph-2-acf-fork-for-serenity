#include "D19cSFPMonitoringInterface.h"
using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
D19cSFPMonitoringInterface::D19cSFPMonitoringInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : RegManager(pId, pUri, pAddressTable) {}
D19cSFPMonitoringInterface::D19cSFPMonitoringInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : RegManager(puHalConfigFileName, pBoardId) {}
D19cSFPMonitoringInterface::D19cSFPMonitoringInterface(RegManager&& theRegManager) : RegManager(std::move(theRegManager)) {}
D19cSFPMonitoringInterface::~D19cSFPMonitoringInterface() {}

uint8_t D19cSFPMonitoringInterface::getSFPSlot(uint8_t pId)
{
    uint32_t fmc2_card_type = ReadReg("fc7_daq_stat.general.info.fmc2_card_type");
    size_t   cLinkOffset    = 0;
    if(fFMCMap[fmc2_card_type] == "OPTO_QUAD") cLinkOffset = 4;
    if(fFMCMap[fmc2_card_type] == "OPTO_OCTA") cLinkOffset = 8;

    uint8_t cSFPslot = (cLinkOffset > 0) ? (pId - cLinkOffset) : pId;
    return cSFPslot;
}
uint8_t D19cSFPMonitoringInterface::getSFPId(uint8_t pId) { return fSlotMapFMC[getSFPSlot(pId)]; }

bool D19cSFPMonitoringInterface::IsDone(std::string pMezzanine)
{
    fErrorCode    = 0;
    int timer_sfp = 0;
    fTimeout      = false;
    while(this->ReadReg("fc7_daq_stat.sfp_ddmi_status.busy_" + pMezzanine))
    {
        this->WriteReg("fc7_daq_cnfg.sfp_ddmi.enable", 0);
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));

        if(timer_sfp > 50)
        {
            fTimeout = true;
            break;
        }
        else
            timer_sfp++;
    }
    fErrorCode = this->ReadReg("fc7_daq_stat.sfp_ddmi_status.error_" + pMezzanine);
    if(fErrorCode) LOG(INFO) << BOLDRED << "Error occurred during communication with the SFP. The error code is: " << fErrorCode << RESET;
    return (fErrorCode == 0);
}
float D19cSFPMonitoringInterface::Read(std::string pParameter, std::string pMezzanine, int pChannel)
{
    this->WriteReg("fc7_daq_cnfg.sfp_ddmi.regAddress", fAddresses[pParameter]);
    this->WriteReg("fc7_daq_cnfg.sfp_ddmi.channel_number", pChannel);
    this->WriteReg("fc7_daq_cnfg.sfp_ddmi.enable", 1);
    bool cSuccess = IsDone(pMezzanine);
    if(!cSuccess) return -1;

    int   cRead        = this->ReadReg("fc7_daq_stat.sfp_ddmi.data_" + pMezzanine);
    float cReturnValue = cRead / fConversion[pParameter];
    // if( pParameter == "RxPower" || pParameter == "TxPower" ) cReturnValue = 10*std::log10(cReturnValue);
    return cReturnValue;
}

} // namespace Ph2_HwInterface