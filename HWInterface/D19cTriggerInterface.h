#ifndef _D19cTriggerInterface_H__
#define _D19cTriggerInterface_H__

#include "TriggerInterface.h"

namespace Ph2_HwInterface
{
struct TestPulseTriggerConfiguration
{
    uint32_t fDelayAfterFastReset = 10;
    uint32_t fDelayAfterTP        = 10;
    uint32_t fDelayBeforeNextTP   = 10;
    uint8_t  fEnableFastReset     = 0;
    uint8_t  fEnableTP            = 1;
    uint8_t  fEnableL1A           = 1;
};

class D19cTriggerInterface : public TriggerInterface
{
  public: // constructors
    D19cTriggerInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    D19cTriggerInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~D19cTriggerInterface();

  public: // virtual functions
    bool     SetNTriggersToAccept(uint32_t pNTriggersToAccept) override;
    void     ResetTriggerFSM() override;
    void     ReconfigureTriggerFSM(std::vector<std::pair<std::string, uint32_t>> pTriggerConfig) override;
    void     Start() override;
    void     Pause() override;
    void     Resume() override;
    bool     Stop() override;
    bool     RunTriggerFSM() override;
    uint32_t GetTriggerState() override;
    bool     WaitForNTriggers(uint32_t pNTriggers) override;
    bool     SendNTriggers(uint32_t pNTriggers) override;
    void     PrintStatus() override;

    // TP FSM
    bool ConfigureTestPulseFSM(TestPulseTriggerConfiguration pTPCnfg);
    // trigger FSM
    bool ConfigureTriggerFSM(TriggerConfiguration pCnfg);

  private:
    void CheckTriggerConfiguration();
};
} // namespace Ph2_HwInterface
#endif