#ifndef _DTCFastCommandInterface_H__
#define _DTCFastCommandInterface_H__

#if defined(__EMP__)

#include "FastCommandInterface.h"
#include <vector>

namespace Ph2_HwInterface
{
class DTCFastCommandInterface : public FastCommandInterface
{
  public: // constructors
    DTCFastCommandInterface(const std::string& puHalConfigFileName, const std::string& pBoardId);
    DTCFastCommandInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    DTCFastCommandInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~DTCFastCommandInterface();

  public:
    void InitialiseTCDS(const Ph2_HwDescription::BeBoard* pBoard);

  public:                                                                // functions
    void SendGlobalReSync(uint8_t pDuration = 0) override;               // 1 clk cycle  ;
    void SendGlobalCalPulse(uint8_t pDuration = 0) override;             // 1 clk cycle  ;
    void SendGlobalL1A(uint8_t pDuration = 0) override;                  // 1 clk cycle  ;
    void SendGlobalRepetitiveL1A(unsigned pNtriggers);                   // 1 clk cycle  ;
    void SendGlobalCounterReset(uint8_t pDuration = 0) override;         // 1 clk cycle  ;
    void SendGlobalCounterResetResync(uint8_t pDuration = 0) override;   // 1 clk cycle  ;
    void SendGlobalCounterResetL1A(uint8_t pDuration = 0) override;      // 1 clk cycle  ;
    void SendGlobalCounterResetCalPulse(uint8_t pDuration = 0) override; // 1 clk cycle  ;
    void SendGlobalCustomFastCommands(std::vector<FastCommand>& pFastCmd) override;
    void ComposeFastCommand(const FastCommand& pFastCommand) override {}

  //private:
    void SendLocalReSync(unsigned pChanId);
    void SendLocalCalPulse(unsigned pChanId);
    void SendLocalCounterReset(unsigned pChanId);
    void SendLocalCustomFastCommands(unsigned pChanId, std::vector<FastCommand>& pFastCmd, bool repeat = false);
    void PrintLocalCounters(unsigned pChanId);
    void SendLocalRepetitiveL1As(unsigned pChanId, unsigned pNtriggers);
    void SendLocalL1A(unsigned pChanId);

    void LocalSourceSelect(unsigned pChanId, unsigned pSource);
    void LocalDelayFastCommand(unsigned pChanId, unsigned pDelay);
    void LocalRepeatFastCommand(unsigned pChanId, unsigned pRepeat);
    void LocalInternalBC0sEnable(unsigned pChanId);
    void LocalInternalBC0sDisable(unsigned pChanId);

  protected:
    void LocalStartPatternGen(unsigned pChanId);
    void LocalResetPatternGen(unsigned pChanId);
    void LocalLoadPatternGen(unsigned pChanId, std::vector<unsigned>& pPatterns);

  private:
    uint32_t             fFastCommand{0};
    void                 InitialiseLocalTCDS(unsigned pChanId);
    std::vector<uint16_t> fChanIds;
};
} // namespace Ph2_HwInterface
#endif
#endif
