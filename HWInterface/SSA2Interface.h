/*!
        \file                                            SSA2Interface.h
        \brief                                           User Interface to the SSA2s
        \author                                          Marc Osherson
        \version                                         1.0
        \date                        Jan 20201
        Support :                    mail to : oshersonmarc@gmail.com

 */

#ifndef __SSA2INTERFACE_H__
#define __SSA2INTERFACE_H__

#include "BeBoardFWInterface.h"
#include "ReadoutChipInterface.h"
#include <fstream>
#include <iostream> // std::cout
#include <string>
#include <vector>

namespace Ph2_HwInterface
{ // start namespace

class SSA2Interface : public ReadoutChipInterface
{ // begin class
  public:
    SSA2Interface(const BeBoardFWMap& pBoardMap);
    ~SSA2Interface();
    bool ConfigureChip(Ph2_HwDescription::Chip* pSSA2, bool pVerifLoop = false, uint32_t pBlockSize = 310) override; // FIXME
    void DumpConfiguration(Ph2_HwDescription::Chip* pSSA2, std::string filename);                                    // FIXME

    void     producePhaseAlignmentPattern(Ph2_HwDescription::ReadoutChip* pChip, uint8_t pWait_ms = 10) override {}
    bool     setInjectionSchema(Ph2_HwDescription::ReadoutChip* pChip, const std::shared_ptr<ChannelGroupBase> group, bool pVerifLoop = true) override;                                        // FIXME
    bool     enableInjection(Ph2_HwDescription::ReadoutChip* pChip, bool inject, bool pVerifLoop = true) override;                                                                             // FIXME
    bool     setInjectionAmplitude(Ph2_HwDescription::ReadoutChip* pChip, uint8_t injectionAmplitude, bool pVerifLoop = true) override;                                                        // FIXME
    bool     maskChannelGroup(Ph2_HwDescription::ReadoutChip* pChip, const std::shared_ptr<ChannelGroupBase> group, bool pVerifLoop = true) override;                                          // FIXME
    bool     maskChannelsAndSetInjectionSchema(Ph2_HwDescription::ReadoutChip* pChip, const std::shared_ptr<ChannelGroupBase> group, bool mask, bool inject, bool pVerifLoop = true) override; // FIXME
    bool     ConfigureChipOriginalMask(Ph2_HwDescription::ReadoutChip* pSSA2, bool pVerifLoop = true, uint32_t pBlockSize = 310) override;                                                     // FIXME
    bool     MaskAllChannels(Ph2_HwDescription::ReadoutChip* pSSA2, bool mask, bool pVerifLoop = true) override;                                                                               // FIXME
    bool     WriteChipReg(Ph2_HwDescription::Chip* pSSA2, const std::string& pRegNode, uint16_t pValue, bool pVerifLoop = true) override;                                                      // FIXME
    bool     WriteChipMultReg(Ph2_HwDescription::Chip* pSSA2, const std::vector<std::pair<std::string, uint16_t>>& pVecReq, bool pVerifLoop = true) override;                                  // FIXME
    bool     WriteChipAllLocalReg(Ph2_HwDescription::ReadoutChip* pSSA2, const std::string& dacName, ChipContainer& pValue, bool pVerifLoop = true) override;                                  // FIXME
    uint16_t ReadChipReg(Ph2_HwDescription::Chip* pSSA2, const std::string& pRegNode) override;
    uint16_t ReadADC(Ph2_HwDescription::ReadoutChip* pChip, uint8_t pInput);
    bool     ConfigureTestPad(Ph2_HwDescription::ReadoutChip* pChip, uint8_t pEnable);
    std::map<std::string, int>         GetADCMap() { return fADCIntMap; }
    std::map<std::string, std::string> GetBiasTrimMap() { return fBiasTrimMap; }
    std::map<std::string, float>       GetTargetBiasMap() { return fTargetBiasMap; }

  private:
    uint8_t ReadChipId(Ph2_HwDescription::Chip* pChip);                                                                                                                         // FIXME
    bool    WriteChipRegBits(Ph2_HwDescription::Chip* pSSA2, const std::string& pRegNode, uint16_t pValue, const std::string& pMaskReg, uint8_t mask, bool pVerifLoop = false); // FIXME
    bool    ConfigureAmux(Ph2_HwDescription::Chip* pChip, const std::string& pRegister, bool pVerifLoop = true);                                                                // FIXME
    std::map<std::string, uint8_t>     fAmuxMap   = {{"BoosterFeedback", 0},                                                                                                    // FIXME
                                               {"PreampBias", 1},
                                               {"Trim", 2},
                                               {"VoltageBias", 3},
                                               {"CurrentBias", 4},
                                               {"CalLevel", 5},
                                               {"BoosterBaseline", 6},
                                               {"Threshold", 7},
                                               {"HipThreshold", 8},
                                               {"DAC", 9},
                                               {"Bandgap", 10},
                                               {"GND", 11},
                                               {"HighZ", 12}};
    std::map<std::string, int>         fADCIntMap = {{"HighImpedance", 0},   {"BoosterFeedback", 1}, {"PreampBias", 2},   {"TrimDACRange", 3}, {"VoltageBias", 4}, {"CurrentBias", 5}, {"CalLevel", 6},
                                             {"BoosterBaseline", 7}, {"Threshold", 8},       {"HipThreshold", 9}, {"DAC", 10},         {"Bandgap", 11},    {"GND", 12},        {"CurrentRef", 13},
                                             {"VoltageRef", 14},     {"TestPad", 15},        {"Temperature", 16}, {"AVDD", 17},        {"PVDD", 18},       {"DVDD", 19}};
    std::map<std::string, std::string> fBiasTrimMap = {{"BoosterFeedback", "Bias_D5BFEED"},
                                                       {"PreampBias", "Bias_D5PREAMP"},
                                                       {"VoltageBias", "Bias_D5ALLV"},
                                                       {"CurrentBias", "Bias_D5ALLI"},
                                                       {"DAC", "Bias_D5DAC8"},
                                                       {"TrimDACRange", "Bias_D5TDR"},
                                                       {"VoltageRef", "ADC_VREF"}};
    std::map<std::string, float>       fTargetBiasMap =
        {{"BoosterFeedback", 82.0}, {"PreampBias", 82.0}, {"VoltageBias", 82.0}, {"CurrentBias", 82.0}, {"DAC", 86.0}, {"TrimDACRange", 115.0}, {"VoltageRef", 850.0}};

}; // end class

} // namespace Ph2_HwInterface

#endif
