#if defined(__EMP__)

#include "DTCOpticalInterface.h"

#include "emp/DatapathNode.hpp"
#include "emp/SCCICNode.hpp"
using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
// ##########################################
// # Constructors #
// #########################################

DTCOpticalInterface::DTCOpticalInterface(const std::string& puHalConfigFileName, const std::string& pId) : FEConfigurationInterface(puHalConfigFileName, pId)
{
    fType                  = ConfigurationType::IC;
    fScaConfig.fFreq       = I2C_FREQ::f1M;
    fScaConfig.fScaAddress = 0;
    fScaConfig.fSclMode    = 0;
}
DTCOpticalInterface::DTCOpticalInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : FEConfigurationInterface(pId, pUri, pAddressTable)
{
    fType                  = ConfigurationType::IC;
    fScaConfig.fFreq       = I2C_FREQ::f1M;
    fScaConfig.fScaAddress = 0;
    fScaConfig.fSclMode    = 0;
}

DTCOpticalInterface::DTCOpticalInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : FEConfigurationInterface(puHalConfigFileName, pBoardId)
{
    fType                  = ConfigurationType::IC;
    fScaConfig.fFreq       = I2C_FREQ::f1M;
    fScaConfig.fScaAddress = 0;
    fScaConfig.fSclMode    = 0;
}

DTCOpticalInterface::~DTCOpticalInterface() {}
// FIXME - replace pChip->getChipAddress() with LpGbt address
void DTCOpticalInterface::icWrite(Chip* pChip, uint32_t pAddress, uint32_t pData)
{
    fController->identify();
    // LOG(DEBUG) << BOLDRED << "emp::Controller id = " << fController->id() << ".." << fController->versionInfo() << RESET;
    fController->getDatapath().selectLink(pChip->getOpticalGroupId());
    if(pChip->getFrontEndType() == FrontEndType::CBC3)
        LOG(DEBUG) << BOLDYELLOW << "DTCOpticalInterface::icWrite on Link#" << +pChip->getOpticalGroupId() << " Chip I2C controller address 0x" << std::hex << +pChip->getI2CControllerAddress()
                   << std::dec << " Chip I2C slave address 0x" << std::hex << +pChip->getChipAddress() << std::dec << " Chip register 0x" << std::hex << +pAddress << std::dec << " Data 0x" << std::hex
                   << +pData << std::dec << RESET;
    fController->getSCCIC().icWrite(pAddress, pData, pChip->getI2CControllerAddress());
}
uint32_t DTCOpticalInterface::icRead(Chip* pChip, uint32_t pAddress)
{
    fController->identify();
    LOG(DEBUG) << BOLDRED << "emp::Controller id = " << fController->id() << ".." << fController->versionInfo() << RESET;
    fController->getDatapath().selectLink(pChip->getOpticalGroupId());
    if(pChip->getFrontEndType() == FrontEndType::CBC3)
        LOG(DEBUG) << BOLDYELLOW << "DTCOpticalInterface::icRead on Link#" << +pChip->getOpticalGroupId() << " Chip address 0x" << std::hex << +pChip->getI2CControllerAddress() << std::dec
                   << " Chip register 0x" << std::hex << +pAddress << std::dec << RESET;
    return fController->getSCCIC().icRead(pAddress, pChip->getI2CControllerAddress()) & 0xff;
}
void DTCOpticalInterface::icBlockWrite(Chip* pChip, uint32_t pAddress, const std::vector<uint32_t>& pData)
{
    LOG(DEBUG) << BOLDYELLOW << "DTCOpticalInterface::icBlockWrite on Link#" << +pChip->getOpticalGroupId() << " Chip I2C controller address 0x" << std::hex << +pChip->getI2CControllerAddress()
               << std::dec << " Chip I2C slave address 0x" << std::hex << +pChip->getChipAddress() << std::dec << " Chip register 0x" << std::hex << +pAddress << std::dec << RESET;
    fController->getDatapath().selectLink(pChip->getOpticalGroupId());
    fController->getSCCIC().icWriteBlock(pAddress, pData, pChip->getI2CControllerAddress());
}
std::vector<uint32_t> DTCOpticalInterface::icBlockRead(Chip* pChip, uint32_t pAddress, uint32_t pNwords)
{
    fController->getDatapath().selectLink(pChip->getOpticalGroupId());
    return fController->getSCCIC().icReadBlock(pAddress, pNwords, pChip->getI2CControllerAddress());
}
void DTCOpticalInterface::LpGbtI2cSetMasterCtrlReg(Chip* pChip)
{
    // struct GbtScaSlaveConfig cScaConfig = {0, 0x41 + cChipId, SlowCommandInterface::f1M, 0};
    unsigned cMaster = pChip->getMasterId();
    unsigned cMasterVersion = pChip->getMasterVersion();
    std::vector<uint32_t> data;
    data.push_back(pChip->getChipAddress());
    data.push_back((((0x01 & fScaConfig.fSclMode) << 7) | ((0x1f & fNBytes) << 2) | (0x03 & fScaConfig.fFreq)) & 0xff);
    icBlockWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 1, data);
    icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_WRITE_CR);
}
void DTCOpticalInterface::LpGbtI2cWrite1Byte(Chip* pChip, unsigned pData)
{
    unsigned cMaster = pChip->getMasterId();
    unsigned cMasterVersion = pChip->getMasterVersion();
    icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 2, pData);
    icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_1BYTE_WRITE);
    uint32_t cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2);
    while(cStatus == 0x00) { cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2); }
    if(cStatus != 0x04) { LOG(ERROR) << BOLDRED << "I2C was not successfully executed. The status = 0x" << std::setw(2) << std::setfill('0') << std::hex << cStatus << std::dec << RESET; }
}
uint32_t DTCOpticalInterface::LpGbtI2cRead1Byte(Chip* pChip)
{
    unsigned cMaster = pChip->getMasterId();
    unsigned cMasterVersion = pChip->getMasterVersion();
    icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_1BYTE_READ);
    uint32_t cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2);
    while(cStatus == 0x00) { cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2); }
    if(cStatus != 0x04)
    {
        LOG(ERROR) << BOLDRED << "I2C was not successfully executed. The status = 0x" << std::setw(2) << std::setfill('0') << std::hex << cStatus << std::dec << RESET;
        return -1;
    }
    return icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 4);
}
void DTCOpticalInterface::LpGbtI2cWriteMulti(Chip* pChip, const std::vector<uint32_t>& pData)
{
    unsigned cMaster = pChip->getMasterId();
    unsigned cMasterVersion = pChip->getMasterVersion();
    // 4BITE0
    if(pData.size() <= 4)
    {
        icBlockWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 2, pData);
        icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_W_MULTI_4BYTE0);
    }
    else
    {
        std::vector<uint32_t> cData0(pData.begin(), pData.begin() + 4);
        icBlockWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 2, cData0);
        icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_W_MULTI_4BYTE0);

        // 4BITE1
        if(pData.size() <= 8)
        {
            std::vector<uint32_t> cData1(pData.begin() + 4, pData.end());
            icBlockWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 2, cData1);
            icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_W_MULTI_4BYTE1);
        }
        else
        {
            std::vector<uint32_t> cData1(pData.begin() + 4, pData.begin() + 8);
            icBlockWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 2, cData1);
            icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_W_MULTI_4BYTE1);

            // 4BITE2
            if(pData.size() <= 12)
            {
                std::vector<uint32_t> cData1(pData.begin() + 8, pData.end());
                icBlockWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 2, cData1);
                icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_W_MULTI_4BYTE2);
            }
            else
            {
                std::vector<uint32_t> cData1(pData.begin() + 8, pData.begin() + 12);
                icBlockWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 2, cData1);
                icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_W_MULTI_4BYTE2);
            }
        }
    }
    icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_WRITE_MULTI);
    uint32_t cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2);
    LOG(DEBUG) << BOLDYELLOW << "I2C status = 0x" << std::setw(2) << std::setfill('0') << std::hex << cStatus << std::dec << RESET;
    while(cStatus == 0x00) { cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2); }
    if(cStatus != 0x04) { LOG(ERROR) << BOLDRED << "I2C was not successfully executed. The status = 0x" << std::setw(2) << std::setfill('0') << std::hex << cStatus << std::dec << RESET; }
}
std::vector<uint32_t> DTCOpticalInterface::LpGbtI2cReadMulti(Chip* pChip, unsigned pNBytes)
{
    unsigned cMaster = pChip->getMasterId();
    unsigned cMasterVersion = pChip->getMasterVersion();
    icWrite(pChip, LpGbt_I2C::IRegAddr[cMasterVersion][cMaster] + 6, LpGbt_I2C::COMM_READ_MULTI);
    uint32_t cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2);
    while(cStatus == 0x00) { cStatus = icRead(pChip, LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 2); }
    if(cStatus != 0x04) { LOG(ERROR) << BOLDRED << "I2C was not successfully executed. The status = 0x" << std::setw(2) << std::setfill('0') << std::hex << cStatus << std::dec << RESET; }
    unsigned cAddr = LpGbt_I2C::ORegAddr[cMasterVersion][cMaster] + 21 - pNBytes;
    return icBlockRead(pChip, cAddr, pNBytes);
}

bool DTCOpticalInterface::SingleWrite(Chip* pChip, ChipRegItem& pItem)
{
    if(pChip->getFrontEndType() == FrontEndType::LpGBT)
    {
        // LOG(INFO) << BOLDGREEN << "DTCOpticalInterface::SingleWrite FrontEndType::LpGBT 0x" << std::hex << pItem.fAddress << std::dec <<  RESET;
        icWrite(pChip, pItem.fAddress, pItem.fValue);
        return true;
    }
    else if(pChip->getFrontEndType() == FrontEndType::CBC3)
        return write130nm(pChip, pItem);
    else
        return write65nm(pChip, pItem);
}
bool DTCOpticalInterface::SingleRead(Chip* pChip, ChipRegItem& pItem)
{
    if(pChip->getFrontEndType() == FrontEndType::LpGBT)
    {
        pItem.fValue = icRead(pChip, pItem.fAddress);
        return true;
    }
    else if(pChip->getFrontEndType() == FrontEndType::CBC3)
        return read130nm(pChip, pItem);
    else
        return read65nm(pChip, pItem);
}
// for now this is just looping over single write
bool DTCOpticalInterface::MultiWrite(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems)
{
    bool cSuccess = true;
    for(auto cRegItem: pRegisterItems) { cSuccess = cSuccess && SingleWrite(pChip, cRegItem); }
    return cSuccess;
}
// for now this is just looping over single read
bool DTCOpticalInterface::MultiRead(Chip* pChip, std::vector<ChipRegItem>& pRegisterItems)
{
    bool cSuccess = true;
    for(auto& cRegItem: pRegisterItems) { cSuccess = cSuccess && SingleRead(pChip, cRegItem); }
    return cSuccess;
}

bool DTCOpticalInterface::SingleWriteRead(Chip* pChip, ChipRegItem& pWriteReg)
{
    // LOG(INFO) << BOLDYELLOW << "DTCOpticalInterface::SingleWriteRead" << RESET;
    size_t      cAttempts = 0;
    ChipRegItem cReadBackReg;
    cReadBackReg  = pWriteReg;
    bool cSuccess = SingleWrite(pChip, pWriteReg);
    do {
        if(SingleRead(pChip, cReadBackReg))
        {
            cSuccess = (cReadBackReg.fValue == pWriteReg.fValue);
            std::stringstream cOut;
            cOut << "\t..Iter#" << cAttempts << " DTCOpticalInterface::SingleWriteRead"
                 << " value read back from register 0x" << std::hex << +pWriteReg.fAddress << " was 0x" << std::hex << +cReadBackReg.fValue << " expected value is 0x" << +pWriteReg.fValue << std::dec;
            if(!cSuccess) LOG(INFO) << BOLDRED << cOut.str() << RESET;
        }
        cAttempts++;
    } while(cAttempts < fConfiguration.fMaxRetryIC && !cSuccess && fConfiguration.fRetryIC);
    return cSuccess;
}
bool DTCOpticalInterface::MultiWriteRead(Chip* pChip, std::vector<ChipRegItem>& pWriteRegs)
{
    bool cSuccess = true;
    for(auto cWriteReg: pWriteRegs) cSuccess = cSuccess && SingleWriteRead(pChip, cWriteReg);
    return cSuccess;
}
bool DTCOpticalInterface::VTRxRegisterWrite(Chip* pChip, uint8_t pRegister, uint8_t pValue)
{
    auto cId      = pChip->getMasterId();
    auto cAddress = pChip->getChipAddress();
    auto cFreq    = fScaConfig.fFreq;
    //
    fNBytes          = 2;
    unsigned cMaster = 1;
    pChip->setMasterId(cMaster);
    fScaConfig.fFreq = 2;
    pChip->setChipAddress(0x50);
    LpGbtI2cSetMasterCtrlReg(pChip);
    pChip->setChipAddress(cAddress);
    fScaConfig.fFreq = cFreq;
    std::vector<uint32_t> cData;
    cData.push_back(pRegister);
    cData.push_back(pValue);
    this->LpGbtI2cWriteMulti(pChip, cData);
    pChip->setMasterId(cId);
    return true;
}
uint8_t DTCOpticalInterface::VTRxRegisterRead(Chip* pChip, uint8_t pRegister)
{
    fNBytes          = 1;
    unsigned cMaster = 1;
    auto     cId     = pChip->getMasterId();
    pChip->setMasterId(cMaster);
    auto cAddress    = pChip->getChipAddress();
    auto cFreq       = fScaConfig.fFreq;
    fScaConfig.fFreq = 2;
    pChip->setChipAddress(0x50);
    LpGbtI2cSetMasterCtrlReg(pChip);
    pChip->setChipAddress(cAddress);
    fScaConfig.fFreq = cFreq;

    this->LpGbtI2cWrite1Byte(pChip, pRegister);
    auto cValue = this->LpGbtI2cRead1Byte(pChip);
    pChip->setMasterId(cId);
    return cValue;
}
bool DTCOpticalInterface::write130nm(Chip* pChip, ChipRegItem& pItem)
{
    // unsigned              cNBytes = (pChip->getFrontEndType() == FrontEndType::CBC3) ? 2 : 3;
    fNBytes = 2;
    LpGbtI2cSetMasterCtrlReg(pChip);
    std::vector<uint32_t> cData;
    cData.push_back(pItem.fAddress);
    cData.push_back(pItem.fValue);
    this->LpGbtI2cWriteMulti(pChip, cData);
    return true;
}
bool DTCOpticalInterface::read130nm(Chip* pChip, ChipRegItem& pItem)
{
    // unsigned              cNBytes = (pChip->getFrontEndType() == FrontEndType::CBC3) ? 2 : 3;
    fNBytes = 2;
    LpGbtI2cSetMasterCtrlReg(pChip);
    this->LpGbtI2cWrite1Byte(pChip, pItem.fAddress);
    pItem.fValue = this->LpGbtI2cRead1Byte(pChip);
    return true;
}
bool DTCOpticalInterface::write65nm(Chip* pChip, ChipRegItem& pItem)
{
    fNBytes = 3;
    LOG(DEBUG) << BOLDYELLOW << "DTCOpticalInterface::write65nm to register 0x" << std::hex << +pItem.fAddress << std::dec << RESET;
    this->LpGbtI2cSetMasterCtrlReg(pChip);
    std::vector<uint32_t> cData;
    cData.push_back((pItem.fAddress & 0xff00) >> 8);
    cData.push_back(pItem.fAddress & 0x00ff);
    cData.push_back(pItem.fValue & 0x00ff);
    this->LpGbtI2cWriteMulti(pChip, cData);
    return true;
}
bool DTCOpticalInterface::read65nm(Chip* pChip, ChipRegItem& pItem)
{
    fNBytes = 2;
    LOG(DEBUG) << BOLDYELLOW << "DTCOpticalInterface::read65nm from register 0x" << std::hex << +pItem.fAddress << std::dec << RESET;
    this->LpGbtI2cSetMasterCtrlReg(pChip);
    std::vector<uint32_t> cData;
    cData.push_back((pItem.fAddress & 0xff00) >> 8);
    cData.push_back(pItem.fAddress & 0x00ff);
    this->LpGbtI2cWriteMulti(pChip, cData);
    pItem.fValue = this->LpGbtI2cRead1Byte(pChip);
    return true;
}

} // namespace Ph2_HwInterface
#endif
