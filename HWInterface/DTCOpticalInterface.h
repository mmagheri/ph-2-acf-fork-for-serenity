#ifndef _DTCOpticalInterface_H__
#define __DTCOpticalInterface_H__

#if defined(__EMP__)

#include "FEConfigurationInterface.h"
#include "emp/Controller.hpp"

#include <vector>

struct GbtScaSlaveConfig
{
    uint32_t fScaAddress;
    uint32_t fFreq;
    uint32_t fSclMode;
};

namespace Ph2_HwInterface
{
// this probably belongs in the HWDescription of the LpGbt
namespace LpGbt_I2C
{

const unsigned IRegAddr[2][3] = {
       {0x0f0, 0x0f7, 0x0fe},
       {0x100, 0x107, 0x10e}
};

const unsigned ORegAddr[2][3] = {
       {0x15f, 0x174, 0x189},
       {0x16f, 0x184, 0x199}
};

const int COMM_WRITE_CR        = 0x0;
const int COMM_WRITE_MASK      = 0x1;
const int COMM_1BYTE_WRITE     = 0x2;
const int COMM_1BYTE_READ      = 0x3;
const int COMM_1BYTE_WRITE_EXT = 0x4;
const int COMM_1BYTE_READ_EXT  = 0x5;
const int COMM_1BYTE_RMW_OR    = 0x6;
const int COMM_1BYTE_RMW_XOR   = 0x7;
const int COMM_W_MULTI_4BYTE0  = 0x8;
const int COMM_W_MULTI_4BYTE1  = 0x9;
const int COMM_W_MULTI_4BYTE2  = 0xa;
const int COMM_W_MULTI_4BYTE3  = 0xb;
const int COMM_WRITE_MULTI     = 0xc;
const int COMM_READ_MULTI      = 0xd;
const int COMM_WRITE_MULTI_EXT = 0xe;
const int COMM_READ_MULTI_EXT  = 0xf;
} // namespace LpGbt_I2C

class DTCOpticalInterface : public FEConfigurationInterface
{
  public:
    DTCOpticalInterface(const std::string& puHalConfigFileName, const std::string& pBoardId);
    DTCOpticalInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    DTCOpticalInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);
    ~DTCOpticalInterface();

  public:
    bool SingleWrite(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem) override;
    bool SingleRead(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem) override;
    // Write+Read-back
    bool SingleWriteRead(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem) override;
    bool MultiWriteRead(Ph2_HwDescription::Chip* pChip, std::vector<Ph2_HwDescription::ChipRegItem>& pItem) override;
    // Multi Write/Read
    bool MultiRead(Ph2_HwDescription::Chip* pChip, std::vector<Ph2_HwDescription::ChipRegItem>& pRegisterItems) override;
    bool MultiWrite(Ph2_HwDescription::Chip* pChip, std::vector<Ph2_HwDescription::ChipRegItem>& pRegisterItems) override;

    // function to link FEConfigurationInterface
    void LinkEMPcontroller(emp::Controller* pController) { fController = pController; }

    // temporary
    bool    VTRxRegisterWrite(Ph2_HwDescription::Chip* pChip, uint8_t pRegister, uint8_t pValue);
    uint8_t VTRxRegisterRead(Ph2_HwDescription::Chip* pChip, uint8_t pRegister);

  private:
    enum I2C_FREQ
    {
        f100K = 0,
        f200K = 1,
        f400K = 2,
        f1M   = 3
    };
    uint8_t           fNBytes{0};
    emp::Controller*  fController;
    GbtScaSlaveConfig fScaConfig;

    // function to read + write to FE ASIcs
    // 130 nm ASICs (i.e. CBC)
    bool write130nm(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem);
    bool write65nm(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem);
    // 65 nm ASICs (i.e. MPA+SSA+CIC)
    bool read130nm(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem);
    bool read65nm(Ph2_HwDescription::Chip* pChip, Ph2_HwDescription::ChipRegItem& pItem);

    // ic write  + read functions
    void                  icWrite(Ph2_HwDescription::Chip* pChip, uint32_t pAddress, uint32_t pData);
    uint32_t              icRead(Ph2_HwDescription::Chip* pChip, uint32_t pAddress);
    void                  icBlockWrite(Ph2_HwDescription::Chip* pChip, uint32_t pAddress, const std::vector<uint32_t>& pData);
    std::vector<uint32_t> icBlockRead(Ph2_HwDescription::Chip* pChip, uint32_t pAddress, uint32_t pNwords);

    // i2c functions
    void                  LpGbtI2cSetMasterCtrlReg(Ph2_HwDescription::Chip* pChip);
    void                  LpGbtI2cWrite1Byte(Ph2_HwDescription::Chip* pChip, unsigned pData);
    uint32_t              LpGbtI2cRead1Byte(Ph2_HwDescription::Chip* pChip);
    void                  LpGbtI2cWriteMulti(Ph2_HwDescription::Chip* pChip, const std::vector<uint32_t>& pData);
    std::vector<uint32_t> LpGbtI2cReadMulti(Ph2_HwDescription::Chip* pChip, unsigned pNBytes);
};
} // namespace Ph2_HwInterface
#endif
#endif
