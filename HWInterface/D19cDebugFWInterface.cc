#include "D19cDebugFWInterface.h"
#include "../HWDescription/Chip.h"

using namespace Ph2_HwDescription;

namespace Ph2_HwInterface
{
D19cDebugFWInterface::D19cDebugFWInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable) : RegManager(pId, pUri, pAddressTable) {}
D19cDebugFWInterface::D19cDebugFWInterface(const std::string& puHalConfigFileName, uint32_t pBoardId) : RegManager(puHalConfigFileName, pBoardId)
{
    LOG(INFO) << BOLDYELLOW << "D19cDebugFWInterface::D19cBackendAlignmentFWInterface Constructor" << RESET;
}
D19cDebugFWInterface::~D19cDebugFWInterface() {}

std::string D19cDebugFWInterface::L1ADebug(uint8_t pWait_ms, bool pPrint)
{
    // LOG(INFO) << BOLDBLUE << "D19cDebugFWInterface::L1ADebug ...." << RESET;
    uint32_t encode_resync    = 0 << 16;
    uint32_t encode_cal_pulse = 0 << 17;
    uint32_t encode_l1a       = 1 << 18;
    uint32_t encode_bc0       = 0 << 19;
    uint32_t encode_duration  = 0 << 28;
    uint32_t final_command    = encode_resync + encode_l1a + encode_cal_pulse + encode_bc0 + encode_duration;
    WriteReg("fc7_daq_ctrl.fast_command_block.control", final_command);
    /*auto cInitFastReset = this->ReadReg("fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable");
    auto cInitBP        = this->ReadReg("fc7_daq_cnfg.fast_command_block.misc.backpressure_enable");
    // enable initial fast reset
    this->WriteReg("fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable", 0);
    // disable back-pressure
    this->WriteReg("fc7_daq_cnfg.fast_command_block.misc.backpressure_enable", 0);
    WriteReg("fc7_daq_ctrl.fast_command_block.control.stop_trigger", 0x1);
    // reset trigger
    this->WriteReg("fc7_daq_ctrl.fast_command_block.control.reset", 0x1);
    // load new trigger configuration
    this->WriteReg("fc7_daq_ctrl.fast_command_block.control.load_config", 0x1);
    WriteReg("fc7_daq_ctrl.fast_command_block.control.start_trigger", 0x1);
    // LOG(INFO) << BOLDBLUE << "Started triggers ...." << RESET;
    // wait until you've received at least one trigger
    auto cNTriggersRxd = this->ReadReg("fc7_daq_stat.fast_command_block.trigger_in_counter");
    auto cStartTime = std::chrono::high_resolution_clock::now(), cEndTime = cStartTime;
    auto cDuration = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();
    do {
        cEndTime      = std::chrono::high_resolution_clock::now();
        cDuration     = std::chrono::duration_cast<std::chrono::microseconds>(cEndTime - cStartTime).count();
        cNTriggersRxd = this->ReadReg("fc7_daq_stat.fast_command_block.trigger_in_counter");
        // LOG(INFO) << BOLDMAGENTA << "Trigger in counter is " << +cNTriggersRxd << " waited for " << cDuration << " us so far" << RESET;
    } while(cNTriggersRxd < 10 && cDuration < pWait_ms * 1e3);
    WriteReg("fc7_daq_ctrl.fast_command_block.control.stop_trigger", 0x1);
    LOG(INFO) << BOLDMAGENTA << "First header found after " << this->ReadReg("fc7_daq_stat.physical_interface_block.slvs_debug.first_header_delay") << " clock cycles." << RESET;
    */
    auto        cWords         = ReadBlockReg("fc7_daq_stat.physical_interface_block.l1a_debug", 50);
    std::string cBuffer        = "";
    std::string cBuffer_wSpace = "";
    size_t      cLineIndx      = 0;
    fDebugData[0]              = "";
    for(auto cWord: cWords)
    {
        auto                     cString = std::bitset<32>(cWord).to_string();
        std::vector<std::string> cOutputWords(0);
        for(size_t cIndex = 0; cIndex < 4; cIndex++) { cOutputWords.push_back(cString.substr(cIndex * 8, 8)); }
        std::string cOutput = "";
        for(auto cIt = cOutputWords.end() - 1; cIt >= cOutputWords.begin(); cIt--)
        {
            cOutput += *cIt + " ";
            cBuffer += *cIt;
        }
        cBuffer_wSpace += cOutput;
        if(pPrint) LOG(INFO) << BOLDBLUE << "#" << +cLineIndx << ":" << cOutput << RESET;
        cLineIndx++;
    }

    // this->WriteReg("fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable", cInitFastReset);
    // this->WriteReg("fc7_daq_cnfg.fast_command_block.misc.backpressure_enable", cInitBP);
    // this->WriteReg("fc7_daq_ctrl.fast_command_block.control.load_config", 0x1);
    fDebugData[0] = cBuffer_wSpace;
    return cBuffer;
}

std::vector<std::string> D19cDebugFWInterface::StubDebug(bool pWithTestPulse, uint8_t pNlines, bool pPrint)
{
    LOG(DEBUG) << BOLDBLUE << "D19cDebugFWInterface::StubDebug ...." << RESET;

    uint8_t cReSync   = 0;
    uint8_t cCalPulse = 0;
    uint8_t cL1A      = 0;
    uint8_t cBC0      = 0;
    uint8_t cDuration = 0;
    if(pWithTestPulse) { cCalPulse = 1; }
    else
        cL1A = 1;

    uint32_t encode_resync    = cReSync << 16;
    uint32_t encode_cal_pulse = cCalPulse << 17;
    uint32_t encode_l1a       = cL1A << 18;
    uint32_t encode_bc0       = cBC0 << 19;
    uint32_t encode_duration  = cDuration << 28;
    uint32_t final_command    = encode_resync + encode_l1a + encode_cal_pulse + encode_bc0 + encode_duration;
    WriteReg("fc7_daq_ctrl.fast_command_block.control", final_command);

    auto cWords = ReadBlockReg("fc7_daq_stat.physical_interface_block.stub_debug", 80);
    LOG(DEBUG) << BOLDBLUE << "Captured stub debug  ...." << RESET;

    std::vector<std::string> cLines(0);
    size_t                   cLine = 0;
    do {
        std::vector<std::string> cOutputWords(0);
        for(size_t cIndex = 0; cIndex < 5; cIndex++)
        {
            auto cWord   = cWords[cLine * 10 + cIndex];
            auto cString = std::bitset<32>(cWord).to_string();
            for(size_t cOffset = 0; cOffset < 4; cOffset++) { cOutputWords.push_back(cString.substr(cOffset * 8, 8)); }
        }

        std::string cOutput_wSpace = "";
        std::string cOutput        = "";
        for(auto cIt = cOutputWords.end() - 1; cIt >= cOutputWords.begin(); cIt--)
        {
            cOutput_wSpace += *cIt + " ";
            cOutput += *cIt;
        }
        if(pPrint) LOG(INFO) << BOLDBLUE << "Line " << +cLine << " : " << cOutput_wSpace << RESET;
        cLines.push_back(cOutput);
        fDebugData[cLine + 1] = cOutput_wSpace; // stub lines are SLVS lines 1 >
        // cStrLength = cOutput.length();
        cLine++;
    } while(cLine < pNlines);
    return cLines;
}
D19cDebugDataErrorCheck D19cDebugFWInterface::CheckData(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pPattern)
{
    auto cType   = pChip->getFrontEndType();
    auto cChipId = (cType == FrontEndType::CIC || cType == FrontEndType::CIC2) ? 0 : pChip->getId() % 8;
    this->WriteReg("fc7_daq_cnfg.physical_interface_block.slvs_debug.hybrid_select", pChip->getHybridId());
    this->WriteReg("fc7_daq_cnfg.physical_interface_block.slvs_debug.chip_select", cChipId);
    return CheckData(pLineId, pPattern);
}
D19cDebugDataErrorCheck D19cDebugFWInterface::CheckData(uint8_t pLineId, uint8_t pPattern)
{
    // accumulate debug data  N times
    fErrorCheck.fData = "";
    for(size_t cIteration = 0; cIteration < fNIterations; cIteration++)
    {
        // capture on all lines
        if(pLineId > 0)
            this->StubDebug(true, 6, false);
        else
            this->L1ADebug(10, true);

        // remove spaces - I don't care about word boundaries here
        auto                  cStr  = fDebugData[pLineId];
        std::string::iterator cIter = std::remove(cStr.begin(), cStr.end(), ' ');
        cStr.erase(cIter, cStr.end());
        fErrorCheck.fData += cStr;
    }

    fErrorCheck.fPattern = pPattern;
    std::stringstream cExpected;
    cExpected << std::bitset<8>(pPattern);
    size_t cLength           = cExpected.str().length();
    size_t cPos              = fErrorCheck.fData.find(cExpected.str(), cPos);
    size_t cEnd              = cPos;
    fErrorCheck.fZeroToOne   = 0;
    fErrorCheck.fOneToZero   = 0;
    fErrorCheck.fBitsChecked = 0;
    fErrorCheck.fChecked     = "";
    fErrorCheck.fGuess       = false;
    fErrorCheck.fErrors.clear();
    // pattern not found anywhere.. best I can do is count number of 1s and 0s vs. what I would expect as a maximum
    // assuming pattern starts at bit 0
    if(cPos == std::string::npos)
    {
        fErrorCheck.fGuess = true;
        cPos               = 0;
    }
    else // go back as far as you can
    {
        auto cNWordsPrev = std::floor(cPos / (float)cLength);
        cPos             = cPos - cNWordsPrev * cLength;
    }
    std::vector<uint16_t> cErrors(0);
    size_t                cNWordsChecked = 0;
    do {
        // search every N bits of the expected pattern
        cEnd = cPos + cLength;
        if(cEnd >= fErrorCheck.fData.length()) cEnd = fErrorCheck.fData.length();
        auto cWrd = fErrorCheck.fData.substr(cPos, cLength);
        if(cWrd.length() == 8 && cNWordsChecked < 8) fErrorCheck.fChecked += cWrd + "\t";
        for(uint8_t cIndx = 0; cIndx < cWrd.length(); cIndx++)
        {
            if(cExpected.str()[cIndx] != cWrd[cIndx])
            {
                fErrorCheck.fErrors.push_back(cPos + cIndx);
                fErrorCheck.fZeroToOne += (cExpected.str()[cIndx] == '0') ? 1 : 0;
                fErrorCheck.fOneToZero += (cExpected.str()[cIndx] == '1') ? 1 : 0;
            }
            fErrorCheck.fBitsChecked++;
        }
        cPos = cEnd; // update seach
        if(cWrd.length() != 8) continue;

        // if( cNWordsChecked < 8 ) fErrorCheck.fChecked += "\t";
        // else if( cNWordsChecked%8 == 0 ) fErrorCheck.fChecked += "\n";
        // else fErrorCheck.fChecked += "\t";
        cNWordsChecked++;
    } while(cPos < fErrorCheck.fData.length()); // && cPos != std::string::npos );

    std::stringstream cOut;
    cOut << "SLVSLine#" << +pLineId << " Checked " << fErrorCheck.fBitsChecked << " bits\t...Found " << fErrorCheck.fZeroToOne << " 0-1 bit flips "
         << " and " << fErrorCheck.fOneToZero << " 1-0 bit flips ";
    LOG(DEBUG) << BOLDYELLOW << cOut.str() << RESET;
    // if( fErrorCheck.fErrors.size() > 0 )
    // {
    //     LOG (INFO) << BOLDRED << " Errors detected on line#" << +pLineId
    //         << " expect [" << fErrorCheck.fPattern
    //         << "] example of capture on line : " << fErrorCheck.fData
    //         << RESET;
    // }
    return fErrorCheck;
}
std::vector<std::string> D19cDebugFWInterface::ScopeStubLines(bool pWithTestPulse)
{
    LOG(INFO) << BOLDBLUE << "D19cDebugFWInterface::ScopeStubLines ...." << RESET;
    uint8_t cReSync   = 0;
    uint8_t cCalPulse = 0;
    uint8_t cL1A      = 0;
    uint8_t cBC0      = 0;
    uint8_t cDuration = 0;
    if(pWithTestPulse) { cCalPulse = 1; }
    else
    {
        cL1A = 1;
    }
    uint32_t encode_resync    = cReSync << 16;
    uint32_t encode_cal_pulse = cCalPulse << 17;
    uint32_t encode_l1a       = cL1A << 18;
    uint32_t encode_bc0       = cBC0 << 19;
    uint32_t encode_duration  = cDuration << 28;
    uint32_t final_command    = encode_resync + encode_l1a + encode_cal_pulse + encode_bc0 + encode_duration;
    WriteReg("fc7_daq_ctrl.fast_command_block.control", final_command);

    auto                     cWords = ReadBlockReg("fc7_daq_stat.physical_interface_block.stub_debug", 80);
    std::vector<std::string> cLines(0);
    size_t                   cLine   = 0;
    size_t                   cNlines = 6;
    // int cStrLength=0;
    do {
        std::vector<std::string> cOutputWords(0);
        for(size_t cIndex = 0; cIndex < cNlines; cIndex++)
        {
            auto cWord   = cWords[cLine * 10 + cIndex];
            auto cString = std::bitset<32>(cWord).to_string();
            for(size_t cOffset = 0; cOffset < 4; cOffset++) { cOutputWords.push_back(cString.substr(cOffset * 8, 8)); }
        }

        std::string cOutput_wSpace = "";
        std::string cOutput        = "";
        for(auto cIt = cOutputWords.end() - 1; cIt >= cOutputWords.begin(); cIt--)
        {
            cOutput_wSpace += *cIt + " ";
            cOutput += *cIt;
        }
        LOG(DEBUG) << BOLDBLUE << "Line " << +cLine << " : " << cOutput_wSpace << RESET;
        cLines.push_back(cOutput);
        cLine++;
    } while(cLine < cNlines);
    return cLines;
}
} // namespace Ph2_HwInterface