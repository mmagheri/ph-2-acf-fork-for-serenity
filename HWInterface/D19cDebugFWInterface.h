#ifndef _D19cDebugFWInterface_H__
#define __D19cDebugFWInterface_H__

#include "../Utils/ConsoleColor.h"
#include "../Utils/Utilities.h"
#include "../Utils/easylogging++.h"
#include "RegManager.h"
#include <string>

namespace Ph2_HwDescription
{
class Chip;
}

struct D19cDebugDataErrorCheck
{
    std::string         fData;
    std::string         fPattern;
    std::string         fChecked;
    size_t              fBitsChecked;
    size_t              fZeroToOne;
    size_t              fOneToZero;
    bool                fGuess;
    std::vector<size_t> fErrors;
};
namespace Ph2_HwInterface
{
class D19cDebugFWInterface : public RegManager
{
  public:
    D19cDebugFWInterface(const std::string& puHalConfigFileName, uint32_t pBoardId);
    D19cDebugFWInterface(const std::string& pId, const std::string& pUri, const std::string& pAddressTable);

    ~D19cDebugFWInterface();

  private:
    // slvs line id, debug data
    std::map<int, std::string> fDebugData;
    D19cDebugDataErrorCheck    fErrorCheck;
    size_t                     fNIterations{1};

  public:
    std::string              GetL1Debug() { return fDebugData[0]; }
    std::string              GetStubDebug(uint8_t pLineId) { return fDebugData[pLineId]; }
    std::vector<std::string> StubDebug(bool pWithTestPulse = true, uint8_t pNlines = 6, bool pPrint = true);
    std::string              L1ADebug(uint8_t pWait_ms = 1, bool pPrint = true);
    std::vector<std::string> ScopeStubLines(bool pWithTestPulse = true);
    D19cDebugDataErrorCheck  CheckData(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pPattern);
    D19cDebugDataErrorCheck  CheckData(uint8_t pLineId, uint8_t pPattern);
    void                     SetNIterations(size_t pIters) { fNIterations = pIters; }
};
} // namespace Ph2_HwInterface
#endif
