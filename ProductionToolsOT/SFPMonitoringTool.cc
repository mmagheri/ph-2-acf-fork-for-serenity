#include "SFPMonitoringTool.h"
#include "D19cSFPMonitoringInterface.h"
#include "FileParser.h"

using namespace Ph2_System;
using namespace Ph2_HwInterface;

SFPMonitoringTool::SFPMonitoringTool(std::string puHalConfigFileName, uint16_t pBoardId)
{
    fConfigurationFile = puHalConfigFileName;
    fBoardId           = pBoardId;
    fMyInterface       = nullptr;
    FileParser                     theFileParser;
    std::map<uint16_t, RegManager> theRegManagerList = theFileParser.getRegManagerList(fConfigurationFile);
    fMyInterface                                     = new D19cSFPMonitoringInterface(std::move(theRegManagerList.at(fBoardId)));
}
SFPMonitoringTool::~SFPMonitoringTool() {}

std::pair<float, float> SFPMonitoringTool::GetMonitorable(std::string pParameter, std::string pMezzanine, uint8_t pLinkId)
{
    std::vector<float> cMeasurements;
    auto               cSFPId = fMyInterface->getSFPId(pLinkId);
    for(size_t cIndx = 0; cIndx < fNMeasurements; cIndx++) cMeasurements.push_back(fMyInterface->Read(pParameter, pMezzanine, cSFPId));
    auto cStats = calculateStats(cMeasurements);
    return cStats;
}
