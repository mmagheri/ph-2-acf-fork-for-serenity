#include "HybridShortFinder.h"
#include "../Utils/Container.h"
#include "../Utils/ContainerFactory.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/Exception.h"
#include "../Utils/Occupancy.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Utilities.h"

#include "../Utils/CBCChannelGroupHandler.h"
#include "../Utils/MPAChannelGroupHandler.h"
#include "../Utils/SSAChannelGroupHandler.h"
#include <math.h>

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;

HybridShortFinder::HybridShortFinder() : PedeNoise() {}

HybridShortFinder::~HybridShortFinder() {}

void HybridShortFinder::Initialise(void)
{
    LOG(INFO) << BOLDYELLOW << "HybridShortFinder::Initialise" << RESET;
    fEventsPerPoint  = findValueInSettings<double>("Nevents", 10);
    fNEventsPerBurst = (fEventsPerPoint >= fMaxNevents) ? fMaxNevents : 0xffff;

    std::vector<bool> cWithCBCs(fDetectorContainer->size(), false);
    std::vector<bool> cWithMPAs(fDetectorContainer->size(), false);
    std::vector<bool> cWithSSAs(fDetectorContainer->size(), false);
    for(auto cBoard: *fDetectorContainer)
    {
        auto cTypes                   = cBoard->connectedFrontEndTypes();
        cWithMPAs[cBoard->getIndex()] = std::find(cTypes.begin(), cTypes.end(), FrontEndType::MPA) != cTypes.end();
        cWithMPAs[cBoard->getIndex()] = cWithMPAs[cBoard->getIndex()] || std::find(cTypes.begin(), cTypes.end(), FrontEndType::MPA2) != cTypes.end();

        cWithSSAs[cBoard->getIndex()] = std::find(cTypes.begin(), cTypes.end(), FrontEndType::SSA) != cTypes.end();
        cWithSSAs[cBoard->getIndex()] = cWithSSAs[cBoard->getIndex()] || std::find(cTypes.begin(), cTypes.end(), FrontEndType::SSA2) != cTypes.end();

        if(!cWithSSAs[cBoard->getIndex()] && !cWithMPAs[cBoard->getIndex()]) continue;
        LOG(INFO) << BOLDYELLOW << "Forcing ShortFinder to use PSAS event" << RESET;
        cBoard->setEventType(EventType::PSAS);
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid) { fReadoutChipInterface->WriteChipReg(cChip, "AnalogueAsync", 1); }
            }
        }
    } // loop over boards and set event type

    // configure channel group handler
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cFrontEndType: cBoard->connectedFrontEndTypes())
        {
            if(cFrontEndType == FrontEndType::CBC3)
            {
                CBCChannelGroupHandler theChannelGroupHandler;
                theChannelGroupHandler.setChannelGroupParameters(16, 2); // 16*2*8
                setChannelGroupHandler(theChannelGroupHandler);
            }
            else if(cFrontEndType == FrontEndType::SSA || cFrontEndType == FrontEndType::SSA2)
            {
                size_t   cNClustersPerGroup = 0;
                uint16_t cRowsToSkip        = 0;
                uint16_t cColsToSkip        = 0;
                // retrieve injection config
                for(auto board: *fDetectorContainer)
                {
                    for(auto opticalGroup: *board)
                    {
                        for(auto hybrid: *opticalGroup)
                        {
                            for(auto chip: *hybrid)
                            {
                                if(chip->getFrontEndType() != cFrontEndType || cNClustersPerGroup != 0) continue;
                                auto cDefInjection = chip->getDefInj();
                                cRowsToSkip        = cDefInjection.first;
                                cColsToSkip        = cDefInjection.second;
                                cNClustersPerGroup = NSSACHANNELS / (cRowsToSkip * cColsToSkip);
                            } // chips
                        }     // hybrids
                    }         // links
                }             // board
                SSAChannelGroupHandler theChannelGroupHandler;
                theChannelGroupHandler.setChannelGroupParameters(cNClustersPerGroup, 2);
                setChannelGroupHandler(theChannelGroupHandler, cFrontEndType);
                LOG(INFO) << BOLDBLUE << "PedeNoise with SSAs : Inject every " << +cRowsToSkip << " row(s). " << RESET;
            }
            else if(cFrontEndType == FrontEndType::MPA || cFrontEndType == FrontEndType::MPA2)
            {
                MPAChannelGroupHandler theChannelGroupHandler;
                theChannelGroupHandler.setChannelGroupParameters(1, NSSACHANNELS * NMPACOLS); // 16*2*8
                setChannelGroupHandler(theChannelGroupHandler, cFrontEndType);
            }
        }
    }
    initializeRecycleBin();
}

void HybridShortFinder::runHybridShortFinder(void)
{
    LOG(INFO) << "Taking Data with " << fEventsPerPoint << " triggers!";

    this->enableTestPulse(true);
    // this has to be done in groups
    this->SetTestAllChannels(false);
    fMaskChannelsFromOtherGroups = false;
    // find threshold at which occupancy is 0.96
    DetectorDataContainer theOccupancyContainer;
    fDetectorDataContainer = &theOccupancyContainer;
    ContainerFactory::copyAndInitStructure<Occupancy>(*fDetectorContainer, *fDetectorDataContainer);
    this->bitWiseScan("Threshold", fEventsPerPoint, 0.96, fNEventsPerBurst);

    this->enableTestPulse(false);
}

void HybridShortFinder::writeObjects()
{
#ifdef __USE_ROOT__
    // Calibration is not running on the SoC: processing the histograms
    // fCBCHistogramPulseShape.process();
#endif
}

// For system on chip compatibility
void HybridShortFinder::Running()
{
    LOG(INFO) << "Starting calibration example measurement.";
    runHybridShortFinder();
    LOG(INFO) << "Done with calibration example.";
}

// For system on chip compatibility
void HybridShortFinder::Stop(void)
{
    LOG(INFO) << "Stopping calibration example measurement.";
    writeObjects();
    dumpConfigFiles();
    SaveResults();
    closeFileHandler();
    LOG(INFO) << "Calibration example stopped.";
}
