#if defined(__TCUSB__)

#include "PSTestCardControl.h"

PSTestCardControl::PSTestCardControl() {}

PSTestCardControl::~PSTestCardControl() {}

void PSTestCardControl::Initialise(uint32_t pUsbBus, uint8_t pUsbDev)
{
    ConfigureUsb(pUsbBus, pUsbDev);
    LOG(INFO) << BOLDBLUE << "Selecting antenna channel to "
              << " disable all charge injection" << RESET;
    TC_PSFE cTC_PSFE(fUsbBus, fUsbDev);
    cTC_PSFE.antenna_fc7(uint16_t(513), TC_PSFE::ant_channel::NONE);
}
void PSTestCardControl::SetDefaultHybridVoltage()
{
    LOG(INFO) << "Setting default hybrid voltage..." << RESET;
    TC_PSFE cTC_PSFE(fUsbBus, fUsbDev);
    cTC_PSFE.set_voltage(cTC_PSFE._1150mV, cTC_PSFE._1250mV);
    // check hybrid voltages
    for(auto cItem: fHybridVoltageMap) { ReadHybridVoltage(cItem.first); }
}
void PSTestCardControl::ETest() {}
void PSTestCardControl::ReadHybridVoltage(const std::string& pVoltageName)
{
    auto cMapIterator = fHybridVoltageMap.find(pVoltageName);
    if(cMapIterator != fHybridVoltageMap.end())
    {
        auto&              cMeasurement = cMapIterator->second;
        TC_PSFE            cTC_PSFE(fUsbBus, fUsbDev);
        std::vector<float> cMeasurements(fNreadings, 0.);
        for(int cIndex = 0; cIndex < fNreadings; cIndex++)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(fVoltageMeasurementWait_ms));
            cTC_PSFE.adc_get(cMeasurement, cMeasurements[cIndex]);
            LOG(DEBUG) << BOLDBLUE << "\t\t..After waiting for " << (cIndex + 1) * 1e-3 * fVoltageMeasurementWait_ms << " seconds ..."
                       << " reading from test card  : " << cMeasurements[cIndex] << " mV." << RESET;
        }
        fVoltageMeasurement = calculateStats(cMeasurements);
        LOG(INFO) << BOLDYELLOW << "Reading " << pVoltageName << " from PS test card"
                  << "\t.. Average reading " << fVoltageMeasurement.first << " - StdDev " << fVoltageMeasurement.second << RESET;
    }
}
void PSTestCardControl::ReadHybridCurrent(const std::string& pCurrentName)
{
    auto cMapIterator = fHybridCurrentMap.find(pCurrentName);
    if(cMapIterator != fHybridCurrentMap.end())
    {
        auto&              cMeasurement = cMapIterator->second;
        TC_PSFE            cTC_PSFE(fUsbBus, fUsbDev);
        std::vector<float> cMeasurements(fNreadings, 0.);
        for(int cIndex = 0; cIndex < fNreadings; cIndex++)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(fVoltageMeasurementWait_ms));
            cTC_PSFE.adc_get(cMeasurement, cMeasurements[cIndex]);
            LOG(INFO) << BOLDBLUE << "\t\t..After waiting for " << (cIndex + 1) * 1e-3 * fVoltageMeasurementWait_ms << " seconds ..."
                      << " reading from test card 1V  : " << cMeasurements[cIndex] << " mA." << RESET;
        }
        fCurrentMeasurement = calculateStats(cMeasurements);
    }
}
void PSTestCardControl::SelectCIC(bool pSelect)
{
    TC_PSFE cTC_PSFE(fUsbBus, fUsbDev);
    // enable CIC in mode of front-end hybrid
    if(pSelect)
        cTC_PSFE.mode_control(TC_PSFE::mode::CIC_IN);
    else
        cTC_PSFE.mode_control(TC_PSFE::mode::SSA_OUT);
}

#endif