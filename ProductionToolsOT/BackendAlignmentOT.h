/*!
 *
 * \file LinkAlignment.h
 * \brief Link alignment class, automated alignment procedure for CIC-lpGBT-BE
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef BackendAlignmentOT_h__
#define BackendAlignmentOT_h__

#include "OTTool.h"

using namespace Ph2_HwDescription;

class BackendAlignmentOT : public OTTool
{
  public:
    BackendAlignmentOT();
    ~BackendAlignmentOT();

    void Initialise();
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;

    // get alignment results
    bool getStatus() const { return fSuccess; }

    bool    WordAlignBEdata(const Ph2_HwDescription::BeBoard* pBoard);
    bool    PhaseAlignBEdata(const Ph2_HwDescription::BeBoard* pBoard);
    uint8_t getBeSamplingDelay(uint8_t pBoardIndx, uint8_t pOGIndx, uint8_t pHybridIndx, uint8_t pLineIndx)
    {
        auto& cBeSamplingDelay      = fBeSamplingDelay.at(pBoardIndx);
        auto& cBeSamplingDelayOG    = cBeSamplingDelay->at(pOGIndx);
        auto& cBeSamplingDelayHybrd = cBeSamplingDelayOG->at(pHybridIndx);
        return cBeSamplingDelayHybrd->getSummary<std::vector<uint8_t>>()[pLineIndx];
    }
    uint8_t getBeBitSlip(uint8_t pBoardIndx, uint8_t pOGIndx, uint8_t pHybridIndx, uint8_t pLineIndx)
    {
        auto& cBeBitSlip      = fBeBitSlip.at(pBoardIndx);
        auto& cBeBitSlipOG    = cBeBitSlip->at(pOGIndx);
        auto& cBeBitSlipHybrd = cBeBitSlipOG->at(pHybridIndx);
        return cBeBitSlipHybrd->getSummary<std::vector<uint8_t>>()[pLineIndx];
    }
    void                     AlignStubPackage();
    std::pair<bool, uint8_t> PhaseTuneLine(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId);                                                                         // generic
    std::pair<bool, uint8_t> WordAlignLine(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pAlignmentPattern, uint8_t pPeriod, uint8_t pSamplingDelay = 0); // generic
    bool                     L1WordAlignment(const Ph2_HwDescription::OpticalGroup* pOpticalGroup, bool pScope);
    void                     ManuallyConfigureLine(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pPhase, uint8_t pBitslip); // generic
    bool                     LineTuning(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pAlignmentPattern, uint8_t pPeriod);  // electrical
    void                     ManualPhaseScan(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pBitslip, uint8_t pPattern = 0xEA, uint8_t pStrt = 0, uint8_t pStop = 16);
    std::map<uint8_t, std::pair<bool, std::string>>
         ManualBitslipScan(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pSamplingDelay, uint8_t pPattern = 0xEA, uint8_t pStrt = 0, uint8_t pStop = 16);
    void CheckSparsified();

  protected:
  private:
    // Alignment parameters
    DetectorDataContainer fLpGBTSamplingDelay;
    DetectorDataContainer fBeSamplingDelay; // one per line per data line from hybrid
    DetectorDataContainer fBeBitSlip;       // one per line per data line from hybrid
    bool                  fStubDebug{false};
    bool                  fL1Debug{false};
    bool                  fAllowZeroBitslip{false};
    bool                  fSkipStubAlignment{false};
    size_t                fMaxAlignmentAttempts{10};
    size_t                fVerificationAttempts{1000};
    float                 fVerificationThreshold{0.9};

    bool VerifyAlignment(const Ph2_HwDescription::Chip* pChip, uint8_t pLineId, uint8_t pAlignmentPattern);
    bool PhaseAlignBEdata(const Ph2_HwDescription::OpticalGroup* pOpticalGroup);
    bool WordAlignBEdata(const Ph2_HwDescription::OpticalGroup* pOpticalGroup);
    bool WordAlignStubdata(const Ph2_HwDescription::OpticalGroup* pOpticalGroup);
    bool WordAlignL1data(const Ph2_HwDescription::OpticalGroup* pOpticalGroup);

    bool AlignStubPackage(Ph2_HwDescription::BeBoard* pBoard);
    bool FindStubLatency(Ph2_HwDescription::BeBoard* pBoard);
    bool Align();
};
#endif
