/*!
 *
 * \file Controller for PS hybrid test card
 *
 * \Support :
 *
 */

#if defined(__TCUSB__)
#define PSHYBRIDMAXV 1.32
#include "../Utils/ConsoleColor.h"
#include "../Utils/Utilities.h"
#include "../Utils/easylogging++.h"
#include "USB_a.h"
#include <map>
#include <stdint.h>
#include <string>

#ifndef PSTestCardControl_h__
#define PSTestCardControl_h__

class TC_PSFE;
class PSTestCardControl
{
  public:
    PSTestCardControl();
    ~PSTestCardControl();

    void Initialise(uint32_t pUsbBus, uint8_t pUsbDev);
    //
    void SetDefaultHybridVoltage();
    void ReadHybridVoltage(const std::string& pVoltageName);
    void ReadHybridCurrent(const std::string& pCurrentName);
    // Hybrid electrical test
    void ETest();
    //
    void SelectCIC(bool pSelect);
    void ConfigureUsb(uint32_t pUsbBus, uint8_t pUsbDev)
    {
        fUsbBus = pUsbBus;
        fUsbDev = pUsbDev;
    }

  private:
    // Maps for electrical measurements on the hybrid:
    std::map<std::string, TC_PSFE::measurement> fHybridVoltageMap = {
        {"TC_GND", TC_PSFE::measurement::GROUND},           {"ROH_GND", TC_PSFE::measurement::ROH_GND},          {"Hybrid1V00", TC_PSFE::measurement::_1V},
        {"Hybrid1V25", TC_PSFE::measurement::_1V25},        {"Hybrid1V25_out", TC_PSFE::measurement::_1V25_OUT}, {"Hybrid3V3", TC_PSFE::measurement::_3V3},
        {"HybridLoadV", TC_PSFE::measurement::_3V3_OUT},    {"VDrop2V55", TC_PSFE::measurement::_3V3_AMP},       {"ADC", TC_PSFE::measurement::AMUX},
        {"ROH_1V", TC_PSFE::measurement::_1V_OUT},          {"VDrop1V00", TC_PSFE::measurement::_1V_AMP},        {"VDrop1V25", TC_PSFE::measurement::_1V25_AMP},
        {"MPA1V_3", TC_PSFE::measurement::MPA_1V_3},        {"MPA_1V25A_3", TC_PSFE::measurement::MPA_1V25A_3},  {"ROH_GND_AMP", TC_PSFE::measurement::ROH_GND_AMP},
        {"MPA_1V25A_4", TC_PSFE::measurement::MPA_1V25A_4}, {"MPA_1V_4", TC_PSFE::measurement::MPA_1V_4},        {"Hybrid2V5", TC_PSFE::measurement::_2V5},
        {"Ref1V25", TC_PSFE::measurement::_1V25_REF},       {"RefV625", TC_PSFE::measurement::_625mV_REF},       {"AntennaPullUp", TC_PSFE::measurement::ANT_PULL}};
    std::map<std::string, TC_PSFE::measurement> fHybridCurrentMap          = {{"Hybrid1V00_current", TC_PSFE::measurement::ISEN_1V},
                                                                     {"Hybrid1V25_current", TC_PSFE::measurement::ISEN_1V25},
                                                                     {"Hybrid3V30_current", TC_PSFE::measurement::ISEN_3V3}};
    std::map<std::string, TC_PSFE::measurement> fHybridOtherMap            = {{"Temperature", TC_PSFE::measurement::THERM_SENSE},
                                                                   {"PGLineContinuity", TC_PSFE::measurement::C_TEST_PG},
                                                                   {"12VLineContinuity", TC_PSFE::measurement::C_TEST_P12},
                                                                   {"MPAContinuity", TC_PSFE::measurement::MPA_RST_TEST}};
    int                                         fVoltageMeasurementWait_ms = 100;
    int                                         fNreadings                 = 3;
    std::pair<float, float>                     fVoltageMeasurement;
    std::pair<float, float>                     fCurrentMeasurement;
    uint32_t                                    fUsbBus;
    uint8_t                                     fUsbDev;
};
#endif
#endif
