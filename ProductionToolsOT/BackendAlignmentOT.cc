#include "BackendAlignmentOT.h"
#include "../HWInterface/D19cBackendAlignmentFWInterface.h"
#include "../HWInterface/D19cDebugFWInterface.h"
#include "../Utils/CBCChannelGroupHandler.h"
#include "../Utils/ContainerFactory.h"
#include "D19cTriggerInterface.h"
//#include "boost/format.hpp"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

BackendAlignmentOT::BackendAlignmentOT() : OTTool() {}
BackendAlignmentOT::~BackendAlignmentOT() {}

// Processing
void BackendAlignmentOT::AlignStubPackage()
{
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getBoardType() != BoardType::D19C)
        {
            LOG(INFO) << BOLDYELLOW << "No need to align stub package for BeBoard#" << +cBoard->getId() << ".. not a uDTC" << RESET;
            continue;
        }
        AlignStubPackage(cBoard);
        // FindStubLatency(cBoard);
    }
    // align stubs
}
bool BackendAlignmentOT::Align()
{
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::Align ..." << RESET;
    for(const auto cBoard: *fDetectorContainer)
    {
        // force trigger source to be internal triggers
        LOG(INFO) << BOLDYELLOW << "Forcing trigger source to internal triggers" << RESET;
        fBeBoardInterface->WriteBoardReg(cBoard, "fc7_daq_cnfg.fast_command_block.trigger_source", 3);
        fBeBoardInterface->WriteBoardReg(cBoard, "fc7_daq_cnfg.ttc.ttc_enable", 0);
        for(auto cOpticalGroup: *cBoard)
        {
            if(cBoard->isOptical() == 0) PhaseAlignBEdata(cOpticalGroup);
            WordAlignBEdata(cOpticalGroup);
        }
        // check that word alignment of L1 data worked
        LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::Align ... trying to readout L1 data.. " << RESET;
    } // align BE
    // CheckSparsified();

    if(!fSkipStubAlignment) AlignStubPackage();
    fSuccess = true;
    return fSuccess;
}
void BackendAlignmentOT::CheckSparsified()
{
    bool cSparsified = true;
    for(const auto cBoard: *fDetectorContainer)
    {
        cBoard->setSparsification(cSparsified);
        fBeBoardInterface->WriteBoardReg(cBoard, "fc7_daq_cnfg.physical_interface_block.cic.2s_sparsified_enable", cSparsified ? 1 : 0);
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;
                fCicInterface->SetSparsification(cCic, cSparsified ? 1 : 0);
            }
        }
        ReadNEvents(cBoard, 1);
    }
    const std::vector<Event*>& cEvents = this->GetEvents();
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::Align ... readout.. " << +cEvents.size() << " events." << RESET;
}
// Initialization function
void BackendAlignmentOT::Initialise()
{
    // prepare common OTTool
    SetName("BackendAlignmentOT");
    Prepare();

    // list of board registers that can be modified by this tool
    std::vector<std::string> cBrdRegsToKeep{"fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9"};
    cBrdRegsToKeep.push_back("fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11");
    SetBrdRegstoPerserve(cBrdRegsToKeep);

    // no Chip registers to perserve

    // initialize containers that hold values found by this tool
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fBeSamplingDelay);
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fBeBitSlip);
    ContainerFactory::copyAndInitHybrid<uint8_t>(*fDetectorContainer, fLpGBTSamplingDelay);
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cBeSamplingDelay = fBeSamplingDelay.at(cBoard->getIndex());
        auto& cBeBitSlip       = fBeBitSlip.at(cBoard->getIndex());
        auto& cLinkSampling    = fLpGBTSamplingDelay.at(cBoard->getIndex());

        for(auto cOpticalGroup: *cBoard)
        {
            auto&  cBeSamplingDelayOG = cBeSamplingDelay->at(cOpticalGroup->getIndex());
            auto&  cBeBitSlipOG       = cBeBitSlip->at(cOpticalGroup->getIndex());
            auto&  cLinkDelayOG       = cLinkSampling->at(cOpticalGroup->getIndex());
            size_t cNlines            = (cOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS) ? 7 : 6;
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cBeSamplingDelayHybrd = cBeSamplingDelayOG->at(cHybrid->getIndex());
                auto& cBeBitSlipHybrd       = cBeBitSlipOG->at(cHybrid->getIndex());
                auto& cThisBeSamplingDelay  = cBeSamplingDelayHybrd->getSummary<std::vector<uint8_t>>();
                auto& cThisBeBitSlip        = cBeBitSlipHybrd->getSummary<std::vector<uint8_t>>();
                auto& cLinkDelay            = cLinkDelayOG->at(cHybrid->getIndex())->getSummary<uint8_t>();
                cLinkDelay                  = 0;
                for(size_t cLineId = 0; cLineId < cNlines; cLineId++)
                {
                    cThisBeSamplingDelay.push_back(0xF);
                    cThisBeBitSlip.push_back(0xF);
                }
            }
        }
    }
}

// Word align L1 + stub data in the backend
bool BackendAlignmentOT::WordAlignBEdata(const BeBoard* pBoard)
{
    bool cAligned = true;
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::WordAlignBEdata" << RESET;
    for(auto cOpticalGroup: *pBoard)
    {
        cAligned = WordAlignBEdata(cOpticalGroup);
        if(!cAligned)
        {
            LOG(INFO) << BOLDRED << "Could not word align-BE data for BeBoard#" << +pBoard->getId() << " Link#" << +cOpticalGroup->getId() << RESET;
            // throw std::runtime_error(std::string("Could not word align-BE data in BackendAlignmentOT..."));
            return cAligned;
        }
    } // stub lines optical groups connected to this  board
    return cAligned;
}
bool BackendAlignmentOT::WordAlignBEdata(const OpticalGroup* pOpticalGroup)
{
    bool cStubAligned = WordAlignStubdata(pOpticalGroup);
    if(!cStubAligned) return cStubAligned;

    return WordAlignL1data(pOpticalGroup);
}
bool BackendAlignmentOT::WordAlignStubdata(const OpticalGroup* pOpticalGroup)
{
    fStubDebug      = true;
    bool cAligned   = false;
    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    if((*cBoardIter)->getBoardType() != BoardType::D19C) return true;

    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::WordAlignBEdata for OG#" << +pOpticalGroup->getId() << RESET;

    auto& cBeBitSlip   = fBeBitSlip.at((*cBoardIter)->getIndex());
    auto& cBeBitSlipOG = cBeBitSlip->at(pOpticalGroup->getIndex());

    // sampling delay
    auto& cBeSamplingDelay   = fBeSamplingDelay.at((*cBoardIter)->getIndex());
    auto& cBeSamplingDelayOG = cBeSamplingDelay->at(pOpticalGroup->getIndex());

    // configure CICs to output alignment pattern on stub lines
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == nullptr) continue;

        // disable alignment output
        fCicInterface->SelectOutput(cCic, true);
    }
    // align stub lines in the BE
    size_t cNStublines = (pOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS || pOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS) ? 6 : 5;
    LOG(INFO) << BOLDMAGENTA << "BackendAlignmentOT::WordAlignBEdata ... word alignment on " << +cNStublines << "/6 lines stub from CIC.." << RESET;
    for(size_t cLineId = 0; cLineId < cNStublines; cLineId++)
    {
        for(auto cHybrid: *pOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            if(cCic == nullptr) continue;

            auto& cBeBitSlipHybrd = cBeBitSlipOG->at(cHybrid->getIndex());
            auto& cThisBeBitSlip  = cBeBitSlipHybrd->getSummary<std::vector<uint8_t>>();

            auto& cBeSamplingDelayHybrd = cBeSamplingDelayOG->at(cHybrid->getIndex());
            auto& cThisBeSamplingDelay  = cBeSamplingDelayHybrd->getSummary<std::vector<uint8_t>>();

            // LOG(INFO) << BOLDMAGENTA << "Aligning Stub line#" << +cLineId << " on Hybrid#" << +cHybrid->getId() << RESET;
            cAligned         = false;
            size_t cAttempts = 0;
            do {
                auto cLnSt                  = WordAlignLine(cCic, 1 + cLineId, 0xEA, 8, cThisBeSamplingDelay[1 + cLineId]);
                cAligned                    = cLnSt.first;
                cThisBeBitSlip[1 + cLineId] = cLnSt.second;
                if(!cAligned)
                {
                    LOG(INFO) << BOLDRED << "Could not word align-BE data for BeBoard#" << +cBoardId << " Link#" << +pOpticalGroup->getId() << " stub line " << +(cLineId - 1) << RESET;
                    // throw std::runtime_error(std::string("Could not word align-BE data in BackendAlignmentOT..."));
                }
                cAttempts++;
            } while(!cAligned && cAttempts < fMaxAlignmentAttempts);
        }
    }
    // print mode
    for(auto cHybrid: *pOpticalGroup)
    {
        auto&                cBeBitSlipHybrd = cBeBitSlipOG->at(cHybrid->getIndex());
        auto&                cThisBeBitSlip  = cBeBitSlipHybrd->getSummary<std::vector<uint8_t>>();
        std::vector<uint8_t> cBitSlipHist(15, 0);
        for(auto cItem: cThisBeBitSlip) cBitSlipHist[cItem]++;
        auto cMode = std::max_element(cBitSlipHist.begin(), cBitSlipHist.end()) - cBitSlipHist.begin();
        LOG(INFO) << BOLDMAGENTA << "Hybrid#" << +cHybrid->getId() << " most frequent bitslip is " << +cMode << RESET;
    }

    // disable stub output
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        fCicInterface->SelectOutput(cCic, false);
    }
    return cAligned;
}
bool BackendAlignmentOT::WordAlignL1data(const OpticalGroup* pOpticalGroup)
{
    // align L1 data in the BE
    fL1Debug = false;
    LOG(INFO) << BOLDMAGENTA << "BackendAlignmentOT::WordAlignBEdata ... word alignment on L1 lines from CIC.." << RESET;
    bool cAligned = L1WordAlignment(pOpticalGroup, fL1Debug);
    LOG(INFO) << BOLDYELLOW << "Reached end of WordAlignBEData" << RESET;
    return cAligned;
}

// Phase align L1 + stub data in the backend
bool BackendAlignmentOT::PhaseAlignBEdata(const BeBoard* pBoard)
{
    bool cAligned = true;
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            cAligned = PhaseAlignBEdata(cOpticalGroup);
            // if(!cAligned) { throw std::runtime_error(std::string("Could not phase align-BE data in BackendAlignmentOT...")); }
        }
    }
    return cAligned;
}
bool BackendAlignmentOT::PhaseAlignBEdata(const OpticalGroup* pOpticalGroup)
{
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::PhaseAlignBEdata OG#" << +pOpticalGroup->getId() << RESET;
    bool cAligned   = true;
    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());

    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();

    auto& cBeSamplingDelay   = fBeSamplingDelay.at((*cBoardIter)->getIndex());
    auto& cBeSamplingDelayOG = cBeSamplingDelay->at(pOpticalGroup->getIndex());
    // configure CICs to output alignment pattern on stub lines
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        // disable alignment output
        fCicInterface->SelectOutput(cCic, true);
    }
    // stop triggers to make sure that there are no L1 packets from the CIC
    fBeBoardInterface->Stop((*cBoardIter));

    // align stub lines in the BE
    // fix me - setting OuterTrackerPS when no lpGBT?
    size_t cNStublines = (pOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS || pOpticalGroup->getFrontEndType() == FrontEndType::HYBRIDPS) ? 6 : 5;
    LOG(INFO) << BOLDMAGENTA << "BackendAlignmentOT::PhaseAlignBEdata ... finding optimal sampling phase on " << +(cNStublines) << " stub  lines and 1 L1 line from CIC.." << RESET;
    for(size_t cLineIndx = 1; cLineIndx < 1 + cNStublines; cLineIndx++)
    {
        for(auto cHybrid: *pOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            if(cCic == nullptr) continue;
            auto& cBeSamplingDelayHybrd = cBeSamplingDelayOG->at(cHybrid->getIndex());
            auto& cThisBeSamplingDelay  = cBeSamplingDelayHybrd->getSummary<std::vector<uint8_t>>();

            auto cLnSt = PhaseTuneLine(cCic, cLineIndx);
            if(cLineIndx > 0)
                LOG(INFO) << BOLDMAGENTA << "Setting sampling delay on Stub line#" << +cLineIndx << " on Hybrid#" << +cHybrid->getId() << RESET;
            else
                LOG(INFO) << BOLDMAGENTA << "Setting sampling delay on L1A line on Hybrid#" << +cHybrid->getId() << RESET;
            cAligned                        = cLnSt.first;
            cThisBeSamplingDelay[cLineIndx] = cLnSt.second;
        } // loop over all hybrids
    }     // loop over all lines

    // configure CICs to NOT output alignment pattern on stub lines
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        // disable alignment output
        fCicInterface->SelectOutput(cCic, false);
    }
    return cAligned;
}
std::map<uint8_t, std::pair<bool, std::string>> BackendAlignmentOT::ManualBitslipScan(const Chip* pChip, uint8_t pLineId, uint8_t pSamplingDelay, uint8_t pPattern, uint8_t pStrt, uint8_t pStop)
{
    auto cBoardId   = pChip->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto                  cInterface      = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();
    std::stringstream     cExpected;
    cExpected << std::bitset<8>(pPattern);
    std::map<uint8_t, std::pair<bool, std::string>> cMap;
    for(uint8_t cBitSlip = 0; cBitSlip <= 0xF; cBitSlip++)
    {
        ManuallyConfigureLine(pChip, pLineId, pSamplingDelay, cBitSlip);
        cDebugInterface->StubDebug(true, 4, false);
        auto                         cDebugData = cDebugInterface->GetStubDebug(pLineId);
        std::pair<bool, std::string> cRslt;
        cRslt.first    = cDebugData.find(cExpected.str()) != std::string::npos;
        cRslt.second   = cDebugData;
        cMap[cBitSlip] = cRslt;
    }
    return cMap;
}
void BackendAlignmentOT::ManualPhaseScan(const Chip* pChip, uint8_t pLineId, uint8_t pBitslip, uint8_t pPattern, uint8_t pStrt, uint8_t pStop)
{
    auto cBoardId   = pChip->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto                  cInterface      = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();
    // std::stringstream cExpected; cExpected << std::bitset<8>(pPattern);
    std::vector<uint8_t> cGoodPhases(0);
    LOG(INFO) << BOLDYELLOW << "Manual phase scan on SLVSLine#" << +pLineId << RESET;
    for(uint8_t cSamplingPhase = pStrt; cSamplingPhase < pStop; cSamplingPhase++)
    {
        LOG(INFO) << BOLDYELLOW << "\tSampling phase of " << +cSamplingPhase << RESET;
        ManuallyConfigureLine(pChip, pLineId, cSamplingPhase, pBitslip);
        auto cResult = cDebugInterface->CheckData(pChip, pLineId, pPattern);
        if(cResult.fErrors.size() == 0) cGoodPhases.push_back(cSamplingPhase);
    }
    LOG(INFO) << BOLDYELLOW << "Found " << cGoodPhases.size() << " good phases on SLVSLine#" << +pLineId << RESET;
    if(cGoodPhases.size() > 0)
    {
        // set first good phase found
        LOG(INFO) << BOLDYELLOW << "\tSampling phase of " << +cGoodPhases[cGoodPhases.size() - 1] << RESET;
        ManuallyConfigureLine(pChip, pLineId, cGoodPhases[cGoodPhases.size() - 1], pBitslip);
        cDebugInterface->CheckData(pChip, pLineId, pPattern);
    }
}
std::pair<bool, uint8_t> BackendAlignmentOT::PhaseTuneLine(const Chip* pChip, uint8_t pLineId)
{
    std::pair<bool, uint8_t> cLineStatus;
    cLineStatus.first  = false;
    cLineStatus.second = 0;

    auto cBoardId   = pChip->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::PhaseTuneLine#" << +pLineId << " for a Chip#" << +pChip->getId() << RESET;
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());

    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(true);

    AlignerObject cAlignerObjct;
    cAlignerObjct.fHybrid  = pChip->getHybridId();
    cAlignerObjct.fChip    = (pChip->getFrontEndType() == FrontEndType::CIC || pChip->getFrontEndType() == FrontEndType::CIC2) ? 0 : pChip->getId() % 8;
    cAlignerObjct.fLine    = pLineId;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    LineConfiguration cLineCnfg;
    cLineCnfg.fPattern       = (pLineId == 0) ? 0xAA : 0xFE;
    cLineCnfg.fPatternPeriod = 8;
    cLineCnfg.fBitslip       = 0;
    cLineCnfg.fDelay         = 0;
    auto cReply              = cAlignerInterface->TunePhase(cAlignerObjct, cLineCnfg);
    cLineStatus.first        = cReply.fSuccess;
    cLineStatus.second       = cReply.fCnfg.fDelay;
    if(!cLineStatus.first)
    {
        LOG(INFO) << BOLDRED << "Could not phase align-BE data for BeBoard#" << +cBoardId << " Board#" << +pChip->getBeBoardId() << " Hybrid#" << +pChip->getHybridId() << " Chip#" << +pChip->getId()
                  << " line# " << +pLineId << RESET;
        // throw std::runtime_error(std::string("Could not phase align-BE data in BackendAlignmentOT..."));
    }
    return cLineStatus;
}
bool BackendAlignmentOT::VerifyAlignment(const Chip* pChip, uint8_t pLineId, uint8_t pAlignmentPattern)
{
    auto cBoardId   = pChip->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    // retrieve alignment constants
    auto& cBeSamplingDelay = fBeSamplingDelay.at((*cBoardIter)->getIndex());
    auto& cBeBitSlip       = fBeBitSlip.at((*cBoardIter)->getIndex());
    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // sampling delay
    auto& cBeSamplingDelayOG    = cBeSamplingDelay->at(cOpticalGroupIndx);
    auto& cBeSamplingDelayHybrd = cBeSamplingDelayOG->at(cHybridIndx);
    auto& cThisBeSamplingDelay  = cBeSamplingDelayHybrd->getSummary<std::vector<uint8_t>>();
    // bit slip
    auto& cBeBitSlipOG    = cBeBitSlip->at(cOpticalGroupIndx);
    auto& cBeBitSlipHybrd = cBeBitSlipOG->at(cHybridIndx);
    auto& cThisBeBitSlip  = cBeBitSlipHybrd->getSummary<std::vector<uint8_t>>();
    LOG(DEBUG) << BOLDYELLOW << "Verification of alignment on Line#" << +pLineId << " for a bitslip of " << +cThisBeBitSlip[pLineId] << " and a sampling delay of " << +cThisBeSamplingDelay[pLineId]
               << RESET;
    // apply alignment values
    // quick scan to chcek
    auto                  cChipId         = (pChip->getFrontEndType() == FrontEndType::CIC || pChip->getFrontEndType() == FrontEndType::CIC2) ? 0 : pChip->getId() % 8;
    D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();
    fBeBoardInterface->WriteBoardReg((*cBoardIter), "fc7_daq_cnfg.physical_interface_block.slvs_debug.hybrid_select", pChip->getHybridId());
    fBeBoardInterface->WriteBoardReg((*cBoardIter), "fc7_daq_cnfg.physical_interface_block.slvs_debug.chip_select", cChipId);
    // LOG (INFO) << BOLDYELLOW << "Manual scan of bitlsip on line " << RESET;
    // for(uint8_t cBitSlip=0; cBitSlip<0xF;cBitSlip++)
    // {
    //     ManuallyConfigureLine(pChip,pLineId,cThisBeSamplingDelay[pLineId], cBitSlip);
    //     for(size_t cIter = 0 ; cIter < 5; cIter++)
    //     {
    //         if( pLineId > 0 )  cDebugInterface->StubDebug(true, 6, false);
    //         else cDebugInterface->L1ADebug(100,false);
    //         auto cDebugData = cDebugInterface->GetStubDebug(pLineId);
    //         LOG (INFO) << BOLDYELLOW << "Bitslip is " << +cBitSlip << " : " << cDebugData << RESET;
    //     }
    // }
    // exit(0);

    std::stringstream cExpected;
    cExpected << std::bitset<8>(pAlignmentPattern);
    ManuallyConfigureLine(pChip, pLineId, cThisBeSamplingDelay[pLineId], cThisBeBitSlip[pLineId]);
    LOG(DEBUG) << BOLDYELLOW << "Line#" << +pLineId << " Sampling delay of " << +cThisBeSamplingDelay[pLineId] << " Bitslip of " << +cThisBeBitSlip[pLineId] << RESET;
    std::vector<uint8_t> cChecks(0);
    for(size_t cTrial = 0; cTrial < fVerificationAttempts; cTrial++)
    {
        if(pLineId > 0)
            cDebugInterface->StubDebug(true, 6, false);
        else
            cDebugInterface->L1ADebug(100, false);
        auto cDebugData   = cDebugInterface->GetStubDebug(pLineId);
        bool cFindPattern = cDebugData.find(cExpected.str()) != std::string::npos;
        cChecks.push_back(cFindPattern ? 1 : 0);
        // if( cFindPattern ) LOG (INFO) << BOLDGREEN << "\t..Pattern found" << RESET;
        // else    LOG (INFO) << BOLDRED << "\t..Pattern not found" << RESET;
    }
    bool cFindPattern = std::accumulate(cChecks.begin(), cChecks.end(), 0) > fVerificationThreshold * fVerificationAttempts;
    if(cFindPattern)
        LOG(DEBUG) << BOLDGREEN << "Word aligned BE-data for  BeBoard#" << +cBoardId << " Board#" << +pChip->getBeBoardId() << " Hybrid#" << +pChip->getHybridId() << " Chip#" << +pChip->getId()
                   << " line# " << +pLineId << " Sampling delay is " << +cThisBeSamplingDelay[pLineId] << " Bitslip on line is " << +cThisBeBitSlip[pLineId] << " found pattern in "
                   << std::accumulate(cChecks.begin(), cChecks.end(), 0) << " out of " << cChecks.size()
                   << " checks."
                   //  << RESET;
                   // << BOLDYELLOW << "\n Expect to see "
                   // << cExpected.str() << " in debug data : " << BOLDGREEN << "\t Stub debug data is : " << cDebugData
                   << RESET;
    else
    {
        LOG(DEBUG) << BOLDRED << "FAILED to word align BE-data for  BeBoard#" << +cBoardId << " Board#" << +pChip->getBeBoardId() << " Hybrid#" << +pChip->getHybridId() << " Chip#" << +pChip->getId()
                   << " line# " << +pLineId << " Sampling delay is " << +cThisBeSamplingDelay[pLineId] << " Bitslip on line is " << +cThisBeBitSlip[pLineId] << " found pattern in "
                   << std::accumulate(cChecks.begin(), cChecks.end(), 0) << " out of " << cChecks.size() << " checks... will try to re-align" << RESET;
    }
    return cFindPattern;
}
std::pair<bool, uint8_t> BackendAlignmentOT::WordAlignLine(const Chip* pChip, uint8_t pLineId, uint8_t pAlignmentPattern, uint8_t pPeriod, uint8_t pSamplingDelay)
{
    auto cBoardId   = pChip->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());

    auto& cBeBitSlip = fBeBitSlip.at((*cBoardIter)->getIndex());
    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop

    // bit slip
    auto& cBeBitSlipOG    = cBeBitSlip->at(cOpticalGroupIndx);
    auto& cBeBitSlipHybrd = cBeBitSlipOG->at(cHybridIndx);
    auto& cThisBeBitSlip  = cBeBitSlipHybrd->getSummary<std::vector<uint8_t>>();

    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->EnablePrintout(false);
    auto cLineStatus = cAlignerInterface->WordAlignLine(pChip, pLineId, pAlignmentPattern, pPeriod, pSamplingDelay, (*cBoardIter)->isOptical());
    if(!cLineStatus.first)
    {
        LOG(INFO) << BOLDRED << "Could not word align-BE data for BeBoard#" << +cBoardId << " Board#" << +pChip->getBeBoardId() << " Hybrid#" << +pChip->getHybridId() << " Chip#" << +pChip->getId()
                  << " line# " << +pLineId << RESET;
    }
    // here if it has worked set alignment value and check
    else
    {
        // record bitslip for this line
        cThisBeBitSlip[pLineId] = cLineStatus.second;
        size_t cIter            = 0;
        size_t cMaxAttempts     = 10;
        bool   cSuccess         = false;
        do {
            cSuccess = VerifyAlignment(pChip, pLineId, pAlignmentPattern);
            if(!cSuccess)
            {
                cLineStatus = cAlignerInterface->WordAlignLine(pChip, pLineId, pAlignmentPattern, pPeriod, pSamplingDelay, (*cBoardIter)->isOptical());
                if(cIter == cMaxAttempts - 1)
                    LOG(ERROR) << BOLDRED << "Verification of word alignment failed on Link#" << +pChip->getOpticalGroupId() << " Line#" << +pLineId << " FE#" << +pChip->getId() << " try again."
                               << RESET;
                if(!cLineStatus.first)
                {
                    cIter    = cMaxAttempts;
                    cSuccess = true;
                }
                else
                    cThisBeBitSlip[pLineId] = cLineStatus.second; // update bitslip
            }
            cIter++;
        } while(!cSuccess && cIter < cMaxAttempts);
    }
    // cLineStatus.first  = cReply.fSuccess;
    // cLineStatus.second = cLineStatus.fBitslip;
    return cLineStatus;
}
void BackendAlignmentOT::ManuallyConfigureLine(const Chip* pChip, uint8_t pLineId, uint8_t pPhase, uint8_t pBitslip)
{
    LOG(DEBUG) << BOLDYELLOW << "BackendAlignmentOT::ManuallyConfigureLine" << RESET;
    auto cBoardId   = pChip->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(false);
    AlignerObject cAlignerObjct;
    cAlignerObjct.fHybrid  = pChip->getHybridId();
    cAlignerObjct.fChip    = (pChip->getFrontEndType() == FrontEndType::CIC || pChip->getFrontEndType() == FrontEndType::CIC2) ? 0 : pChip->getId() % 8;
    cAlignerObjct.fLine    = pLineId;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical();
    LineConfiguration cLineCnfg;
    cLineCnfg.fDelay   = pPhase;
    cLineCnfg.fBitslip = pBitslip;
    LOG(DEBUG) << BOLDYELLOW << "Manual alignment of line .. bit slip is " << +cLineCnfg.fBitslip << " and sampling delay is " << +cLineCnfg.fDelay << RESET;
    cAlignerInterface->ManuallyConfigureLine(cAlignerObjct, cLineCnfg);
}
bool BackendAlignmentOT::LineTuning(const Chip* pChip, uint8_t pLineId, uint8_t pAlignmentPattern, uint8_t pPeriod)
{
    LOG(INFO) << BOLDRED << "BackendAlignmentOT::LineTuning DEPRECATED" << RESET;
    return true;
}
bool BackendAlignmentOT::L1WordAlignment(const OpticalGroup* pOpticalGroup, bool pScope)
{
    // back-end tuning on l1 lines
    uint8_t  cLineId  = 0;
    uint16_t cPattern = 0xFE;
    //
    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::L1WordAlignment " << RESET;
    bool                             cSparsified       = (*cBoardIter)->getSparsification();
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    D19cDebugFWInterface*            cDebugInterface   = cInterface->getDebugInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(false);
    bool cSuccess = true;

    // configure triggers
    // make sure you're only sending one trigger at a time
    std::vector<std::pair<std::string, uint32_t>> cVecReg;
    cVecReg.clear();
    std::vector<std::string> cFcmdRegs{"misc.trigger_multiplicity", "user_trigger_frequency", "trigger_source", "misc.backpressure_enable", "triggers_to_accept"};
    std::vector<uint16_t>    cFcmdRegVals{0, 100, 3, 0, 0};
    std::vector<uint8_t>     cFcmdRegOrigVals(0);
    for(size_t cIndx = 0; cIndx < cFcmdRegs.size(); cIndx++)
    {
        std::string cRegName = "fc7_daq_cnfg.fast_command_block." + cFcmdRegs[cIndx];
        cVecReg.push_back({cRegName, cFcmdRegVals[cIndx]});
    }
    cVecReg.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
    cVecReg.push_back({"fc7_daq_cnfg.tlu_block.tlu_enabled", 0x0});
    cVecReg.push_back({"fc7_daq_cnfg.readout_block.global.data_handshake_enable", 0x1});
    fBeBoardInterface->WriteBoardMultReg(*cBoardIter, cVecReg);

    auto& cBeBitSlip   = fBeBitSlip.at((*cBoardIter)->getIndex());
    auto& cBeBitSlipOG = cBeBitSlip->at(pOpticalGroup->getIndex());

    // sampling delay
    auto& cBeSamplingDelay   = fBeSamplingDelay.at((*cBoardIter)->getIndex());
    auto& cBeSamplingDelayOG = cBeSamplingDelay->at(pOpticalGroup->getIndex());

    LOG(INFO) << BOLDBLUE << "Aligning the back-end to properly decode L1A data coming from the front-end objects." << RESET;
    // fBeBoardInterface->ChipReSync(*cBoardIter);
    fBeBoardInterface->Stop(*cBoardIter);
    // turn on sparsification to get as many headers as possible
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == nullptr) continue;
        fCicInterface->SetSparsification(cCic, true);
    }
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == nullptr) continue;
        if(cCic->isOptical()) continue;

        auto  cLineStatus             = PhaseTuneLine(cCic, cLineId);
        auto& cBeSamplingDelayHybrd   = cBeSamplingDelayOG->at(cHybrid->getIndex());
        auto& cThisBeSamplingDelay    = cBeSamplingDelayHybrd->getSummary<std::vector<uint8_t>>();
        cThisBeSamplingDelay[cLineId] = cLineStatus.first ? cLineStatus.second : 15;
    } // if electrical readout.. run phase aligner on L1 line when triggers have been stopped

    std::vector<uint8_t> cFeEnableRegs(0);
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        cFeEnableRegs.push_back(fCicInterface->ReadChipReg(cCic, "FE_ENABLE"));
        fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, false);
    } // disable all FEs from CIC so you only receive L1 headers

    fBeBoardInterface->Start(*cBoardIter);
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == nullptr)
        {
            LOG(INFO) << BOLDYELLOW << " No CIC to use for L1 Word alignment..." << RESET;
            continue;
        }

        auto& cBeBitSlipHybrd = cBeBitSlipOG->at(cHybrid->getIndex());
        auto& cThisBeBitSlip  = cBeBitSlipHybrd->getSummary<std::vector<uint8_t>>();

        auto& cBeSamplingDelayHybrd = cBeSamplingDelayOG->at(cHybrid->getIndex());
        auto& cThisBeSamplingDelay  = cBeSamplingDelayHybrd->getSummary<std::vector<uint8_t>>();

        LOG(INFO) << BOLDBLUE << "Performing word alignment [in the back-end] to prepare for receiving CIC L1A data ...: FE " << +cHybrid->getId() << " CIC#" << +cCic->getId() << RESET;
        // configure pattern
        LOG(INFO) << BOLDBLUE << "BackendAlignmentOT::L1WordAlignment for CIC data" << RESET;

        std::pair<bool, uint8_t> cStThsHybrd;
        cStThsHybrd.first  = false;
        cStThsHybrd.second = 0;
        for(uint16_t cPatternLength = 40; cPatternLength < 41; cPatternLength++)
        {
            if(cStThsHybrd.first) continue;
            LOG(INFO) << BOLDYELLOW << "Trying to align data with patttern length " << +cPatternLength << RESET;
            cStThsHybrd = WordAlignLine(cCic, cLineId, cPattern, cPatternLength, cThisBeSamplingDelay[cLineId]);
            if(cStThsHybrd.first) cThisBeBitSlip[cLineId] = cStThsHybrd.second;
        }
        // check alignment
        if(!cStThsHybrd.first)
        {
            LOG(INFO) << BOLDYELLOW << "Checking..." << RESET;
            for(uint8_t cBitSlip = 0; cBitSlip < 0xF; cBitSlip++)
            {
                cThisBeBitSlip[cLineId] = cBitSlip;
                if(VerifyAlignment(cCic, cLineId, cPattern)) break;
            }
            LOG(INFO) << BOLDYELLOW << "Manually found bitslip of " << +cThisBeBitSlip[cLineId] << RESET;
        }
    }
    fBeBoardInterface->Stop(*cBoardIter);
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == nullptr || !pScope) continue;

        // select lines for slvs debug
        fBeBoardInterface->WriteBoardReg(*cBoardIter, "fc7_daq_cnfg.physical_interface_block.slvs_debug.hybrid_select", cHybrid->getId());
        fBeBoardInterface->WriteBoardReg(*cBoardIter, "fc7_daq_cnfg.physical_interface_block.slvs_debug.chip_select", 0);
        cDebugInterface->L1ADebug();
    }

    size_t cIndx = 0;
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        fCicInterface->SetSparsification(cCic, cSparsified ? 1 : 0);
        fCicInterface->WriteChipReg(cCic, "FE_ENABLE", cFeEnableRegs[cIndx]);
        cIndx++;
    } // reselect FEs

    return cSuccess;
}
// really simple package alignment
bool BackendAlignmentOT::AlignStubPackage(BeBoard* pBoard)
{
    LOG(INFO) << BOLDYELLOW << "BackendAlignmentOT::AlignStubPackage For BeBoard#" << +pBoard->getId() << RESET;
    bool cVerify = true;
    fBeBoardInterface->setBoard(pBoard->getId());
    std::vector<std::pair<std::string, uint32_t>> cVecReg;
    cVecReg.clear();
    cVecReg.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", 0x3});
    cVecReg.push_back({"fc7_daq_cnfg.ttc.ttc_enable", 0x0});
    cVecReg.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
    fBeBoardInterface->WriteBoardMultReg(pBoard, cVecReg);

    // when doing this want to make sure all stubs are disabled
    // I think I can just disable the FEs?
    std::vector<uint8_t> cFeEnableRegs(0);
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            cFeEnableRegs.push_back(fCicInterface->ReadChipReg(cCic, "FE_ENABLE"));
            fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, false);
        } // disable all FEs from CIC so you only receive L1 headers
    }

    std::vector<uint16_t> cHybridIds(0);
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            uint16_t cId = (cOpticalGroup->getId() << 8) | (cHybrid->getId());
            cHybridIds.push_back(cId);
        }
    }

    std::map<uint8_t, uint8_t>      cGoodPackageDelays;
    std::map<std::string, uint32_t> cConfigurationValues;
    cConfigurationValues["fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9"]   = 0x00;
    cConfigurationValues["fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11"] = 0x00;
    for(auto cOpticalGroup: *pBoard)
    {
        LOG(DEBUG) << BOLDYELLOW << "For hybrids connected to Link#" << +cOpticalGroup->getId() << RESET;
        std::map<uint8_t, bool> cAlignmentMap;
        std::string             cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9";
        if(cOpticalGroup->getId() > 9) cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11";
        for(uint8_t cPackageDelay = 0; cPackageDelay < 8; cPackageDelay++)
        {
            // LOG (INFO) << BOLDYELLOW << "Package delay of " << +cPackageDelay << " on link#" << +cOpticalGroup->getId() << RESET;
            uint32_t cRegValue = (cPackageDelay << cOpticalGroup->getId() % 10 * 3);

            fBeBoardInterface->WriteBoardReg(pBoard, cRegName, cRegValue);
            fBeBoardInterface->ChipReSync(pBoard);
            // read events and fill vector
            LOG(INFO)
            
            ReadNEvents(pBoard, 10);
            LOG(INFO)
            const std::vector<Event*>& cEvents = this->GetEvents();
            // BxIds for hybrids on this link
            std::map<uint8_t, std::vector<size_t>> cBxIdsThisDelay;
            cAlignmentMap[cPackageDelay] = true;
            for(auto& cEvent: cEvents)
            {
                int               cBxDifference = 0;
                std::stringstream cOut;
                int               cNHybrids = 0;
                for(auto cId: cHybridIds)
                {
                    if((cId >> 8) != cOpticalGroup->getId()) continue;
                    auto cBxId = cEvent->BxId(cId & 0xFF);
                    cBxIdsThisDelay[cId & 0xFF].push_back(cBxId);
                    // LOG (INFO) << BOLDYELLOW << "\t.. BxId " << cBxId << " Hybrid#" << +(cId&0xFF) << RESET;

                    if(cNHybrids == 0)
                        cBxDifference = cBxId;
                    else
                        cBxDifference = cBxDifference - cBxId;
                    cNHybrids++;
                } // all hybrid ids from all links
                cAlignmentMap[cPackageDelay] = cAlignmentMap[cPackageDelay];
                if(cNHybrids > 1) cAlignmentMap[cPackageDelay] = cAlignmentMap[cPackageDelay] && (cBxDifference == 0);
            } // events from the readout

            // check BxIds from each hybrid
            // for a given hybrid not all can be the same
            if(!cAlignmentMap[cPackageDelay])
            {
                LOG(INFO) << BOLDRED << "\t... Pkg Delay of " << +cPackageDelay << " Mismatch in BxIds for hybrids on the same link!!!" << RESET;
                continue;
            }
            else
                LOG(DEBUG) << BOLDYELLOW << "\t... Matching BxIds for hybrids on the same link!!!" << RESET;
            for(auto cId: cHybridIds)
            {
                if((cId >> 8) != cOpticalGroup->getId()) continue;
                std::stringstream cOut;
                cOut << "Hybrid#" << (cId & 0xFF) << "\t.. Package delay of " << +cPackageDelay << " -- reg value " << std::bitset<32>(cRegValue);
                for(auto cBxId: cBxIdsThisDelay[cId & 0xFF]) { cOut << " BxId" << cBxId << ", "; } // loop over BxIds from the readout events
                sort(cBxIdsThisDelay[cId & 0xFF].begin(), cBxIdsThisDelay[cId & 0xFF].end());
                auto cIter = std::unique(cBxIdsThisDelay[cId & 0xFF].begin(), cBxIdsThisDelay[cId & 0xFF].end());
                cBxIdsThisDelay[cId & 0xFF].resize(distance(cBxIdsThisDelay[cId & 0xFF].begin(), cIter));
                cAlignmentMap[cPackageDelay] = cAlignmentMap[cPackageDelay] && cBxIdsThisDelay[cId & 0xFF].size() > 1;
                if(!cAlignmentMap[cPackageDelay])
                {
                    // LOG (INFO) << BOLDRED << cOut.str() << RESET;
                    continue;
                }
                // cant just have 0-7 in the BxIds
                // explicitly check here
                cAlignmentMap[cPackageDelay] = false;
                for(auto cBxId: cBxIdsThisDelay[cId & 0xFF])
                {
                    if(cBxId > 7) cAlignmentMap[cPackageDelay] = true;
                }
                if(cAlignmentMap[cPackageDelay])
                {
                    LOG(INFO) << BOLDGREEN << cOut.str() << RESET;
                    auto cStbCnfg  = cOpticalGroup->getStubCnfg();
                    cStbCnfg.first = cPackageDelay;
                    cOpticalGroup->setStubCnfg(cStbCnfg);
                }
                // else LOG (INFO) << BOLDRED << cOut.str() << RESET;
            }
        }
        for(auto cMapItem: cAlignmentMap)
        {
            if(cMapItem.second == true)
            {
                cGoodPackageDelays[cOpticalGroup->getId()] = cMapItem.first;
                cConfigurationValues[cRegName]             = cConfigurationValues[cRegName] | (cMapItem.first << (cOpticalGroup->getId() % 10) * 3);
            }
        }
    }
    // set value of package delays
    auto cIter = cConfigurationValues.begin();
    do {
        LOG(INFO) << BOLDYELLOW << "Want to set package delay register " << cIter->first << " to " << std::bitset<32>(cIter->second) << RESET;
        fBeBoardInterface->WriteBoardReg(pBoard, cIter->first, cIter->second);
        cIter++;
    } while(cIter != cConfigurationValues.end());
    fBeBoardInterface->ChipReSync(pBoard);
    if(cVerify)
    {
        fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity", 0);
        fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_ctrl.fast_command_block.control.load_config", 0x1);

        // configure TP
        auto                          cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cTriggerInterface*         cTriggerInterface = dynamic_cast<D19cTriggerInterface*>(cInterface->getTriggerInterface());
        TestPulseTriggerConfiguration cTPCnfg;
        cTPCnfg.fDelayAfterFastReset = 100;
        cTPCnfg.fDelayAfterTP        = 100;
        cTPCnfg.fDelayBeforeNextTP   = 200;
        cTPCnfg.fEnableFastReset     = 0;
        cTPCnfg.fEnableTP            = 1;
        cTPCnfg.fEnableL1A           = 1;
        cTriggerInterface->ConfigureTestPulseFSM(cTPCnfg);

        // read events and fill vector
        ReadNEvents(pBoard, 10);
        const std::vector<Event*>&             cEvents = this->GetEvents();
        std::map<uint8_t, std::vector<size_t>> cBxIds;
        for(auto& cEvent: cEvents)
        {
            for(auto cId: cHybridIds)
            {
                auto cBxId = cEvent->BxId(cId & 0xFF);
                cBxIds[cId & 0xFF].push_back(cBxId);
            } // all hybrid ids from all links
        }     // events from the readout

        // check BxIds from each hybrid
        // for a given hybrid not all can be the same
        for(auto cId: cHybridIds)
        {
            auto        cOpticalGroupId = cId >> 8;
            std::string cRegName        = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9";
            if(cOpticalGroupId > 9) cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11";

            std::stringstream cOut;
            uint8_t           cPackageDelay = (cConfigurationValues[cRegName] >> (cOpticalGroupId % 10) * 3) & 0x7;
            cOut << "Hybrid#" << (cId & 0xFF) << "\t.. Package delay of " << +cPackageDelay;
            for(auto cBxId: cBxIds[cId & 0xFF]) { cOut << " BxId" << cBxId << ", "; } // loop over BxIds from the readout events
            sort(cBxIds[cId & 0xFF].begin(), cBxIds[cId & 0xFF].end());
            auto cIter = std::unique(cBxIds[cId & 0xFF].begin(), cBxIds[cId & 0xFF].end());
            cBxIds[cId & 0xFF].resize(distance(cBxIds[cId & 0xFF].begin(), cIter));
            if(cBxIds[cId & 0xFF].size() > 1)
                LOG(INFO) << BOLDGREEN << cOut.str() << RESET;
            else
                LOG(INFO) << BOLDRED << cOut.str() << RESET;
        }
    }

    size_t cIndx = 0;
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            fCicInterface->WriteChipReg(cCic, "FE_ENABLE", cFeEnableRegs[cIndx]);
            cIndx++;
        } // reselect FEs
    }
    return cGoodPackageDelays.size() > 0;
}
bool BackendAlignmentOT::FindStubLatency(BeBoard* pBoard)
{
    DetectorDataContainer cContainerSL;
    fDetectorDataContainer = &cContainerSL;
    ContainerFactory::copyAndInitHybrid<uint16_t>(*fDetectorContainer, *fDetectorDataContainer);

    PrepareForTP(pBoard);
    std::map<uint8_t, uint16_t> cStubLatencyMap;
    int                         cMaximumLatency = 0;
    int                         cDelay          = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse");
    for(int cLatency = 0; cLatency < 512; cLatency++)
    // for(int cLatency = cDelay-60; cLatency < cDelay-20; cLatency++)
    {
        for(auto cOpticalGroup: *pBoard)
        {
            int               cBaseLinkId = cOpticalGroup->getId() / 3;
            std::stringstream cRegName;
            cRegName << "fc7_daq_cnfg.readout_block.stub_latency_link" << cBaseLinkId * 3;
            cRegName << "_link" << cBaseLinkId * 3 + 2;
            fBeBoardInterface->WriteBoardReg(pBoard, cRegName.str(), 0);
        } // reset stub latency register for all links back to  0

        for(auto cOpticalGroup: *pBoard)
        {
            int               cBaseLinkId = cOpticalGroup->getId() / 3;
            std::stringstream cRegName;
            cRegName << "fc7_daq_cnfg.readout_block.stub_latency_link" << cBaseLinkId * 3;
            cRegName << "_link" << cBaseLinkId * 3 + 2;
            uint32_t cVal           = fBeBoardInterface->ReadBoardReg(pBoard, cRegName.str());
            uint32_t cBitShiftedVal = (cLatency << (cOpticalGroup->getId() % 3) * 9);
            cVal                    = cVal | cBitShiftedVal; // cStubLatency+cOff;//cVal | ((cStubLatency+cOff) << (cOpticalGroup->getId()%3)*9);
            fBeBoardInterface->WriteBoardReg(pBoard, cRegName.str(), cVal);
            cVal = fBeBoardInterface->ReadBoardReg(pBoard, cRegName.str());
            LOG(DEBUG) << BOLDYELLOW << "Stub Latency" << cLatency << " on Link#" << +cOpticalGroup->getId() << " " << cRegName.str() << " set to " << std::bitset<32>(cVal) << RESET;
        } // set this latency for all links

        // read events and check for stubs
        ReadNEvents(pBoard, 1);
        const std::vector<Event*>& cEvents = this->GetEvents();
        for(auto cEvent: cEvents)
        {
            std::map<uint8_t, bool> cStubsMatch;
            for(auto cOpticalGroup: *pBoard)
            {
                cStubsMatch[cOpticalGroup->getId()] = true;
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto cBxId   = cEvent->BxId(cHybrid->getId());
                    int  cNStubs = 0;
                    for(auto cChip: *cHybrid)
                    {
                        auto cStubs = cEvent->StubVector(cHybrid->getId(), cChip->getId());
                        auto cHits  = cEvent->GetHits(cHybrid->getId(), cChip->getId());
                        cNStubs += cStubs.size();
                        if(cHits.size() > 0 && cStubs.size() > 0)
                            LOG(INFO) << BOLDYELLOW << "StubLatency " << cLatency << " Event#" << cEvent->GetEventCount() << "\t\t.. Hybrid#" << +cHybrid->getId() << " Bx" << cBxId << "\t\t\t.. ROC#"
                                      << +cChip->getId() << " " << cHits.size() << " hits and " << cStubs.size() << " stubs" << RESET;
                    } // ROCs
                    cStubsMatch[cOpticalGroup->getId()] = cStubsMatch[cOpticalGroup->getId()] && (cNStubs > 0);
                } // hybrids
                if(cStubsMatch[cOpticalGroup->getId()])
                {
                    // auto& cSLThisOG = cSLThisBoard->at(cOpticalGroup->getIndex());
                    // auto& cSLThisHybrid = cSLThisOG->at(cHybrid->getIndex());
                    cStubLatencyMap[cOpticalGroup->getId()] = cLatency;
                    if(cLatency >= cMaximumLatency) cMaximumLatency = cLatency;
                    // cSLThisHybrid = cLatency;
                }
            } // links
        }     // events
    }         // offset

    for(auto cOpticalGroup: *pBoard)
    {
        int               cBaseLinkId = cOpticalGroup->getId() / 3;
        std::stringstream cRegName;
        cRegName << "fc7_daq_cnfg.readout_block.stub_latency_link" << cBaseLinkId * 3;
        cRegName << "_link" << cBaseLinkId * 3 + 2;
        fBeBoardInterface->WriteBoardReg(pBoard, cRegName.str(), 0);
    } // reset stub latency register for all links back to  0

    for(auto cOpticalGroup: *pBoard)
    {
        int               cLatency    = cStubLatencyMap[cOpticalGroup->getId()];
        int               cBaseLinkId = cOpticalGroup->getId() / 3;
        std::stringstream cRegName;
        cRegName << "fc7_daq_cnfg.readout_block.stub_latency_link" << cBaseLinkId * 3;
        cRegName << "_link" << cBaseLinkId * 3 + 2;
        uint32_t cVal           = fBeBoardInterface->ReadBoardReg(pBoard, cRegName.str());
        uint32_t cBitShiftedVal = (cLatency << (cOpticalGroup->getId() % 3) * 9);
        cVal                    = cVal | cBitShiftedVal; // cStubLatency+cOff;//cVal | ((cStubLatency+cOff) << (cOpticalGroup->getId()%3)*9);
        fBeBoardInterface->WriteBoardReg(pBoard, cRegName.str(), cVal);
        cVal = fBeBoardInterface->ReadBoardReg(pBoard, cRegName.str());
        LOG(INFO) << BOLDYELLOW << "Stub Latency " << cLatency << " on Link#" << +cOpticalGroup->getId() << " maximum latency found for this readout chain is " << cMaximumLatency << " "
                  << cRegName.str() << " set to " << std::bitset<32>(cVal) << RESET;
    } // set this latency for all links

    return true;
}

// State machine control functions
void BackendAlignmentOT::Running()
{
    Initialise();
    this->Align();
    fSuccess = true;
    Reset();
}

void BackendAlignmentOT::Stop() {}

void BackendAlignmentOT::Pause() {}

void BackendAlignmentOT::Resume() {}
