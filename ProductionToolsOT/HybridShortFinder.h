/*!
 *
 * \file HybridShortFinder.h
 * \brief Calibration example -> use it as a template
 * \author Fabio Ravera
 * \date 25 / 07 / 19
 *
 * \Support : fabio.ravera@cern.ch
 *
 */

#ifndef HybridShortFinder_h__
#define HybridShortFinder_h__

#include "PedeNoise.h"
#include <map>
#ifdef __USE_ROOT__
// Calibration is not running on the SoC: I need to instantiate the DQM histrgrammer here
#endif

class HybridShortFinder : public PedeNoise
{
  public:
    HybridShortFinder();
    ~HybridShortFinder();

    void Initialise(void);
    void runHybridShortFinder(void);
    void writeObjects(void);

    // State machine
    void Running() override;
    void Stop(void) override;

  private:
    uint16_t fPulseAmplitude{0};
    bool     fPlotPulseShapeSCurves{false};

#ifdef __USE_ROOT__
    // Calibration is not running on the SoC: Histogrammer is handeld by the calibration itself
    // CBCHistogramPulseShape fCBCHistogramPulseShape;
#endif
};

#endif
