#include "CicInputTest.h"
#include "D19cBackendAlignmentFWInterface.h"
#include "D19cDPInterface.h"
#include "D19cDebugFWInterface.h"
#include "DPInterface.h"
#include "PSTestCardControl.h"
#include "Tool.h"
#include <chrono>

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

CicInputTester::CicInputTester(uint32_t pUsbBus, uint8_t pUsbDev, uint8_t pPattern) : Tool(), fUsbBus(pUsbBus), fUsbDev(pUsbDev), fPattern(pPattern) {}

CicInputTester::~CicInputTester() {}

void CicInputTester::SetInputPatterns(std::vector<uint8_t> pPatterns)
{
    fInputPatterns.clear();
    for(auto cPattern: pPatterns) { fInputPatterns.push_back(cPattern); }
}

void CicInputTester::Initialise()
{
    // assign the front-end type
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getIndex() > 0) continue;
        for(auto cOpticalGroup: *cBoard)
        {
            if(cOpticalGroup->getIndex() > 0) continue;
            fFrontEndType = cOpticalGroup->getFrontEndType();
        }
    }
    if(fFrontEndType == FrontEndType::HYBRIDPS)
    {
        // configure hybrid test card to select CIC
        // PSTestCardControl cHybridController;
        // cHybridController.Initialise(fUsbBus,fUsbDev);
        // cHybridController.SelectCIC(true);
    }

    // initialize containers that hold values found by this tool
    // one value per SLVS line
    ContainerFactory::copyAndInitHybrid<std::map<uint8_t, std::vector<CicInput>>>(*fDetectorContainer, fCicInputLines);
    ContainerFactory::copyAndInitHybrid<std::map<uint8_t, LineConfiguration>>(*fDetectorContainer, fSLVSLineConfigs);
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // Alignment interface
        auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
        cAlignerInterface->InitializeConfiguration();
        cAlignerInterface->InitializeAlignerObject();
        cAlignerInterface->EnablePrintout(false);
        AlignerObject cAlignerObjct;
        cAlignerObjct.fChip    = 0;
        cAlignerObjct.fOptical = cBoard->isOptical() ? 1 : 0;
        auto& cLineConfigs     = fSLVSLineConfigs.at(cBoard->getIndex());
        auto& cInputLines      = fCicInputLines.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cInputLinesOG  = cInputLines->at(cOpticalGroup->getIndex());
            auto& cLineConfigsOG = cLineConfigs->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cLineConfigsHybrd = cLineConfigsOG->at(cHybrid->getIndex());
                auto& cCnfg             = cLineConfigsHybrd->getSummary<std::map<uint8_t, LineConfiguration>>();
                for(uint8_t cLineIndx = 0; cLineIndx < 7; cLineIndx++)
                {
                    cAlignerObjct.fHybrid = cHybrid->getId();
                    cAlignerObjct.fLine   = cLineIndx;
                    LineConfiguration cLineCnfg;
                    // get sampling phase configured in the BE
                    auto cReply               = cAlignerInterface->RetrieveConfig(cAlignerObjct, cLineCnfg);
                    cCnfg[cLineIndx].fDelay   = cReply.fCnfg.fDelay;
                    cCnfg[cLineIndx].fBitslip = cReply.fCnfg.fBitslip;
                    LOG(INFO) << BOLDYELLOW << "Be-Align configuration on SLVSLIne#" << +cAlignerObjct.fLine << " delay of : " << +cCnfg[cLineIndx].fDelay << " bit-slip of "
                              << +cCnfg[cLineIndx].fBitslip << RESET;
                }
                auto& cInputLinesHybrd = cInputLinesOG->at(cHybrid->getIndex());
                auto& cCicInputMap     = cInputLinesHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                cCicInputMap.clear();
                auto cPortConnections = fCicInterface->GetPhyPortConnections();
                for(uint8_t cPhyPort = 0; cPhyPort < 12; cPhyPort++)
                {
                    CicInput              cInput;
                    std::vector<CicInput> cCicInputs;
                    cCicInputs.clear();
                    size_t cIndx = 0; // phy-port input counter
                    for(auto cConnection: cPortConnections[cPhyPort])
                    {
                        cInput.fFe         = cConnection.first;
                        cInput.fOut        = cIndx;
                        cInput.fFeOut      = cConnection.second;
                        cInput.fBeLineIndx = cIndx + 1; // first input from phy-port muxed to first stub line [line1 in the BE]
                        cInput.fData       = "";
                        cCicInputs.push_back(cInput);
                        cIndx++;
                    } // loop over connections to this phy-port
                    cCicInputMap[cPhyPort] = cCicInputs;
                } // loop over phy-ports
            }     // hybrids
        }         // links
    }             // boards

#ifdef __USE_ROOT__
    fDQMHistogram.book(fResultFile, *fDetectorContainer, fSettingsMap);
#endif
}
void CicInputTester::StartInputPattern_PSHybrid()
{
    StopInputPattern_PSHybrid();
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cDPInterface* cDPInterface = cInterface->getDPInterface();
        // check if you've got the same pattern on all lines
        if(fInputPatterns.size() == 1)
            cDPInterface->ConfigurePatternAllLines(fInputPatterns[0]);
        else
        {
            size_t cInput = 0;
            for(auto cPattern: fInputPatterns)
            {
                std::vector<uint8_t> cPatterns(1, cPattern);
                cDPInterface->ConfigureLineBRAM(cInput, cPatterns);
                cInput++;
            }
        } // lines from FEs [MPAs]
        cDPInterface->Start();
    } // all boards
}
void CicInputTester::StopInputPattern_PSHybrid()
{
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        auto             cInterface   = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cDPInterface* cDPInterface = cInterface->getDPInterface();
        cDPInterface->Stop();
    }
}
void CicInputTester::StartInputPattern()
{
    if(fFrontEndType == FrontEndType::HYBRIDPS)
        StartInputPattern_PSHybrid();
    else
    {
        // for modules - configure Chips to produce phase alignment patterns
        for(auto cBoard: *fDetectorContainer)
        {
            for(const auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    for(auto cChip: *cHybrid)
                    {
                        if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) continue;
                        LOG(INFO) << BOLDBLUE << "Generating Patterns needed for phase alignment of CIC inputs on FE#" << +cChip->getId() << RESET;
                        fReadoutChipInterface->producePhaseAlignmentPattern(cChip, 10);
                    } // ROCs
                }     // hybrids
            }         // links
        }             // boards
    }
}
void CicInputTester::StopInputPattern()
{
    if(fFrontEndType == FrontEndType::HYBRIDPS) StopInputPattern_PSHybrid();
}

// check
std::vector<CicInput> CicInputTester::CheckLines(Chip* pCic, uint8_t pPhyPort)
{
    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    if(fVerbose == 1)
        LOG(INFO) << BOLDYELLOW << "At " << startTimeUTC_us << "...For CIC#" << +pCic->getId() << " in OG#" << +pCic->getOpticalGroupId() << " on Hybrid#" << +pCic->getHybridId() << " on BeBoard#"
                  << +pCic->getBeBoardId() << " . Checking sampling delays for stub lines connected to PhyPort#" << +pPhyPort << RESET;

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardIndx        = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != pCic->getBeBoardId()) continue;
        cBoardIndx = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at(cBoardIndx)->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
    // Debug interface
    fBeBoardInterface->setBoard(pCic->getBeBoardId());
    auto                  cInterface      = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();
    cDebugInterface->SetNIterations(10);
    fCicInterface->SelectMux(pCic, pPhyPort);
    std::vector<CicInput> cFailedCicInputs;
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        // check phase-alignment result
        auto cRslt         = cDebugInterface->CheckData(pCic, cCicInput.fBeLineIndx, cCicInput.fExpected);
        cCicInput.fData    = cRslt.fData;
        cCicInput.fNErrors = cRslt.fErrors.size();
        if(cCicInput.fNErrors > 0) cFailedCicInputs.push_back(cCicInput);
        cCicInput.fZeroToOne   = cRslt.fZeroToOne;
        cCicInput.fOneToZero   = cRslt.fOneToZero;
        cCicInput.fBitsChecked = cRslt.fBitsChecked;
        std::stringstream cOut;
        cOut << "\t..For a sampling phase (in the BE) of " << +cCicInput.fSamplingPhaseBE << " and a sampling phase (in the CIC) of " << +cCicInput.fSamplingPhaseCIC << " on line#"
             << +cCicInput.fFeOut << " from FE#" << +cCicInput.fFe << " connected to PhyPort#" << +pPhyPort << " error check on " << cCicInput.fBitsChecked << " bits found " << cCicInput.fZeroToOne
             << " 0-1 bit flips "
             << " and " << cCicInput.fOneToZero << " 1-0 bit flips "
             << " (total = " << cCicInput.fNErrors << " errors) on the SLVSLine#" << +cCicInput.fBeLineIndx;
        //<< " pattern " << std::bitset<8>(fPattern)
        //<< "\n" << cCicInput.fData ;
        // if( fVerbose > 0  && cCicInput.fNErrors == 0 ) LOG (INFO) << BOLDGREEN << cOut.str() << RESET;
        // else if (cCicInput.fNErrors > 0) LOG (INFO) << BOLDRED << cOut.str() << RESET;
    }
    fCicInterface->ControlMux(pCic, 0);
    return cFailedCicInputs;
}

void CicInputTester::UpdatePatterns()
{
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cInputLines = fCicInputLines.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cInputLinesOG = cInputLines->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cInputLinesHybrd = cInputLinesOG->at(cHybrid->getIndex());
                auto& cCicInputMap     = cInputLinesHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                for(auto& cMapItem: cCicInputMap) // map with phy ports
                {
                    for(auto& cCicInput: cMapItem.second) // inputs per phy port
                    {
                        // get sampling phase configured in CIC
                        cCicInput.fExpected = (fInputPatterns.size() == 1) ? fInputPatterns[0] : fInputPatterns[cCicInput.fFeOut];
                    } // inputs
                }     // phy-ports
            }         // hybrids
        }             // links
    }                 // boards
}
void CicInputTester::UpdateInputMap()
{
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // Alignment interface
        auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
        cAlignerInterface->InitializeConfiguration();
        cAlignerInterface->InitializeAlignerObject();
        cAlignerInterface->EnablePrintout(false);
        AlignerObject cAlignerObjct;
        cAlignerObjct.fChip    = 0;
        cAlignerObjct.fOptical = cBoard->isOptical() ? 1 : 0;
        auto& cInputLines      = fCicInputLines.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cInputLinesOG = cInputLines->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cInputLinesHybrd = cInputLinesOG->at(cHybrid->getIndex());
                auto& cCicInputMap     = cInputLinesHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                cAlignerObjct.fHybrid  = cCic->getHybridId();
                for(auto& cMapItem: cCicInputMap) // map with phy ports
                {
                    for(auto& cCicInput: cMapItem.second) // inputs per phy port
                    {
                        // get sampling phase configured in CIC
                        cCicInput.fSamplingPhaseCIC = fCicInterface->GetOptimalTap(cCic, cMapItem.first, cCicInput.fOut);
                        cAlignerObjct.fLine         = cCicInput.fBeLineIndx;
                        cCicInput.fExpected         = (fInputPatterns.size() == 1) ? fInputPatterns[0] : fInputPatterns[cCicInput.fFeOut];
                        LineConfiguration cLineCnfg;
                        // get sampling phase configured in the BE
                        auto cReply = cAlignerInterface->RetrieveConfig(cAlignerObjct, cLineCnfg);
                        if(cReply.fSuccess)
                            LOG(DEBUG) << BOLDGREEN << "\t\t... Retreival of alignment values SUCCEEDED on SLVSLine#" << +cCicInput.fBeLineIndx << RESET;
                        else
                            LOG(DEBUG) << BOLDRED << "\t\t... Retreival of alignment values FAILED on SLVSLine#" << +cCicInput.fBeLineIndx << RESET;
                        cCicInput.fSamplingPhaseBE = cReply.fSuccess ? cReply.fCnfg.fDelay : 0xFF;
                        cCicInput.fBitSlipBE       = cReply.fSuccess ? cReply.fCnfg.fBitslip : 0xFF;
                    } // inputs
                }     // phy-ports
            }         // hybrids
        }             // links
    }                 // boards
}
// manual scan 2D phase
void CicInputTester::CicInOffsetCheck(Chip* pCic, uint8_t pPhyPort, uint8_t pSamplingDelay, int pCicInputOffset)
{
    auto cBoardId = pCic->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at((*cBoardIter)->getIndex())->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();

    // Alignment interface
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(true);
    AlignerObject cAlignerObjct;
    cAlignerObjct.fChip    = 0;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    cAlignerObjct.fHybrid  = pCic->getHybridId();
    LineConfiguration cLineCnfg;
    // analogue mux from CIC connected to first 4 stub lines
    // LOG (INFO) << BOLDYELLOW << "Manual scan of sampling delay when looking at lines from PhyPort#" << +pPhyPort <<  " - sampling delay of " << pSamplingDelay << RESET;
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        cCicInput.fSamplingPhaseBE  = pSamplingDelay;
        cCicInput.fBitSlipBE        = 0;
        cCicInput.fSamplingPhaseCIC = fCicInterface->GetOptimalTap(pCic, pPhyPort, cCicInput.fOut) + pCicInputOffset;
    } // loop over stub lines from CIC
    CheckSamplingPhases(pCic, pPhyPort);
}

void CicInputTester::ManualPhaseCheck(Chip* pCic, uint8_t pPhyPort, uint8_t pSamplingDelay, uint8_t pCicInputDelay)
{
    auto cBoardId = pCic->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at((*cBoardIter)->getIndex())->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();

    // Alignment interface
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(true);
    AlignerObject cAlignerObjct;
    cAlignerObjct.fChip    = 0;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    cAlignerObjct.fHybrid  = pCic->getHybridId();
    LineConfiguration cLineCnfg;
    // analogue mux from CIC connected to first 4 stub lines
    // LOG (INFO) << BOLDYELLOW << "Manual scan of sampling delay when looking at lines from PhyPort#" << +pPhyPort <<  " - sampling delay of " << pSamplingDelay << RESET;
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        // fCicInterface->GetOptimalTap(pCic, pPhyPort, cCicInput.fOut)
        cCicInput.fSamplingPhaseBE  = pSamplingDelay;
        cCicInput.fBitSlipBE        = 0;
        cCicInput.fSamplingPhaseCIC = pCicInputDelay;
    } // loop over stub lines from CIC
    CheckSamplingPhases(pCic, pPhyPort);
}
void CicInputTester::Manual2DSCan(int pPhyPort)
{
    uint8_t pStart          = (pPhyPort < 0) ? 0 : pPhyPort;
    uint8_t pEnd            = (pPhyPort < 0) ? 11 : pPhyPort;
    auto    startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    LOG(INFO) << BOLDYELLOW << "CicInputTester::Manual2DSCan @ " << startTimeUTC_us << " started manual scan of sampling delays for "
              << " PhyPort(s) [ " << +pStart << " - " << +pEnd << " ] " << RESET;
    for(uint8_t cSamplingDelay = 0; cSamplingDelay <= 0x1F; cSamplingDelay++)
    {
        for(int cCicPhaseTapOffset = 0; cCicPhaseTapOffset <= 0; cCicPhaseTapOffset++)
        {
            auto currentTime_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            LOG(INFO) << BOLDYELLOW << "\t\t.. BE-Sampling delay set to " << +cSamplingDelay << " CIC phase tap offset set to " << cCicPhaseTapOffset << " ... so far test has taken "
                      << (currentTime_us - startTimeUTC_us) * 1e-6 / 60. << " minutes to run" << RESET;
            DetectorDataContainer cTestData;
            ContainerFactory::copyAndInitChip<std::map<uint8_t, float>>(*fDetectorContainer, cTestData);
            for(auto cBoard: *fDetectorContainer)
            {
                auto& cErrorsThisBrd = cTestData.at(cBoard->getIndex());
                auto& cCicInputLines = fCicInputLines.at(cBoard->getIndex());
                for(auto cOpticalGroup: *cBoard)
                {
                    auto& cErrorsThisOG        = cErrorsThisBrd->at(cOpticalGroup->getIndex());
                    auto& cCicInputLinesThisOG = cCicInputLines->at(cOpticalGroup->getIndex());
                    for(auto cHybrid: *cOpticalGroup)
                    {
                        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                        if(cCic == nullptr) continue;
                        for(uint8_t cPhyPort = pStart; cPhyPort < pEnd + 1; cPhyPort++) { CicInOffsetCheck(cCic, cPhyPort, cSamplingDelay, cCicPhaseTapOffset); }
                        // now .. convert from phy-port to hybrid
                        auto  cMapping                = fCicInterface->getMapping(cCic);
                        auto& cErrorsThisHybrd        = cErrorsThisOG->at(cHybrid->getIndex());
                        auto& cCicInputLinesThisHybrd = cCicInputLinesThisOG->at(cHybrid->getIndex());
                        auto& cCicInputMap            = cCicInputLinesThisHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                        for(auto cChip: *cHybrid)
                        {
                            if(cOpticalGroup->getFrontEndType() != FrontEndType::HYBRIDPS && (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2))
                                continue;
                            auto& cErrorsThisChip = cErrorsThisHybrd->at(cChip->getIndex());
                            auto& cErrors         = cErrorsThisChip->getSummary<std::map<uint8_t, float>>();
                            for(auto cMapItem: cCicInputMap)
                            {
                                if(pPhyPort >= 0 && cMapItem.first != pPhyPort) continue;
                                for(auto cCicInput: cMapItem.second)
                                {
                                    if(cCicInput.fFe != cMapping[cChip->getId()]) continue;
                                    float             cBER = (cCicInput.fNErrors == 0) ? -std::log(1 - 0.95) / cCicInput.fBitsChecked : (float)cCicInput.fNErrors / cCicInput.fBitsChecked;
                                    std::stringstream cOut;
                                    cOut << "\t\t\tROC#" << +cChip->getId() << " CIC_FE#" << +cCicInput.fFe << " PhyPort#" << +cMapItem.first << " PhyPort_In#" << +cCicInput.fOut << " FE_SLVS_Out# "
                                         << +cCicInput.fFeOut << " BeLine#" << +cCicInput.fBeLineIndx << " found " << +cCicInput.fNErrors << " errors when checking " << +cCicInput.fBitsChecked
                                         << " bits on BeLine#" << +cCicInput.fBeLineIndx << " approximate error rate to be " << cBER;
                                    if(cCicInput.fNErrors == 0) LOG(INFO) << BOLDGREEN << cOut.str() << RESET;
                                    if(cCicInput.fNErrors > 0) LOG(INFO) << BOLDRED << cOut.str() << RESET;
                                    cErrors[cCicInput.fFeOut] = cBER;
                                }
                            } // loop over phy ports
                        }     // all ROCs
                    }         // all hybrids
                }             // all OGs
            }                 // all boards
#ifdef __USE_ROOT__
            fDQMHistogram.fillManualPhaseScan(cSamplingDelay, cCicPhaseTapOffset, cTestData);
#endif
        } // phase taps
    }     // sampling delays
}
// auto align BE
void CicInputTester::BePhaseAlign(Chip* pCic, uint8_t pPhyPort)
{
    if(!fPhaseAlign)
    {
        LOG(INFO) << BOLDRED << "CicInputTester will NOT use uDTC to find optimal sampling delay for stub lines"
                  << " connected to PhyPort#" << +pPhyPort << " on CIC#" << +pCic->getId() << " in OG#" << +pCic->getOpticalGroupId() << " on Hybrid#" << +pCic->getHybridId() << " on BeBoard#"
                  << +pCic->getBeBoardId() << RESET;
        return;
    }
    LOG(INFO) << BOLDYELLOW << "CicInputTester WILL use uDTC to find optimal sampling delay for stub lines"
              << " connected to PhyPort#" << +pPhyPort << " on CIC#" << +pCic->getId() << " in OG#" << +pCic->getOpticalGroupId() << " on Hybrid#" << +pCic->getHybridId() << " on BeBoard#"
              << +pCic->getBeBoardId() << RESET;

    auto cBoardId = pCic->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at((*cBoardIter)->getIndex())->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();

    // Alignment interface
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout(true);
    AlignerObject cAlignerObjct;
    cAlignerObjct.fChip    = 0;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    cAlignerObjct.fHybrid  = pCic->getHybridId();
    LineConfiguration cLineCnfg;
    cLineCnfg.fPatternPeriod = 100;
    fCicInterface->SelectMux(pCic, pPhyPort);
    // analogue mux from CIC connected to first 4 stub lines
    LOG(INFO) << BOLDYELLOW << "Auto-aligning sampling delay when looking at lines from PhyPort#" << +pPhyPort << RESET;

    std::vector<CicInput> cFailedCicInputs;
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        cAlignerObjct.fLine = cCicInput.fBeLineIndx;
        auto cReply         = cAlignerInterface->TunePhase(cAlignerObjct, cLineCnfg);
        if(cReply.fSuccess)
            LOG(INFO) << BOLDGREEN << "\t\t... Auto-phase alignment SUCCEEDED on SLVSLine#" << +cCicInput.fBeLineIndx << RESET;
        else
        {
            LOG(INFO) << BOLDRED << "\t\t... Auto-phase alignment FAILED on SLVSLine#" << +cCicInput.fBeLineIndx << RESET;
            cFailedCicInputs.push_back(cCicInput);
        }
        cCicInput.fAutoAlignBE     = cReply.fSuccess;
        cCicInput.fSamplingPhaseBE = cReply.fSuccess ? cReply.fCnfg.fDelay : 0xFF;
        cCicInput.fBitSlipBE       = 0;
    } // loop over stub lines from CIC
    fCicInterface->ControlMux(pCic, 0);
    // if( cFailedCicInputs.size() != 0 ){
    //     LOG (INFO) << BOLDRED << "Auto-aligniner failed to align "
    //         << cFailedCicInputs.size() << " line(s) on PhyPort#" << +pPhyPort
    //         << " will run manual scan of phases to check offline"
    //         << RESET;
    //     Manual2DSCan(pPhyPort);
    // }
}
void CicInputTester::CheckSamplingPhases(Chip* pCic)
{
    for(uint8_t cPhyPort = 0; cPhyPort < 12; cPhyPort++) { CheckSamplingPhases(pCic, cPhyPort); }
}
void CicInputTester::CheckSamplingPhases(Chip* pCic, uint8_t pPhyPort)
{
    auto cBoardId = pCic->getBeBoardId();
    fBeBoardInterface->setBoard(cBoardId);
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });

    // get indices of hybrid + optical group
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pCic->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pCic->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    // cic input lines map
    auto& cCicInputLinesOG = fCicInputLines.at((*cBoardIter)->getIndex())->at(cOpticalGroupIndx);
    auto& cCicInputLines   = cCicInputLinesOG->at(cHybridIndx);
    auto& cCicInputMap     = cCicInputLines->getSummary<std::map<uint8_t, std::vector<CicInput>>>();

    auto& cLineConfigsOG = fSLVSLineConfigs.at((*cBoardIter)->getIndex())->at(cOpticalGroupIndx);
    auto& cLineConfig    = cLineConfigsOG->at(cHybridIndx);
    auto& cLineConfigMap = cLineConfig->getSummary<std::map<uint8_t, LineConfiguration>>();

    // Alignment interface
    auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
    cAlignerInterface->InitializeConfiguration();
    cAlignerInterface->InitializeAlignerObject();
    cAlignerInterface->EnablePrintout((fVerbose == 1));
    AlignerObject cAlignerObjct;
    cAlignerObjct.fChip    = 0;
    cAlignerObjct.fOptical = (*cBoardIter)->isOptical() ? 1 : 0;
    cAlignerObjct.fHybrid  = pCic->getHybridId();
    LineConfiguration cLineCnfg;
    // analogue mux from CIC connected to first 4 stub lines
    std::vector<uint8_t> cPhases(0);
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        cPhases.push_back(fCicInterface->GetOptimalTap(pCic, pPhyPort, cCicInput.fOut)); // get sampling phase from CIC
        fCicInterface->SetPhaseTap(pCic, pPhyPort, cCicInput.fOut, cCicInput.fSamplingPhaseCIC);
        cAlignerObjct.fLine = cCicInput.fBeLineIndx;
        cLineCnfg.fDelay    = cCicInput.fSamplingPhaseBE;
        cLineCnfg.fBitslip  = cCicInput.fBitSlipBE;
        cAlignerInterface->ManuallyConfigureLine(cAlignerObjct, cLineCnfg);

    } // loop over stub lines from CIC
    CheckLines(pCic, pPhyPort);
    size_t cIndx = 0;
    // reconfigure back to original settings
    cAlignerInterface->EnablePrintout((fVerbose == 1));
    for(auto& cCicInput: cCicInputMap[pPhyPort])
    {
        cAlignerObjct.fLine = cCicInput.fBeLineIndx;
        cLineCnfg.fDelay    = cLineConfigMap[cAlignerObjct.fLine].fDelay;
        cLineCnfg.fBitslip  = cLineConfigMap[cAlignerObjct.fLine].fBitslip;
        cAlignerInterface->ManuallyConfigureLine(cAlignerObjct, cLineCnfg);
        LOG(DEBUG) << BOLDYELLOW << "Re-setting phases : CIC PhyPort#" << +pPhyPort << " Input#" << +cCicInput.fOut << " to " << +cPhases[cIndx] << RESET;
        fCicInterface->SetPhaseTap(pCic, pPhyPort, cCicInput.fOut, cPhases[cIndx]);
        cIndx++;
    }
}
void CicInputTester::ResetBe()
{
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        // Alignment interface
        auto                             cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        D19cBackendAlignmentFWInterface* cAlignerInterface = cInterface->getBackendAlignmentInterface();
        cAlignerInterface->InitializeConfiguration();
        cAlignerInterface->InitializeAlignerObject();
        cAlignerInterface->EnablePrintout(false);
        AlignerObject cAlignerObjct;
        cAlignerObjct.fChip    = 0;
        cAlignerObjct.fOptical = cBoard->isOptical() ? 1 : 0;
        auto& cLineConfigs     = fSLVSLineConfigs.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cLineConfigsOG = cLineConfigs->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == nullptr) continue;

                auto& cLineConfigsHybrd = cLineConfigsOG->at(cHybrid->getIndex());
                auto& cLineConfigMap    = cLineConfigsHybrd->getSummary<std::map<uint8_t, LineConfiguration>>();
                for(uint8_t cLineIndx = 0; cLineIndx < 7; cLineIndx++)
                {
                    cAlignerObjct.fLine = cLineIndx;
                    LineConfiguration cLineCnfg;
                    cLineCnfg.fDelay   = cLineConfigMap[cAlignerObjct.fLine].fDelay;
                    cLineCnfg.fBitslip = cLineConfigMap[cAlignerObjct.fLine].fBitslip;
                    LOG(INFO) << BOLDYELLOW << "Re-setting Be-Sampling on SLVSLine#" << +cAlignerObjct.fLine << " to " << +cLineCnfg.fDelay << " , Be-bitSlip to " << +cLineCnfg.fBitslip << RESET;
                    cAlignerInterface->ManuallyConfigureLine(cAlignerObjct, cLineCnfg);
                } // SLVS lines
            }     // hybrids
        }         // links
    }             // boards
}
// Production State machine control functions
void CicInputTester::Running()
{
    LOG(INFO) << BOLDYELLOW << "CicInputTester::Running" << RESET;
    // replace this with something that loads generic patterns per type
    std::vector<uint8_t> cTestPatterns{0xAA, 0x88, 0xCC, 0xDD, 00, 0xFF};
    // std::vector<uint8_t> cWordAlignmentPatterns{0xD5,0xD5,0xD5,0xD5,0xD5,0xD5};
    // std::vector<uint8_t> cWordAlignmentPatterns{0xA1,0xD0,0x68,0x34,0x1A,0x0D};
    int                                 cPatternId = 0;
    std::map<uint8_t, std::vector<int>> cFailureMap;
    for(auto cPattern: cTestPatterns)
    {
        std::vector<int> cFailingPorts;
        cFailingPorts.clear();
        std::vector<uint8_t> cAlignmentPattern;
        for(uint8_t cLineId = 0; cLineId < 6; cLineId++)
        {
            cAlignmentPattern.push_back(cPattern); // cWordAlignmentPatterns[cLineId]);
        }
        SetInputPatterns(cAlignmentPattern);
        if(cPatternId == 0) UpdateInputMap();
        UpdatePatterns();
        StartInputPattern();

        // either run BE alignment + check pattern in backend or
        // simply check pattern in the BE
        for(auto cBoard: *fDetectorContainer)
        {
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                    if(cCic == nullptr) continue;
                    for(uint8_t cPhyPort = 0; cPhyPort < 12; cPhyPort++)
                    {
                        // can replace this with a check for if pattern has enough 1's in an 8 bit period
                        if(cPattern == 0xAA || cPattern == 0x55)
                        {
                            BePhaseAlign(cCic, cPhyPort);
                            if(fVerify) CheckSamplingPhases(cCic, cPhyPort);
                        }
                        else
                            CheckSamplingPhases(cCic, cPhyPort);
                    } // loop over all phy-ports
                }     // all hybrids
            }         // all links
        }             // all boards

        // then fill histograms where I store alignment values
        DetectorDataContainer cPhaseTapData, cSamplingData;
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cPhaseTapData);
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cSamplingData);
        for(auto cBoard: *fDetectorContainer)
        {
            if(cPatternId > 0) continue;
            auto& cTapsThisBoard   = cPhaseTapData.at(cBoard->getIndex());
            auto& cDelaysThisBoard = cSamplingData.at(cBoard->getIndex());
            auto& cCicInputLines   = fCicInputLines.at(cBoard->getIndex());
            for(auto cOpticalGroup: *cBoard)
            {
                auto& cTapsThisOG          = cTapsThisBoard->at(cOpticalGroup->getIndex());
                auto& cDelaysThisOG        = cDelaysThisBoard->at(cOpticalGroup->getIndex());
                auto& cCicInputLinesThisOG = cCicInputLines->at(cOpticalGroup->getIndex());
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                    if(cCic == nullptr) continue;

                    auto& cTapsThisHybrid         = cTapsThisOG->at(cHybrid->getIndex());
                    auto& cTaps                   = cTapsThisHybrid->getSummary<std::map<uint8_t, float>>();
                    auto& cDelaysThisHybrid       = cDelaysThisOG->at(cHybrid->getIndex());
                    auto& cDelays                 = cDelaysThisHybrid->getSummary<std::map<uint8_t, float>>();
                    auto& cCicInputLinesThisHybrd = cCicInputLinesThisOG->at(cHybrid->getIndex());
                    auto& cCicInputMap            = cCicInputLinesThisHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                    for(auto cMapItem: cCicInputMap)
                    {
                        for(auto cCicInput: cMapItem.second)
                        {
                            cTaps[cCicInput.fOut]          = (float)cCicInput.fSamplingPhaseCIC;
                            cDelays[cCicInput.fBeLineIndx] = (float)cCicInput.fSamplingPhaseBE;
                        }
#ifdef __USE_ROOT__
                        fDQMHistogram.fillAlignmentValues(cMapItem.first, cPhaseTapData, cSamplingData);
#endif
                    } // loop over phy ports
                }     // all hybrids
            }         // all OGs
        }             // all boards

        // fill pattern check histogram per hybrid
        DetectorDataContainer cAggrErrors, cAggrTxBits;
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cAggrErrors);
        ContainerFactory::copyAndInitHybrid<std::map<uint8_t, float>>(*fDetectorContainer, cAggrTxBits);

        for(auto cBoard: *fDetectorContainer)
        {
            auto& cErrorsThisBrd = cAggrErrors.at(cBoard->getIndex());
            auto& cTxBitsThisBrd = cAggrTxBits.at(cBoard->getIndex());
            auto& cCicInputLines = fCicInputLines.at(cBoard->getIndex());
            for(auto cOpticalGroup: *cBoard)
            {
                auto& cErrorsThisOG        = cErrorsThisBrd->at(cOpticalGroup->getIndex());
                auto& cTxBitsThisOG        = cTxBitsThisBrd->at(cOpticalGroup->getIndex());
                auto& cCicInputLinesThisOG = cCicInputLines->at(cOpticalGroup->getIndex());
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                    if(cCic == nullptr) continue;

                    // now .. convert from phy-port to hybrid
                    auto  cMapping                = fCicInterface->getMapping(cCic);
                    auto& cErrorsThisHybrd        = cErrorsThisOG->at(cHybrid->getIndex());
                    auto& cErrors                 = cErrorsThisHybrd->getSummary<std::map<uint8_t, float>>();
                    auto& cTxBitsThisHybrd        = cTxBitsThisOG->at(cHybrid->getIndex());
                    auto& cTxBits                 = cTxBitsThisHybrd->getSummary<std::map<uint8_t, float>>();
                    auto& cCicInputLinesThisHybrd = cCicInputLinesThisOG->at(cHybrid->getIndex());
                    auto& cCicInputMap            = cCicInputLinesThisHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                    for(auto cMapItem: cCicInputMap)
                    {
                        cErrors.clear();
                        cTxBits.clear();
                        for(auto cCicInput: cMapItem.second)
                        {
                            float             cBER = (cCicInput.fNErrors == 0) ? -std::log(1 - 0.95) / cCicInput.fBitsChecked : (float)cCicInput.fNErrors / cCicInput.fBitsChecked;
                            std::stringstream cOut;
                            cOut << "\t\t\tCIC_FE#" << +cCicInput.fFe << " PhyPort#" << +cMapItem.first << " PhyPort_In#" << +cCicInput.fOut << " FE_SLVS_Out# " << +cCicInput.fFeOut << " BeLine#"
                                 << +cCicInput.fBeLineIndx << " Pattern on line " << std::bitset<8>(cCicInput.fExpected) << " found " << +cCicInput.fNErrors << " errors when checking "
                                 << +cCicInput.fBitsChecked << " bits on BeLine#" << +cCicInput.fBeLineIndx << " approximate error rate to be " << cBER;
                            cErrors[cCicInput.fBeLineIndx] = cCicInput.fNErrors;
                            cTxBits[cCicInput.fBeLineIndx] = cCicInput.fBitsChecked;
                            if(cCicInput.fNErrors > 0)
                            {
                                if(std::find(cFailingPorts.begin(), cFailingPorts.end(), cMapItem.first) == cFailingPorts.end()) { cFailingPorts.push_back(cMapItem.first); }
                            }
                        } // all inputs connected to phy port
#ifdef __USE_ROOT__
                        fDQMHistogram.fillAggregatedPatternCheck(cMapItem.first, cAggrErrors, cAggrTxBits);
#endif
                    } // loop over phy ports
                }     // all hybrids
            }         // all OGs
        }             // all boards

        // fill pattern check per ROC
        DetectorDataContainer cTestData;
        ContainerFactory::copyAndInitChip<std::map<uint8_t, float>>(*fDetectorContainer, cTestData);
        LOG(INFO) << BOLDYELLOW << "Displaying results of pattern check.. pattern at input is " << std::bitset<8>(cPattern) << RESET;
        for(auto cBoard: *fDetectorContainer)
        {
            auto& cErrorsThisBrd = cTestData.at(cBoard->getIndex());
            auto& cCicInputLines = fCicInputLines.at(cBoard->getIndex());
            for(auto cOpticalGroup: *cBoard)
            {
                auto& cErrorsThisOG        = cErrorsThisBrd->at(cOpticalGroup->getIndex());
                auto& cCicInputLinesThisOG = cCicInputLines->at(cOpticalGroup->getIndex());
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                    if(cCic == nullptr) continue;

                    // now .. convert from phy-port to hybrid
                    auto  cMapping                = fCicInterface->getMapping(cCic);
                    auto& cErrorsThisHybrd        = cErrorsThisOG->at(cHybrid->getIndex());
                    auto& cCicInputLinesThisHybrd = cCicInputLinesThisOG->at(cHybrid->getIndex());
                    auto& cCicInputMap            = cCicInputLinesThisHybrd->getSummary<std::map<uint8_t, std::vector<CicInput>>>();
                    for(auto cChip: *cHybrid)
                    {
                        if(cOpticalGroup->getFrontEndType() != FrontEndType::HYBRIDPS && (cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)) continue;
                        auto& cErrorsThisChip = cErrorsThisHybrd->at(cChip->getIndex());
                        auto& cErrors         = cErrorsThisChip->getSummary<std::map<uint8_t, float>>();
                        for(auto cMapItem: cCicInputMap)
                        {
                            for(auto cCicInput: cMapItem.second)
                            {
                                if(cCicInput.fFe != cMapping[cChip->getId()]) continue;
                                float             cBER = (cCicInput.fNErrors == 0) ? -std::log(1 - 0.95) / cCicInput.fBitsChecked : (float)cCicInput.fNErrors / cCicInput.fBitsChecked;
                                std::stringstream cOut;
                                cOut << "\t\t\tROC#" << +cChip->getId() << " CIC_FE#" << +cCicInput.fFe << " PhyPort#" << +cMapItem.first << " PhyPort_In#" << +cCicInput.fOut << " FE_SLVS_Out# "
                                     << +cCicInput.fFeOut << " BeLine#" << +cCicInput.fBeLineIndx << " Pattern on line " << std::bitset<8>(cCicInput.fExpected) << " found " << +cCicInput.fNErrors
                                     << " errors when checking " << +cCicInput.fBitsChecked << " bits on BeLine#" << +cCicInput.fBeLineIndx << " approximate error rate to be " << cBER;
                                // if( cCicInput.fNErrors == 0 ) LOG (INFO) << BOLDGREEN << cOut.str() << RESET;
                                if(cCicInput.fNErrors > 0) LOG(INFO) << BOLDRED << cOut.str() << RESET;
                                cErrors[cCicInput.fFeOut] = cBER;
                            }
                        } // loop over phy ports
                    }     // all ROCs
                }         // all hybrids
            }             // all OGs
        }                 // all boards

#ifdef __USE_ROOT__
        fDQMHistogram.fillPatternCheck(cPatternId, cPattern, cTestData);
#endif
        cFailureMap[cPattern] = cFailingPorts;
        cPatternId++;
    } // all patterns

    LOG(INFO) << BOLDYELLOW << "Pattern check found errors for " << cFailureMap.size() << " of the pattern(s) checked." << RESET;
    cPatternId = 0;
    for(auto cMapItem: cFailureMap)
    {
        if(cMapItem.first != 0xAA) continue;

        std::vector<uint8_t> cAlignmentPattern;
        for(uint8_t cLineId = 0; cLineId < 6; cLineId++) { cAlignmentPattern.push_back(cMapItem.first); }
        SetInputPatterns(cAlignmentPattern);
        UpdatePatterns();
        StartInputPattern();

        for(auto cPort: cMapItem.second)
        {
            LOG(INFO) << BOLDRED << "Line check failed for sone  line(s) on PhyPort#" << +cPort << " for pattern " << std::bitset<8>(cMapItem.first)
                      << " will run manual scan of phases to check offline" << RESET;
            Manual2DSCan(cPort);
        }
        cPatternId++;
    } // failing patterns

    if(cFailureMap.size() == 0) { Manual2DSCan(-1); }
}

void CicInputTester::Stop()
{
    LOG(INFO) << BOLDBLUE << "Stopping CIC HybriInput tester" << RESET;
    StopInputPattern();
    ResetBe();
    // writeObjects();
    // dumpConfigFiles();
    // Destroy();
}

void CicInputTester::Pause() {}

void CicInputTester::Resume() {}
