#ifndef CicInputTester_h__
#define CicInputTester_h__

#include "Tool.h"
#ifdef __USE_ROOT__
#include "DQMUtils/DQMHistogramCicInTest.h"
#endif
struct CicInput
{
    uint8_t     fFe;
    uint8_t     fOut;
    uint8_t     fFeOut;
    uint8_t     fBeLineIndx;
    uint8_t     fSamplingPhaseCIC;
    uint8_t     fBitSlipBE;
    uint8_t     fAlignerCIC;
    uint8_t     fSamplingPhaseBE;
    uint8_t     fAlignerBE;
    size_t      fNErrors;
    size_t      fZeroToOne;
    size_t      fOneToZero;
    size_t      fTimestamp;
    size_t      fBitsChecked;
    bool        fAutoAlignBE;
    std::string fData;
    uint8_t     fExpected;
};
class CicInputTester : public Tool
{
  public:
    CicInputTester(uint32_t pUsbBus, uint8_t pUsbDev, uint8_t pPattern);
    ~CicInputTester();

    void Initialise();
    // Production State machine control functions
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;

    void StartInputPattern();
    void StopInputPattern();
    void ConfigureUsb(uint32_t pUsbBus, uint8_t pUsbDev)
    {
        fUsbBus = pUsbBus;
        fUsbDev = pUsbDev;
    }
    void ConfigurePattern(uint8_t pPattern) { fPattern = pPattern; }
    void SetInputPatterns(std::vector<uint8_t> pPatterns);
    void UpdateInputMap();
    void UpdatePatterns();
    void ResetBe();

  private:
    void                  StartInputPattern_PSHybrid();
    void                  StopInputPattern_PSHybrid();
    void                  ManualPhaseCheck(Ph2_HwDescription::Chip* pCic, uint8_t pPhyPort, uint8_t pSamplingDelay, uint8_t pCicInputDelay);
    void                  CicInOffsetCheck(Ph2_HwDescription::Chip* pCic, uint8_t pPhyPort, uint8_t pSamplingDelay, int pCicInputOffset);
    void                  BePhaseAlign(Ph2_HwDescription::Chip* pCic, uint8_t cPhyPort);
    void                  CheckSamplingPhases(Ph2_HwDescription::Chip* pCic);
    void                  CheckSamplingPhases(Ph2_HwDescription::Chip* pCic, uint8_t pPhyPort);
    std::vector<CicInput> CheckLines(Ph2_HwDescription::Chip* pCic, uint8_t pPhyPort);
    // void Manual2DSCan();
    void         Manual2DSCan(int pPhyPort = -1);
    bool         fPhaseAlign{true};
    bool         fVerify{true};
    uint32_t     fUsbBus;
    uint8_t      fUsbDev;
    uint8_t      fPattern{0xAA};
    size_t       fWait_us{100};
    FrontEndType fFrontEndType;

    DetectorDataContainer fCicInputLines;
    DetectorDataContainer fSLVSLineConfigs;
    std::vector<uint8_t>  fSLVSLines{1, 2, 3, 4};
    std::vector<uint8_t>  fMultiPatterns;
    std::vector<uint8_t>  fInputPatterns;

    uint8_t fVerbose{0};

#ifdef __USE_ROOT__
    DQMHistogramCicInTest fDQMHistogram;
#endif
};

#endif
