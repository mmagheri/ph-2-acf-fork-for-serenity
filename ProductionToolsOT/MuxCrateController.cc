#include "MuxCrateController.h"
#include "D19cMuxBackplaneFWInterface.h"
#include "FileParser.h"

using namespace Ph2_System;
using namespace Ph2_HwInterface;

MuxCrateController::MuxCrateController(std::string puHalConfigFileName, uint16_t pBoardId)
{
    fAvailableCards = 0;
    fAvailable.clear();
    fConfigurationFile = puHalConfigFileName;
    fBoardId           = pBoardId;
    fMyInterface       = nullptr;
    FileParser                     theFileParser;
    std::map<uint16_t, RegManager> theRegManagerList = theFileParser.getRegManagerList(fConfigurationFile);
    fMyInterface                                     = new D19cMuxBackplaneFWInterface(std::move(theRegManagerList.at(fBoardId)));
    fInterlockEnabled                                = true;
}
MuxCrateController::~MuxCrateController() {}
void MuxCrateController::Initialise()
{
    bool cSetupScanned = (fMyInterface->ReadReg("fc7_daq_stat.physical_interface_block.multiplexing_bp.setup_scanned") == 1);
    // if its not been scanned.. then send a reset
    if(cSetupScanned) { LOG(INFO) << BOLDBLUE << "Set-up has already been scanned..." << RESET; }
    else
    {
        LOG(INFO) << BOLDBLUE << "Set-up has not been scanned..." << RESET;
        LOG(INFO) << BOLDBLUE << "Sending a global reset to the FC7 ..... " << RESET;
        fMyInterface->WriteReg("fc7_daq_ctrl.command_processor_block.global.reset", 0x1);
        // std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}
void MuxCrateController::Scan()
{
    LOG(INFO) << BOLDBLUE << "Scanning MuxCrate connected to BeBoard " << +fBoardId << RESET;
    fAvailableCards = fMyInterface->ScanMultiplexingSetup();
    parseAvailable(false);
    printAvailableCards();
}

// Disconnect multiplexing set-up
void MuxCrateController::Disconnect()
{
    LOG(INFO) << BOLDBLUE << "Disconnecting all backplanes and cards on BeBoard " << +fBoardId << RESET;
    fMyInterface->DisconnectMultiplexingSetup();
}

void MuxCrateController::ConfigureSingleCard(uint8_t pBackPlaneId, uint8_t pCardId)
{
    LOG(INFO) << BOLDBLUE << "Configuring backplane " << +pBackPlaneId << " card " << +pCardId << " on BeBoard " << +fBoardId << RESET;
    if(fInterlockEnabled)
    {
        LOG(INFO) << "Enabling the interlock feature. Now controlled by the interlock switch" << RESET;
        fMyInterface->WriteReg("fc7_daq_cnfg.physical_interface_block.multiplexing_bp.interlock_switch_feature", 0x1);
    }
    else
    {
        LOG(INFO) << "Disabling the interlock feature. Now controlled by the Backend" << RESET;
        fMyInterface->WriteReg("fc7_daq_cnfg.physical_interface_block.multiplexing_bp.interlock_switch_feature", 0x0);
        fMyInterface->WriteReg("fc7_daq_cnfg.physical_interface_block.multiplexing_bp.interlock_switch_output", 0x1);
    }

    bool    cInterlockState         = fMyInterface->ReadReg("fc7_daq_stat.physical_interface_block.multiplexing_bp.status_interlock_switch") == 1;
    int32_t cPowerGoodGlitchesCount = fMyInterface->ReadReg("fc7_daq_stat.physical_interface_block.counter_powergood_glitches");
    LOG(INFO) << BOLDCYAN << "Interlock state: " << +cInterlockState << RESET;
    LOG(INFO) << BOLDCYAN << "Number of glitches in the power good lines: " << cPowerGoodGlitchesCount << RESET;
    fMyInterface->ConfigureMultiplexingSetup(pBackPlaneId, pCardId);
    parseAvailable();
    printAvailableCards();
    cInterlockState         = (fMyInterface->ReadReg("fc7_daq_stat.physical_interface_block.multiplexing_bp.status_interlock_switch") == 1);
    cPowerGoodGlitchesCount = fMyInterface->ReadReg("fc7_daq_stat.physical_interface_block.counter_powergood_glitches");
    LOG(INFO) << BOLDCYAN << "Interlock state: " << +cInterlockState << RESET;
    LOG(INFO) << BOLDCYAN << "Number of glitches in the power good lines: " << cPowerGoodGlitchesCount << RESET;

    // LOG(INFO) << BOLDBLUE << "Configuring backplane " << +pBackPlaneId << " card " << +pCardId << " on BeBoard " << +fBoardId << RESET;
    // fMyInterface->ConfigureMultiplexingSetup(pBackPlaneId, pCardId);
    // parseAvailable();
    // printAvailableCards();
}

void MuxCrateController::ConfigureAll()
{
    LOG(INFO) << BOLDBLUE << "Configuring all cards on BeBoard " << +fBoardId << RESET;
    fAvailableCards = fMyInterface->ScanMultiplexingSetup();
    parseAvailable(false);
    printAvailableCards();
    for(const auto& el: fAvailable)
    {
        int         cBackPlaneId = el.first;
        const auto& cCardIds     = el.second;
        for(auto cCardId: cCardIds) this->ConfigureSingleCard(cBackPlaneId, cCardId);
    }
}

void MuxCrateController::printAvailableCards()
{
    for(auto const& itBPCard: fAvailable)
    {
        std::stringstream sstr;
        if(itBPCard.second.empty())
            sstr << "No cards";
        else
            for(auto const& itCard: itBPCard.second) sstr << itCard << " ";
        LOG(INFO) << BLUE << "Available cards for bp " << itBPCard.first << ":"
                  << "[ " << sstr.str() << "]" << RESET;
    }
}
std::map<int, std::vector<int>> MuxCrateController::getAvailableCards(bool filterBoardsWithoutCards)
{
    parseAvailable(filterBoardsWithoutCards);
    return fAvailable;
}