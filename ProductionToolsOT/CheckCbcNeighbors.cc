#include "CheckCbcNeighbors.h"
#include "../Utils/ChannelGroupHandler.h"
#include "../Utils/ContainerFactory.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

CheckCbcNeighbors::CheckCbcNeighbors() : Tool() {}

CheckCbcNeighbors::~CheckCbcNeighbors() {}

void CheckCbcNeighbors::Initialise(void)
{
    fNEvents = findValueInSettings<double>("Nevents", 10);
    GenerateChannelList();
}

void CheckCbcNeighbors::GenerateChannelList()
{
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fSharedCorrelationLayerOutHigh);
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fSharedCorrelationLayerOutLow);
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fSharedSeedLayerOutHigh);
    ContainerFactory::copyAndInitHybrid<std::vector<uint8_t>>(*fDetectorContainer, fSharedSeedLayerOutLow);

    for(auto cBoard: *fDetectorContainer)
    {
        auto& cBrdCorrLyrH = fSharedCorrelationLayerOutHigh.at(cBoard->getIndex());
        auto& cBrdCorrLyrL = fSharedCorrelationLayerOutLow.at(cBoard->getIndex());
        auto& cBrdSeedLyrH = fSharedSeedLayerOutHigh.at(cBoard->getIndex());
        auto& cBrdSeedLyrL = fSharedSeedLayerOutLow.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cLnkCorrLyrH = cBrdCorrLyrH->at(cOpticalGroup->getIndex());
            auto& cLnkCorrLyrL = cBrdCorrLyrL->at(cOpticalGroup->getIndex());
            auto& cLnkSeedLyrH = cBrdSeedLyrH->at(cOpticalGroup->getIndex());
            auto& cLnkSeedLyrL = cBrdSeedLyrL->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cHybrdCorrLyrH = cLnkCorrLyrH->at(cHybrid->getIndex());
                auto& cHybrdCorrLyrL = cLnkCorrLyrL->at(cHybrid->getIndex());
                auto& cHybrdSeedLyrH = cLnkSeedLyrH->at(cHybrid->getIndex());
                auto& cHybrdSeedLyrL = cLnkSeedLyrL->at(cHybrid->getIndex());

                auto& cCorrLyrH = cHybrdCorrLyrH->getSummary<std::vector<uint8_t>>();
                cCorrLyrH.clear();
                auto& cCorrLyrL = cHybrdCorrLyrL->getSummary<std::vector<uint8_t>>();
                cCorrLyrL.clear();
                auto& cSeedLyrH = cHybrdSeedLyrH->getSummary<std::vector<uint8_t>>();
                cSeedLyrH.clear();
                auto& cSeedLyrL = cHybrdSeedLyrL->getSummary<std::vector<uint8_t>>();
                cSeedLyrL.clear();

                /*std::map<float,uint16_t> cBendEncoding;
                for(int cReg=0; cReg<15; cReg++)
                {
                    for( int cNibble=0; cNibble<2; cNibble++)
                    {
                        float cBend = -7.0 + 0.5*cNibble + cReg;
                        cBendEncoding[cBend] = (cNibble << 4 ) | cReg;
                    }
                }
                float   cCenterBend = 0.;
                float   cResolution = 0.5;
                float   cBend = cCenterBend;
                uint8_t cCode = 0;
                do
                {
                    auto cReg = cBendEncoding[cBend] & 0xF ;
                    auto cNbl = cBendEncoding[cBend] >> 4;
                    LOG (INFO) << BOLDYELLOW << "Bend of " << cBend << " -- " << cBend+cResolution << " register " << +cReg << " nibble " << +cNbl << RESET;
                    cBend += cResolution;
                    cCode++;
                }while( cCode < 7);
                */
                // 0 -- bottom sensor seed; 1 -- top sensor seed
                for(auto cChip: *cHybrid)
                {
                    fReadoutChipInterface->WriteChipReg(cChip, "CoincWind&Offset34", 0);
                    fReadoutChipInterface->WriteChipReg(cChip, "CoincWind&Offset12", 0);
                    fReadoutChipInterface->WriteChipReg(cChip, "LayerSwap", 0);
                    fReadoutChipInterface->WriteChipReg(cChip, "ClusterCut", 4);
                }
                std::stringstream cCorrOut;
                for(size_t cIndx = 0; cIndx < fNLowestCorrelationLayer; cIndx++)
                {
                    cCorrLyrL.push_back(1 + cIndx * 2);
                    cCorrOut << 1 + cIndx * 2 << ",";
                } // top sensor.. odd channels
                for(size_t cIndx = 0; cIndx < fNHighestCorrelationLayer; cIndx++)
                {
                    cCorrLyrH.push_back(cHybrid->at(0)->size() - 1 - cIndx * 2);
                    cCorrOut << cHybrid->at(0)->size() - 1 - cIndx * 2 << ",";
                } // top sensor.. odd channels
                std::stringstream cSeedOut;
                for(size_t cIndx = 0; cIndx < fNLowestSeedLayer; cIndx++)
                {
                    cSeedLyrL.push_back(0 + cIndx * 2);
                    cSeedOut << 0 + cIndx * 2 << ",";
                } // bottom sensor.. even channels
                for(size_t cIndx = 0; cIndx < fNHighestSeedLayer; cIndx++)
                {
                    cSeedLyrH.push_back(cHybrid->at(0)->size() - 2 - cIndx * 2);
                    cSeedOut << cHybrid->at(0)->size() - 2 - cIndx * 2 << ",";
                } // bottom sensor.. even channels
                LOG(INFO) << BOLDYELLOW << "Correlation layer : " << cCorrOut.str() << RESET;
                LOG(INFO) << BOLDYELLOW << "Seed layer : " << cSeedOut.str() << RESET;
            }
        }
    }
}
// check communication from left to right
void CheckCbcNeighbors::NeighbourCheck()
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                    ChannelGroup<NCHANNELS, 1> cChannelMask;
                    cChannelMask.disableAllChannels();
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));
                }
            }
        }
    } // first disable everything on all chips

    for(auto cBoard: *fDetectorContainer)
    {
        setSameDacBeBoard(cBoard, "Threshold", 1023);
        auto& cBrdCorrLyrH = fSharedCorrelationLayerOutHigh.at(cBoard->getIndex());
        auto& cBrdCorrLyrL = fSharedCorrelationLayerOutLow.at(cBoard->getIndex());
        auto& cBrdSeedLyrH = fSharedSeedLayerOutHigh.at(cBoard->getIndex());
        auto& cBrdSeedLyrL = fSharedSeedLayerOutLow.at(cBoard->getIndex());

        for(auto cOpticalGroup: *cBoard)
        {
            auto& cLnkCorrLyrH = cBrdCorrLyrH->at(cOpticalGroup->getIndex());
            auto& cLnkCorrLyrL = cBrdCorrLyrL->at(cOpticalGroup->getIndex());
            auto& cLnkSeedLyrH = cBrdSeedLyrH->at(cOpticalGroup->getIndex());
            auto& cLnkSeedLyrL = cBrdSeedLyrL->at(cOpticalGroup->getIndex());

            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cHybrdCorrLyrL = cLnkCorrLyrL->at(cHybrid->getIndex());
                auto& cHybrdSeedLyrH = cLnkSeedLyrH->at(cHybrid->getIndex());
                auto& cHybrdCorrLyrH = cLnkCorrLyrH->at(cHybrid->getIndex());
                auto& cHybrdSeedLyrL = cLnkSeedLyrL->at(cHybrid->getIndex());

                // generate with stub seeds in even chips
                auto& cCorrLyrH = cHybrdCorrLyrH->getSummary<std::vector<uint8_t>>();
                auto& cSeedLyrL = cHybrdSeedLyrL->getSummary<std::vector<uint8_t>>();
                // auto& cSeedLyrH = cHybrdSeedLyrH->getSummary<std::vector<uint8_t>>();
                // auto& cCorrLyrL = cHybrdCorrLyrL->getSummary<std::vector<uint8_t>>();
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getIndex() == cHybrid->size() - 1) continue;
                    const auto& cNeighbour = cHybrid->at(cChip->getIndex() + 1);
                    NeighbourGen(cNeighbour, cChip);
                    // NeighbourGen(cNeighbour, cSeedLyrL, cChip, cCorrLyrH);
                }

                // // generate with stub seeds in neighbours
                // auto& cHybrdCorrLyrH = cLnkCorrLyrH->at(cHybrid->getIndex());
                // auto& cHybrdSeedLyrL = cLnkSeedLyrL->at(cHybrid->getIndex());
                // auto& cCorrLyrH = cHybrdCorrLyrH->getSummary<std::vector<uint8_t>>();
                // auto& cSeedLyrL = cHybrdSeedLyrL->getSummary<std::vector<uint8_t>>();
                // for(auto cChip : *cHybrid )
                // {
                //     if(cChip->getIndex() == cHybrid->size()-1) continue;
                //     const auto& cNeighbour = cHybrid->at(cChip->getIndex()+1);
                //     LOG (INFO) << BOLDYELLOW << "Running test with seeds in low channels in CBC#" << +cNeighbour->getId()
                //         << " and correlated hits in high channels in CBC#" << +cChip->getId() << RESET;
                //     NeighbourGen(cNeighbour, cSeedLyrL, cChip, cCorrLyrH);
                // }
            }
        }
    }
}
void CheckCbcNeighbors::NeighbourGen(ReadoutChip* pNeighbour, ReadoutChip* pChip)
{
    if(pChip->getId() != 0) return;

    auto cWindowOffset = fReadoutChipInterface->ReadChipReg(pNeighbour, "CoincWind&Offset12");
    fReadoutChipInterface->WriteChipReg(pNeighbour, "CoincWind&Offset12", (cWindowOffset & 0xF0) | 10);
    LOG(INFO) << BOLDYELLOW << "Running test with hits in high channels in CBC#" << +pChip->getId() << " and low channels in CBC#" << +pNeighbour->getId() << RESET;
    CbcInterface* theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);

    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardIndx        = 0;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != pChip->getBeBoardId()) continue;
        cBoardIndx = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    auto& cSeedLyrL = fSharedSeedLayerOutLow.at(cBoardIndx)->at(cOpticalGroupIndx)->at(cHybridIndx)->getSummary<std::vector<uint8_t>>();
    auto& cCorrLyrH = fSharedCorrelationLayerOutHigh.at(cBoardIndx)->at(cOpticalGroupIndx)->at(cHybridIndx)->getSummary<std::vector<uint8_t>>();

    ChannelGroup<NCHANNELS, 1> cNeighbourMask;
    cNeighbourMask.disableAllChannels();
    this->UnmaskChannels({cSeedLyrL[0]}, cNeighbourMask);
    LOG(INFO) << BOLDYELLOW << "\t..Unmasking channel " << +cSeedLyrL[0] << " in CBC#" << +pNeighbour->getId() << RESET;
    theCbcInterface->maskChannelGroup(pNeighbour, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cNeighbourMask)));

    ChannelGroup<NCHANNELS, 1> cMask;
    for(size_t i = 0; i < 2; i++)
    {
        cMask.disableAllChannels();
        std::vector<uint8_t> cChnls;
        cChnls.clear();
        for(size_t j = 0; j < 4; j++)
        {
            size_t cIndx = 4 * i + j;
            if(cIndx >= cCorrLyrH.size()) continue;
            cChnls.push_back(cCorrLyrH[4 * i + j]);
        }
        if(cChnls.size() == 0) continue;
        // cChnls.push_back(252); //cChnls.push_back(250);//cChnls.push_back(248);
        this->UnmaskChannels(cChnls, cMask);
        std::stringstream cOut;
        cOut << "\t\t..Unmasking channel ";
        for(auto cChnl: cChnls) cOut << +cChnl << ", ";
        cOut << " in CBC#" << +pChip->getId();
        LOG(INFO) << BOLDYELLOW << cOut.str() << RESET;
        theCbcInterface->maskChannelGroup(pChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cMask)));
        CheckStubs(pNeighbour->getHybridId(), pNeighbour->getId());
    }
    {
        std::vector<uint8_t> cChnls;
        cChnls.clear();
        for(size_t j = 0; j < 3; j++) cChnls.push_back(cCorrLyrH[cCorrLyrH.size() - 1] + 2 * (j + 1));
        std::stringstream cOut;
        cOut << "\t\t..Unmasking channel ";
        for(auto cChnl: cChnls) cOut << +cChnl << ", ";
        cOut << " in CBC#" << +pChip->getId();
        LOG(INFO) << BOLDYELLOW << cOut.str() << RESET;
        cMask.disableAllChannels();
        this->UnmaskChannels(cChnls, cMask);
        theCbcInterface->maskChannelGroup(pChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cMask)));
        CheckStubs(pNeighbour->getHybridId(), pNeighbour->getId());
        // then add one more hit and watch the stub disappear
        cChnls.push_back(cCorrLyrH[cCorrLyrH.size() - 1]);
        LOG(INFO) << BOLDYELLOW << "\t\t.. Adding channel " << +cCorrLyrH[cCorrLyrH.size() - 1] << " to mask..." << RESET;
        cMask.disableAllChannels();
        this->UnmaskChannels(cChnls, cMask);
        theCbcInterface->maskChannelGroup(pChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cMask)));
        CheckStubs(pNeighbour->getHybridId(), pNeighbour->getId());
    }
    fReadoutChipInterface->WriteChipReg(pNeighbour, "CoincWind&Offset12", cWindowOffset);

    cWindowOffset = fReadoutChipInterface->ReadChipReg(pChip, "CoincWind&Offset34");
    fReadoutChipInterface->WriteChipReg(pChip, "CoincWind&Offset34", (6 << 4) | (cWindowOffset & 0xF));
    auto& cSeedLyrH = fSharedSeedLayerOutHigh.at(cBoardIndx)->at(cOpticalGroupIndx)->at(cHybridIndx)->getSummary<std::vector<uint8_t>>();
    auto& cCorrLyrL = fSharedCorrelationLayerOutLow.at(cBoardIndx)->at(cOpticalGroupIndx)->at(cHybridIndx)->getSummary<std::vector<uint8_t>>();
    cMask.disableAllChannels();
    this->UnmaskChannels(cSeedLyrH, cMask);
    // this->UnmaskChannels({cSeedLyrH[0]}, cMask);
    LOG(INFO) << BOLDYELLOW << "\t..Unmasking channels 250,252 in CBC#" << +pChip->getId() << RESET;
    theCbcInterface->maskChannelGroup(pChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cMask)));
    for(size_t i = 0; i < 5; i++)
    {
        cNeighbourMask.disableAllChannels();
        std::vector<uint8_t> cChnls;
        cChnls.clear();
        for(size_t j = 0; j < 4; j++)
        {
            size_t cIndx = 4 * i + j;
            if(cIndx >= cCorrLyrL.size()) continue;
            cChnls.push_back(cCorrLyrL[4 * i + j]);
        }
        if(cChnls.size() <= 1) continue;
        cChnls.push_back(0);
        cChnls.push_back(2);
        this->UnmaskChannels(cChnls, cNeighbourMask);
        std::stringstream cOut;
        cOut << "\t\t..Unmasking channel ";
        for(auto cChnl: cChnls) cOut << +cChnl << ", ";
        cOut << " in CBC#" << +pNeighbour->getId();
        LOG(INFO) << BOLDYELLOW << cOut.str() << RESET;
        theCbcInterface->maskChannelGroup(pNeighbour, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cNeighbourMask)));
        CheckStubs(pChip->getHybridId(), pChip->getId());
    }
    // fReadoutChipInterface->WriteChipReg(pChip,"CoincWind&Offset34",cWindowOffset );
    // fReadoutChipInterface->WriteChipReg(pNeighbour,"CoincWind&Offset12",cWindowOffset );

    // cWindowOffset = fReadoutChipInterface->ReadChipReg(pNeighbour,"CoincWind&Offset12");
    // fReadoutChipInterface->WriteChipReg(pNeighbour,"CoincWind&Offset12",(cWindowOffset &0xF0) | 6 );
    // cMask.disableAllChannels();
    // this->UnmaskChannels({cSeedLyrH[0],cSeedLyrH[1]}, cMask);
    // theCbcInterface->maskChannelGroup(pChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cMask)));
    // LOG (INFO) << BOLDYELLOW << "\t..Unmasking channels 252 in CBC#" << +pChip->getId() << RESET;
    // for the last one .. first check if you've got a stub with the following combination
    std::stringstream cOut;
    cNeighbourMask.disableAllChannels();
    std::vector<uint8_t> cChnls;
    cChnls.clear();
    cChnls.push_back(0);
    cChnls.push_back(2);
    cChnls.push_back(21); // cChnls.push_back(23);
    // for(size_t j=0; j < 2;j++) cChnls.push_back( cCorrLyrL[cCorrLyrL.size()-2] - 2*(j+1) );
    cOut << "\t\t..Unmasking channel ";
    for(auto cChnl: cChnls) cOut << +cChnl << ", ";
    cOut << " in CBC#" << +pNeighbour->getId();
    LOG(INFO) << BOLDYELLOW << cOut.str() << RESET;
    cNeighbourMask.disableAllChannels();
    this->UnmaskChannels(cChnls, cNeighbourMask);
    theCbcInterface->maskChannelGroup(pNeighbour, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cNeighbourMask)));
    CheckStubs(pChip->getHybridId(), pChip->getId());
    // // then add one more hit and watch the stub disappear
    // cChnls.push_back( cCorrLyrL[cCorrLyrL.size()-1]);
    // cNeighbourMask.disableAllChannels();
    // this->UnmaskChannels(cChnls, cNeighbourMask);
    // theCbcInterface->maskChannelGroup(pNeighbour, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cNeighbourMask)));
    // CheckStubs(pNeighbour->getHybridId(), pNeighbour->getId());
}
void CheckCbcNeighbors::NeighbourGen(ReadoutChip* pSeedChip, std::vector<uint8_t> pSeedChnls, ReadoutChip* pCorrChip, std::vector<uint8_t> pCorrelationChnls)
{
    if(pCorrChip->getId() != 0) return;
    CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
    ChannelGroup<NCHANNELS, 1> cSeedMask;
    cSeedMask.disableAllChannels();
    this->UnmaskChannels({pSeedChnls[0]}, cSeedMask);
    theCbcInterface->maskChannelGroup(pSeedChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cSeedMask)));

    ChannelGroup<NCHANNELS, 1> cCorrMask;
    // unmask all of correlation layer
    for(size_t i = 0; i < 3; i++)
    {
        cCorrMask.disableAllChannels();
        std::vector<uint8_t> cChnls;
        cChnls.clear();
        for(size_t j = 0; j < 4; j++)
        {
            size_t cIndx = 4 * i + j;
            if(cIndx >= pCorrelationChnls.size()) continue;
            cChnls.push_back(pCorrelationChnls[4 * i + j]);
        }
        // cChnls.push_back(237);cChnls.push_back(235);cChnls.push_back(233);cChnls.push_back(231);
        cChnls.push_back(252);
        cChnls.push_back(250);
        cChnls.push_back(248);
        if(cChnls.size() == 0) continue;
        this->UnmaskChannels(cChnls, cCorrMask);
        theCbcInterface->maskChannelGroup(pCorrChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cCorrMask)));
        CheckStubs(pSeedChip->getHybridId(), pCorrChip->getId());
    }

    // size_t cNSeeds=0;
    // for(auto cSeed : pSeedChnls)
    // {
    //     cNSeeds++;
    //     if(cNSeeds!=1) continue;
    //     // unmask first seed
    //     int cSeedStrip = cSeed/2;
    //     LOG (INFO) << BOLDGREEN << "Hit in seed chip channel " << +cSeed << RESET;
    //     cSeedMask.disableAllChannels();
    //     this->UnmaskChannels({cSeed}, cSeedMask);
    //     theCbcInterface->maskChannelGroup(pSeedChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cSeedMask)));
    //     // unmask all of correlation layer
    //     for(int i=0; i < 7; i++)
    //     {
    //         cCorrMask.disableAllChannels();
    //         this->UnmaskChannels({pCorrelationChnls[2*i],pCorrelationChnls[2*i+1]}, cCorrMask);
    //         this->UnmaskChannels({0,2,4}, cCorrMask);
    //         theCbcInterface->maskChannelGroup(pCorrChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cCorrMask)));
    //         CheckStubs(pSeedChip->getHybridId(), pSeedChip->getId()+1);
    //     }

    //     // // I should get a stub in the seed CBC
    //     // CheckStubs(pSeedChip->getHybridId(), pSeedChip->getId());
    //     // for(auto cCorr : pCorrelationChnls)
    //     // {
    //     //     int cCorrStrip = cCorr/2;
    //     //     int cBend = std::fabs(127*(pCorrChip->getId()-pSeedChip->getId()) + cCorrStrip - cSeedStrip);
    //     //     if( cBend > 7 ) continue;//make sure pT cut is at 14
    //     //     cCorrMask.disableAllChannels();
    //     //     this->UnmaskChannels({cCorr}, cCorrMask);
    //     //     theCbcInterface->maskChannelGroup(pCorrChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cCorrMask)));
    //     //     auto cMatch = CheckData(pSeedChip->getId(), (cSeedStrip+1)*2);
    //     //     if( cMatch == fNEvents){
    //     //         LOG (DEBUG) << BOLDGREEN << "Seed hit in CBC#" << +pSeedChip->getId() << " channel "
    //     //             << +cSeed
    //     //             << " correlated hit in CBC#" << +pCorrChip->getId()
    //     //             << " channel " << +cCorr
    //     //             << " found correct stub in " << cMatch
    //     //             << "/" << fNEvents << " events "
    //     //             << RESET;
    //     //     }
    //     //     else{
    //     //         LOG (WARNING) << BOLDRED << "Seed hit in CBC#" << +pSeedChip->getId() << " channel "
    //     //             << +cSeed
    //     //             << " correlated hit in CBC#" << +pCorrChip->getId()
    //     //             << " channel " << +cCorr
    //     //             << " found correct stub in " << cMatch
    //     //             << "/" << fNEvents << " events "
    //     //             << RESET;
    //     //     }
    //     // }//correlated hits
    // }//seeds
}
size_t CheckCbcNeighbors::CheckData(uint8_t pExpectedChip, uint8_t pExpectedSeed)
{
    // LOG (INFO) << BOLDYELLOW << "Expect a stub in CBC#" << +pExpectedChip << " with seed " << +pExpectedSeed << RESET;
    size_t cMatchedCount = 0;
    for(auto cBoard: *fDetectorContainer)
    {
        ReadNEvents(cBoard, fNEvents);
        const std::vector<Event*>& cEvents = this->GetEvents();
        for(auto& cEvent: cEvents)
        {
            for(const auto cOpticalGroup: *cBoard)
            {
                for(const auto cHybrid: *cOpticalGroup)
                {
                    auto cBxId = cEvent->BxId(cHybrid->getId());
                    LOG(DEBUG) << BOLDYELLOW << "Event#" << cEvent->GetEventCount() << " Hybrid#" << +cHybrid->getId() << " BxId " << cBxId << RESET;
                    for(const auto cChip: *cHybrid)
                    {
                        // if( cChip->getId() != pExpectedChip ) continue;
                        // auto cThreshold = fReadoutChipInterface->ReadChipReg(cChip,"Threshold");
                        auto cStubs = cEvent->StubVector(cChip->getHybridId(), cChip->getId());
                        auto cHits  = cEvent->GetHits(cChip->getHybridId(), cChip->getId());
                        if(cHits.size() > 0) LOG(DEBUG) << BOLDYELLOW << "\t.." << +cHits.size() << " hits and " << cStubs.size() << " stubs in ROC#" << +cChip->getId() << RESET;
                        for(auto cStub: cStubs)
                        {
                            if(cStub.getPosition() != pExpectedSeed) continue;
                            cMatchedCount++;
                            // LOG (DEBUG) << BOLDYELLOW << "\t\t..Chip#" << +cChip->getId()
                            // << " [ " << cThreshold << " ] "
                            // << cStubs.size() << " stubs and "
                            // << cHits.size() <<  " hits "
                            // << " BxId " << cBxId
                            // << " Pos " << +cStub.getPosition()
                            // << RESET;
                        }
                    }
                }
            }
        }
    }
    // LOG (INFO) << BOLDYELLOW << "Found " << cMatchedCount << "/" << fNEvents << " matches in CBC#" << +pExpectedChip << RESET;
    return cMatchedCount;
}
bool CheckCbcNeighbors::TestCbcNeighbors()
{
    bool result;
    bool allPass = true;

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                // first disable everything on all chips
                for(auto cChip: *cHybrid)
                {
                    CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
                    ChannelGroup<NCHANNELS, 1> cChannelMask;
                    cChannelMask.disableAllChannels();
                    theCbcInterface->maskChannelGroup(cChip, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask)));
                }

                // now begin test
                for(auto cChip1: *cHybrid)
                {
                    for(auto cChip2: *cHybrid)
                    {
                        ChannelGroup<NCHANNELS, 1> cChannelMask_1;
                        ChannelGroup<NCHANNELS, 1> cChannelMask_2;
                        CbcInterface*              theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);

                        if(cChip2->getId() != cChip1->getId() + 1) { continue; }
                        // if(! (cChip1->getId() == 0 && cChip2->getId() == 1)) continue;

                        LOG(DEBUG) << "CheckCBCNeighbors with Hybrid " << +cHybrid->getId() << " chip1: " << +cChip1->getId() << " chip2: " << +cChip2->getId();

                        //-------------------------------------
                        // check stubs from 1->2 -- all of these will be read out as being on chip1
                        //-------------------------------------
                        LOG(INFO) << BOLDCYAN << "Checking stubs from lower chip " << +cChip1->getId() << " to higher chip " << +cChip2->getId() << RESET;
                        cChannelMask_1.disableAllChannels();
                        cChannelMask_2.disableAllChannels();
                        theCbcInterface->maskChannelGroup(cChip1, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_1)));
                        theCbcInterface->maskChannelGroup(cChip2, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_2)));

                        this->UnmaskChannels(fSharedTopHigh, cChannelMask_1);
                        this->UnmaskChannels(fSharedBottomLow, cChannelMask_2);

                        theCbcInterface->maskChannelGroup(cChip1, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_1)));
                        theCbcInterface->maskChannelGroup(cChip2, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_2)));

                        LOG(DEBUG) << "Unmasked Chip1 " << +cChannelMask_1.getNumberOfEnabledChannels() << " Unmasked Chip2 " << +cChannelMask_2.getNumberOfEnabledChannels() << RESET;
                        result = CheckStubs(cHybrid->getId(), cChip1->getId());
                        LOG(DEBUG) << "result " << +result;
                        allPass = allPass && result;
                        if(!result)
                            LOG(INFO) << RED << "FAILED hybrid " << +cHybrid->getId() << " chip1 " << +cChip1->getId() << " chip2 " << +cChip2->getId() << std::endl << RESET;
                        else
                            LOG(INFO) << GREEN << "PASSED hybrid " << +cHybrid->getId() << " chip1 " << +cChip1->getId() << " chip2 " << +cChip2->getId() << std::endl << RESET;
                        // theStubContainer.at(cBoard->getIndex())->at(cOpticalGroup->getIndex())->at(cHybrid->getIndex())->at(cChip1->getIndex())->getSummary<bool>() = result;

                        //-------------------------------------
                        // check stubs from 2->1 -- all of these will be read out as if they are on chip2
                        //-------------------------------------
                        LOG(INFO) << BOLDMAGENTA << "Checking stubs from higher chip to lower chip" << RESET;
                        cChannelMask_1.disableAllChannels();
                        cChannelMask_2.disableAllChannels();
                        theCbcInterface->maskChannelGroup(cChip1, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_1)));
                        theCbcInterface->maskChannelGroup(cChip2, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_2)));

                        this->UnmaskChannels(fSharedBottomHigh, cChannelMask_1);
                        this->UnmaskChannels(fSharedTopLow, cChannelMask_2);

                        theCbcInterface->maskChannelGroup(cChip1, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_1)));
                        theCbcInterface->maskChannelGroup(cChip2, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_2)));

                        LOG(DEBUG) << "Unmasked Chip1 " << +cChannelMask_1.getNumberOfEnabledChannels() << " Unmasked Chip2 " << +cChannelMask_2.getNumberOfEnabledChannels() << RESET;
                        result = CheckStubs(cHybrid->getId(), cChip2->getId());
                        LOG(DEBUG) << "result " << +result;
                        allPass = allPass && result;
                        if(!result)
                            LOG(INFO) << RED << "FAILED hybrid " << +cHybrid->getId() << " chip1 " << +cChip1->getId() << " chip2 " << +cChip2->getId() << std::endl << RESET;
                        else
                            LOG(INFO) << GREEN << "PASSED hybrid " << +cHybrid->getId() << " chip1 " << +cChip1->getId() << " chip2 " << +cChip2->getId() << std::endl << RESET;

                        // remask channels for the next test
                        cChannelMask_2.disableAllChannels();
                        cChannelMask_1.disableAllChannels();
                        theCbcInterface->maskChannelGroup(cChip1, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_1)));
                        theCbcInterface->maskChannelGroup(cChip2, std::make_shared<ChannelGroup<NCHANNELS, 1>>(std::move(cChannelMask_2)));
                    }
                }
            }
        }
    }
    if(allPass)
        LOG(INFO) << BOLDGREEN << "CheckCbcNeighbors PASSED" << RESET;
    else
        LOG(INFO) << BOLDRED << "CheckCbcNeighbors FAILED" << RESET;
    return allPass;
}

bool CheckCbcNeighbors::CheckStubs(uint8_t hybridId, uint8_t chipId)
{
    uint32_t      eventsWStubs    = 0;
    CbcInterface* theCbcInterface = static_cast<CbcInterface*>(fReadoutChipInterface);
    for(auto cBoard: *fDetectorContainer)
    {
        auto cDelay = fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_cnfg.physical_interface_block.stubs.stub_package_delay");
        ReadNEvents(cBoard, 1);
        const std::vector<Event*>& cEvents = this->GetEvents();
        // LOG(INFO) << "Got " << cEvents.size() << " events - stub package delay is " << cDelay << RESET;
        for(auto& cEvent: cEvents)
        {
            for(const auto cOpticalGroup: *cBoard)
            {
                for(const auto cHybrid: *cOpticalGroup)
                {
                    auto cBxId = cEvent->BxId(cHybrid->getId());
                    for(const auto cChip: *cHybrid)
                    {
                        if(cChip->getId() != chipId) continue;
                        auto              cLUT       = theCbcInterface->readLUT(cChip);
                        auto              cThreshold = fReadoutChipInterface->ReadChipReg(cChip, "Threshold");
                        auto              cStubs     = cEvent->StubVector(cChip->getHybridId(), cChip->getId());
                        auto              cHits      = cEvent->GetHits(cChip->getHybridId(), cChip->getId());
                        std::stringstream cOut;
                        cOut << " Hits in Channels :";
                        for(auto cHit: cHits) cOut << " " << +cHit << ",";
                        cOut << " Stubs [Seed,BendCode]:";
                        for(auto cStub: cStubs) cOut << " [" << +cStub.getPosition() << "," << +cStub.getBend() << " ],";
                        if(cStubs.size() > 0 || cHits.size() > 0)
                            LOG(INFO) << BOLDYELLOW << "Chip#" << +cChip->getId() << " [ " << cThreshold << " ] " << cStubs.size() << " stubs and " << cHits.size() << " hits "
                                      << " BxId " << cBxId << "\t..." << cOut.str() << RESET;

                        // for(auto cStub : cStubs ){
                        //     int   cBendIndx = std::distance(cLUT.begin(), std::find(cLUT.begin(), cLUT.end(), cStub.getBend()));
                        //     float cBend     = -7.0 + cBendIndx * 0.5;
                        //     LOG (INFO) << BOLDYELLOW << "\t.. Pos " << +cStub.getPosition() << "\t.. Bend code " << +cStub.getBend()
                        //         << "\t.. bend in strips " << cBend
                        //         << RESET;
                        // }
                    }
                }
            }
        }
    }
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     ReadNEvents(cBoard, fNEvents);
    //     const std::vector<Event*>& cEvents = this->GetEvents();
    //     LOG(DEBUG) << "Got " << cEvents.size() << " events.";
    //     for(auto& cEvent: cEvents)
    //     {
    //         auto cStubs = cEvent->StubVector(hybridId, chipId);
    //         if( cEvent->GetEventCount() == 10 ) LOG (INFO) << BOLDYELLOW << cStubs.size() << " stubs in Event#" << +cEvent->GetEventCount() << RESET;
    //         if(cStubs.size() > 0) eventsWStubs++;

    //         for(auto cReadoutStub: cStubs)
    //         {
    //             // bad stub
    //             if((cReadoutStub.getPosition() >= 2 && cReadoutStub.getPosition() <= 6) || (cReadoutStub.getPosition() >= 252 && cReadoutStub.getPosition() <= 254))
    //             {
    //                 LOG(DEBUG) << "Stub position " << +cReadoutStub.getPosition() << " bend " << +cReadoutStub.getBend() << " row " << +cReadoutStub.getRow() << " center " <<
    //                 +cReadoutStub.getCenter()
    //                            << " on hybrid " << +hybridId << " and chip " << +chipId << RESET;
    //                 LOG(DEBUG) << CYAN << "\t Databit string " << cEvent->DataBitString(hybridId, chipId) << RESET;
    //             }
    //             // good stub
    //             else
    //             {
    //                 LOG(INFO) << RED << "WRONG STUB FOUND Stub position " << +cReadoutStub.getPosition() << " bend " << +cReadoutStub.getBend() << " row " << +cReadoutStub.getRow() << " center "
    //                           << +cReadoutStub.getCenter() << " on hybrid " << +hybridId << " and chip " << +chipId << RESET;
    //             }
    //         }
    //     }
    // }
    LOG(DEBUG) << eventsWStubs << " events with stubs";
    return eventsWStubs > 0;
}

void CheckCbcNeighbors::UnmaskChannels(std::vector<uint8_t> pToUnmask, ChannelGroup<NCHANNELS, 1>& pChannelMask)
{
    for(size_t cChnl: pToUnmask)
    {
        LOG(DEBUG) << GREEN << "enabling " << +cChnl << RESET;
        pChannelMask.enableChannel(cChnl);
    }
}

void CheckCbcNeighbors::ConfigureCalibration() {}

void CheckCbcNeighbors::Running()
{
    LOG(INFO) << "Starting CheckCbcNeighbors measurement.";
    Initialise();
    // TestCbcNeighbors();
    NeighbourCheck();
    LOG(INFO) << "Done with CheckCbcNeighbors.";
}

void CheckCbcNeighbors::Stop(void)
{
    LOG(INFO) << "Stopping CheckCbcNeighbors measurement.";
    dumpConfigFiles();
    SaveResults();
    closeFileHandler();
    LOG(INFO) << "CheckCbcNeighbors stopped.";
}

void CheckCbcNeighbors::Pause() {}

void CheckCbcNeighbors::Resume() {}
