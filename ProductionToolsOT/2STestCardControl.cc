#if defined(__TCUSB__)

#include "2STestCardControl.h"

TestCardControl2S::TestCardControl2S() {}

TestCardControl2S::~TestCardControl2S() {}

void TestCardControl2S::Initialise(uint32_t pUsbBus, uint8_t pUsbDev)
{
    ConfigureUsb(pUsbBus, pUsbDev);
    LOG(INFO) << BOLDBLUE << "Selecting antenna channel to "
              << " disable all charge injection" << RESET;
}
void TestCardControl2S::SetDefaultHybridVoltage()
{
    LOG(INFO) << "Setting default hybrid voltage..." << RESET;
    TC_2SFE_V2 cTC_2SFE(fUsbBus, fUsbDev);
    cTC_2SFE.set_voltage(cTC_2SFE._1250mV);
    // check hybrid voltages
    for(auto cItem: fHybridVoltageMap) { ReadHybridVoltage(cItem.first); }
}
void TestCardControl2S::ReadHybridVoltage(const std::string& pVoltageName)
{
    auto cMapIterator = fHybridVoltageMap.find(pVoltageName);
    if(cMapIterator != fHybridVoltageMap.end())
    {
        auto& cMeasurement = cMapIterator->second;
        LOG(INFO) << BOLDYELLOW << "Reading " << pVoltageName << RESET;
        TC_2SFE_V2         cTC_2SFE(fUsbBus, fUsbDev);
        std::vector<float> cMeasurements(fNreadings, 0.);
        for(int cIndex = 0; cIndex < fNreadings; cIndex++)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(fVoltageMeasurementWait_ms));
            cTC_2SFE.adc_get(cMeasurement, cMeasurements[cIndex]);
            LOG(INFO) << BOLDYELLOW << "\t\t..After waiting for " << (cIndex + 1) * 1e-3 * fVoltageMeasurementWait_ms << " seconds ..."
                      << " reading from test card  : " << cMeasurements[cIndex] << " mV." << RESET;
        }
        fVoltageMeasurement = calculateStats(cMeasurements);
    }
}
void TestCardControl2S::ReadHybridCurrent(const std::string& pCurrentName)
{
    auto cMapIterator = fHybridCurrentMap.find(pCurrentName);
    if(cMapIterator != fHybridCurrentMap.end())
    {
        auto&              cMeasurement = cMapIterator->second;
        TC_2SFE_V2         cTC_2SFE(fUsbBus, fUsbDev);
        std::vector<float> cMeasurements(fNreadings, 0.);
        for(int cIndex = 0; cIndex < fNreadings; cIndex++)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(fVoltageMeasurementWait_ms));
            cTC_2SFE.adc_get(cMeasurement, cMeasurements[cIndex]);
            LOG(INFO) << BOLDBLUE << "\t\t..After waiting for " << (cIndex + 1) * 1e-3 * fVoltageMeasurementWait_ms << " seconds ..."
                      << " reading from test card 1V  : " << cMeasurements[cIndex] << " mA." << RESET;
        }
        fCurrentMeasurement = calculateStats(cMeasurements);
    }
}
#endif