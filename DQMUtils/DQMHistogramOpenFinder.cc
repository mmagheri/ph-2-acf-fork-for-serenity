/*!
        \file                DQMHistogramOpenFinder.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
 */

#include "../DQMUtils/DQMHistogramOpenFinder.h"
#include "../HWDescription/ReadoutChip.h"
#include "../RootUtils/HistContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ChannelContainerStream.h"
#include "../Utils/Container.h"
#include "../Utils/ContainerFactory.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/EmptyContainer.h"
#include "../Utils/HybridContainerStream.h"
#include "../Utils/Occupancy.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Utilities.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

using namespace Ph2_HwDescription;

//========================================================================================================================
DQMHistogramOpenFinder::DQMHistogramOpenFinder() {}

//========================================================================================================================
DQMHistogramOpenFinder::~DQMHistogramOpenFinder() {}

//========================================================================================================================
void DQMHistogramOpenFinder::book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_System::SettingsMap& pSettingsMap)
{
    // copy detector structrure
    fDetectorContainer = &theDetectorStructure;

    // find front-end types
    for(auto cBoard: *fDetectorContainer)
    {
        auto cFrontEndTypes = cBoard->connectedFrontEndTypes();
        fWithCBC            = std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::CBC3) != cFrontEndTypes.end();
        fWithSSA            = std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::SSA) != cFrontEndTypes.end() ||
                   std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), FrontEndType::SSA2) != cFrontEndTypes.end();
    }

    // find maximum number of channels
    std::vector<size_t> cNStripChannels(0);
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    auto cNChannels = theDetectorStructure.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->size();
                    auto cType      = cChip->getFrontEndType();
                    if(cType == FrontEndType::CBC3 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2) { cNStripChannels.push_back(cNChannels); }
                }
            }
        }
    }
    auto fNStripChannels = *std::max_element(std::begin(cNStripChannels), std::end(cNStripChannels));
    auto cSetting        = pSettingsMap.find("PlotSCurves");
    fPlotSCurves         = (cSetting != std::end(pSettingsMap)) ? boost::any_cast<double>(cSetting->second) : 0;
    cSetting             = pSettingsMap.find("FitSCurves");
    fFitSCurves          = (cSetting != std::end(pSettingsMap)) ? boost::any_cast<double>(cSetting->second) : 0;
    if(fFitSCurves) fPlotSCurves = true;

    ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

    if(fPlotSCurves)
    {
        uint16_t nYbins = (fWithSSA) ? 255 : 1024;
        float    minY   = -0.5;
        float    maxY   = (fWithSSA) ? 254.5 : 1023.5;

        HistContainer<TH2F> theTH2FChipStripSCurve("SCurve_OF", "SCurve_OF", fNStripChannels, -0.5, fNStripChannels - 0.5, nYbins, minY, maxY);
        RootContainerFactory::bookChipHistograms<HistContainer<TH2F>>(theOutputFile, theDetectorStructure, fDetectorChipStripSCurveHistograms, theTH2FChipStripSCurve);

        // if(fFitSCurves)
        // {
        //     HistContainer<TH1F> theTH1FChannelStripSCurveContainer("SCurve", "SCurve", nYbins, minY, maxY);
        //     RootContainerFactory::bookChannelHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChannelStripSCurveHistograms, theTH1FChannelStripSCurveContainer);
        // }
    }

    // // Pedestal
    // HistContainer<TH1F> theTH1FChipStripPedestalContainer("PedestalDistribution", "PedestalDistribution", 2048, -0.5, 1023.5);
    // RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChipStripPedestalHistograms, theTH1FChipStripPedestalContainer);
    // //
    // HistContainer<TH1F> theTH1FChannelStripPedestalContainer("ChannelPedestalDistribution", "ChannelPedestal", fNStripChannels, -0.5, float(fNStripChannels) - 0.5);
    // RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChannelStripPedestalHistograms, theTH1FChannelStripPedestalContainer);

    // // Noise
    // HistContainer<TH1F> theTH1FHybridStripNoiseContainer("HybridStripNoiseDistribution", "HybridStripNoise", fNStripChannels * 8, -0.5, float(fNStripChannels) * 8 - 0.5);
    // RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridStripNoiseHistograms, theTH1FHybridStripNoiseContainer);
    // //
    // HistContainer<TH1F> theTH1FChipStripNoiseContainer("NoiseDistribution", "NoiseDistribution", 200, 0., 20.);
    // RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChipStripNoiseHistograms, theTH1FChipStripNoiseContainer);
    // //
    // HistContainer<TH1F> theTH1FChannelStripNoiseContainer("ChannelNoiseDistribution", "ChannelNoise", fNStripChannels, -0.5, float(fNStripChannels) - 0.5);
    // RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChannelStripNoiseHistograms, theTH1FChannelStripNoiseContainer);

    // if(fWithCBC)
    // {
    //     // Strip Noise Even
    //     HistContainer<TH1F> theTH1FHybridStripNoiseEvenContainer("HybridNoiseEvenDistribution", "HybridNoiseEven", fNStripChannels * 4, -0.5, float(fNStripChannels) * 4 - 0.5);
    //     RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridStripNoiseEvenHistograms, theTH1FHybridStripNoiseEvenContainer);
    //     //
    //     HistContainer<TH1F> theTH1FChannelStripNoiseEvenContainer("ChannelNoiseEvenDistribution", "ChannelNoiseEven", fNStripChannels / 2, -0.5, float(fNStripChannels / 2) - 0.5);
    //     RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChannelStripNoiseEvenHistograms, theTH1FChannelStripNoiseEvenContainer);

    //     // Strip Noise Odd
    //     HistContainer<TH1F> theTH1FHybridStripNoiseOddContainer("HybridNoiseOddDistribution", "HybridNoiseOdd", fNStripChannels * 4, -0.5, float(fNStripChannels) * 4 - 0.5);
    //     RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridStripNoiseOddHistograms, theTH1FHybridStripNoiseOddContainer);
    //     //
    //     HistContainer<TH1F> theTH1FChannelStripNoiseOddContainer("ChannelNoiseOddDistribution", "ChannelNoiseOdd", fNStripChannels / 2, -0.5, float(fNStripChannels / 2) - 0.5);
    //     RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorChannelStripNoiseOddHistograms, theTH1FChannelStripNoiseOddContainer);
    // }

    // // Validation
    // HistContainer<TH1F> theTH1FStripValidationContainer("Occupancy", "Occupancy", fNStripChannels, -0.5, float(fNStripChannels) - 0.5);
    // RootContainerFactory::bookChipHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorStripValidationHistograms, theTH1FStripValidationContainer);

    // // Reset query function from only including strip chips in the data container
    // fDetectorContainer->resetReadoutChipQueryFunction();

    // SCurve
    // if(fPlotSCurves && fFitSCurves) { ContainerFactory::copyAndInitStructure<ThresholdAndNoise>(theDetectorStructure, fThresholdAndNoiseContainer); }

    // Hybrid Noise
    // HistContainer<TH1F> theTH1FHybridNoiseContainer("HybridNoiseDistribution", "HybridNoiseDistribution", 200, 0., 20.);
    // RootContainerFactory::bookHybridHistograms<HistContainer<TH1F>>(theOutputFile, theDetectorStructure, fDetectorHybridNoiseHistograms, theTH1FHybridNoiseContainer);
}

//========================================================================================================================
bool DQMHistogramOpenFinder::fill(std::vector<char>& dataBuffer) { return false; }

//========================================================================================================================
void DQMHistogramOpenFinder::process()
{
    // if(fFitSCurves) fitSCurves();

    // for(auto cBoard: *fDetectorContainer)
    // {
    //     for(auto cOpticalGroup: *cBoard)
    //     {
    //         for(auto cHybrid: *cOpticalGroup)
    //         {
    //             std::string validationCanvasName = "Validation_B_" + std::to_string(cBoard->getId()) + "_O_" + std::to_string(cOpticalGroup->getId()) + "_H_" + std::to_string(cHybrid->getId());
    //             std::string pedeNoiseCanvasName  = "PedeNoise_B_" + std::to_string(cBoard->getId()) + "_O_" + std::to_string(cOpticalGroup->getId()) + "_H_" + std::to_string(cHybrid->getId());

    //             TCanvas* cValidation = new TCanvas(validationCanvasName.data(), validationCanvasName.data(), 0, 0, 650, fPlotSCurves ? 900 : 650);
    //             TCanvas* cPedeNoise  = new TCanvas(pedeNoiseCanvasName.data(), pedeNoiseCanvasName.data(), 670, 0, 650, 650);

    //             cValidation->Divide(cHybrid->size(), fPlotSCurves ? 3 : 2);
    //             cPedeNoise->Divide(cHybrid->size(), 2);

    //             for(auto cChip: *cHybrid)
    //             {
    //                 auto cType = cChip->getFrontEndType();
    //                 if(cType == FrontEndType::CBC3 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
    //                 {
    //                     cValidation->cd(cChip->getIndex() + 1 + cHybrid->size() * 0);
    //                     TH1F* validationHistogram = fDetectorStripValidationHistograms.getObject(cBoard->getId())
    //                                                     ->getObject(cOpticalGroup->getId())
    //                                                     ->getObject(cHybrid->getId())
    //                                                     ->getObject(cChip->getId())
    //                                                     ->getSummary<HistContainer<TH1F>>()
    //                                                     .fTheHistogram;
    //                     validationHistogram->SetStats(false);
    //                     validationHistogram->DrawCopy();
    //                     gPad->SetLogy();

    //                     cPedeNoise->cd(cChip->getIndex() + 1 + cHybrid->size() * 1);
    //                     fDetectorChipStripPedestalHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getObject(cChip->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->DrawCopy();

    //                     cPedeNoise->cd(cChip->getIndex() + 1 + cHybrid->size() * 0);
    //                     fDetectorChipStripNoiseHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getObject(cChip->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->DrawCopy();

    //                     if(fWithCBC)
    //                     {
    //                         cValidation->cd(cChip->getIndex() + 1 + cHybrid->size() * 1);
    //                         TH1F* cChannelStripNoiseEvenHistogram = fDetectorChannelStripNoiseEvenHistograms.getObject(cBoard->getId())
    //                                                                     ->getObject(cOpticalGroup->getId())
    //                                                                     ->getObject(cHybrid->getId())
    //                                                                     ->getObject(cChip->getId())
    //                                                                     ->getSummary<HistContainer<TH1F>>()
    //                                                                     .fTheHistogram;
    //                         TH1F* cChannelStripNoiseOddHistogram = fDetectorChannelStripNoiseOddHistograms.getObject(cBoard->getId())
    //                                                                    ->getObject(cOpticalGroup->getId())
    //                                                                    ->getObject(cHybrid->getId())
    //                                                                    ->getObject(cChip->getId())
    //                                                                    ->getSummary<HistContainer<TH1F>>()
    //                                                                    .fTheHistogram;
    //                         cChannelStripNoiseEvenHistogram->SetLineColor(kBlue);
    //                         cChannelStripNoiseEvenHistogram->SetMaximum(20);
    //                         cChannelStripNoiseEvenHistogram->SetMinimum(0);
    //                         cChannelStripNoiseOddHistogram->SetLineColor(kRed);
    //                         cChannelStripNoiseOddHistogram->SetMaximum(20);
    //                         cChannelStripNoiseOddHistogram->SetMinimum(0);
    //                         cChannelStripNoiseEvenHistogram->SetStats(false);
    //                         cChannelStripNoiseOddHistogram->SetStats(false);
    //                         cChannelStripNoiseEvenHistogram->DrawCopy();
    //                         cChannelStripNoiseOddHistogram->DrawCopy("same");

    //                         fDetectorChannelStripNoiseEvenHistograms.getObject(cBoard->getId())
    //                             ->getObject(cOpticalGroup->getId())
    //                             ->getObject(cHybrid->getId())
    //                             ->getObject(cChip->getId())
    //                             ->getSummary<HistContainer<TH1F>>()
    //                             .fTheHistogram->GetYaxis()
    //                             ->SetRangeUser(0., 20.);
    //                         fDetectorChannelStripNoiseOddHistograms.getObject(cBoard->getId())
    //                             ->getObject(cOpticalGroup->getId())
    //                             ->getObject(cHybrid->getId())
    //                             ->getObject(cChip->getId())
    //                             ->getSummary<HistContainer<TH1F>>()
    //                             .fTheHistogram->GetYaxis()
    //                             ->SetRangeUser(0., 20.);
    //                     }
    //                 }
    //                 else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
    //                 {
    //                     cValidation->cd(cChip->getIndex() + 1 + cHybrid->size() * 0);
    //                     TH1F* validationHistogram = fDetectorPixelValidationHistograms.getObject(cBoard->getId())
    //                                                     ->getObject(cOpticalGroup->getId())
    //                                                     ->getObject(cHybrid->getId())
    //                                                     ->getObject(cChip->getId())
    //                                                     ->getSummary<HistContainer<TH1F>>()
    //                                                     .fTheHistogram;
    //                     validationHistogram->SetStats(false);
    //                     validationHistogram->DrawCopy();
    //                     gPad->SetLogy();

    //                     cPedeNoise->cd(cChip->getIndex() + 1 + cHybrid->size() * 1);
    //                     fDetectorChipPixelPedestalHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getObject(cChip->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->DrawCopy();

    //                     cPedeNoise->cd(cChip->getIndex() + 1 + cHybrid->size() * 0);
    //                     fDetectorChipPixelNoiseHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getObject(cChip->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->DrawCopy();
    //                 }

    //                 if(fPlotSCurves)
    //                 {
    //                     if(cType == FrontEndType::CBC3 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
    //                     {
    //                         TH2F* cChipStripSCurveHist = fDetectorChipStripSCurveHistograms.getObject(cBoard->getId())
    //                                                          ->getObject(cOpticalGroup->getId())
    //                                                          ->getObject(cHybrid->getId())
    //                                                          ->getObject(cChip->getId())
    //                                                          ->getSummary<HistContainer<TH2F>>()
    //                                                          .fTheHistogram;

    //                         TH1D* cTmp = cChipStripSCurveHist->ProjectionY();
    //                         cChipStripSCurveHist->GetYaxis()->SetRangeUser(cTmp->GetBinCenter(cTmp->FindFirstBinAbove(0)) - 10, cTmp->GetBinCenter(cTmp->FindLastBinAbove(0.99)) + 10);
    //                         // cSCurveHist->GetZaxis()->SetRangeUser(0,1.);
    //                         delete cTmp;
    //                         cValidation->cd(cChip->getIndex() + 1 + cHybrid->size() * 2);
    //                         cChipStripSCurveHist->SetStats(false);
    //                         cChipStripSCurveHist->DrawCopy("colz");

    //                         fDetectorChannelStripNoiseHistograms.getObject(cBoard->getId())
    //                             ->getObject(cOpticalGroup->getId())
    //                             ->getObject(cHybrid->getId())
    //                             ->getObject(cChip->getId())
    //                             ->getSummary<HistContainer<TH1F>>()
    //                             .fTheHistogram->GetYaxis()
    //                             ->SetRangeUser(0., 20.);
    //                     }
    //                     else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
    //                     {
    //                         TH2F* cChipPixelSCurveHist = fDetectorChipPixelSCurveHistograms.getObject(cBoard->getId())
    //                                                          ->getObject(cOpticalGroup->getId())
    //                                                          ->getObject(cHybrid->getId())
    //                                                          ->getObject(cChip->getId())
    //                                                          ->getSummary<HistContainer<TH2F>>()
    //                                                          .fTheHistogram;

    //                         TH1D* cTmp = cChipPixelSCurveHist->ProjectionY();
    //                         cChipPixelSCurveHist->GetYaxis()->SetRangeUser(cTmp->GetBinCenter(cTmp->FindFirstBinAbove(0)) - 10, cTmp->GetBinCenter(cTmp->FindLastBinAbove(0.99)) + 10);
    //                         // cSCurveHist->GetZaxis()->SetRangeUser(0,1.);
    //                         delete cTmp;
    //                         cValidation->cd(cChip->getIndex() + 1 + cHybrid->size() * 2);
    //                         cChipPixelSCurveHist->SetStats(false);
    //                         cChipPixelSCurveHist->DrawCopy("colz");

    //                         fDetectorChannelPixelNoiseHistograms.getObject(cBoard->getId())
    //                             ->getObject(cOpticalGroup->getId())
    //                             ->getObject(cHybrid->getId())
    //                             ->getObject(cChip->getId())
    //                             ->getSummary<HistContainer<TH1F>>()
    //                             .fTheHistogram->GetYaxis()
    //                             ->SetRangeUser(0., 20.);
    //                     }
    //                 }
    //             }

    //             if(fWithCBC || fWithSSA)
    //             {
    //                 fDetectorHybridStripNoiseHistograms.getObject(cBoard->getId())
    //                     ->getObject(cOpticalGroup->getId())
    //                     ->getObject(cHybrid->getId())
    //                     ->getSummary<HistContainer<TH1F>>()
    //                     .fTheHistogram->GetXaxis()
    //                     ->SetRangeUser(-0.5, fNStripChannels * 8 - 0.5);
    //                 fDetectorHybridStripNoiseHistograms.getObject(cBoard->getId())
    //                     ->getObject(cOpticalGroup->getId())
    //                     ->getObject(cHybrid->getId())
    //                     ->getSummary<HistContainer<TH1F>>()
    //                     .fTheHistogram->GetYaxis()
    //                     ->SetRangeUser(0., 20.);

    //                 if(fWithCBC)
    //                 {
    //                     TH1F* cHybridStripNoiseEvenHistogram = fDetectorHybridStripNoiseEvenHistograms.getObject(cBoard->getId())
    //                                                                ->getObject(cOpticalGroup->getId())
    //                                                                ->getObject(cHybrid->getId())
    //                                                                ->getSummary<HistContainer<TH1F>>()
    //                                                                .fTheHistogram;
    //                     TH1F* cHybridStripNoiseOddHistogram = fDetectorHybridStripNoiseOddHistograms.getObject(cBoard->getId())
    //                                                               ->getObject(cOpticalGroup->getId())
    //                                                               ->getObject(cHybrid->getId())
    //                                                               ->getSummary<HistContainer<TH1F>>()
    //                                                               .fTheHistogram;
    //                     cHybridStripNoiseEvenHistogram->SetLineColor(kBlue);
    //                     cHybridStripNoiseEvenHistogram->SetMaximum(20);
    //                     cHybridStripNoiseEvenHistogram->SetMinimum(0);
    //                     cHybridStripNoiseOddHistogram->SetLineColor(kRed);
    //                     cHybridStripNoiseOddHistogram->SetMaximum(20);
    //                     cHybridStripNoiseOddHistogram->SetMinimum(0);
    //                     cHybridStripNoiseEvenHistogram->SetStats(false);
    //                     cHybridStripNoiseOddHistogram->SetStats(false);

    //                     fDetectorHybridStripNoiseEvenHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->GetXaxis()
    //                         ->SetRangeUser(-0.5, fNStripChannels * 8 - 0.5);
    //                     fDetectorHybridStripNoiseEvenHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->GetYaxis()
    //                         ->SetRangeUser(0., 20.);

    //                     fDetectorHybridStripNoiseOddHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->GetXaxis()
    //                         ->SetRangeUser(-0.5, fNStripChannels * 8 - 0.5);
    //                     fDetectorHybridStripNoiseOddHistograms.getObject(cBoard->getId())
    //                         ->getObject(cOpticalGroup->getId())
    //                         ->getObject(cHybrid->getId())
    //                         ->getSummary<HistContainer<TH1F>>()
    //                         .fTheHistogram->GetYaxis()
    //                         ->SetRangeUser(0., 20.);
    //                 }
    //             }
    //             if(fWithMPA)
    //             {
    //                 fDetectorHybridPixelNoiseHistograms.getObject(cBoard->getId())
    //                     ->getObject(cOpticalGroup->getId())
    //                     ->getObject(cHybrid->getId())
    //                     ->getSummary<HistContainer<TH1F>>()
    //                     .fTheHistogram->GetXaxis()
    //                     ->SetRangeUser(-0.5, fNPixelChannels * 8 - 0.5);
    //                 fDetectorHybridPixelNoiseHistograms.getObject(cBoard->getId())
    //                     ->getObject(cOpticalGroup->getId())
    //                     ->getObject(cHybrid->getId())
    //                     ->getSummary<HistContainer<TH1F>>()
    //                     .fTheHistogram->GetYaxis()
    //                     ->SetRangeUser(0., 20.);
    //             }
    //         }
    //     }
    // }
}

//========================================================================================================================
void DQMHistogramOpenFinder::reset(void) {}

//========================================================================================================================

//========================================================================================================================
void DQMHistogramOpenFinder::fillSCurvePlots(uint16_t pStripTh, DetectorDataContainer& fSCurveOccupancy)
{
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    uint16_t cTh         = pStripTh;
                    TH2F*    cChipSCurve = fDetectorChipStripSCurveHistograms.getObject(cBoard->getId())
                                            ->getObject(cOpticalGroup->getId())
                                            ->getObject(cHybrid->getId())
                                            ->getObject(cChip->getId())
                                            ->getSummary<HistContainer<TH2F>>()
                                            .fTheHistogram;

                    auto cChannelContainer =
                        fSCurveOccupancy.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->getChannelContainer<Occupancy>();
                    if(cChannelContainer == nullptr) continue;
                    uint16_t cChannelNumber = 0;
                    for(auto cChannel: *cChannelContainer)
                    {
                        float tmpOccupancy      = cChannel.fOccupancy;
                        float tmpOccupancyError = cChannel.fOccupancyError;
                        cChipSCurve->SetBinContent(cChannelNumber + 1, cTh + 1, tmpOccupancy);
                        cChipSCurve->SetBinError(cChannelNumber + 1, cTh + 1, tmpOccupancyError);

                        // if(fFitSCurves)
                        // {
                        //     TH1F* cChannelSCurve = nullptr;

                        //     if(cType == FrontEndType::CBC3 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
                        //     {
                        //         cChannelSCurve = fDetectorChannelStripSCurveHistograms.getObject(cBoard->getId())
                        //                              ->getObject(cOpticalGroup->getId())
                        //                              ->getObject(cHybrid->getId())
                        //                              ->getObject(cChip->getId())
                        //                              ->getChannel<HistContainer<TH1F>>(cChannelNumber)
                        //                              .fTheHistogram;
                        //     }
                        //     else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
                        //     {
                        //         cChannelSCurve = fDetectorChannelPixelSCurveHistograms.getObject(cBoard->getId())
                        //                              ->getObject(cOpticalGroup->getId())
                        //                              ->getObject(cHybrid->getId())
                        //                              ->getObject(cChip->getId())
                        //                              ->getChannel<HistContainer<TH1F>>(cChannelNumber)
                        //                              .fTheHistogram;
                        //     }
                        //     cChannelSCurve->SetBinContent(cTh + 1, tmpOccupancy);
                        //     cChannelSCurve->SetBinError(cTh + 1, tmpOccupancyError);
                        // }
                        ++cChannelNumber;
                    }
                }
            }
        }
    }
}

// void DQMHistogramOpenFinder::fillSCurvePlots(DetectorDataContainer& fThresholds, DetectorDataContainer& fSCurveOccupancy)
// {
//     for(auto cBoard: *fDetectorContainer)
//     {
//         auto cThThisBrd = fThresholds.getObject(cBoard->getId());
//         for(auto cOpticalGroup: *cBoard)
//         {
//             auto cThThisGrp = cThThisBrd->getObject(cOpticalGroup->getId());
//             for(auto cHybrid: *cOpticalGroup)
//             {
//                 auto cThThisHybrd = cThThisGrp->getObject(cHybrid->getId());
//                 for(auto cChip: *cHybrid)
//                 {
//                     auto  cThThisChip = cThThisHybrd->getObject(cChip->getId());
//                     TH2F* cChipSCurve = fDetectorChipStripSCurveHistograms.getObject(cBoard->getId())
//                                           ->getObject(cOpticalGroup->getId())
//                                           ->getObject(cHybrid->getId())
//                                           ->getObject(cChip->getId())
//                                           ->getSummary<HistContainer<TH2F>>()
//                                           .fTheHistogram;

//                     auto cChannelContainer =
//                         fSCurveOccupancy.getObject(cBoard->getId())->getObject(cOpticalGroup->getId())->getObject(cHybrid->getId())->getObject(cChip->getId())->getChannelContainer<Occupancy>();
//                     if(cChannelContainer == nullptr) continue;
//                     uint16_t cChannelNumber = 0;
//                     for(auto cChannel: *cChannelContainer)
//                     {
//                         float tmpOccupancy      = cChannel.fOccupancy;
//                         float tmpOccupancyError = cChannel.fOccupancyError;
//                         cChipSCurve->SetBinContent(cChannelNumber + 1, cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancy);
//                         cChipSCurve->SetBinError(cChannelNumber + 1, cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancyError);

//                         // if(fFitSCurves)
//                         // {
//                         //     TH1F* cChannelSCurve = nullptr;
//                         //     if(cType == FrontEndType::CBC3 || cType == FrontEndType::SSA || cType == FrontEndType::SSA2)
//                         //     {
//                         //         cChannelSCurve = fDetectorChannelStripSCurveHistograms.getObject(cBoard->getId())
//                         //                              ->getObject(cOpticalGroup->getId())
//                         //                              ->getObject(cHybrid->getId())
//                         //                              ->getObject(cChip->getId())
//                         //                              ->getChannel<HistContainer<TH1F>>(cChannelNumber)
//                         //                              .fTheHistogram;
//                         //     }
//                         //     else if(cType == FrontEndType::MPA || cType == FrontEndType::MPA2)
//                         //     {
//                         //         cChannelSCurve = fDetectorChannelPixelSCurveHistograms.getObject(cBoard->getId())
//                         //                              ->getObject(cOpticalGroup->getId())
//                         //                              ->getObject(cHybrid->getId())
//                         //                              ->getObject(cChip->getId())
//                         //                              ->getChannel<HistContainer<TH1F>>(cChannelNumber)
//                         //                              .fTheHistogram;
//                         //     }
//                         //     cChannelSCurve->SetBinContent(cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancy);
//                         //     cChannelSCurve->SetBinError(cThThisChip->getSummary<uint16_t>() + 1, tmpOccupancyError);
//                         // }
//                         ++cChannelNumber;
//                     }
//                 }
//             }
//         }
//     }
// }
