/*!
        \file                DQMHistogramCicInTest.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
 */

#include "../DQMUtils/DQMHistogramCicInTest.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/Container.h"
#include "../Utils/ContainerFactory.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/EmptyContainer.h"
#include "../Utils/GenericDataArray.h"
#include "../Utils/Utilities.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

//========================================================================================================================
DQMHistogramCicInTest::DQMHistogramCicInTest() {}

//========================================================================================================================
DQMHistogramCicInTest::~DQMHistogramCicInTest() {}

//========================================================================================================================
void DQMHistogramCicInTest::book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_System::SettingsMap& pSettingsMap)
{
    // need to get settings from settings map
    parseSettings(pSettingsMap);

    ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);
    // histogram to hold result of alignment procedured
    HistContainer<TH2F> hPhaseTaps("PhaseTap", "Phase Taps; Phy Port [0-12]; Phy Port Input [0-4]", 12, 0, 12, 4, 0, 4);
    RootContainerFactory::bookHybridHistograms(theOutputFile, theDetectorStructure, fPhaseTaps, hPhaseTaps);
    HistContainer<TH2F> hBeDelays("BeDelays", "BE Samplings Delays; Phy Port [0-12]; BE Line [0-7]", 12, 0, 12, 7, 0, 7);
    RootContainerFactory::bookHybridHistograms(theOutputFile, theDetectorStructure, fBeDelays, hBeDelays);
    // histogram to hold aggreagated result of pattern test
    HistContainer<TH2F> hAggErrors("AggregatedErrors", "Aggregated Result of Pattern Test [Errors]; Phy Port [0-12]; BE Line [0-7]", 12, 0, 12, 7, 0, 7);
    RootContainerFactory::bookHybridHistograms(theOutputFile, theDetectorStructure, fLineErrors, hAggErrors);
    HistContainer<TH2F> hAggTxBits("AggregatedTransmittedBits", "Aggregated Result of Pattern Test [Transmitted bits]; Phy Port [0-12]; BE Line [0-7]", 12, 0, 12, 7, 0, 7);
    RootContainerFactory::bookHybridHistograms(theOutputFile, theDetectorStructure, fTransmittedBits, hAggTxBits);

    // histogram to hold result of pattern test
    HistContainer<TH2F> hPatternTest("PatternTest", "Pattern Test; SLVS Line Out; Pattern", 6, 0, 6, 6, 0, 6);
    RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, fPatternTest, hPatternTest);

    HistContainer<TH2F> hManualPhaseScan_Out0("ManualPhaseScan_Out0", "SLVS_Out0; CIC Input Sampling Tap Offset; BE Sampling Delay", 21, -10.5, 10.5, 32, 0, 32);
    RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, fManualPhaseScan_Out0, hManualPhaseScan_Out0);

    HistContainer<TH2F> hManualPhaseScan_Out1("ManualPhaseScan_Out1", "SLVS_Out1; CIC Input Sampling Tap Offset; BE Sampling Delay", 21, -10.5, 10.5, 32, 0, 32);
    RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, fManualPhaseScan_Out1, hManualPhaseScan_Out1);

    HistContainer<TH2F> hManualPhaseScan_Out2("ManualPhaseScan_Out2", "SLVS_Out2; CIC Input Sampling Tap Offset; BE Sampling Delay", 21, -10.5, 10.5, 32, 0, 32);
    RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, fManualPhaseScan_Out2, hManualPhaseScan_Out2);

    HistContainer<TH2F> hManualPhaseScan_Out3("ManualPhaseScan_Out3", "SLVS_Out3; CIC Input Sampling Tap Offset; BE Sampling Delay", 21, -10.5, 10.5, 32, 0, 32);
    RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, fManualPhaseScan_Out3, hManualPhaseScan_Out3);

    HistContainer<TH2F> hManualPhaseScan_Out4("ManualPhaseScan_Out4", "SLVS_Out4; CIC Input Sampling Tap Offset; BE Sampling Delay", 21, -10.5, 10.5, 32, 0, 32);
    RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, fManualPhaseScan_Out4, hManualPhaseScan_Out4);

    HistContainer<TH2F> hManualPhaseScan_Out5("ManualPhaseScan_Out5", "SLVS_Out4; CIC Input Sampling Tap Offset; BE Sampling Delay", 21, -10.5, 10.5, 32, 0, 32);
    RootContainerFactory::bookChipHistograms(theOutputFile, theDetectorStructure, fManualPhaseScan_Out5, hManualPhaseScan_Out5);
}

//========================================================================================================================
bool DQMHistogramCicInTest::fill(std::vector<char>& dataBuffer) { return false; }

//========================================================================================================================
void DQMHistogramCicInTest::process()
{
    // for(auto board: fManualPhaseScan)
    // {
    //     for(auto opticalGroup: *board)
    //     {
    //         for(auto hybrid: *opticalGroup)
    //         {
    //         }
    //     }
    // }
}

//========================================================================================================================

void DQMHistogramCicInTest::reset(void) {}

void DQMHistogramCicInTest::parseSettings(const Ph2_System::SettingsMap& pSettingsMap)
{
    // auto cSetting = pSettingsMap.find("StartLatency");
    // if(cSetting != std::end(pSettingsMap))
    //     fStartLatency = cSetting->second;
    // else
    //     fStartLatency = 0;

    // cSetting = pSettingsMap.find("LatencyRange");
    // if(cSetting != std::end(pSettingsMap))
    //     fLatencyRange = cSetting->second;
    // else
    //     fLatencyRange = 512;
}

void DQMHistogramCicInTest::fillAlignmentValues(uint8_t pPhyPort, DetectorDataContainer& pTaps, DetectorDataContainer& pDelays)
{
    for(auto cDataThisBrd: pTaps)
    {
        auto& cDelaysThisBrd      = pDelays.at(cDataThisBrd->getIndex());
        auto& cHistTaps_ThisBrd   = fPhaseTaps.at(cDataThisBrd->getIndex());
        auto& cHistDelays_ThisBrd = fBeDelays.at(cDataThisBrd->getIndex());
        for(auto cDataThisOG: *cDataThisBrd)
        {
            auto& cDelaysThisOG      = cDelaysThisBrd->at(cDataThisOG->getIndex());
            auto& cHistTaps_ThisOG   = cHistTaps_ThisBrd->at(cDataThisOG->getIndex());
            auto& cHistDelays_ThisOG = cHistDelays_ThisBrd->at(cDataThisOG->getIndex());
            for(auto cDataThisHybrd: *cDataThisOG)
            {
                auto& cDelaysThisHybrd      = cDelaysThisOG->at(cDataThisOG->getIndex());
                auto& cHistTaps_ThisHybrd   = cHistTaps_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cHistDelays_ThisHybrd = cHistDelays_ThisOG->at(cDataThisOG->getIndex());

                auto& cTapsMap  = cDataThisHybrd->getSummary<std::map<uint8_t, float>>();
                auto& cHistTaps = cHistTaps_ThisHybrd->getSummary<HistContainer<TH2F>>().fTheHistogram;
                for(auto cMapItem: cTapsMap)
                {
                    auto cBin = cHistTaps->FindBin((float)pPhyPort, (float)cMapItem.first);
                    if(cHistTaps->GetBinContent(cBin) == 0) // fill only once
                    {
                        cHistTaps->SetBinContent(cBin, cMapItem.second);
                        cHistTaps->SetBinError(cBin, 0);
                    }
                }
                auto& cDelaysMap  = cDelaysThisHybrd->getSummary<std::map<uint8_t, float>>();
                auto& cHistDelays = cHistDelays_ThisHybrd->getSummary<HistContainer<TH2F>>().fTheHistogram;
                for(auto cMapItem: cDelaysMap)
                {
                    auto cBin = cHistDelays->FindBin((float)pPhyPort, (float)cMapItem.first);
                    if(cHistDelays->GetBinContent(cBin) == 0) // fill only once
                    {
                        cHistDelays->SetBinContent(cBin, cMapItem.second);
                        cHistDelays->SetBinError(cBin, 0);
                    }
                }
            } // hybrids
        }     // optical groups
    }         // boards
}

void DQMHistogramCicInTest::fillAggregatedPatternCheck(uint8_t pPhyPort, DetectorDataContainer& pErrors, DetectorDataContainer& pTxBits)
{
    for(auto cDataThisBrd: pErrors)
    {
        auto& cTxBits_ThisBrd     = pTxBits.at(cDataThisBrd->getIndex());
        auto& cHistRxErr_ThisBrd  = fLineErrors.at(cDataThisBrd->getIndex());
        auto& cHistTxBits_ThisBrd = fTransmittedBits.at(cDataThisBrd->getIndex());
        for(auto cDataThisOG: *cDataThisBrd)
        {
            auto& cTxBits_ThisOG     = cTxBits_ThisBrd->at(cDataThisOG->getIndex());
            auto& cHistRxErr_ThisOG  = cHistRxErr_ThisBrd->at(cDataThisOG->getIndex());
            auto& cHistTxBits_ThisOG = cHistTxBits_ThisBrd->at(cDataThisOG->getIndex());
            for(auto cDataThisHybrd: *cDataThisOG)
            {
                auto& cHistRxErr_ThisHybrd = cHistRxErr_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cErrorsMap           = cDataThisHybrd->getSummary<std::map<uint8_t, float>>();
                auto& cHistRxErr           = cHistRxErr_ThisHybrd->getSummary<HistContainer<TH2F>>().fTheHistogram;
                for(auto cMapItem: cErrorsMap)
                {
                    auto cBin        = cHistRxErr->FindBin((float)pPhyPort, (float)cMapItem.first);
                    auto cBinContent = cHistRxErr->GetBinContent(cBin);
                    cHistRxErr->SetBinContent(cBin, cBinContent + cMapItem.second);
                    cBinContent = cHistRxErr->GetBinContent(cBin);
                    cHistRxErr->SetBinError(cBin, std::sqrt(cBinContent));
                }

                auto& cTxBits_ThisHybrd     = cTxBits_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cTxBitsMap            = cTxBits_ThisHybrd->getSummary<std::map<uint8_t, float>>();
                auto& cHistTxBits_ThisHybrd = cHistTxBits_ThisOG->at(cDataThisOG->getIndex());
                auto& cHistTxBits           = cHistTxBits_ThisHybrd->getSummary<HistContainer<TH2F>>().fTheHistogram;
                for(auto cMapItem: cTxBitsMap)
                {
                    auto cBin        = cHistTxBits->FindBin((float)pPhyPort, (float)cMapItem.first);
                    auto cBinContent = cHistTxBits->GetBinContent(cBin);
                    cHistTxBits->SetBinContent(cBin, cBinContent + cMapItem.second);
                    cBinContent = cHistTxBits->GetBinContent(cBin);
                    cHistTxBits->SetBinError(cBin, std::sqrt(cBinContent));
                }
            } // hybrids
        }     // optical groups
    }         // boards
}

void DQMHistogramCicInTest::fillPatternCheck(int pPatternId, uint8_t pPattern, DetectorDataContainer& pData)
{
    for(auto cDataThisBrd: pData)
    {
        auto& cHist_ThisBrd = fPatternTest.at(cDataThisBrd->getIndex());
        for(auto cDataThisOG: *cDataThisBrd)
        {
            auto& cHist_ThisOG = cHist_ThisBrd->at(cDataThisOG->getIndex());
            for(auto cDataThisHybrd: *cDataThisOG)
            {
                auto& cHist_ThisHybrd = cHist_ThisOG->at(cDataThisHybrd->getIndex());
                for(auto cDataThisChip: *cDataThisHybrd)
                {
                    auto&             cMap           = cDataThisChip->getSummary<std::map<uint8_t, float>>();
                    auto&             cHist_ThisChip = cHist_ThisHybrd->at(cDataThisChip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
                    auto              cBinYaxis      = cHist_ThisChip->GetYaxis()->FindBin((float)pPatternId);
                    std::stringstream cLbl;
                    cLbl << std::bitset<8>(pPattern);
                    cHist_ThisChip->GetYaxis()->SetBinLabel(cBinYaxis, cLbl.str().c_str());
                    for(auto cMapItem: cMap)
                    {
                        auto cBin = cHist_ThisChip->FindBin((float)cMapItem.first, (float)pPatternId);
                        if(cHist_ThisChip->GetBinContent(cBin) == 0)
                        {
                            // LOG (INFO) << BOLDYELLOW << "Hist fill ROC#" << +cDataThisChip->getId()
                            //     << " Line#" << +cMapItem.first
                            //     << " Bin#" << cBin << " : " << cMapItem.second << RESET;
                            cHist_ThisChip->SetBinContent(cBin, cMapItem.second);
                            cHist_ThisChip->SetBinError(cBin, 0);
                        }
                    }
                } // ROCs
            }     // hybrids
        }         // optical groups
    }             // boards
}
void DQMHistogramCicInTest::fillManualPhaseScan(uint8_t pSamplingDelay, int pCicTapOffset, DetectorDataContainer& pData)
{
    for(auto cDataThisBrd: pData)
    {
        auto& cOut0_ThisBrd = fManualPhaseScan_Out0.at(cDataThisBrd->getIndex());
        auto& cOut1_ThisBrd = fManualPhaseScan_Out1.at(cDataThisBrd->getIndex());
        auto& cOut2_ThisBrd = fManualPhaseScan_Out2.at(cDataThisBrd->getIndex());
        auto& cOut3_ThisBrd = fManualPhaseScan_Out3.at(cDataThisBrd->getIndex());
        auto& cOut4_ThisBrd = fManualPhaseScan_Out4.at(cDataThisBrd->getIndex());
        auto& cOut5_ThisBrd = fManualPhaseScan_Out5.at(cDataThisBrd->getIndex());
        for(auto cDataThisOG: *cDataThisBrd)
        {
            auto& cOut0_ThisOG = cOut0_ThisBrd->at(cDataThisOG->getIndex());
            auto& cOut1_ThisOG = cOut1_ThisBrd->at(cDataThisOG->getIndex());
            auto& cOut2_ThisOG = cOut2_ThisBrd->at(cDataThisOG->getIndex());
            auto& cOut3_ThisOG = cOut3_ThisBrd->at(cDataThisOG->getIndex());
            auto& cOut4_ThisOG = cOut4_ThisBrd->at(cDataThisOG->getIndex());
            auto& cOut5_ThisOG = cOut5_ThisBrd->at(cDataThisOG->getIndex());
            for(auto cDataThisHybrd: *cDataThisOG)
            {
                auto& cOut0_ThisHybrd = cOut0_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cOut1_ThisHybrd = cOut1_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cOut2_ThisHybrd = cOut2_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cOut3_ThisHybrd = cOut3_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cOut4_ThisHybrd = cOut4_ThisOG->at(cDataThisHybrd->getIndex());
                auto& cOut5_ThisHybrd = cOut5_ThisOG->at(cDataThisHybrd->getIndex());
                for(auto cDataThisChip: *cDataThisHybrd)
                {
                    auto& cData          = cDataThisChip->getSummary<std::map<uint8_t, float>>();
                    auto& cOut0_ThisChip = cOut0_ThisHybrd->at(cDataThisChip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
                    auto& cOut1_ThisChip = cOut1_ThisHybrd->at(cDataThisChip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
                    auto& cOut2_ThisChip = cOut2_ThisHybrd->at(cDataThisChip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
                    auto& cOut3_ThisChip = cOut3_ThisHybrd->at(cDataThisChip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
                    auto& cOut4_ThisChip = cOut4_ThisHybrd->at(cDataThisChip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
                    auto& cOut5_ThisChip = cOut5_ThisHybrd->at(cDataThisChip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
                    auto  cBin           = cOut0_ThisChip->FindBin((float)pCicTapOffset, (float)pSamplingDelay);
                    // extrapolated upper limits on errors in a million transmitted bits
                    if(cOut0_ThisChip->GetBinContent(cBin) == 0 && cData.find(0) != cData.end())
                    {
                        LOG(DEBUG) << BOLDYELLOW << "Hist fill ROC#" << +cDataThisChip->getId() << " Line#0"
                                   << " Bin#" << cBin << " : " << cData[0] << RESET;
                        cOut0_ThisChip->SetBinContent(cBin, cData[0]);
                        cOut0_ThisChip->SetBinError(cBin, 0);
                    }
                    if(cOut1_ThisChip->GetBinContent(cBin) == 0 && cData.find(1) != cData.end())
                    {
                        LOG(DEBUG) << BOLDYELLOW << "Hist fill ROC#" << +cDataThisChip->getId() << " Line#1"
                                   << " Bin#" << cBin << " : " << cData[1] << RESET;
                        cOut1_ThisChip->SetBinContent(cBin, cData[1]);
                        cOut1_ThisChip->SetBinError(cBin, 0);
                    }
                    if(cOut2_ThisChip->GetBinContent(cBin) == 0 && cData.find(2) != cData.end())
                    {
                        LOG(DEBUG) << BOLDYELLOW << "Hist fill ROC#" << +cDataThisChip->getId() << " Line#2"
                                   << " Bin#" << cBin << " : " << cData[2] << RESET;
                        cOut2_ThisChip->SetBinContent(cBin, cData[2]);
                        cOut2_ThisChip->SetBinError(cBin, 0);
                    }
                    if(cOut3_ThisChip->GetBinContent(cBin) == 0 && cData.find(3) != cData.end())
                    {
                        LOG(DEBUG) << BOLDYELLOW << "Hist fill ROC#" << +cDataThisChip->getId() << " Line#3"
                                   << " Bin#" << cBin << " : " << cData[3] << RESET;
                        cOut3_ThisChip->SetBinContent(cBin, cData[3]);
                        cOut3_ThisChip->SetBinError(cBin, 0);
                    }
                    if(cOut4_ThisChip->GetBinContent(cBin) == 0 && cData.find(4) != cData.end())
                    {
                        LOG(DEBUG) << BOLDYELLOW << "Hist fill ROC#" << +cDataThisChip->getId() << " Line#4"
                                   << " Bin#" << cBin << " : " << cData[4] << RESET;
                        cOut4_ThisChip->SetBinContent(cBin, cData[4]);
                        cOut4_ThisChip->SetBinError(cBin, 0);
                    }
                    if(cOut5_ThisChip->GetBinContent(cBin) == 0 && cData.find(5) != cData.end())
                    {
                        LOG(DEBUG) << BOLDYELLOW << "Hist fill ROC#" << +cDataThisChip->getId() << " Line#5"
                                   << " Bin#" << cBin << " : " << cData[5] << RESET;
                        cOut5_ThisChip->SetBinContent(cBin, cData[5]);
                        cOut5_ThisChip->SetBinError(cBin, 0);
                    }
                } // ROCs
            }     // hybrids
        }         // optical groups
    }             // boards
}