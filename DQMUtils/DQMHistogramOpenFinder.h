/*!
        \file                DQMHistogramOpenFinder.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Younes Otarid
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
        Support :            mail to : younes.otarid@cern.ch
*/

#ifndef __DQMHistogramOpenFinder_H__
#define __DQMHistogramOpenFinder_H__
#include "../DQMUtils/DQMHistogramBase.h"
#include "../Utils/Container.h"
#include "../Utils/DataContainer.h"

class TFile;

/*!
 * \class DQMHistogramOpenFinder
 * \brief Class for PedeNoise monitoring histograms
 */
class DQMHistogramOpenFinder : public DQMHistogramBase
{
  public:
    /*!
     * constructor
     */
    DQMHistogramOpenFinder();

    /*!
     * destructor
     */
    ~DQMHistogramOpenFinder();

    /*!
     * Book histograms
     */
    void book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_System::SettingsMap& pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill(std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process() override;

    /*!
     * Reset histogram
     */
    void reset(void) override;

    /*!
     * \brief Fill SCurve histograms
     * \param fSCurveOccupancyMap : maps of Vthr and DataContainer
     */
    void fillSCurvePlots(uint16_t pStripTh, DetectorDataContainer& fSCurveOccupancy);

  private:
    DetectorContainer*    fDetectorContainer;
    DetectorDataContainer fDetectorData;
    DetectorDataContainer fDetectorChipStripSCurveHistograms;

    bool fPlotSCurves{false};
    bool fFitSCurves{false};
    bool fWithSSA{false};
    bool fWithCBC{false};
};
#endif
