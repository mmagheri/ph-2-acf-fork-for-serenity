/*!
        \file                DQMHistogramLatencyScan.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
*/

#ifndef __DQMHistogramCicInTest_H__
#define __DQMHistogramCicInTest_H__
#include "../DQMUtils/DQMHistogramBase.h"
#include "../Utils/Container.h"
#include "../Utils/DataContainer.h"

class TFile;

/*!
 * \class DQMHistogramLatencyScan
 * \brief Class for PedeNoise monitoring histograms
 */
class DQMHistogramCicInTest : public DQMHistogramBase
{
  public:
    /*!
     * constructor
     */
    DQMHistogramCicInTest();

    /*!
     * destructor
     */
    ~DQMHistogramCicInTest();

    /*!
     * Book histograms
     */
    void book(TFile* theOutputFile, DetectorContainer& theDetectorStructure, const Ph2_System::SettingsMap& pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill(std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process() override;

    /*!
     * Reset histogram
     */
    void reset(void) override;
    // virtual void summarizeHistos();

    // Histogram Fillers
    void fillAggregatedPatternCheck(uint8_t pPhyPort, DetectorDataContainer& pErrors, DetectorDataContainer& pBits);
    void fillAlignmentValues(uint8_t pPhyPort, DetectorDataContainer& pTaps, DetectorDataContainer& pDelays);
    void fillManualPhaseScan(uint8_t pSamplingDelay, int pCicTapOffset, DetectorDataContainer& pData);
    void fillPatternCheck(int pPatternId, uint8_t pPattern, DetectorDataContainer& pData);

  private:
    void parseSettings(const Ph2_System::SettingsMap& pSettingsMap);

    DetectorDataContainer fDetectorData;
    DetectorDataContainer fPatternTest;
    DetectorDataContainer fPhaseTaps;
    DetectorDataContainer fBeDelays;
    DetectorDataContainer fLineErrors;
    DetectorDataContainer fTransmittedBits;

    DetectorDataContainer fManualPhaseScan_Out0; // SLVS out 0 from FE
    DetectorDataContainer fManualPhaseScan_Out1; // SLVS out 1 from FE
    DetectorDataContainer fManualPhaseScan_Out2; // SLVS out 2 from FE
    DetectorDataContainer fManualPhaseScan_Out3; // SLVS out 3 from FE
    DetectorDataContainer fManualPhaseScan_Out4; // SLVS out 4 from FE
    DetectorDataContainer fManualPhaseScan_Out5; // SLVS out 5 from FE
};
#endif
