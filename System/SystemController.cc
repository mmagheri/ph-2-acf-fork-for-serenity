/*!
  \file                  SystemController.cc
  \brief                 Controller of the System, overall wrapper of the framework
  \author                Mauro DINARDO
  \version               2.0
  \date                  01/01/20
  Support:               email to mauro.dinardo@cern.ch
*/

#include "SystemController.h"
#include "../HWInterface/LinkInterface.h"
#include "../HWInterface/RD53AInterface.h"
#include "../HWInterface/RD53BInterface.h"
#include "../MonitorUtils/CBCMonitor.h"
#include "../MonitorUtils/DetectorMonitor.h"
#include "../MonitorUtils/RD53Monitor.h"
#include "../MonitorUtils/SEHMonitor.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
bool cBrokenPS = false;

namespace Ph2_System
{
SystemController::SystemController()
    : fBeBoardInterface(nullptr)
    , fReadoutChipInterface(nullptr)
    , flpGBTInterface(nullptr)
    , fCicInterface(nullptr)
    , fDetectorContainer(nullptr)
    , fSettingsMap()
    , fFileHandler(nullptr)
    , fRawFileName("")
    , fWriteHandlerEnabled(false)
    , fDQMStreamerEnabled(false)
    , fMonitorDQMStreamerEnabled(false)
    , fDQMStreamer(nullptr)
    , fMonitorDQMStreamer(nullptr)
    , fDetectorMonitor(nullptr)
    , fChannelGroupHandlerContainer(nullptr)
{
}

SystemController::~SystemController() {}

void SystemController::Inherit(const SystemController* pController)
{
    fBeBoardInterface             = pController->fBeBoardInterface;
    fReadoutChipInterface         = pController->fReadoutChipInterface;
    flpGBTInterface               = pController->flpGBTInterface;
    fBeBoardFWMap                 = pController->fBeBoardFWMap;
    fSettingsMap                  = pController->fSettingsMap;
    fFileHandler                  = pController->fFileHandler;
    fDQMStreamerEnabled           = pController->fDQMStreamerEnabled;
    fMonitorDQMStreamerEnabled    = pController->fMonitorDQMStreamerEnabled;
    fDQMStreamer                  = pController->fDQMStreamer;
    fMonitorDQMStreamer           = pController->fMonitorDQMStreamer;
    fDetectorContainer            = pController->fDetectorContainer;
    fCicInterface                 = pController->fCicInterface;
    fPowerSupplyClient            = pController->fPowerSupplyClient;
    fChannelGroupHandlerContainer = pController->fChannelGroupHandlerContainer;

#ifdef __TCP_SERVER__
    fTestcardClient = pController->fTestcardClient;
#endif
}

void SystemController::Destroy()
{
    this->closeFileHandler();

    LOG(INFO) << BOLDRED << ">>> Destroying interfaces <<<" << RESET;

    RD53Event::JoinDecodingThreads();

    delete fDetectorMonitor;
    fDetectorMonitor = nullptr;

    delete fBeBoardInterface;
    fBeBoardInterface = nullptr;

    delete fReadoutChipInterface;
    fReadoutChipInterface = nullptr;

    delete flpGBTInterface;
    flpGBTInterface = nullptr;

    delete fDetectorContainer;
    fDetectorContainer = nullptr;

    delete fCicInterface;
    fCicInterface = nullptr;

    fBeBoardFWMap.clear();
    fSettingsMap.clear();

    delete fDQMStreamer;
    fDQMStreamer = nullptr;

    delete fMonitorDQMStreamer;
    fMonitorDQMStreamer = nullptr;

    delete fPowerSupplyClient;
    fPowerSupplyClient = nullptr;

    delete fChannelGroupHandlerContainer;
    fChannelGroupHandlerContainer = nullptr;

#ifdef __TCP_SERVER__
    delete fTestcardClient;
    fTestcardClient = nullptr;
#endif

    LOG(INFO) << BOLDRED << ">>> Interfaces  destroyed <<<" << RESET;
}

void SystemController::addFileHandler(const std::string& pFilename, char pOption)
{
    if(pOption == 'r')
        fFileHandler = new FileHandler(pFilename, pOption);
    else if(pOption == 'w')
    {
        fRawFileName         = pFilename;
        fWriteHandlerEnabled = true;
    }
}

void SystemController::closeFileHandler()
{
    if(fFileHandler != nullptr)
    {
        if(fFileHandler->isFileOpen() == true) fFileHandler->closeFile();
        delete fFileHandler;
        fFileHandler = nullptr;
    }
}

void SystemController::readFile(std::vector<uint32_t>& pVec, uint32_t pNWords32)
{
    if(pNWords32 == 0)
        pVec = fFileHandler->readFile();
    else
        pVec = fFileHandler->readFileChunks(pNWords32);
}

void SystemController::InitializeHw(const std::string& pFilename, std::ostream& os, bool streamData, uint16_t DQMportNumber, uint16_t monitorDQMportNumber)
{
    fDQMStreamerEnabled        = streamData;
    fMonitorDQMStreamerEnabled = streamData;

    if(streamData == true)
    {
        fDQMStreamer = new TCPPublishServer(DQMportNumber, 1);
        fDQMStreamer->startAccept();

        fMonitorDQMStreamer = new TCPPublishServer(monitorDQMportNumber, 1);
        fMonitorDQMStreamer->startAccept();
    }

    fDetectorContainer = new DetectorContainer;
    this->fParser.parseHW(pFilename, fBeBoardFWMap, fDetectorContainer, os);
    fBeBoardInterface = new BeBoardInterface(fBeBoardFWMap);
    fBeBoardInterface->setBoard(0);

    fChannelGroupHandlerContainer = new DetectorDataContainer();
    ContainerFactory::copyAndInitChip<std::shared_ptr<ChannelGroupHandler>>(*fDetectorContainer, *fChannelGroupHandlerContainer);

    LOG(INFO) << BOLDYELLOW << "Trying to connect to the Power Supply Server..." << RESET;
    fPowerSupplyClient = new TCPClient("127.0.0.1", 7000);
    if(!fPowerSupplyClient->connect(1))
    {
        LOG(INFO) << BOLDYELLOW << "Cannot connect to the Power Supply Server, power supplies will need to be controlled manually" << RESET;
        delete fPowerSupplyClient;
        fPowerSupplyClient = nullptr;
    }
    else
    {
        LOG(INFO) << BOLDYELLOW << "Connected to the Power Supply Server!" << RESET;
    }

#ifdef __TCP_SERVER__
    fTestcardClient = new TCPClient("127.0.0.1", 8000);
    if(!fTestcardClient->connect(1))
    {
        std::cerr << "Cannot connect to the Testcard Server" << '\n';
        delete fTestcardClient;
        fTestcardClient = nullptr;
    }
#endif

    if(fDetectorContainer->size() > 0 && fInitializeInterfaces == 1)
    {
        const BeBoard* cFirstBoard = fDetectorContainer->at(0);
        if(cFirstBoard->getBoardType() != BoardType::RD53)
        {
            LOG(INFO) << BOLDBLUE << "Initializing HwInterfaces for OT BeBoards.." << RESET;
            if(cFirstBoard->size() > 0) // # of optical groups connected to Board0
            {
                auto cFirstOpticalGroup = cFirstBoard->at(0);
                LOG(INFO) << BOLDBLUE << "\t...Initializing HwInterfaces for OpticalGroups.." << +cFirstBoard->size() << " optical group(s) found ..." << RESET;
                bool cWithLpGBT = (cFirstOpticalGroup->flpGBT != nullptr);
                if(cWithLpGBT)
                {
                    LOG(INFO) << BOLDBLUE << "\t\t\t.. Initializing HwInterface for lpGBT" << RESET;
                    flpGBTInterface = new D19clpGBTInterface(fBeBoardFWMap, cFirstOpticalGroup->flpGBT->isOptical());
// check link to external interface
#ifdef __TCUSB__
#if defined(__SEH_USB__) || defined(__ROH_USB__)
                    if(flpGBTInterface->GetExternalController() != nullptr)
                    {
                        LOG(INFO) << BOLDBLUE << "TC interface should be initialized... type is " << flpGBTInterface->GetExternalController()->getName() << RESET;
#ifdef __ROH_USB__
                        // check reading of ADC from PSROH TC
                        float cOutput;
                        flpGBTInterface->GetExternalController()->getInterface().adc_get(TC_PSROH::measurement::_1V25_REF, cOutput);
                        LOG(INFO) << BOLDBLUE << "Checking communication with test card by reading 1V25_Ref : " << cOutput << RESET;
#endif
                    }
#endif
#endif
                }

                LOG(INFO) << BOLDBLUE << "Found " << +cFirstOpticalGroup->size() << " hybrids in this group..." << RESET;
                if(cFirstOpticalGroup->size() > 0) // # of hybrids connected to OpticalGroup0
                {
                    LOG(INFO) << BOLDBLUE << "\t\t...Initializing HwInterfaces for FrontEnd Hybrids.." << +cFirstOpticalGroup->size() << " hybrid(s) found ..." << RESET;
                    auto cFirstHybrid = cFirstOpticalGroup->at(0);
                    auto cType        = FrontEndType::CBC3;
                    bool cWithCBC  = (std::find_if(cFirstHybrid->begin(), cFirstHybrid->end(), [&cType](Ph2_HwDescription::Chip* x) { return x->getFrontEndType() == cType; }) != cFirstHybrid->end());
                    cType          = FrontEndType::SSA;
                    bool cWithSSA  = (std::find_if(cFirstHybrid->begin(), cFirstHybrid->end(), [&cType](Ph2_HwDescription::Chip* x) { return x->getFrontEndType() == cType; }) != cFirstHybrid->end());
                    cType          = FrontEndType::SSA2;
                    bool cWithSSA2 = (std::find_if(cFirstHybrid->begin(), cFirstHybrid->end(), [&cType](Ph2_HwDescription::Chip* x) { return x->getFrontEndType() == cType; }) != cFirstHybrid->end());
                    cType          = FrontEndType::MPA;
                    bool cWithMPA  = (std::find_if(cFirstHybrid->begin(), cFirstHybrid->end(), [&cType](Ph2_HwDescription::Chip* x) { return x->getFrontEndType() == cType; }) != cFirstHybrid->end());
                    cType          = FrontEndType::MPA2;
                    bool cWithMPA2 = (std::find_if(cFirstHybrid->begin(), cFirstHybrid->end(), [&cType](Ph2_HwDescription::Chip* x) { return x->getFrontEndType() == cType; }) != cFirstHybrid->end());
                    bool cMPAtype  = cWithMPA2 | cWithMPA;
                    bool cSSAtype  = cWithSSA2 | cWithSSA;

                    if(cWithCBC)
                    {
                        LOG(INFO) << BOLDBLUE << "\t\t\t\t.. Initializing HwInterface(s) for CBC(s)" << RESET;
                        fReadoutChipInterface = new CbcInterface(fBeBoardFWMap);
                    }

                    if(cSSAtype && !cMPAtype) // SSA boards?
                    {
                        if(cWithSSA)
                        {
                            LOG(INFO) << BOLDBLUE << "\t\t\t\t.. Initializing HwInterface(s) for SSA(s)" << RESET;
                            fReadoutChipInterface = new SSAInterface(fBeBoardFWMap);
                        }
                        if(cWithSSA2)
                        {
                            LOG(INFO) << BOLDBLUE << "\t\t\t\t.. Initializing HwInterface(s) for SSA2(s)" << RESET;
                            fReadoutChipInterface = new SSA2Interface(fBeBoardFWMap);
                        }
                    }

                    if(cMPAtype && !cSSAtype) // MPA boards?
                    {
                        if(cWithMPA)
                        {
                            LOG(INFO) << BOLDBLUE << "\t\t\t\t.. Initializing HwInterface(s) for MPA(s)" << RESET;
                            fReadoutChipInterface = new MPAInterface(fBeBoardFWMap);
                        }
                        if(cWithMPA2)
                        {
                            LOG(INFO) << BOLDBLUE << "\t\t\t\t.. Initializing HwInterface(s) for MPA(s)" << RESET;
                            fReadoutChipInterface = new MPA2Interface(fBeBoardFWMap);
                        }
                    }
                    if((cMPAtype && cSSAtype) && cWithLpGBT)
                    {
                        LOG(INFO) << BOLDBLUE << "\t\t\t\t.. Initializing HwInterface(s) for PS module(s)" << RESET;
                        fReadoutChipInterface = new PSInterface(fBeBoardFWMap);
                    }
                    if(fReadoutChipInterface != nullptr)
                    {
                        bool cFoundLpgbt = fReadoutChipInterface->lpGBTCheck(cFirstBoard);
                        if(cFoundLpgbt) LOG(INFO) << BOLDGREEN << "\t\t\t\t\t.. Readout chip interface aware of the lpGBT connected to this board ... " << RESET;
                        if(cWithMPA && cWithSSA) static_cast<PSInterface*>(fReadoutChipInterface)->SetOptical();
                    }
                } // creat Chip interfaces

                LOG(INFO) << BOLDBLUE << "\t\t\t.. Initializing HwInterface for CIC" << RESET;
                fCicInterface = new CicInterface(fBeBoardFWMap);
                // check event type
                bool cWithCBC3 = false;
                if(cFirstBoard->getBoardType() == BoardType::DTC){
                    cWithCBC3 = !(cFirstBoard->getEventType() == EventType::DTC2SUNSPARSIFIED);
                }
                else cWithCBC3 = !(cFirstBoard->getEventType() == EventType::VR2S);
                fCicInterface->setWith8CBC3(cWithCBC3);
                if(cFirstOpticalGroup->flpGBT != nullptr)
                {
                    bool cFoundLpgbt = fCicInterface->lpGBTCheck(cFirstBoard);
                    if(cFoundLpgbt) LOG(INFO) << BOLDGREEN << "\t\t\t\t\t.. CIC interface aware of the lpGBT connected to this board ... " << RESET;
                }
                else
                    fCicInterface->setWithLpGBT(false);
            }
        }
        else
        {
            flpGBTInterface = new RD53lpGBTInterface(fBeBoardFWMap);
            if(cFirstBoard->getFrontEndType() == FrontEndType::RD53A)
                fReadoutChipInterface = new RD53AInterface(fBeBoardFWMap);
            else
                fReadoutChipInterface = new RD53BInterface(fBeBoardFWMap);
            RD53Shared::firstChip = static_cast<RD53*>(fDetectorContainer->at(0)->at(0)->at(0)->at(0));
        }
    } // if there is something to create an interface for

    if(fWriteHandlerEnabled == true) this->initializeWriteFileHandler();

    DetectorMonitorConfig theDetectorMonitorConfig;
    std::string           monitoringType = fParser.parseMonitor(pFilename, theDetectorMonitorConfig, os);

    if(monitoringType != "None")
    {
        if(monitoringType == "2S")
            fDetectorMonitor = new CBCMonitor(this, theDetectorMonitorConfig);
        else if(monitoringType == "RD53")
            fDetectorMonitor = new RD53Monitor(this, theDetectorMonitorConfig);
        else if(monitoringType == "2SSEH")
            fDetectorMonitor = new SEHMonitor(this, theDetectorMonitorConfig);
        else
        {
            LOG(ERROR) << BOLDRED << "Unrecognized monitor type, Aborting" << RESET;
            abort();
        }
        fDetectorMonitor->forkMonitor();
    }

    // ############################################
    // # Make sure  all interfaces are configured #
    // ############################################
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getBoardType() != BoardType::D19C) continue;
        fBeBoardInterface->setBoard(cBoard->getId());
        static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface())->ConfigureInterfaces(cBoard);
    }

    // ###################
    // # Set module type #
    // ###################
    /*for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getBoardType() == BoardType::RD53) continue;

        auto cConnectedFeTypes = cBoard->connectedFrontEndTypes();
        bool cMPAfound =
            (std::find_if(cConnectedFeTypes.begin(), cConnectedFeTypes.end(), [](FrontEndType x) { return (x == FrontEndType::MPA || x == FrontEndType::MPA2); }) != cConnectedFeTypes.end());
        bool cSSAfound =
            (std::find_if(cConnectedFeTypes.begin(), cConnectedFeTypes.end(), [](FrontEndType x) { return (x == FrontEndType::SSA || x == FrontEndType::SSA2); }) != cConnectedFeTypes.end());
        bool cCBCfound = (std::find_if(cConnectedFeTypes.begin(), cConnectedFeTypes.end(), [](FrontEndType x) { return x == FrontEndType::CBC3; }) != cConnectedFeTypes.end());
        for(auto cOpticalGroup: *cBoard)
        {
            bool cWithLpGBT    = (cOpticalGroup->flpGBT != nullptr);
            bool cWithPSmodule = (cMPAfound || cSSAfound) && cWithLpGBT;
            bool cWith2Smodule = cCBCfound && cWithLpGBT;
            bool cWithPSHybrid = (cSSAfound && !cWithLpGBT);
            bool cWith2SHybrid = (cCBCfound && !cWithLpGBT);

            if(cWithPSmodule) { cOpticalGroup->setFrontEndType(FrontEndType::OuterTrackerPS); }
            else if(cWith2Smodule)
            {
                cOpticalGroup->setFrontEndType(FrontEndType::OuterTracker2S);
            }
            else if(cWithPSHybrid)
            {
                LOG(INFO) << BOLDYELLOW << "HYBRIDPS" << RESET;
                cOpticalGroup->setFrontEndType(FrontEndType::HYBRIDPS);
            }
            else if(cWith2SHybrid)
            {
                cOpticalGroup->setFrontEndType(FrontEndType::HYBRID2S);
            }
            else if(cWithLpGBT && flpGBTInterface != nullptr)
                static_cast<D19clpGBTInterface*>(flpGBTInterface)->setFrontEndType(cOpticalGroup->getFrontEndType());
            else
                LOG(INFO) << BOLDMAGENTA << "UN-KNOWN MODULE TYPE" << RESET;
        }
    }*/
    LOG(INFO) << BOLDYELLOW << "SystemController::End InitializeHw" << RESET;
}

void SystemController::InitializeSettings(const std::string& pFilename, std::ostream& os) { this->fParser.parseSettings(pFilename, fSettingsMap, os); }

void SystemController::ReadSystemMonitor(BeBoard* pBoard, const std::vector<std::string>& args) const
{
    if(args.size() != 0)
        for(const auto cOpticalGroup: *pBoard)
            for(const auto cHybrid: *cOpticalGroup)
                for(const auto cChip: *cHybrid)
                {
                    LOG(INFO) << GREEN << "Monitor data for [board/opticalGroup/hybrid/chip = " << BOLDYELLOW << pBoard->getId() << "/" << cOpticalGroup->getId() << "/" << cHybrid->getId() << "/"
                              << +cChip->getId() << RESET << GREEN << "]" << RESET;
                    fBeBoardInterface->ReadHybridVoltageMonitor(fReadoutChipInterface, cChip);
                    fBeBoardInterface->ReadHybridTemperatureMonitor(fReadoutChipInterface, cChip);
                    fBeBoardInterface->ReadChipMonitor(fReadoutChipInterface, cChip, args);
                    LOG(INFO) << BOLDBLUE << "\t--> Done" << RESET;
                }
}

// ######################################
// # Configuring Inner Tracker hardware #
// ######################################
void SystemController::ConfigureIT(BeBoard* pBoard)
{
    // ###################
    // # Configuring FSM #
    // ###################
    size_t nTRIGxEvent = SystemController::findValueInSettings<double>("nTRIGxEvent");
    size_t injType     = SystemController::findValueInSettings<double>("INJtype");
    size_t injLatency  = SystemController::findValueInSettings<double>("InjLatency");
    size_t nClkDelays  = SystemController::findValueInSettings<double>("nClkDelays");
    size_t colStart    = SystemController::findValueInSettings<double>("COLstart");
    LOG(INFO) << CYAN << "=== Configuring FSM fast command block ===" << RESET;

    auto& theBeBoardFW = this->fBeBoardFWMap[pBoard->getId()];

    static_cast<RD53FWInterface*>(theBeBoardFW)
        ->SetAndConfigureFastCommands(pBoard, nTRIGxEvent, injType, injLatency, nClkDelays, RD53Shared::firstChip->getFEtype(colStart, colStart) == &RD53A::SYNC);
    LOG(INFO) << CYAN << "================== Done ==================" << RESET;

    // ########################
    // # Configuring from XML #
    // ########################
    static_cast<RD53FWInterface*>(theBeBoardFW)->ConfigureFromXML(pBoard);

    // ########################
    // # Configure LpGBT chip #
    // ########################
    for(auto cOpticalGroup: *pBoard)
    {
        if(cOpticalGroup->flpGBT != nullptr)
        {
            LOG(INFO) << GREEN << "Initializing communication to Low-power Gigabit Transceiver (LpGBT): " << BOLDYELLOW << +cOpticalGroup->getId() << RESET;

            static_cast<RD53lpGBTInterface*>(flpGBTInterface)->SetDownLinkMapping(cOpticalGroup);
            static_cast<RD53lpGBTInterface*>(flpGBTInterface)->SetUpLinkMapping(cOpticalGroup);
            LOG(INFO) << BOLDBLUE << "\t--> Configured up and down link mapping in firmware" << RESET;

            if(flpGBTInterface->ConfigureChip(cOpticalGroup->flpGBT) == true)
            // && (static_cast<RD53lpGBTInterface*>(flpGBTInterface)->ExternalPhaseAlignRx(cOpticalGroup->flpGBT, pBoard, cOpticalGroup, theBeBoardFW, fReadoutChipInterface) == true)) // @TMP@
            {
                static_cast<RD53lpGBTInterface*>(flpGBTInterface)->PhaseAlignRx(cOpticalGroup->flpGBT, pBoard, cOpticalGroup, fReadoutChipInterface);
                LOG(INFO) << BOLDBLUE << ">>> LpGBT chip configured <<<" << RESET;
            }
            else
                LOG(ERROR) << BOLDRED << ">>> LpGBT chip not configured, reached maximum number of attempts (" << BOLDYELLOW << +RD53Shared::MAXATTEMPTS << BOLDRED << ") <<<" << RESET;
        }
    }

    // #######################
    // # Status optical link #
    // #######################
    uint32_t txStatus, rxStatus, mgtStatus;
    LOG(INFO) << GREEN << "Checking status of the optical links:" << RESET;
    static_cast<RD53FWInterface*>(theBeBoardFW)->StatusOptoLink(txStatus, rxStatus, mgtStatus);

    // ######################################################
    // # Configure down and up links to/from frontend chips #
    // ######################################################
    LOG(INFO) << CYAN << "=== Configuring frontend chip communication ===" << RESET;
    static_cast<RD53Interface*>(fReadoutChipInterface)->InitRD53Downlink(pBoard);
    for(auto cOpticalGroup: *pBoard)
        for(auto cHybrid: *cOpticalGroup)
        {
            LOG(INFO) << GREEN << "Initializing chip communication of hybrid: " << BOLDYELLOW << +cHybrid->getId() << RESET;
            for(const auto cChip: *cHybrid)
            {
                LOG(INFO) << GREEN << "Initializing communicationng to/from RD53: " << BOLDYELLOW << +cChip->getId() << RESET;
                static_cast<RD53Interface*>(fReadoutChipInterface)->InitRD53Uplinks(cChip);
            }
        }
    LOG(INFO) << CYAN << "==================== Done =====================" << RESET;

    // ####################################
    // # Check AURORA lock on data stream #
    // ####################################
    try
    {
        static_cast<RD53FWInterface*>(theBeBoardFW)->CheckChipCommunication(pBoard);
    }
    catch(const std::exception& e)
    {
        LOG(WARNING) << BOLDRED << "===== Aborting: " << BOLDYELLOW << e.what() << BOLDRED << " =====" << RESET;
    }
}

void SystemController::ConfigureFrontendIT(BeBoard* pBoard)
{
    // ############################
    // # Configuration parameters #
    // ############################
    bool resetMask = SystemController::findValueInSettings<double>("ResetMask");
    bool resetTDAC = SystemController::findValueInSettings<double>("ResetTDAC");

    // ############################
    // # Configure frontend chips #
    // ############################
    LOG(INFO) << CYAN << "===== Configuring frontend chip registers =====" << RESET;
    for(auto cOpticalGroup: *pBoard)
        for(auto cHybrid: *cOpticalGroup)
        {
            LOG(INFO) << GREEN << "Configuring chip of hybrid: " << BOLDYELLOW << +cHybrid->getId() << RESET;
            for(const auto cChip: *cHybrid)
            {
                LOG(INFO) << GREEN << "Configuring RD53: " << BOLDYELLOW << +cChip->getId() << RESET << GREEN " (fused ID " << BOLDYELLOW << +fReadoutChipInterface->ReadChipFuseID(cChip) << RESET
                          << GREEN << ")" << RESET;
                if(resetMask == true) static_cast<RD53*>(cChip)->enableAllPixels();
                if(resetTDAC == true) static_cast<RD53*>(cChip)->resetTDAC();
                static_cast<RD53*>(cChip)->copyMaskToDefault();
                static_cast<RD53Interface*>(fReadoutChipInterface)->ConfigureChip(cChip);
                LOG(INFO) << GREEN << "Number of masked pixels: " << BOLDYELLOW << static_cast<RD53*>(cChip)->getNbMaskedPixels() << RESET;
            }
        }

    LOG(INFO) << CYAN << "==================== Done =====================" << RESET;
}

// ######################################
// # Configuring Outer Tracker hardware #
// ######################################
void SystemController::ConfigureOT(BeBoard* pBoard, bool doAlsoFrontend)
{
    // Hard reset Chips on hybrid if lpGBT is there; if no lpGBT this
    // is already taken care of by ConfigureBoard
    // for(auto cOpticalGroup: *pBoard)
    // {
    //     auto& clpGBT = cOpticalGroup->flpGBT;
    //     for(auto cHybrid: *cOpticalGroup)
    //     {
    //         if(clpGBT == nullptr) continue;

    //         // uint8_t cSide = cHybrid->getId() % 2;
    //         LOG(INFO) << BOLDBLUE << "Configuring ReadoutOutChips on Hybrid" << +cHybrid->getId() << RESET;

    //         // if(cHybrid->getReset() == 0)
    //         // {
    //         //     LOG(INFO) << BOLDYELLOW << "Will not send a hard-reset to Chips on Hybrid#" << +cHybrid->getId() << RESET;
    //         //     continue;
    //         // }
    //         // // cWithLpGBT=true;
    //         // // no SSA because I don't want to reset it here. . already done earlier
    //         // if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS)
    //         // {
    //         //     LOG(DEBUG) << BOLDBLUE << "\t... Applying hard reset to MPAs" << RESET;
    //         //     if(!cBrokenPS) { static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetMPA(clpGBT, cSide); }
    //         //     else
    //         //     {
    //         //         static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetMPA(clpGBT, 1);
    //         //     }
    //         // }
    //         // if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTracker2S)
    //         // {
    //         //     LOG(DEBUG) << BOLDBLUE << "\t... Applying hard reset to CBCs" << RESET;
    //         //     static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetCBC(clpGBT, cSide);
    //         // }
    //     } // hybrid
    // }     // OG

    if(!doAlsoFrontend)
    {
        LOG(INFO) << BOLDYELLOW << "Will not configure FE ASICs connected to BeBoard#" << +pBoard->getId() << RESET;
        return;
    }

    LOG(INFO) << BOLDYELLOW << "Configuring FE ASICs connected to BeBoard#" << +pBoard->getId() << RESET;
    // configure chips
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid) { fReadoutChipInterface->ConfigureChip(cChip); } // Chip config
        }                                                                              // hybrid
    }                                                                                  // OG
    LOG(INFO) << BOLDMAGENTA << "Configured OT module" << RESET;
}
void SystemController::ConfigureHw(bool doAlsoFrontend)
{
    if(fDetectorContainer == nullptr)
    {
        LOG(ERROR) << BOLDRED << "Hardware not initialized: run SystemController::InitializeHw first" << RESET;
        return;
    }

    LOG(INFO) << BOLDMAGENTA << "@@@ Configuring HW parsed from xml file @@@" << RESET;
    for(const auto cBoard: *fDetectorContainer)
    {
        cBoard->printBoardType();
        fBeBoardInterface->setBoard(cBoard->getId());
        fBeBoardInterface->ConfigureBoard(cBoard);
        if(cBoard->getBoardType() == BoardType::D19C || cBoard->getBoardType() == BoardType::DTC)
        {
            // Set board sparisification
            // based on what is configured in the fw register
            // read CIC sparsification setting from fW register
            // make sure board is also set to the same thing
            if(cBoard->getBoardType() == BoardType::D19C)
            {
                bool cSparsified = (fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_cnfg.physical_interface_block.cic.2s_sparsified_enable") == 1);
                cBoard->setSparsification(cSparsified);
            }
            ConfigureOT(cBoard, doAlsoFrontend);

            LOG(INFO) << CYAN << "==================== Done =====================" << RESET;
        }
        else if(cBoard->getBoardType() == BoardType::RD53)
        {
            ConfigureIT(cBoard);
            if(doAlsoFrontend == true) ConfigureFrontendIT(cBoard);

            // ######################################
            // # Dispatch threads for data decoding #
            // ######################################
            LOG(INFO) << GREEN << "Using " << BOLDYELLOW << RD53Shared::NTHREADS << RESET << GREEN << " threads for data decoding during running time" << RESET;
            RD53Event::ForkDecodingThreads();
        }
    }

    // ####################
    // # Start monitoring #
    // ####################
    if(fDetectorMonitor != nullptr)
    {
        LOG(INFO) << GREEN << "Starting monitoring thread" << RESET;
        fDetectorMonitor->startMonitoring();
    }
}

void SystemController::initializeWriteFileHandler()
{
    for(const auto cBoard: *fDetectorContainer)
    {
        uint32_t cNChip        = 0;
        uint32_t cBeId         = cBoard->getId();
        uint32_t cNEventSize32 = this->computeEventSize32(cBoard);

        std::string cBoardTypeString;
        BoardType   cBoardType = cBoard->getBoardType();

        for(const auto cOpticalGroup: *cBoard)
            for(const auto cHybrid: *cOpticalGroup) cNChip += cHybrid->size();

        if(cBoardType == BoardType::D19C)
            cBoardTypeString = "D19C";
        else if(cBoardType == BoardType::DTC)
            cBoardTypeString = "DTC";
        else if(cBoardType == BoardType::RD53)
            cBoardTypeString = "RD53";

        uint32_t cFWWord  = fBeBoardInterface->getBoardInfo(cBoard);
        uint32_t cFWMajor = (cFWWord & 0xFFFF0000) >> 16;
        uint32_t cFWMinor = (cFWWord & 0x0000FFFF);

        FileHeader cHeader(cBoardTypeString, cFWMajor, cFWMinor, cBeId, cNChip, cNEventSize32, cBoard->getEventType());

        std::stringstream cBeBoardString;
        cBeBoardString << "_Board" << std::setw(3) << std::setfill('0') << cBeId;
        std::string cFilename = fRawFileName;
        if(fRawFileName.find(".raw") != std::string::npos) cFilename.insert(fRawFileName.find(".raw"), cBeBoardString.str());

        fFileHandler = new FileHandler(cFilename, 'w', cHeader);

        fBeBoardInterface->SetFileHandler(cBoard, fFileHandler);
        LOG(INFO) << GREEN << "Saving binary data into: " << BOLDYELLOW << cFilename << RESET;
    }
}

uint32_t SystemController::computeEventSize32(const BeBoard* pBoard)
{
    uint32_t cNEventSize32 = 0;
    uint32_t cNChip        = 0;

    for(const auto cOpticalGroup: *pBoard)
        for(const auto cHybrid: *cOpticalGroup) cNChip += cHybrid->size();

    if(pBoard->getBoardType() == BoardType::D19C) cNEventSize32 = D19C_EVENT_HEADER1_SIZE_32_CBC3 + cNChip * D19C_EVENT_SIZE_32_CBC3;

    return cNEventSize32;
}

void SystemController::Configure(std::string cHWFile, bool enableStream, uint16_t DQMportNumber, bool doAlsoFrontend)
{
    std::stringstream outp;

    InitializeHw(cHWFile, outp, enableStream, DQMportNumber);
    InitializeSettings(cHWFile, outp);
    std::cout << outp.str() << std::endl;
    ConfigureHw(doAlsoFrontend);
}

void SystemController::Start(int runNumber)
{
    for(auto cBoard: *fDetectorContainer) fBeBoardInterface->Start(cBoard);
}

void SystemController::Stop()
{
    for(auto cBoard: *fDetectorContainer) fBeBoardInterface->Stop(cBoard);
}

void SystemController::Pause()
{
    for(auto cBoard: *fDetectorContainer) fBeBoardInterface->Pause(cBoard);
}

void SystemController::Resume()
{
    for(auto cBoard: *fDetectorContainer) fBeBoardInterface->Resume(cBoard);
}

void SystemController::StartBoard(BeBoard* pBoard) { fBeBoardInterface->Start(pBoard); }
void SystemController::StopBoard(BeBoard* pBoard) { fBeBoardInterface->Stop(pBoard); }
void SystemController::PauseBoard(BeBoard* pBoard) { fBeBoardInterface->Pause(pBoard); }
void SystemController::ResumeBoard(BeBoard* pBoard) { fBeBoardInterface->Resume(pBoard); }

void SystemController::Abort() { LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " Abort not implemented" << RESET; }

uint32_t SystemController::ReadData(BeBoard* pBoard, bool pWait)
{
    std::vector<uint32_t> cData;
    return this->ReadData(pBoard, cData, pWait);
}

void SystemController::ReadData(bool pWait)
{
    for(auto cBoard: *fDetectorContainer) this->ReadData(cBoard, pWait);
}

uint32_t SystemController::ReadData(BeBoard* pBoard, std::vector<uint32_t>& pData, bool pWait)
{
    uint32_t cNPackets = fBeBoardInterface->ReadData(pBoard, false, pData, pWait);
    if(cNPackets == 0) return cNPackets;
    LOG(INFO) << "SystemController::ReadData" << RESET;
    this->DecodeData(pBoard, pData, cNPackets, fBeBoardInterface->getBoardType(pBoard));
    LOG(INFO) << "SystemController::ReadData end" << RESET;
    return cNPackets;
}

void SystemController::ReadNEvents(BeBoard* pBoard, uint32_t pNEvents)
{
    std::vector<uint32_t> cData;
    return this->ReadNEvents(pBoard, pNEvents, cData, true);
}

void SystemController::ReadNEvents(uint32_t pNEvents)
{
    for(auto cBoard: *fDetectorContainer) this->ReadNEvents(cBoard, pNEvents);
}

void SystemController::ReadNEvents(BeBoard* pBoard, uint32_t pNEvents, std::vector<uint32_t>& pData, bool pWait)
{
    fBeBoardInterface->ReadNEvents(pBoard, pNEvents, pData, pWait);
    uint32_t cMultiplicity = 0;
    if(fBeBoardInterface->getBoardType(pBoard) == BoardType::D19C) cMultiplicity = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity");
    pNEvents = pNEvents * (cMultiplicity + 1);
    //LOG(INFO) << "SystemController::ReadNEvents" << RESET;
    this->DecodeData(pBoard, pData, pNEvents, fBeBoardInterface->getBoardType(pBoard));
    //LOG(INFO) << "SystemController::ReadNEvents end" << RESET;
}

// #################
// # Data decoding #
// #################

void SystemController::SetFuture(const BeBoard* pBoard, const std::vector<uint32_t>& pData, uint32_t pNevents, BoardType pType)
{
    if(pData.size() != 0) fFuture = std::async(&SystemController::DecodeData, this, pBoard, pData, pNevents, pType);
}

void SystemController::DecodeData(const BeBoard* pBoard, const std::vector<uint32_t>& pData, uint32_t pNevents, BoardType pType)
{
    // ####################
    // # Decoding IT data #
    // ####################
    if(pType == BoardType::RD53)
    {
        uint32_t status;
        fEventList.clear();
        if(RD53Event::decodedEvents.size() == 0) RD53Event::DecodeEventsMultiThreads(pData, RD53Event::decodedEvents, status);
        RD53Event::addBoardInfo2Events(pBoard, RD53Event::decodedEvents);
        for(auto& evt: RD53Event::decodedEvents) fEventList.push_back(&evt);
    }
    // ####################
    // # Decoding OT data #
    // ####################
    else if(pType == BoardType::D19C && pBoard->getEventType() != EventType::PSAS)
    {
        bool cTLUconfig = 2;
        // bool cTLUconfig = (fBeBoardInterface->ReadBoardReg(fDetectorContainer->at(pBoard->getIndex()), "fc7_daq_cnfg.tlu_block.handshake_mode") == 2 &&
        //                    fBeBoardInterface->ReadBoardReg(fDetectorContainer->at(pBoard->getIndex()), "fc7_daq_cnfg.tlu_block.tlu_enabled") == 1);
        // for (auto L : pData) LOG(INFO) << BOLDBLUE << std::bitset<32>(L) << RESET;
        for(auto& pevt: fEventList) delete pevt;
        fEventList.clear();

        if(pNevents == 0)
        {
            // LOG(INFO) << BOLDRED << "Asking to decode 0 events. . something might not be right here!!!" << RESET;
        }
        else
        {
            EventType fEventType = pBoard->getEventType();
            uint32_t  fNHybrid   = pBoard->getNHybrid();
            // uint32_t  cBlockSize = 0x0000FFFF & pData.at(0);
            // LOG(INFO) << BOLDBLUE << "Reading events from " << +fNHybrid << " FEs connected to uDTC...[ " << +cBlockSize * 4 << " 32 bit words to decode]" << RESET;
            fEventSize = static_cast<uint32_t>((pData.size()) / pNevents);
            // uint32_t nmpa = 0;
            uint32_t maxind = 0;
            for(auto opticalGroup: *pBoard)
            {
                for(auto hybrid: *opticalGroup) { maxind = std::max(maxind, uint32_t(hybrid->size())); }
            }

            if(fEventType != EventType::ZS)
            {
                // check data words because I'm desperate
                // for( auto cWord : pData )
                //     LOG (INFO) << BOLDYELLOW << "SystemController \t..." << std::bitset<32>(cWord) << RESET;

                size_t cEventIndex    = 0;
                auto   cEventIterator = pData.begin();
                do {
                    uint32_t cHeader = (0xFFFF0000 & (*cEventIterator)) >> 16;
                    if(cHeader != 0xFFFF)
                    {
                        int cPositionInData = (int)std::distance(pData.begin(), cEventIterator);
                        // first part of data header
                        if(fFileHandler != nullptr && cPositionInData > 12)
                            LOG(INFO) << BOLDRED << "SystemController::DecodeData Invalid header from the FW in position#" << cPositionInData << RESET;
                        else if(fFileHandler == nullptr)
                            LOG(INFO) << BOLDRED << "SystemController::DecodeData Invalid header from the FW in position#" << cPositionInData << RESET;
                        cEventIterator++;
                    }
                    else // valid event  // decode
                    {
                        uint32_t cEventSize = (0x0000FFFF & (*cEventIterator)) * 4; // event size is given in 128 bit words
                        // LOG (INFO) << BOLDMAGENTA << "SystemController::DecodeData Decoding event made of up " << +cEventSize << " 32 bit words. " << RESET;
                        auto cEnd = ((cEventIterator + cEventSize) > pData.end()) ? pData.end() : (cEventIterator + cEventSize);
                        // retrieve chunck of data vector belonging to this event
                        if(cEnd - cEventIterator == cEventSize)
                        {
                            std::vector<uint32_t> cEvent(cEventIterator, cEnd);
                            // some useful debug information
                            LOG(DEBUG) << BOLDGREEN << "Event" << +cEventIndex << " .. Data word that should be event header ..  " << std::bitset<32>(*cEventIterator) << ". Event is made up of "
                                       << +cEventSize << " 32 bit words..." << RESET;
                            if(pBoard->getFrontEndType() == FrontEndType::CBC3) { fEventList.push_back(new D19cCbc3Event(pBoard, cEvent)); }
                            else if(pBoard->getFrontEndType() == FrontEndType::CIC || pBoard->getFrontEndType() == FrontEndType::CIC2)
                            {
                                bool cWithCBC3 = (fEventType != EventType::VR2S);
                                if(cWithCBC3)
                                    LOG(INFO) << BOLDBLUE << "Decoding CIC data : with 8CBC3 " << RESET;
                                else
                                    LOG(DEBUG) << BOLDBLUE << "Decoding CIC data : with 2S-FEH  " << RESET;
                                fEventList.push_back(new D19cCic2Event(pBoard, cEvent, cWithCBC3, cTLUconfig));
                            }
                            else if(pBoard->getFrontEndType() == FrontEndType::SSA)
                            {
                                fEventList.push_back(new D19cSSAEvent(pBoard, maxind, fNHybrid, cEvent));
                            }
                            else if(pBoard->getFrontEndType() == FrontEndType::SSA2)
                            {
                                fEventList.push_back(new D19cSSA2Event(pBoard, maxind, fNHybrid, cEvent));
                            }
                            else if(pBoard->getFrontEndType() == FrontEndType::MPA)
                            {
                                fEventList.push_back(new D19cMPAEvent(pBoard, maxind, fNHybrid, cEvent));
                                LOG(INFO) << BOLDBLUE << "Decoding SSA data " << RESET;
                                // auto cL1Counter0 = (cEvent[4+2] & (0xF<<16)) >> 16;
                                // auto cL1Counter1 = (cEvent[4+8+4+2] & (0xF<<16)) >> 16;
                                // LOG (INFO) << BOLDBLUE << "L1A counter chip0 : " << cL1Counter0 << RESET;
                                // LOG (INFO) << BOLDBLUE << "L1A counter chip1 : " << cL1Counter1 << RESET;
                                // for(auto cWord : cEvent )
                                //   LOG (INFO) << BOLDMAGENTA << std::bitset<32>(cWord) << RESET;
                                fEventList.push_back(new D19cSSAEvent(pBoard, maxind + 1, fNHybrid, cEvent));
                            }
                            else if(pBoard->getFrontEndType() == FrontEndType::MPA)
                            {
                                LOG(INFO) << BOLDBLUE << "Decoding MPA data " << RESET;
                                // fEventList.push_back(new D19cCic2Event(pBoard, cEvent));
                                fEventList.push_back(new D19cMPAEvent(pBoard, maxind + 1, fNHybrid, cEvent));
                            }
                            cEventIndex++;
                        }
                        cEventIterator += cEventSize;
                    }
                } while(cEventIterator < pData.end());
            }
        } // end zero check
    }
    else if(pType == BoardType::D19C && pBoard->getEventType() == EventType::PSAS)
    {
        fEventList.clear();
        fEventList.push_back(new D19cPSEventAS(pBoard, pData));
    }
    else if(pType == BoardType::DTC && pBoard->getEventType() == EventType::DTC2SUNSPARSIFIED) {
        //LOG(INFO) << "Well well well, here I am decoding DTC2SUNSPARSIFIED data" << RESET;
        fEventList.clear();
        unsigned int cEventIndex = 0;
        unsigned int cNWordsInThisEvent = 0;
        auto         cEventIterator = pData.begin();
        do {
            //LOG(INFO) << "Event: " << cEventIndex << RESET;
            cNWordsInThisEvent = static_cast<unsigned int>(*cEventIterator & 0x0000FFFF)*2 + 1;//nWords is doubled because the original data format is in 64bit words. +1 is the event header word which contains the eventID.
            //LOG(INFO) << "We have " << std::dec << cNWordsInThisEvent << " words in this event" << RESET;
            auto cEventEnd = cEventIterator + cNWordsInThisEvent;
            //LOG(INFO) << "Found eventEnd words in this event" << RESET;
            if(cEventEnd > pData.end()) throw Exception("SystemController::DecodeData. Event size is larger than the data size");
            std::vector<uint32_t> cEvent(cEventIterator, cEventEnd);
            //LOG(INFO) << "Built event" << RESET;
            Event* cEvPtr = new DTCCic2Event(pBoard, cEvent, false);
            //LOG(INFO) << "Created event" << RESET;
            fEventList.push_back(cEvPtr); //TODO: update this decalration, no need anymore to keep the number of header words
            //LOG(INFO) << "Pushed back" << RESET;
            cEventIndex++;
            //LOG(INFO) << BOLDBLUE << "SystemController::DecodeData. Event " << std::dec << cEventIndex << " has been decoded" << RESET;
            cEventIterator += cNWordsInThisEvent;
            //LOG(INFO) << "cEvent iterator @ " << std::dec << cEventIterator - pData.begin() << " over " << pData.end() - pData.begin() << " we asked for " << pNevents << " Events and are at: " << cEventIndex << RESET;
        } while(cEventIterator < pData.end() && cEventIndex < pNevents);
        if(cEventIndex != pNevents) throw Exception("SystemController::DecodeData. Number of events decoded is not equal to the number of events requested"); //That's what happens atm
    }
    //LOG(INFO) << "DECODING DONE" << RESET;
}

void SystemController::setChannelGroupHandler(ChannelGroupHandler& theChannelGroupHandler, std::vector<FrontEndType> cFrontEndTypes)
{
    auto selectChipFlavourFunction = [cFrontEndTypes](const ChipContainer* theChip)
    { return (std::find(cFrontEndTypes.begin(), cFrontEndTypes.end(), static_cast<const ReadoutChip*>(theChip)->getFrontEndType()) != cFrontEndTypes.end()); };
    setChannelGroupHandler(theChannelGroupHandler, selectChipFlavourFunction);
}

void SystemController::setChannelGroupHandler(ChannelGroupHandler& theChannelGroupHandler, FrontEndType theFrontEndType)
{
    auto selectChipFlavourFunction = [theFrontEndType](const ChipContainer* theChip) { return (static_cast<const ReadoutChip*>(theChip)->getFrontEndType() == theFrontEndType); };
    setChannelGroupHandler(theChannelGroupHandler, selectChipFlavourFunction);
}

void SystemController::setChannelGroupHandler(ChannelGroupHandler& theChannelGroupHandler, std::function<bool(const ChipContainer*)> theQueryFunction)
{
    auto theChannelGroupHandlerPointer = std::make_shared<ChannelGroupHandler>(std::move(theChannelGroupHandler));
    setChannelGroupHandler(theChannelGroupHandlerPointer, theQueryFunction);
}

void SystemController::setChannelGroupHandler(std::shared_ptr<ChannelGroupHandler> theChannelGroupHandlerPointer, std::function<bool(const ChipContainer*)> theQueryFunction)
{
    uint16_t totalNumberOfChips = 0;
    for(const auto board: *fDetectorContainer)
    {
        for(const auto opticalGroup: *board)
        {
            for(const auto hybrid: *opticalGroup) { totalNumberOfChips += hybrid->size(); }
        }
    }

    uint16_t totalNumberOfQueriedChips = 0;
    fDetectorContainer->addReadoutChipQueryFunction(theQueryFunction);
    for(const auto board: *fDetectorContainer)
    {
        for(const auto opticalGroup: *board)
        {
            for(const auto hybrid: *opticalGroup)
            {
                totalNumberOfQueriedChips += hybrid->size();
                for(const auto chip: *hybrid)
                {
                    fChannelGroupHandlerContainer->getObject(board->getId())
                        ->getObject(opticalGroup->getId())
                        ->getObject(hybrid->getId())
                        ->getObject(chip->getId())
                        ->getSummary<std::shared_ptr<ChannelGroupHandler>>() = theChannelGroupHandlerPointer;
                }
            }
        }
    }

    fDetectorContainer->resetReadoutChipQueryFunction();
    fSameChannelGroupForAllChannels = (totalNumberOfQueriedChips == totalNumberOfChips);
}

void SystemController::setChannelGroupHandler(std::shared_ptr<ChannelGroupHandler> theChannelGroupHandlerPointer, uint16_t boardId, uint16_t opticalGroupId, uint16_t hybridId, uint16_t chipId)
{
    fChannelGroupHandlerContainer->getObject(boardId)->getObject(opticalGroupId)->getObject(hybridId)->getObject(chipId)->getSummary<std::shared_ptr<ChannelGroupHandler>>() =
        theChannelGroupHandlerPointer;
}

void SystemController::disableAllChannels()
{
    // ###########################################
    // # Disable channels of the entire detector #
    // ###########################################
    if(SystemController::findValueInSettings<double>("DisableChannelsAtExit", false) == true)
        for(const auto cBoard: *fDetectorContainer)
            for(const auto cOpticalGroup: *cBoard)
                for(const auto cHybrid: *cOpticalGroup)
                    for(const auto cChip: *cHybrid) fReadoutChipInterface->MaskAllChannels(cChip, true);
}

void SystemController::DumpFrontendRegisters()
{
    for(const auto cBoard: *fDetectorContainer)
        for(const auto cOpticalGroup: *cBoard)
            for(const auto cHybrid: *cOpticalGroup)
                for(const auto cChip: *cHybrid)
                {
                    LOG(INFO) << GREEN << "Readout chip register content for [board/opticalGroup/hybrid/chip = " << BOLDYELLOW << cBoard->getId() << "/" << cOpticalGroup->getId() << "/"
                              << cHybrid->getId() << "/" << +cChip->getId() << RESET << GREEN << "]" << RESET;
                    fReadoutChipInterface->DumpChipRegisters(cChip);
                }
}

} // namespace Ph2_System
