#include "OTTool.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

#include "../HWInterface/FEConfigurationInterface.h"
// #include "../HWInterface/L1ReadoutInterface.h"
#include "../HWInterface/D19cL1ReadoutInterface.h"
#include "../HWInterface/TriggerInterface.h"
#include "../Utils/ContainerFactory.h"

OTTool::OTTool() : Tool()
{
    fBoardRegContainer.reset();
    fBrdRegsToPerserve.clear();
    fChipRegsToPerserve.reset();
    fSuccess = false;
    fMyName  = "OTTool";
#ifdef __USE_ROOT__
    fTree = nullptr;
#endif
    fReadoutData.clear();
}

OTTool::~OTTool() {}
// Reset register on BeBoard + Chips
void OTTool::Reset()
{
    if(fReadoutMode == 1) return;

    LOG(INFO) << BOLDGREEN << "Resetting registers touched  by " << fMyName << RESET;
    // set everything back to original values .. like I wasn't here
    for(auto cBoard: *fDetectorContainer)
    {
        LOG(DEBUG) << BOLDBLUE << fMyName << ":Resetting all registers on back-end board " << +cBoard->getId() << RESET;
        auto&                                         cBeRegMap = fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>();
        std::vector<std::pair<std::string, uint32_t>> cVecBeBoardRegs;
        cVecBeBoardRegs.clear();
        for(auto cReg: cBeRegMap)
        {
            // skip registers that I should perserve for this board
            if(std::find(fBrdRegsToPerserve.begin(), fBrdRegsToPerserve.end(), cReg.first) != fBrdRegsToPerserve.end())
            {
                LOG(DEBUG) << BOLDBLUE << "Will not reconfigure " << cReg.first << RESET;
                continue;
            }
            else
                LOG(DEBUG) << BOLDBLUE << "Will reconfigure " << cReg.first << " to " << cReg.second << RESET;
            cVecBeBoardRegs.push_back(make_pair(cReg.first, cReg.second));
        }
        LOG(DEBUG) << BOLDYELLOW << fMyName << " will re-write " << cVecBeBoardRegs.size() << " registers." << RESET;
        fBeBoardInterface->WriteBoardMultReg(cBoard, cVecBeBoardRegs);
    } // for the board - reset registers

    for(auto cBoard: *fDetectorContainer) // now reset Chip registers
    {
        auto& cChipRegsToPreserveThisBrd = fChipRegsToPerserve.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cChipRegsToPreserveThisOG = cChipRegsToPreserveThisBrd->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                LOG(DEBUG) << BOLDYELLOW << fMyName << ":Resetting all registers on readout chips connected to FEhybrid#" << +(cHybrid->getId()) << " back to their original values..." << RESET;
                auto& cChipRegsToPreserveThisHybrd = cChipRegsToPreserveThisOG->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    auto& cChipRegsToPreserveThisChip = cChipRegsToPreserveThisHybrd->at(cChip->getIndex());
                    auto& cRegsToPerserve             = cChipRegsToPreserveThisChip->getSummary<std::vector<std::string>>();
                    // reset registers
                    auto                                          cModMap = cChip->GetModifiedRegisterMap();
                    std::vector<std::pair<std::string, uint16_t>> cRegList;
                    for(auto cMapItem: cModMap)
                    {
                        if(cMapItem.first.empty())
                        {
                            LOG(ERROR) << BOLDRED << fMyName << " Reset trying to revert a value of a register but.. name is an empty string" << RESET;
                            continue;
                        }

                        // skip registers that I should perserve for this Chip
                        if(std::find(cRegsToPerserve.begin(), cRegsToPerserve.end(), cMapItem.first) != cRegsToPerserve.end())
                        {
                            LOG(DEBUG) << BOLDBLUE << "Skipping reconfiguration of " << cMapItem.first << RESET;
                            continue;
                        }
                        auto cValueInMemory = cChip->getReg(cMapItem.first);
                        if(cValueInMemory == cMapItem.second.fValue) continue;

                        LOG(INFO) << BOLDYELLOW << fMyName << "::Resetting Register " << cMapItem.first << " on Chip#" << +cChip->getId() << " from " << cValueInMemory << " to "
                                  << cMapItem.second.fValue << RESET;

                        cRegList.push_back(std::make_pair(cMapItem.first, cMapItem.second.fValue));
                    }
                    fReadoutChipInterface->WriteChipMultReg(cChip, cRegList);
                    LOG(INFO) << BOLDYELLOW << "Chip#" << +cChip->getId() << " map of modified registers contains " << cRegList.size() << " items." << RESET;

                    // then clear modified register map
                    // and also disable register tracking for this chip
                    cChip->ClearModifiedRegisterMap();
                    cChip->setRegisterTracking(0);
                    LOG(DEBUG) << BOLDYELLOW << fMyName << "::Reset Chip#" << +cChip->getId() << " register tracking set to " << +cChip->getRegisterTracking() << RESET;
                }
            }
        }
    } // Chip registers

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() != FrontEndType::CBC3) continue;
                    auto cPageReg = fReadoutChipInterface->ReadChipReg(cChip, "FeCtrl&TrgLat2");
                    LOG(DEBUG) << BOLDYELLOW << "CBC#" << +cChip->getId() << " PageCtrlReg set to 0x" << std::hex << +cPageReg << std::dec << " after " << fMyName << " RESET." << RESET;
                } // Chip config
            }     // hybrid
        }
    } // check CBC page0 register

    for(auto cBoard: *fDetectorContainer)
    {
        // now .. go back to original setting
        auto cSparsified = fSparsified[cBoard->getId()];
        LOG(INFO) << BOLDYELLOW << fMyName << "::Reset for sparsification settings in system" << RESET;
        cBoard->setSparsification(cSparsified);
        SetSparsification(cBoard);
    }

    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            auto cStubCnfg = cOpticalGroup->getStubCnfg();
            LOG(INFO) << BOLDYELLOW << fMyName << "::Reset -- Stub readout cnfg is Link#" << +cOpticalGroup->getId() << " package delay of " << +cStubCnfg.first << " latency is " << +cStubCnfg.second
                      << RESET;
        }
    }
    resetPointers();
}
// Function to make sure sparsification matches board
void OTTool::SetSparsification(BeBoard* pBoard)
{
    uint32_t cSparsified = pBoard->getSparsification(); // this is set in the file parser .. so check using that
    LOG(INFO) << BOLDYELLOW << "Sparisfication set to " << +cSparsified << RESET;
    fSparsified[pBoard->getId()] = (cSparsified != 0);
    // make sure that CIC + fw match
    if(pBoard->getBoardType() == BoardType::D19C) fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_cnfg.physical_interface_block.cic.2s_sparsified_enable", cSparsified);
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            if(cCic == nullptr) continue;
            fCicInterface->SetSparsification(cCic, cSparsified);
        } // hybrids
    }     // links
}
// Fucntion to make sure package delay is set correctly
void OTTool::ConfigureStubReadout(BeBoard* pBoard)
{
    if(pBoard->getBoardType() != BoardType::D19C) return;

    LOG(INFO) << BOLDYELLOW << "BeBoard#" << +pBoard->getId() << "... stub latency set to " << +pBoard->getStubOffset() << RESET;
    // first configure package delays on each link
    std::map<std::string, uint32_t> cConfigurationValues;
    cConfigurationValues["fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9"]   = 0x00;
    cConfigurationValues["fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11"] = 0x00;
    for(auto cOpticalGroup: *pBoard)
    {
        auto cStubCnfg = cOpticalGroup->getStubCnfg();
        LOG(INFO) << BOLDYELLOW << "Link#" << +cOpticalGroup->getId() << " Stub package delay to "
                  << +cStubCnfg.first
                  // << " Stub latency set to " << +cStubCnfg.second
                  << RESET;

        std::string cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9";
        if(cOpticalGroup->getId() > 9) cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11";
        cConfigurationValues[cRegName] = cConfigurationValues[cRegName] | (cStubCnfg.first << (cOpticalGroup->getId() % 10) * 3);
    } // update value of register
    // set value of package delays
    auto cIter = cConfigurationValues.begin();
    do {
        LOG(INFO) << BOLDYELLOW << "Set package delay register " << cIter->first << " to " << std::bitset<32>(cIter->second) << RESET;
        fBeBoardInterface->WriteBoardReg(pBoard, cIter->first, cIter->second);
        cIter++;
    } while(cIter != cConfigurationValues.end());
    // now configure stub latency on each link
    for(auto cOpticalGroup: *pBoard)
    {
        int               cBaseLinkId = cOpticalGroup->getId() / 3;
        std::stringstream cRegName;
        cRegName << "fc7_daq_cnfg.readout_block.stub_latency_link" << cBaseLinkId * 3;
        cRegName << "_link" << cBaseLinkId * 3 + 2;
        fBeBoardInterface->WriteBoardReg(pBoard, cRegName.str(), 0);
    } // reset stub latency register for all links back to  0

    for(auto cOpticalGroup: *pBoard)
    {
        auto cStubCnfg = cOpticalGroup->getStubCnfg();
        LOG(INFO) << BOLDYELLOW << "Link#" << +cOpticalGroup->getId() << " Stub latency set to " << +cStubCnfg.second << RESET;
        int               cBaseLinkId = cOpticalGroup->getId() / 3;
        std::stringstream cRegName;
        cRegName << "fc7_daq_cnfg.readout_block.stub_latency_link" << cBaseLinkId * 3;
        cRegName << "_link" << cBaseLinkId * 3 + 2;
        auto     cLatency       = cStubCnfg.second;
        uint32_t cVal           = fBeBoardInterface->ReadBoardReg(pBoard, cRegName.str());
        uint32_t cBitShiftedVal = (cLatency << (cOpticalGroup->getId() % 3) * 9);
        cVal                    = cVal | cBitShiftedVal; // cStubLatency+cOff;//cVal | ((cStubLatency+cOff) << (cOpticalGroup->getId()%3)*9);
        fBeBoardInterface->WriteBoardReg(pBoard, cRegName.str(), cVal);
        cVal = fBeBoardInterface->ReadBoardReg(pBoard, cRegName.str());
        LOG(INFO) << BOLDYELLOW << "Stub Latency" << cLatency << " on Link#" << +cOpticalGroup->getId() << " " << cRegName.str() << " set to " << std::bitset<32>(cVal) << RESET;
    } // configure stub latency
}
// Initialization function
void OTTool::Prepare()
{
    // retreive number of events from settings file
    fNevents = findValueInSettings<double>("Nevents", 10);

    if(fReadoutMode == 1)
    {
        LOG(INFO) << BOLDYELLOW << "Reading from RawFile" << RESET;
        return;
    }
    // retreive original settings for all chips and all back-end boards
    fBoardRegContainer.reset();
    ContainerFactory::copyAndInitBoard<BeBoardRegMap>(*fDetectorContainer, fBoardRegContainer);
    for(auto cBoard: *fDetectorContainer)
    {
        auto&                cBoardRegMap = fBoardRegContainer.at(cBoard->getIndex())->getSummary<BeBoardRegMap>();
        const BeBoardRegMap& cOrigRegMap  = static_cast<const BeBoard*>(cBoard)->getBeBoardRegMap();
        cBoardRegMap.insert(cOrigRegMap.begin(), cOrigRegMap.end());
    }

    // check readout + configure stub latency
    for(auto cBoard: *fDetectorContainer)
    {
        SetSparsification(cBoard);
        ConfigureStubReadout(cBoard);
    }

    // clear map of modified registers
    // probably this should be a container per board
    // since we should be able to mix different types of boards
    // but need to decide if this is per board/OG/hybrid
    // TO-DO
    fWithCIC   = 0;
    fWithLpGBT = 0;
    fWithSSA   = 0;
    fWithMPA   = 0;
    fWithCBC   = 0;
    // set-up register tracking
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                for(auto cChip: *cHybrid)
                {
                    cChip->setRegisterTracking(1);
                    cChip->ClearModifiedRegisterMap();
                    LOG(DEBUG) << BOLDYELLOW << fMyName << "::Prepare Chip#" << +cChip->getId() << " register tracking set to " << +cChip->getRegisterTracking() << RESET;
                } // chips
            }     // hybrids
        }         // optical groups
    }             // board

    // figure out what type of FEs are connected
    for(auto cBoard: *fDetectorContainer)
    {
        auto cConnectedFEs = cBoard->connectedFrontEndTypes();
        fWithSSA = (std::find_if(cConnectedFEs.begin(), cConnectedFEs.end(), [](FrontEndType x) { return (x == FrontEndType::SSA || x == FrontEndType::SSA2); }) != cConnectedFEs.end()) ? 1 : 0;
        fWithMPA = (std::find_if(cConnectedFEs.begin(), cConnectedFEs.end(), [](FrontEndType x) { return (x == FrontEndType::MPA || x == FrontEndType::MPA2); }) != cConnectedFEs.end()) ? 1 : 0;
        fWithCBC = (std::find_if(cConnectedFEs.begin(), cConnectedFEs.end(), [](FrontEndType x) { return x == FrontEndType::CBC3; }) != cConnectedFEs.end()) ? 1 : 0;
        for(auto cOpticalGroup: *cBoard)
        {
            fWithLpGBT = (cOpticalGroup->flpGBT != nullptr) ? 1 : 0;
            fWithCIC   = fWithLpGBT;
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                fWithCIC   = fWithCIC || (cCic != nullptr);
            } // hybrid
        }     // optical group
    }         // board

    // prepare list of Chip registers to perserve
    fDetectorDataContainer = &fChipRegsToPerserve;
    ContainerFactory::copyAndInitChip<std::vector<std::string>>(*fDetectorContainer, *fDetectorDataContainer);

    fSuccess = false;
}
void OTTool::PrepareForUser(BeBoard* pBoard, uint8_t pLimitTriggers)
{
    fBeBoardInterface->setBoard(pBoard->getId());
    BeBoardRegMap cRegMap = pBoard->getBeBoardRegMap();
    // reload all registers in daq cnfg from xml
    // uint32_t                                      cHandshakeMode = (pLimitTriggers == 0) ? 0 : 1;
    std::vector<std::pair<std::string, uint32_t>> cRegVec;
    cRegVec.clear();
    for(auto cMapItem: cRegMap)
    {
        // if(cMapItem.first.find("fc7_daq_cnfg") != std::string::npos)
        //{
        // uint32_t cVal = (cMapItem.first.find("readout_block.global.data_handshake_enable") != std::string::npos) ? cHandshakeMode : cMapItem.second;
        cRegVec.push_back({cMapItem.first, cMapItem.second});
        //}
    }
    fBeBoardInterface->WriteBoardMultReg(pBoard, cRegVec);
    fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_ctrl.fast_command_block.control.load_config", 0x1);
    uint32_t cTriggerMult = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity");
    LOG(INFO) << BOLDYELLOW << "Trigger multiplicity is " << cTriggerMult << RESET;
    // if(cHandshakeMode == 1)
    // {
    //     auto cNEvents = fNevents * (cTriggerMult + 1);
    //     fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_cnfg.readout_block.packet_nbr", cNEvents - 1);
    //     LOG(INFO) << BOLDYELLOW << "ReadoutBlock #packets set to " << cNEvents - 1 << RESET;
    // }
    auto cNpackets = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_cnfg.readout_block.packet_nbr");
    LOG(INFO) << BOLDYELLOW << "ReadoutBlock #packets set to " << cNpackets << RESET;

    // stop triggers
    fBeBoardInterface->Stop(pBoard);
    // make sure everything is configured
    UpdateFromRegMap(pBoard);
    //  set triggers to accept to 0 and load new trigger config
    for(auto cBoard: *fDetectorContainer)
    {
        std::vector<std::pair<std::string, uint32_t>> cRegVec;
        cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.triggers_to_accept", 0});
        cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
        fBeBoardInterface->WriteBoardMultReg(cBoard, cRegVec);
    }
}
// configure print-out options
void OTTool::ConfigurePrintout(PrintConfig pCnfg)
{
    fPrintConfig.fVerbose    = pCnfg.fVerbose;
    fPrintConfig.fPrintEvery = pCnfg.fPrintEvery;
}
// set list of board registers to perserve
void OTTool::SetBrdRegstoPerserve(std::vector<std::string> pListOfRegs)
{
    fBrdRegsToPerserve.clear();
    for(const auto& cRegName: pListOfRegs)
    {
        LOG(DEBUG) << BOLDBLUE << "Adding " << cRegName << " to list of Brd Regs to perserve..." << RESET;
        fBrdRegsToPerserve.push_back(cRegName);
    }
}
// set list of Chip registers to perserve
void OTTool::SetChipRegstoPerserve(FrontEndType pType, std::vector<std::string> pListOfRegs)
{
    LOG(INFO) << BOLDBLUE << fMyName << " setting registers to store on Chips." << RESET;
    for(auto cBoard: *fDetectorContainer)
    {
        auto& cChipRegsToPreserveThisBrd = fChipRegsToPerserve.at(cBoard->getIndex());
        for(auto cOpticalGroup: *cBoard)
        {
            auto& cChipRegsToPreserveThisOG = cChipRegsToPreserveThisBrd->at(cOpticalGroup->getIndex());
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cChipRegsToPreserveThisHybrd = cChipRegsToPreserveThisOG->at(cHybrid->getIndex());
                for(auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() != pType) continue;

                    auto& cChipRegsToPreserveThisChip = cChipRegsToPreserveThisHybrd->at(cChip->getIndex());
                    auto& cRegsToPerserve             = cChipRegsToPreserveThisChip->getSummary<std::vector<std::string>>();
                    cRegsToPerserve.clear();
                    for(const auto& cRegName: pListOfRegs)
                    {
                        LOG(DEBUG) << BOLDBLUE << "Adding " << cRegName << " to list of Chip Regs to perserve...Chip#" << +cChip->getId() << RESET;
                        cRegsToPerserve.push_back(cRegName);
                    }
                } // Chips
            }     // Hybrds
        }         // OGs
    }             // brd
}

// read data from file
void OTTool::ReadDataFromFile(std::string pRawFileName)
{
    this->addFileHandler(pRawFileName, 'r');
    std::vector<uint32_t> cData;
    this->readFile(cData);
    LOG(INFO) << BOLDBLUE << "BeamTestCheck::ReadDataFromFile Read back " << +cData.size() << " 32-bit words from the .raw file : " << pRawFileName << RESET;
    for(auto cBoard: *fDetectorContainer)
    {
        size_t cNevents = fNevents;
        DecodeData(cBoard, cData, cNevents, fBeBoardInterface->getBoardType(cBoard));
        if(fPrintConfig.fVerbose) PrintData(cBoard);
        if(fSaveTree) FillTree(cBoard);
    }
#ifdef __USE_ROOT__
    if(fSaveTree && fTree != nullptr) fTree->Write();
#endif
}

// print data
void OTTool::PrintData(BeBoard* pBoard)
{
    const std::vector<Event*>& cEvents = GetEvents();
    LOG(INFO) << BOLDRED << "Printing events from FC7.. collected : " << +cEvents.size() << " events." << RESET;
    fEventCountInt = 0;
    for(auto& cEvent: cEvents)
    {
        EventPrintout(pBoard, cEvent);
        fEventCountInt++;
    }
}

// wait for triggers
void OTTool::WaitForTriggers(BeBoard* pBoard)
{
    // get D19cFW Interface
    fBeBoardInterface->setBoard(pBoard->getId());
    auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    auto cTriggerInterface = cInterface->getTriggerInterface();

    LOG(INFO) << BOLDBLUE << fMyName << "::WaitForTriggers with ReadData.. will wait to readout until I've seen " << fNevents << " triggers " << RESET;
    std::vector<uint32_t> cCompleteData(0);

    // stop triggers
    fBeBoardInterface->Stop(pBoard);
    size_t              cCounter = 0;
    uint32_t            cNevents = 0;
    bool                cBreak   = false;
    bool                cWait    = false;
    std::vector<size_t> cTriggerCounters(0);
    do {
        // check state of triggers FSM
        if(cTriggerInterface->GetTriggerState() != 1)
        {
            cTriggerInterface->Stop();
            cTriggerInterface->Start();
        }

        std::this_thread::sleep_for(std::chrono::microseconds(fReadoutPause));
        auto cTriggerCounter = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_stat.fast_command_block.trigger_in_counter");
        cTriggerCounters.push_back(cTriggerCounter);
        if(cCounter % 200 == 0 && cCounter > 0)
        {
            LOG(INFO) << BOLDMAGENTA << "BeamTestCheck continuousReadout loop ... " << +cTriggerCounters[cTriggerCounters.size() - 1] << " triggers received" << RESET;
        }
        cCounter++;
        cBreak = (cTriggerCounter >= fNevents);
    } while(!cBreak);
    // stop triggers
    cTriggerInterface->Stop();
    // wait for 100 ms after stopping triggers just in case
    // we are still reading out a very large event
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::vector<uint32_t> cData(0);
    cNevents += ReadData(pBoard, cData, cWait);
    if(cData.size() != 0) std::move(cData.begin(), cData.end(), std::back_inserter(cCompleteData));
    LOG(INFO) << "WaitForTriggers" << RESET;
    DecodeData(pBoard, cCompleteData, cNevents, fBeBoardInterface->getBoardType(pBoard));
    LOG(INFO) << BOLDYELLOW << fMyName << " : Mean trigger rate is " << cTriggerCounters[cTriggerCounters.size() - 1] / (cCounter * fReadoutPause * 1e-6) << " Hz"
              << " .... readout " << cNevents << " when " << fNevents << " were requested." << RESET;
}

// poll boards for number of triggers
void OTTool::TriggerMonitor(uint32_t pDelta_s)
{
    // launch thread to catch ctrl+c from command line
    std::thread cCatchStopTh;

    // make sure that triggers have been started on all board
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        auto cTriggerInterface = cInterface->getTriggerInterface();
        if(cTriggerInterface->GetTriggerState() == 0) cTriggerInterface->Start();
    }

    // get start time for monitoring
    auto startTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    // create text file to store data
    // file name :
    std::stringstream cFileName;
    cFileName << fDirectoryName << "TriggerMonitor_" << startTimeUTC_us << ".dat";
    // file format :
    std::ofstream cLogFile;
    cLogFile.open(cFileName.str(), std::ios::out | std::ios::app);
    cLogFile.close();
    LOG(INFO) << fMyName << ":Starting trigger monitor ... Results will be saved to " << cFileName.str() << RESET;
    cCatchStopTh = std::thread(&OTTool::CatchStop, this);
    try
    {
        size_t cLoopCounter = 0;
        // initialize container to hold trigger counters
        DetectorDataContainer cTrigCounters;
        ContainerFactory::copyAndInitBoard<std::vector<uint32_t>>(*fDetectorContainer, cTrigCounters);
        for(auto cBoard: *fDetectorContainer)
        {
            auto& cCounterThisBrd = cTrigCounters.at(cBoard->getIndex())->getSummary<std::vector<uint32_t>>();
            cCounterThisBrd.clear();
        }
        auto cTime0 = startTimeUTC_us;
        do {
            std::this_thread::sleep_for(std::chrono::milliseconds(pDelta_s * 1000));
            auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            auto cDeltaTime_us     = currentTimeUTC_us - cTime0;
            cLogFile.open(cFileName.str(), std::ios::out | std::ios::app);
            for(auto cBoard: *fDetectorContainer)
            {
                fBeBoardInterface->setBoard(cBoard->getId());
                auto  cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
                auto  cTriggerInterface = cInterface->getTriggerInterface();
                auto  cTriggerState     = cTriggerInterface->GetTriggerState();
                auto& cCounterThisBrd   = cTrigCounters.at(cBoard->getIndex())->getSummary<std::vector<uint32_t>>();
                cCounterThisBrd.push_back(fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_stat.fast_command_block.trigger_in_counter"));
                auto cDeltaTriggers   = (cCounterThisBrd.size() == 1) ? cCounterThisBrd[0] : cCounterThisBrd[cCounterThisBrd.size() - 1] - cCounterThisBrd[cCounterThisBrd.size() - 2];
                auto cTriggerSource   = fBeBoardInterface->ReadBoardReg(cBoard, "fc7_daq_cnfg.fast_command_block.trigger_source");
                auto cTriggerRateInst = cDeltaTriggers / (cDeltaTime_us * 1e-6); // Hz
                // output to terminal
                LOG(INFO) << BOLDBLUE << "Monitoring triggers...BeBoard#" << +cBoard->getId() << " --- received " << cCounterThisBrd[cCounterThisBrd.size() - 1] << " triggers so far "
                          << " --- " << cDeltaTriggers << " triggers since the last check "
                          << " Inst. Trigger rate " << std::scientific << std::setprecision(3) << cTriggerRateInst << std::dec << " Hz "
                          << " [ trigger source is " << +cTriggerSource << " ]"
                          << " [ trigger state is " << +cTriggerState << " ]" << RESET;
                // send to file once you've got more than one point
                if(cCounterThisBrd.size() > 1) cLogFile << +cBoard->getId() << "\t" << currentTimeUTC_us << "\t" << cDeltaTriggers << "\n";
            }
            cLogFile.close();
            cTime0 = currentTimeUTC_us;
            cLoopCounter++;
        } while(fStopTriggerMonitor == 0);
    }
    catch(const std::exception& e)
    {
        LOG(INFO) << BOLDBLUE << "CatchStop caught ctrl+c ... will now exit main monitoring thread" << RESET;
        // wait for all threads to finish
        cCatchStopTh.join();
        cLogFile.close();
    }
}
void OTTool::CheckForResync(BeBoard* pBoard)
{
    // check if a resync is needed
    LOG(INFO) << BOLDBLUE << "Checking if a ReSync is needed for Board" << +pBoard->getId() << RESET;
    bool cReSyncNeeded = false;
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            if(cCic == NULL) continue;
            if(cReSyncNeeded) continue;

            bool cReSyncCIC = fCicInterface->GetResyncRequest(cCic);
            if(cReSyncCIC) LOG(INFO) << BOLDMAGENTA << "\t... CIC" << +cHybrid->getId() << " requires a ReSync" << RESET;

            std::vector<uint8_t> cResyncsCbcs(0);
            for(auto cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() != FrontEndType::CBC3) continue;
                auto cValue = fReadoutChipInterface->ReadChipReg(cChip, "SerialIface&Error");
                if(cValue != 0x00)
                    LOG(INFO) << BOLDMAGENTA << "\t...Link#" << +cOpticalGroup->getId() << " Hybrid#" << +cHybrid->getId() % 2 << " CBC" << +cChip->getId() << " requires a ReSync : Error Register "
                              << std::bitset<8>(cValue) << RESET;

                cResyncsCbcs.push_back(((cValue & 0x1F) != 0x00) ? 1 : 0);
            }
            bool cReSyncCBCs = std::accumulate(cResyncsCbcs.begin(), cResyncsCbcs.end(), 0) > 0;
            cReSyncNeeded    = cReSyncNeeded || cReSyncCBCs || cReSyncCIC;
        }
    }

    if(cReSyncNeeded)
    {
        LOG(INFO) << BOLDMAGENTA << "Sending a ReSync ..." << RESET;
        // send a ReSync to all chips before starting
        fBeBoardInterface->ChipReSync(pBoard);
        // check resync request has been cleared
        for(auto cOpticalGroup: *pBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == NULL) continue;

                if(fCicInterface->GetResyncRequest(cCic))
                {
                    LOG(INFO) << BOLDRED << "ReSync request ofrom CIC" << +cHybrid->getId() << RESET;
                    throw std::runtime_error(std::string("FAILED to clear CIC ReSync request"));
                }
            }
        }
    }
}
// thread to monitor exit signal from main program
void OTTool::CatchStop()
{
    try
    {
        signal(SIGINT, StopTriggerMonitor);
    }
    catch(const std::exception& e)
    {
        LOG(INFO) << BOLDBLUE << "Caught stop signal from terminal..." << RESET;
        fStopTriggerMonitor = 1;
    }
}
// void OTTool::EventMonitor()
// {

// }
// poll board from data
// one thread per BeBoard connected to this computer
void OTTool::ContinousReadout()
{
    // std::vector<uint32_t> cBrdEvntCntrs(fDetectorContainer->size(), 0);
    // reset all event counters
    for(auto cBoard: *fDetectorContainer)
    {
        fBeBoardInterface->setBoard(cBoard->getId());
        auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
        cInterface->ResetEventCounter();
        // cBrdEvntCntrs[cBoard->getIndex()] = cInterface->GetEventCounter();
    }

    // temporary thread object representing a new thread
    // there will be one check thread per board
    std::thread* cStartThreads = new std::thread[fDetectorContainer->size()];
    for(auto cBoard: *fDetectorContainer)
    {
        // launch threads for continuous readout
        cStartThreads[cBoard->getIndex()] = std::thread(&OTTool::StartReadoutTh, this, cBoard->getIndex());
    }
    for(auto cBoard: *fDetectorContainer)
    {
        // launch threads for continuous readout
        cStartThreads[cBoard->getIndex()].join(); // pauses until first finishes
    }

    auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    auto cReadoutInterface = static_cast<D19cL1ReadoutInterface*>(cInterface->getL1ReadoutInterface());
    cReadoutInterface->setNEvents(fNevents);
    cReadoutInterface->StartReadout();
    // while readout is running .. also decode data
    while(!cReadoutInterface->ReadoutFinished() || cReadoutInterface->fEventQueue.size() != 0)
    {
        for(auto cBoard: *fDetectorContainer)
        {
            std::vector<uint32_t> cData(0);
            ReadData(cBoard, cData);
        }
        const std::vector<Event*>& cEvents = GetEvents();
        std::stringstream          cOut;
        cOut << "Event#" << fEventCounter;
        for(auto cBoard: *fDetectorContainer)
        {
            bool cHardOverflow = false;
            bool cErrorBitsSet = false;
            for(auto cOpticalGroup: *cBoard)
            {
                for(auto cHybrid: *cOpticalGroup)
                {
                    auto cL1Status      = static_cast<D19cCic2Event*>(cEvents[0])->L1Status(cHybrid->getId());
                    cErrorBitsSet       = cErrorBitsSet || (cL1Status != 0x000);
                    auto L1Id           = static_cast<D19cCic2Event*>(cEvents[0])->L1Id(cHybrid->getId(), 0);
                    cHardOverflow       = cHardOverflow || (L1Id == 511);
                    auto  cBxId         = (cEvents[0])->BxId(cHybrid->getId());
                    float cHitOccupancy = 0.;
                    for(auto cChip: *cHybrid)
                    {
                        auto cHits = cEvents[0]->GetHits(cChip->getHybridId(), cChip->getId());
                        cHitOccupancy += cHits.size() / (cHybrid->size() * 254.);
                    }
                    cOut << "\t" << cBxId << "\t" << L1Id << "\t[" << std::bitset<9>(cL1Status) << "]\t Hit Occupancy " << std::setprecision(2) << std::fixed << 100 * cHitOccupancy;
                    //"OG#_" << +cOpticalGroup->getId() << "_H#" << +cHybrid->getId() << "_L1Id_" << +L1Id << "_L1Status_" << cL1Status << "\t";
                }
            }
            if(cHardOverflow) LOG(WARNING) << BOLDRED << cOut.str() << RESET;
            // else if( cErrorBitsSet ) LOG (WARNING) << BOLDYELLOW << cOut.str() << RESET;
            if(fEventCounter % 10000 == 0) LOG(INFO) << BOLDGREEN << cOut.str() << RESET;
        }
        fEventCounter++;
    }
    cReadoutInterface->StopReadout();
    LOG(INFO) << BOLDYELLOW << cReadoutInterface->fEventQueue.size() << " events left in the queue once the readout has stopped" << RESET;
    LOG(INFO) << BOLDYELLOW << "Decoded : " << fEventCounter << " events." << RESET;
}
//
void OTTool::ContinousReadoutExt(BeBoard* pBoard, std::thread* cReadoutThread)
{
    fBeBoardInterface->setBoard(pBoard->getId());
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    cInterface->ResetEventCounter();

    std::thread cStartThread = std::thread(&OTTool::StartReadoutTh, this, pBoard->getIndex());
    cStartThread.join(); // pauses until first finishes

    // launch threads for continuous readout
    if(cReadoutThread != nullptr)
    {
        cReadoutThread = nullptr;
        delete cReadoutThread;
    }
    // cReadoutThread = std::thread(&OTTool::ContinousReadoutTh, this, pBoard->getIndex());
}
void OTTool::CheckForResyncTh(uint8_t cBrdId)
{
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBrdId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBrdId; });
    CheckForResync(*cBoardIter);
}
// poll board to check if readout has completed
void OTTool::StartReadoutTh(uint8_t cBrdId)
{
    // get D19cFW Interface
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBrdId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBrdId; });
    CheckForResync(*cBoardIter);
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    auto cTriggerInterface = cInterface->getTriggerInterface();
    auto cReadoutInterface = cInterface->getL1ReadoutInterface();
    fBeBoardInterface->ChipReSync(*cBoardIter);
    cReadoutInterface->ResetReadout();
    cTriggerInterface->Start();
    LOG(DEBUG) << BOLDMAGENTA << "Started triggers on BeBoard#" << +cBrdId << RESET;
}
// continuous readout
// this will continue to read data until the stop triggers command
// has been reached
void OTTool::ContinousReadout(BeBoard* pBoard)
{
    // get D19cFW Interface
    fBeBoardInterface->setBoard(pBoard->getId());
    auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    auto cTriggerInterface = cInterface->getTriggerInterface();

    LOG(INFO) << BOLDBLUE << fMyName << "::ContinousReadout ... until I've received " << fNevents << " events in the readout" << RESET;
    std::vector<uint32_t> cCompleteData(0);
    // stop triggers
    cTriggerInterface->Start();
    fEventCounter                = 0;
    size_t              cCounter = 0;
    bool                cBreak   = false;
    bool                cWait    = false;
    std::vector<size_t> cTriggerCounters(0);
    do {
        // check state of triggers FSM
        if(cTriggerInterface->GetTriggerState() != 1)
        {
            cTriggerInterface->Stop();
            cTriggerInterface->Start();
        }
        std::this_thread::sleep_for(std::chrono::microseconds(fReadoutPause));
        auto                  cTriggerCounter = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_stat.fast_command_block.trigger_in_counter");
        std::vector<uint32_t> cData(0);
        fEventCounter += ReadData(pBoard, cData, cWait);
        if(cData.size() != 0) std::move(cData.begin(), cData.end(), std::back_inserter(cCompleteData));
        cTriggerCounters.push_back(cTriggerCounter);
        if(cCounter % 200 == 0 && cCounter > 0)
        {
            LOG(INFO) << BOLDMAGENTA << "BeamTestCheck continuousReadout loop ... " << +cTriggerCounters[cTriggerCounters.size() - 1] << " triggers received" << RESET;
        }
        cCounter++;
        cBreak = (fEventCounter >= fNevents);
    } while(!cBreak);
    cTriggerInterface->Stop();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::vector<uint32_t> cData(0);
    fEventCounter += ReadData(pBoard, cData, cWait);
    if(cData.size() != 0) std::move(cData.begin(), cData.end(), std::back_inserter(cCompleteData));
    LOG(INFO) << "ContinuousReadout" << RESET;
    DecodeData(pBoard, cCompleteData, fEventCounter, fBeBoardInterface->getBoardType(pBoard));
    LOG(INFO) << BOLDYELLOW << fMyName << " : Mean trigger rate is " << cTriggerCounters[cTriggerCounters.size() - 1] / (cCounter * fReadoutPause * 1e-6) << " Hz"
              << " .... readout " << fEventCounter << RESET;
}
// poll board to check if readout has completed
void OTTool::CheckFinishedTh(uint8_t cBrdId)
{
    // get D19cFW Interface
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBrdId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBrdId; });
    fBeBoardInterface->setBoard((*cBoardIter)->getId());
    auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    auto cTriggerInterface = cInterface->getTriggerInterface();

    // wait until triggers have started
    size_t cWaitCounter = 0;
    size_t cMaxWait     = 10000;
    do {
        std::this_thread::sleep_for(std::chrono::microseconds(fThreadWait));
        if(cWaitCounter % 100 == 0) LOG(INFO) << BOLDBLUE << "\t\t" << fMyName << ":Waiting for triggers to start on BeBoard#" << +cBrdId << RESET;
        cWaitCounter++;
    } while(cTriggerInterface->GetTriggerState() != 1 && cWaitCounter < cMaxWait);

    auto cCounter = fEvenCounter[cBrdId]; // cInterface->GetEventCounter();
    do {
        if(cCounter >= fNevents || cTriggerInterface->GetTriggerState() == 0)
        {
            LOG(INFO) << BOLDBLUE << fMyName << ":Main thread ... finished collecting all requested events from BeBoard" << +cBrdId << RESET;
            break;
        }
        std::this_thread::sleep_for(std::chrono::microseconds(fReadoutPause));
        cCounter = fEvenCounter[cBrdId]; // cInterface->GetEventCounter();
    } while(cCounter < fNevents);
    LOG(INFO) << BOLDBLUE << fMyName << ": check finished thread ... finished collecting all requested events from BeBoard" << +cBrdId << RESET;
    // if(fStopTriggersDone == 1)
    // {
    // LOG(DEBUG) << BOLDBLUE << fMyName << ": check finished thread ... stopping triggers on BeBoard#" << +cBrdId << RESET;
    // cInterface->Stop();
    // }
}
// count events
void OTTool::EventCountingTh(uint8_t cBrdId)
{
    // auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBrdId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBrdId; });
    // fBeBoardInterface->setBoard(cBrdId);
    // fEvenCounter[cBrdId]=0;
    // bool cMissedEvent=false;
    // do {
    //     auto cEventWrd =  fReadoutQueue[cBrdId].pop();//front();
    //     if(cEventWrd == 0xBADDBADD)
    //     {
    //         LOG (INFO) << BOLDYELLOW << "Reached end of valid words from DDR3" << RESET;
    //         break;
    //     }
    //     if(cEventWrd == 0xFFFFFFFF){
    //         continue;
    //     }

    //     uint32_t cHeader    = (0xFFFF0000 & cEventWrd) >> 16;
    //     if(cHeader != 0xFFFF){
    //         continue;
    //     }
    //     uint32_t cEventSize = (0x0000FFFF & cEventWrd) * 4; // event size is given in 128 bit words
    //     if( cEventSize == 0 ) continue;
    //     std::stringstream cOut;
    //     cOut << "Event#" << fEvenCounter[cBrdId] << " : " << std::bitset<32>(cEventWrd) << "\t"
    //         << cEventSize << " 32 bit words";

    //     std::vector<uint32_t> cEventData;
    //     cEventData.push_back(cEventWrd);
    //     for(uint32_t i=1;i<cEventSize;i++){
    //         cEventWrd =  fReadoutQueue[cBrdId].pop();
    //         cEventData.push_back(cEventWrd);
    //         if(i==2)
    //         {
    //             uint32_t cEventIdFW = (0xFFFFF & cEventWrd);
    //             cOut << "\tEventIdFw " << cEventIdFW ;// << "\t... " << fReadoutQueue[cBrdId].size() << " entries in the queue";
    //             if( cEventIdFW!= fEvenCounter[cBrdId]){
    //                 LOG (WARNING) << BOLDRED << cOut.str() << RESET;
    //                 cMissedEvent=true;
    //             }
    //             else if(fEvenCounter[cBrdId]%(fNevents/100)==0) LOG (INFO) << BOLDYELLOW << cOut.str() << RESET;
    //         }
    //     }
    //     // and decode this event
    //     fMyEvents.push_back(new D19cCic2Event(*cBoardIter, cEventData, false));
    //     //DecodeData(*cBoardIter, cEventData, 1, fBeBoardInterface->getBoardType(*cBoardIter));
    //     fEvenCounter[cBrdId]++;
    // }while(fEvenCounter[cBrdId]<fNevents && fReadoutValid[cBrdId] && !cMissedEvent );
    // LOG (INFO) << BOLDYELLOW << "Decoded " << fEvenCounter[cBrdId] << " events from DDR3" << RESET;
    // fEvenCounter[cBrdId]=fNevents;
}
// poll board from data
// one thread per BeBoard connected to this computer
void OTTool::ContinousReadoutTh(uint8_t cBrdId)
{
    // auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBrdId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBrdId; });
    // auto cBoard = (*cBoardIter);
    // fBeBoardInterface->setBoard((*cBoardIter)->getId());
    // auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    // auto cTriggerInterface = cInterface->getTriggerInterface();

    // LOG(DEBUG) << BOLDBLUE << fMyName << ":Starting continuous readout thread for BeBoard#" << +cBrdId << RESET;
    // // this is a single thread which fills the readout data queue
    // size_t cNReads=0;
    // uint32_t cMaxReadout = 10000;// in 32 bit words
    // std::vector<uint32_t> cOffsets(fDetectorContainer->size(),0);
    // std::vector<uint32_t> cNWrds(fDetectorContainer->size(),0);
    // fReadoutValid[cBoard->getId()]=true;
    // fReadoutData[cBoard->getId()].clear();
    // do
    // {
    //     // std::this_thread::sleep_for(std::chrono::microseconds(fThreadWait));
    //     uint32_t cWordCounter =fBeBoardInterface->ReadBoardReg(cBoard,"fc7_daq_stat.readout_block.general.words_cnt");
    //     if( cWordCounter == 0 ){
    //         // LOG (INFO) << BOLDGREEN << " NO WORDS IN THE READOUT " << RESET;
    //         // if( cTriggerInterface->GetTriggerState() == 0 ) break;
    //         // else continue;
    //         continue;
    //     }
    //     try
    //     {
    //         fReadoutValid[cBoard->getId()]=true;
    //         cWordCounter = std::min(cWordCounter, cMaxReadout);
    //         auto cData = cInterface->ReadBlockRegOffset("fc7_daq_ddr3",cWordCounter,cOffsets[cBoard->getId()]);
    //         for (const auto& e: cData) fReadoutQueue[cBoard->getId()].push(e);
    //         if(cNReads%1000 == 0)  LOG (INFO) << BOLDMAGENTA << cWordCounter << "\t" << cOffsets[cBoard->getId()]*32*1e-9 <<  " Gb" << RESET;
    //         cOffsets[cBoard->getId()]+=cWordCounter;
    //     }
    //     catch(...)
    //     {
    //         fReadoutValid[cBoard->getId()]=false;
    //         std::exception_ptr p = std::current_exception();
    //         LOG (INFO) << BOLDRED << "Exception thrown "
    //             // << " after reading out " << fEvenCounter[cBoard->getId()] << " events : "
    //             // << " total word count " << cNWrds[cBoard->getId()]
    //             << " DDR3 offset " << cOffsets[cBoard->getId()]*32*1e-9
    //             << " Gb "
    //             << (p ? p.__cxa_exception_type()->name() : "null")
    //             << RESET;
    //         // push done word to queue
    //         fReadoutQueue[cBoard->getId()].push(0xBADDBADD);
    //     }
    //     cNReads++;
    // }while(fReadoutValid[cBoard->getId()] && fEvenCounter[cBrdId]< fNevents );
    // LOG (INFO) << BOLDYELLOW << "End of thread that polls data from the DDR3 on the FC7" << RESET;
}
void OTTool::FillTree(BeBoard* pBoard)
{
#ifdef __USE_ROOT__
    LOG(INFO) << BOLDYELLOW << "Creating TTree to hold event data in ROOT file.." << RESET;
    TTree* myEvents = new TTree();
    myEvents->SetName("Events");
    myEvents->Branch("BeBoard", &fBoardData.boardId);
    myEvents->Branch("Trigger", &fBoardData.cTriggerId);
    myEvents->Branch("BunchCounter", &fBoardData.cBxIdBeBoard);
    myEvents->Branch("Event", &fBoardData.cEventId);
    myEvents->Branch("L1_Id", &fBoardData.L1Id);
    myEvents->Branch("Bx_Id", &fBoardData.BxId);
    // occupancy
    myEvents->Branch("HitOccupancy", &fFeData.HitOccupancy);
    myEvents->Branch("ClusterMultiplicity", &fFeData.ClusterMultiplicity);
    myEvents->Branch("RocClusterMultiplicity", &fFeData.RocClusterMultiplicity);
    myEvents->Branch("RocHitOccupancy", &fFeData.RocHitOccupancy);
    myEvents->Branch("BottomSensorOccupancy", &fFeData.BottomSensorOccupancy);
    myEvents->Branch("TopSensorOccupancy", &fFeData.TopSensorOccupancy);
    // expected status
    myEvents->Branch("GlobalClusterOverflow", &fFeData.GlobalClusterOverflow);
    myEvents->Branch("RocClusterOverflow", &fFeData.RocClusterOverflow);
    // status bits
    myEvents->Branch("L1Status", &fFeData.CicL1Status);
    myEvents->Branch("OrRocStatus", &fFeData.OrRocL1Status);
    myEvents->Branch("RocL1Status", &fFeData.RocL1Status);
    // hits
    myEvents->Branch("Xdigi", &fFeData.hitX);
    myEvents->Branch("Ydigi", &fFeData.hitY);
    myEvents->Branch("Zdigi", &fFeData.hitZ);

    const std::vector<Event*>& cEvents = GetEvents();
    LOG(INFO) << BOLDRED << "Saving events from FC7 into a TTree.. collected : " << +cEvents.size() << " events." << RESET;
    fEventCountInt = 0;

    std::ofstream cOutFile_L1IdLog, cOutFile_BxIdLog, cOutFile_BxIdDiff, cOutFile_HitOcc, cOutFile_L1Stat;
    cOutFile_L1IdLog.open(fDirectoryName + "/OutFile_L1IdLog.dat", std::ios_base::app);
    cOutFile_BxIdLog.open(fDirectoryName + "/OutFile_BxIdLog.dat", std::ios_base::app);
    cOutFile_BxIdDiff.open(fDirectoryName + "/OutFile_BxIdDiff.dat", std::ios_base::app);
    cOutFile_HitOcc.open(fDirectoryName + "/OutFile_HitOcc.dat", std::ios_base::app);
    cOutFile_L1Stat.open(fDirectoryName + "/OutFile_L1Stat.dat", std::ios_base::app);
    for(auto& cEvent: cEvents)
    {
        if(cEvent->GetEventCount() > fNevents) continue;
        fBoardData.boardId      = pBoard->getId();
        fBoardData.cEventId     = cEvent->GetEventCount();
        fBoardData.boardId      = pBoard->getId();
        fBoardData.cTriggerId   = cEvent->GetL1Number();
        fBoardData.cBxIdBeBoard = cEvent->GetBunch();
        cOutFile_L1IdLog << fBoardData.cBxIdBeBoard << "\t";
        cOutFile_BxIdLog << fBoardData.cBxIdBeBoard << "\t";
        cOutFile_HitOcc << fBoardData.cBxIdBeBoard << "\t";
        cOutFile_L1Stat << fBoardData.cBxIdBeBoard << "\t";
        std::vector<uint8_t>  cBxIdsHist(4000, 0);
        std::vector<int>      cBxIds;
        std::vector<uint32_t> cHitOcc(0);
        fBoardData.L1Id.clear();
        fBoardData.BxId.clear();
        fFeData.CicL1Status.clear();
        fFeData.RocL1Status.clear();
        fFeData.OrRocL1Status.clear();
        fFeData.HitOccupancy.clear();
        fFeData.ClusterMultiplicity.clear();
        fFeData.RocClusterMultiplicity.clear();
        fFeData.RocHitOccupancy.clear();
        fFeData.GlobalClusterOverflow.clear();
        fFeData.RocClusterOverflow.clear();
        fFeData.BottomSensorOccupancy.clear();
        fFeData.TopSensorOccupancy.clear();
        fFeData.hitX.clear();
        fFeData.hitY.clear();
        fFeData.hitZ.clear();
        for(auto cOpticalGroup: *pBoard)
        {
            std::stringstream cHitOutput;
            bool              cHitFound = false;
            fFeData.BottomSensorOccupancy.push_back(0);
            fFeData.TopSensorOccupancy.push_back(0);
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic      = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                auto  cMapping  = fCicInterface->getMapping(cCic);
                auto  cL1Status = static_cast<D19cCic2Event*>(cEvent)->L1Status(cHybrid->getId());
                fFeData.CicL1Status.push_back(cL1Status & 0x1);
                fFeData.OrRocL1Status.push_back((cL1Status >> 1) == 0 ? 0 : 1);
                auto cCurrentSize = fFeData.RocL1Status.size();
                fFeData.RocL1Status.resize(cCurrentSize + 8, 0);
                for(uint8_t cIndx = 0; cIndx < 8; cIndx++) { fFeData.RocL1Status[cCurrentSize + cMapping[cIndx]] = (cL1Status >> (1 + cIndx)) && 0x1; } // mapped to hybrid id
                auto L1Id = static_cast<D19cCic2Event*>(cEvent)->L1Id(cHybrid->getId(), 0);
                fBoardData.L1Id.push_back(L1Id);
                auto cBxId = (cEvent)->BxId(cHybrid->getId());
                fBoardData.BxId.push_back(cBxId);
                fFeData.HitOccupancy.push_back(0);
                fFeData.ClusterMultiplicity.push_back(0);
                std::stringstream cEventHeader;
                cEventHeader << "Event#" << fBoardData.cEventId << " Hybrid#" << +cHybrid->getId() << " BxCounter#" << fBoardData.cBxIdBeBoard << " BxId#" << cBxId << " L1Id#" << L1Id
                             << " Status bits: " << std::bitset<9>(cL1Status);
                std::stringstream cHitList;
                for(auto cChip: *cHybrid)
                {
                    auto cClusters = static_cast<D19cCic2Event*>(cEvent)->getClusters(cChip->getHybridId(), cChip->getId());
                    auto cHits     = static_cast<D19cCic2Event*>(cEvent)->GetHits(cChip->getHybridId(), cChip->getId());
                    fFeData.HitOccupancy[fFeData.HitOccupancy.size() - 1] += cHits.size();
                    fFeData.ClusterMultiplicity[fFeData.ClusterMultiplicity.size() - 1] += cClusters.size();
                    fFeData.RocClusterMultiplicity.push_back(cClusters.size());
                    fFeData.RocClusterOverflow.push_back(cClusters.size() >= 31 ? 1 : 0);
                    fFeData.RocHitOccupancy.push_back(cHits.size());
                    uint16_t cOffset = cChip->getId() * cChip->size() / 2;
                    auto     cLocalX = cOpticalGroup->getId() + 0.5 * (cChip->getHybridId() % 2);
                    for(auto cHit: cHits)
                    {
                        uint16_t cStripId = cOffset + cHit / 2.;
                        if(cChip->getHybridId() % 2 == 0) // RHS hybrid first strip is in ROC#0
                        {
                            cStripId = 8 * 127 - (cOffset + cHit / 2);
                        }
                        auto cLocalY = cStripId;
                        auto cLocalZ = (cHit % 2 == 0) ? 1 : 0;
                        fFeData.hitX.push_back(cLocalX);
                        fFeData.hitY.push_back(cLocalY);
                        fFeData.hitZ.push_back(cLocalZ);
                        cHitList << "(" << +cChip->getId() << "," << cHit << ") ";
                        if(cHit % 2 == 0)
                            fFeData.BottomSensorOccupancy[fFeData.BottomSensorOccupancy.size() - 1]++;
                        else
                            fFeData.TopSensorOccupancy[fFeData.TopSensorOccupancy.size() - 1]++;
                    }
                }
                if(fFeData.ClusterMultiplicity[fFeData.ClusterMultiplicity.size() - 1] == 0)
                    fFeData.GlobalClusterOverflow.push_back(0);
                else if(fFeData.ClusterMultiplicity[fFeData.ClusterMultiplicity.size() - 1] == 127)
                    fFeData.GlobalClusterOverflow.push_back(2);
                else
                    fFeData.GlobalClusterOverflow.push_back(1);

                if(fFeData.CicL1Status[fFeData.CicL1Status.size() - 1] == 0 && fFeData.ClusterMultiplicity[fFeData.ClusterMultiplicity.size() - 1] == 127)
                {
                    LOG(WARNING) << BOLDRED << cEventHeader.str() << " : " << cHitList.str() << RESET;
                }
                // else LOG (INFO) << BOLDGREEN << cEventHeader.str() << RESET;
                // fFeData.GlobalClusterOverflow.push_back( fFeData.ClusterMultiplicity[fFeData.ClusterMultiplicity.size()-1] >= 127 ? 1 : 0 );
                // cOutFile_L1IdLog << L1Id << "\t";
                // cOutFile_BxIdLog << cBxId << "\t";
                // cOutFile_L1Stat  << std::bitset<9>(cL1Status) << "\t";
                // cBxIdsHist[cBxId]++;
                // cBxIds.push_back(cBxId);
                // std::vector<uint32_t> cLocalOcc(0);
                // std::stringstream cHitListBottom;
                // std::stringstream cHitListTop;
                // std::vector<uint8_t> cOverflow(0);
                // for(auto cChip: *cHybrid)
                // {
                //     auto cClusters = static_cast<D19cCic2Event*>(cEvent)->getClusters(cChip->getHybridId(), cChip->getId());
                //     auto cHits = static_cast<D19cCic2Event*>(cEvent)->GetHits(cChip->getHybridId(), cChip->getId());
                //     cOverflow.push_back( cHits.size() > 30 ? 1 : 0);
                //     cCicOcc[cHybrid->getId()] += cHits.size();
                //     cCicClusterMult[cHybrid->getId()] += cClusters.size();
                //     cHitOcc.push_back( cHits.size() );
                //     cLocalOcc.push_back(  cHits.size() );
                //     cOutFile_HitOcc << cHits.size() << "\t";
                //     cEventHeader << "\t" << cHits.size();
                //     cHitListBottom << "FE#" << +cChip->getId() << "_B : ";
                //     cHitListTop << "FE#" << +cChip->getId() << "_T : ";
                //     for(auto cHit : cHits){
                //         if( cHit%2 == 0 ) cHitListBottom << cHit/2 << ",";
                //         else cHitListTop << cHit/2 << ",";
                //     }
                //     cHitListTop << "\n";
                //     cHitListBottom << "\n";
                // }
                // if(  cCicClusterMult[cHybrid->getId()] == 127 ) cHighMultEvents[cHybrid->getId()]++;
                // bool cFeOverflow = std::accumulate(cOverflow.begin(),cOverflow.end(),0) > 0;
                // float cMeanOcc = std::accumulate(cHitOcc.begin(), cHitOcc.end(), 0.)/cHitOcc.size();
                // float cMean = std::accumulate(cLocalOcc.begin(), cLocalOcc.end(), 0.)/cLocalOcc.size();
                // std::stringstream cExtra;
                // cEventHeader << " Lcl " << cMean << " Glbl " << cMeanOcc
                //     << " " << cCicClusterMult[cHybrid->getId()];
                // // if( (cL1Status&0x1) != 0x00 && cCicClusterMult[cHybrid->getId()] == 127 ) LOG (INFO) << BOLDMAGENTA << "CO " << cEventHeader.str() <<  RESET;
                // // if( (cL1Status&0x1) != 0x00 && cCicClusterMult[cHybrid->getId()] < 127 && cCicClusterMult[cHybrid->getId()] == 0 )  LOG (INFO) << BOLDYELLOW << cEventHeader.str() <<  RESET;
                // // if( (cL1Status&0x1) != 0x00 && cCicClusterMult[cHybrid->getId()] < 127 && cCicClusterMult[cHybrid->getId()] > 0 && cFeOverflow )  LOG (INFO) << BOLDYELLOW << cEventHeader.str()
                // <<  RESET; if(  (cL1Status&0x1) == 0x00 && cL1Status != 0x00 && !cFeOverflow)  LOG (INFO) << BOLDYELLOW << cEventHeader.str() <<  RESET; if(  cL1Status != 0x00 &&
                // cCicClusterMult[cHybrid->getId()] < 127 && cCicClusterMult[cHybrid->getId()] > 0 && !cFeOverflow )  LOG (INFO) << BOLDMAGENTA << cEventHeader.str() <<  RESET;
                // // if(  cL1Status != 0x00 && cCicClusterMult[cHybrid->getId()] < 127 && cCicClusterMult[cHybrid->getId()] > 0 &&  cFeOverflow )  LOG (INFO) << BOLDCYAN << cEventHeader.str() <<
                // RESET;
                // // else LOG (INFO) << BOLDGREEN << cEventHeader.str() << RESET;
            }
        }
        // cNoiseHitOcc = std::accumulate(cHitOcc.begin(), cHitOcc.end(), 0.);
        // float cMeanOcc = std::accumulate(cHitOcc.begin(), cHitOcc.end(), 0.)/cHitOcc.size();
        // int cBxIdMode = std::max_element(cBxIdsHist.begin(), cBxIdsHist.end()) - cBxIdsHist.begin();
        // cOutFile_L1IdLog << "\n";
        // cOutFile_BxIdLog << "\n";
        // cOutFile_BxIdDiff << cBxIdMode << "\t";
        // for(auto cBxId : cBxIds ) cOutFile_BxIdDiff << (cBxId-cBxIdMode) << "\t";
        // cOutFile_BxIdDiff << "\n";
        // cOutFile_HitOcc << cMeanOcc << "\n";
        // cOutFile_L1Stat << cMeanOcc << "\n";
        myEvents->Fill();
    }
    // for(auto cHighMultCount : cHighMultEvents ) LOG (INFO) << BOLDBLUE << "Fraction of events with cluster overflow : " << 100.*(float)cHighMultCount/cEvents.size() << RESET;

    myEvents->Write();
    cOutFile_L1IdLog.close();
    cOutFile_BxIdLog.close();
    cOutFile_BxIdDiff.close();
    cOutFile_L1Stat.close();
#endif
}

void OTTool::EventPrintout(BeBoard* pBoard, Event* pEvent)
{
    size_t cEventCutOff = 5000;
    auto   cSparsified  = pBoard->getSparsification();
    if(cSparsified)
        LOG(DEBUG) << BOLDBLUE << "Checking with internal - sparisified data" << RESET;
    else
        LOG(DEBUG) << BOLDBLUE << "Checking with internal - un-sparisified data" << RESET;

    if(pEvent->GetEventCount() % fPrintConfig.fPrintEvery != 0) return;
    std::stringstream cEvntHeader;
    cEvntHeader << "Event#" << +pEvent->GetEventCount() << " -- " << +fEventCountInt << " in readout..." << RESET;
    std::stringstream cHeader;

    std::ofstream cOutFile_LT, cOutFile_RT, cOutFile_LB, cOutFile_RB;
    cOutFile_LT.open("OTTool_LT.dat", std::ios_base::app);
    cOutFile_RT.open("OTTool_RT.dat", std::ios_base::app);
    cOutFile_LB.open("OTTool_LB.dat", std::ios_base::app);
    cOutFile_RB.open("OTTool_RB.dat", std::ios_base::app);
    fBoardData.cEventId     = pEvent->GetEventCount();
    fBoardData.boardId      = pBoard->getId();
    fBoardData.cTriggerId   = pEvent->GetL1Number();
    fBoardData.cBxIdBeBoard = pEvent->GetBunch();
    if(pEvent->GetEventCount() < cEventCutOff)
        LOG(INFO) << BOLDYELLOW << "Event#" << fBoardData.cEventId << " Trigger#" << fBoardData.cTriggerId << " FC7BxId#" << fBoardData.cBxIdBeBoard << " Board#" << +fBoardData.boardId << RESET;
    for(auto cOpticalGroup: *pBoard)
    {
        std::vector<uint16_t> cBxIds(0);
        std::vector<uint16_t> cL1Ids(0);
        std::stringstream     cOutOfSync;
        fBoardData.opticalGroupId = cOpticalGroup->getId();
        for(auto cHybrid: *cOpticalGroup)
        {
            // fBoardData.L1Id = static_cast<D19cCic2Event*>(pEvent)->L1Id(cHybrid->getId(), 0);
            auto cL1Status   = static_cast<D19cCic2Event*>(pEvent)->L1Status(cHybrid->getId());
            fBoardData.cBxId = (pEvent)->BxId(cHybrid->getId());
            auto cStubStat   = static_cast<D19cCic2Event*>(pEvent)->Status(cHybrid->getId());
            if(pEvent->GetEventCount() < cEventCutOff)
            {
                LOG(INFO) << BOLDYELLOW << "\t... CIC on Link#"
                          << +cHybrid->getOpticalGroupId()
                          // << " : L1Id " << fBoardData.L1Id
                          << " BxId " << fBoardData.cBxId << " L1Status " << std::bitset<9>(cL1Status) << " StubStatus " << std::bitset<9>(cStubStat) << RESET;
            }

            if(pEvent->GetEventCount() < cEventCutOff)
            {
                std::vector<uint32_t> cHits_TopSensor(cHybrid->size() * 127, 0);
                std::vector<uint32_t> cHits_BottomSensor(cHybrid->size() * 127, 0);
                uint32_t              cTotalNHits = 0;
                for(auto cChip: *cHybrid)
                {
                    // uint16_t cOffset = cChip->getId() * cChip->size() / 2.;
                    auto cHits = pEvent->GetHits(cHybrid->getId(), cChip->getId());
                    LOG(DEBUG) << cHits.size() << RESET;
                    // for(auto cChnl = 0; cChnl < (int)cChip->size(); cChnl++)
                    // {
                    //     uint16_t cStripOffset = cOffset; //(cChnlIndx % 2 == 0) ? cOffset : (cNchannels*8) / 2 + cOffset;
                    //     uint16_t cStripId     = cStripOffset + cChnl / 2;
                    //     if(cHybrid->getId() % 2 == 0)
                    //     {
                    //         cStripOffset = (cChip->size() * 8) / 2 - 1; //(cChnlIndx % 2 == 0) ? (cNchannels*8) / 2 : (cNchannels*8);
                    //         cStripId     = cStripOffset - (cChip->getId() * cChip->size() / 2 + cChnl / 2);
                    //     }
                    //     fBoardData.localX    = cHybrid->getId() % 2;
                    //     fBoardData.localY    = cStripId;
                    //     fBoardData.cSensorId = (cChnl % 2 == 0) ? 0 : 1;
                    //     auto cHitFound       = std::find(cHits.begin(), cHits.end(), cChnl) != cHits.end();
                    //     fBoardData.hit       = (cHitFound) ? 1 : 0;
                    //     #ifdef __USE_ROOT__
                    //         if(fSaveTree) fTree->Fill();
                    //     #endif
                    //     cTotalNHits += (cHitFound) ? 1 : 0;
                    //     if(cChnl % 2 == 0)
                    //         cHits_BottomSensor[cStripId] = cHitFound ? 1 : 0;
                    //     else
                    //         cHits_TopSensor[cStripId] = cHitFound ? 1 : 0;
                    // }
                }

                // if(cTotalNHits >= 0 )
                //{
                std::stringstream cEventPrintout_TopSensor;
                std::stringstream cEventPrintout_BottomSensor;
                // cEventPrintout_TopSensor << cBxId << "\t";
                // cEventPrintout_BottomSensor << cBxId << "\t";
                for(auto cStripId = 0; cStripId < cHybrid->size() * 127; cStripId++)
                {
                    if(cStripId < cHybrid->size() * 127 - 1)
                    {
                        cEventPrintout_TopSensor << cHits_TopSensor[cStripId] << "\t";
                        cEventPrintout_BottomSensor << cHits_BottomSensor[cStripId] << "\t";
                    }
                    else
                    {
                        cEventPrintout_TopSensor << cHits_TopSensor[cStripId];
                        cEventPrintout_BottomSensor << cHits_BottomSensor[cStripId];
                    }
                }

                if(cHybrid->getId() % 2 == 0)
                {
                    cOutFile_RT << cEventPrintout_TopSensor.str() << "\n";
                    cOutFile_RB << cEventPrintout_BottomSensor.str() << "\n";
                }
                else
                {
                    cOutFile_LT << cEventPrintout_TopSensor.str() << "\n";
                    cOutFile_LB << cEventPrintout_BottomSensor.str() << "\n";
                }
            }

            // std::stringstream cOutStubs;
            // std::stringstream cOutL1;
            // std::stringstream cOutAna;
            // std::stringstream cOutEvntHeader;
            // if(cStubStat != 0x00 || cL1Status != 0x00)
            // {
            //     cOutEvntHeader << cEvntHeader.str() << BOLDRED << " Hybrid#" << +cHybrid->getId() << " on Link#" << +cOpticalGroup->getId() << " L1Id " << +fBoardData.L1Id << " Stub status is "
            //                    << std::bitset<8>(cStubStat) << " L1 status [FEs] is " << std::bitset<8>(cL1Status) << " L1 status [CIC] is " << std::bitset<1>(cL1Status & 0x1) << " BxId is "
            //                    << +fBoardData.cBxId;
            //     cHeader << BOLDRED << "\t\t.. "
            //             << " Hybrid#" << +cHybrid->getId() << " on Link#" << +cOpticalGroup->getId() << " L1Id " << +fBoardData.L1Id << " Stub status is " << std::bitset<8>(cStubStat)
            //             << " L1 status [FEs] is " << std::bitset<8>(cL1Status) << " L1 status [CIC] is " << std::bitset<1>(cL1Status & 0x1) << " BxId is " << +fBoardData.cBxId << "\n"
            //             << RESET;
            // }
            // else
            // {
            //     cOutEvntHeader << cEvntHeader.str() << BOLDGREEN << " Hybrid#" << +cHybrid->getId() << " on Link#" << +cOpticalGroup->getId() << " L1Id " << +fBoardData.L1Id << " Stub status is "
            //                    << std::bitset<8>(cStubStat) << " L1 status [FEs] is " << std::bitset<8>(cL1Status) << " L1 status [CIC] is " << std::bitset<1>(cL1Status & 0x1) << " BxId is "
            //                    << +fBoardData.cBxId;
            //     cHeader << BOLDGREEN << "\t\t.. "
            //             << " Hybrid#" << +cHybrid->getId() << " on Link#" << +cOpticalGroup->getId() << " L1Id " << +fBoardData.L1Id << " Stub status is " << std::bitset<8>(cStubStat)
            //             << " L1 status [FEs] is " << std::bitset<8>(cL1Status) << " L1 status [CIC] is " << std::bitset<1>(cL1Status & 0x1) << " BxId is " << +fBoardData.cBxId << "\n"
            //             << RESET;
            // }

            // if(cBxIds.size() == 0)
            // {
            //     cBxIds.push_back(fBoardData.cBxId);
            //     cL1Ids.push_back(fBoardData.L1Id);
            // }
            // else
            // {
            //     if(std::find(cBxIds.begin(), cBxIds.end(), fBoardData.cBxId) == cBxIds.end()) cBxIds.push_back(fBoardData.cBxId);
            //     if(std::find(cL1Ids.begin(), cL1Ids.end(), fBoardData.L1Id) == cL1Ids.end()) cL1Ids.push_back(fBoardData.L1Id);
            // }
        }
    }

    cOutFile_LT.close();
    cOutFile_RT.close();
    cOutFile_LB.close();
    cOutFile_RB.close();
}

//
void OTTool::InjectPattern(BeBoard* pBoard, std::vector<Injection> pInjections, int pChipId)
{
    bool                  cUseSingleChip = (pChipId >= 0);
    std::vector<uint16_t> cRows(0);
    for(auto cInjection: pInjections)
    {
        if(std::find(cRows.begin(), cRows.end(), cInjection.fRow) == cRows.end()) cRows.push_back(cInjection.fRow);
    }
    // injection rows + rows/cols
    if(cUseSingleChip)
        LOG(INFO) << BOLDYELLOW << "Injecting in single chip..  " << pInjections.size() << " pixels and " << cRows.size() << " strips." << RESET;
    else
        LOG(INFO) << BOLDYELLOW << "Injecting in all chips ..  " << pInjections.size() << " pixels and " << cRows.size() << " strips." << RESET;
    size_t cIndx = 0;
    if(fInjectionType == 0)
    {
        setSameDacBeBoard(pBoard, "DigitalSync", 0x01);        // digital sync mode
        setSameDacBeBoard(pBoard, "CalibrationPattern", 0x00); // global injection pattern off for 8 Bx
        for(auto cInjection: pInjections)
        {
            uint32_t cPixelId = (uint32_t)(cInjection.fColumn) * NSSACHANNELS + (uint32_t)cInjection.fRow;
            for(auto cOpticalReadout: *pBoard)
            {
                for(auto cHybrid: *cOpticalReadout)
                {
                    for(auto cChip: *cHybrid)
                    {
                        std::stringstream cRegName;
                        if(cChip->getId() % 8 != pChipId && pChipId >= 0) continue;

                        if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                        {
                            cRegName << "CalibrationPattern_P" << cPixelId;
                            LOG(INFO) << BOLDYELLOW << "MPA#" << +cChip->getId() << " Digital injection pattern enabled in pixel " << +cPixelId << " column " << +cInjection.fColumn << " row "
                                      << +cInjection.fRow << RESET;
                        }
                        else if(cIndx < cRows.size())
                        {
                            LOG(INFO) << BOLDYELLOW << "SSA#" << +cChip->getId() << " Digital injection pattern enabled in strip# " << +cInjection.fRow << RESET;
                            cRegName << "CalibrationPattern_S" << cRows[cIndx];
                        }
                        if(!cRegName.str().empty()) { fReadoutChipInterface->WriteChipReg(cChip, cRegName.str(), 0x01); }
                    } // chips
                }     // hybrids
            }         // OGs
            cIndx++;
        } // injections
    }
    else
    {
        setSameDacBeBoard(pBoard, "AnalogueSync", 0x00); // analogue sync mode off for all
        for(auto cInjection: pInjections)
        {
            uint32_t cPixelId = (uint32_t)(cInjection.fColumn) * NSSACHANNELS + (uint32_t)cInjection.fRow;
            LOG(INFO) << BOLDBLUE << " Analogue injection enabled in pixel " << +cPixelId << " column " << +cInjection.fColumn << " row " << +cInjection.fRow << RESET;
            for(auto cOpticalReadout: *pBoard)
            {
                for(auto cHybrid: *cOpticalReadout)
                {
                    for(auto cChip: *cHybrid)
                    {
                        std::stringstream cRegName;
                        if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                            cRegName << "AnalogueSync_P" << cPixelId;
                        else if(cIndx < cRows.size())
                            cRegName << "AnalogueSync_S" << cRows[cIndx];
                        if(!cRegName.str().empty()) { fReadoutChipInterface->WriteChipReg(cChip, cRegName.str(), 0x01); }
                    } // chips
                }     // hybrids
            }         // OGs
            cIndx++;
        } // injections
    }

    /*
    // inject pixel clusters
    for(auto cOpticalReadout: *pBoard)
    {
        for(auto cHybrid: *cOpticalReadout)
        {
            for(auto cChip: *cHybrid)
            {
                // fReadoutChipInterface->WriteChipReg(cChip, "ReadoutMode",0x0);
                if(cChip->getId() % 8 != pChipId && pChipId > 0) continue;
                LOG(DEBUG) << BOLDMAGENTA << "Injecting patterns in Chip#" << +cChip->getId() << RESET;
                // make sure L1 latency is configured
                if(cChip->getFrontEndType() == FrontEndType::MPA)
                {
                    if(fInjectionType == 0)
                    {
                        // for digi injection .. explicity disable all other pixels
                        fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x0);
                        (static_cast<PSInterface*>(fReadoutChipInterface))->digiInjection(cChip, pInjections, 0x01);
                    }
                    else
                    {
                        for(auto cInjection: pInjections)
                        {
                            if(cInjectAll) continue;
                            auto cPxl = cInjection.fColumn * NSSACHANNELS + (uint32_t)cInjection.fRow;
                            LOG(INFO) << BOLDBLUE << " Injecting in pixel " << +cPxl << " column " << +cInjection.fColumn << " row " << +cInjection.fRow << RESET;
                            std::stringstream cRegName;
                            cRegName << "ENFLAGS_P" << +cPxl;
                            fReadoutChipInterface->WriteChipReg(cChip, cRegName.str(), 0x5F, false);
                        }
                        if(cInjectAll) fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x5F);
                    }
                }
                if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    // for digi injection .. explicity disable all other strips
                    if(pInjections.size() > 0 && fInjectionType == 0)
                    {
                        fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x0);
                        fReadoutChipInterface->WriteChipReg(cChip, "DigCalibPattern_L_ALL", 0x00);
                        fReadoutChipInterface->WriteChipReg(cChip, "DigCalibPattern_H_ALL", 0x00);
                        fReadoutChipInterface->WriteChipReg(cChip, "CalPulse_duration", 0x01);
                    }
                    for(auto cInjection: pInjections)
                    {
                        if(fInjectionType == 0)
                        {
                            std::stringstream cRegNameEn;
                            cRegNameEn << "ENFLAGS_S" << +cInjection.fRow;
                            fReadoutChipInterface->WriteChipReg(cChip, cRegNameEn.str(), 0x8);
                            std::stringstream cRegNamePattern;
                            cRegNamePattern << "DigCalibPattern_L_S" << +cInjection.fRow;
                            fReadoutChipInterface->WriteChipReg(cChip, cRegNamePattern.str(), 0x01);
                        }
                        else
                        {
                            if(cInjectAll) continue;
                            LOG(INFO) << BOLDGREEN << " Inection in Strip#" << +cInjection.fRow << RESET;
                            fReadoutChipInterface->WriteChipReg(cChip, "CalPulse_duration", 0x08);
                            fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_S" + std::to_string(cInjection.fRow), 0x11);
                        }
                    }
                    if(fInjectionType == 1 && cInjectAll)
                    {
                        fReadoutChipInterface->WriteChipReg(cChip, "CalPulse_duration", 0x08);
                        fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x11);
                    }
                }
            } // chip
        }     // hybrid
    }         // optica]l group
    */
}
void OTTool::StubDump()
{
    for(auto cBoard: *fDetectorContainer) StubDump(cBoard);
}
void OTTool::StubDump(BeBoard* pBoard)
{
    fBeBoardInterface->setBoard(pBoard->getId());
    auto cDDR3Source = fBeBoardInterface->ReadBoardReg(pBoard, "fc7_daq_cnfg.ddr3_block.source");
    fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_cnfg.ddr3_block.source", 1);
    auto cInterface        = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    auto cReadoutInterface = cInterface->getL1ReadoutInterface();
    auto cTriggerInterface = cInterface->getTriggerInterface();
    if(cTriggerInterface->GetTriggerState() == 0) cTriggerInterface->Start();
    cReadoutInterface->ResetReadout();
    fBeBoardInterface->ChipReSync(pBoard);
    std::this_thread::sleep_for(std::chrono::microseconds(fReadoutPause * 10));
    auto cData = fBeBoardInterface->ReadBlockBoardReg(pBoard, "fc7_daq_ddr3", 100);
    for(auto cWord: cData) LOG(INFO) << BOLDYELLOW << std::bitset<32>(cWord) << RESET;
    fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_cnfg.ddr3_block.source", cDDR3Source);
}
void OTTool::UpdateFromRegMap(BeBoard* pBoard)
{
    LOG(INFO) << BOLDYELLOW << fMyName << ":UpdateFromRegMap" << RESET;
    // important registers for beBoard
    std::vector<std::string> cBoardRegs{"fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity", "fc7_daq_cnfg.fast_command_block.trigger_source"};
    cBoardRegs.push_back("fc7_daq_cnfg.tlu_block.trigger_id_delay");
    cBoardRegs.push_back("fc7_daq_cnfg.tlu_block.tlu_enabled");
    cBoardRegs.push_back("fc7_daq_cnfg.tlu_block.handshake_mode");
    BeBoardRegMap cRegMap = pBoard->getBeBoardRegMap();
    for(auto cReg: cBoardRegs)
    {
        LOG(INFO) << BOLDBLUE << "Setting " << cReg << " to " << +cRegMap[cReg] << RESET;
        fBeBoardInterface->WriteBoardReg(pBoard, cReg, cRegMap[cReg]);
    }

    // set thresholds
    LOG(INFO) << BOLDYELLOW << fMyName << ":UpdateFromRegMap set thresholds" << RESET;
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                uint32_t cThreshold = 0;
                if(cChip->getFrontEndType() == FrontEndType::CBC3) cThreshold = (cChip->getReg("VCth1") + (cChip->getReg("VCth2") << 8));
                if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2) cThreshold = cChip->getReg("Bias_THDAC");
                if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2) cThreshold = cChip->getReg("ThDAC0");
                LOG(DEBUG) << BOLDMAGENTA << "Setting threshold on Chip#" << +cChip->getId() << " to 0x" << std::hex << +cThreshold << std::dec << RESET;
                fReadoutChipInterface->WriteChipReg(cChip, "Threshold", cThreshold);
            }
        }
    }
    // set stub mode from xml
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() == FrontEndType::MPA)
                {
                    auto cMode = cChip->getReg("ECM");
                    fReadoutChipInterface->WriteChipReg(cChip, "ECM", cMode);
                    LOG(DEBUG) << BOLDMAGENTA << "Setting StubMode regisger on Chip#" << +cChip->getId() << " to " << cMode << RESET;
                }
            }
        }
    }
    // set hit mode from xml
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                {
                    auto cMode = cChip->getReg("ModeSel_ALL");
                    fReadoutChipInterface->WriteChipReg(cChip, "ModeSel_ALL", cMode);
                    LOG(DEBUG) << BOLDMAGENTA << "Setting HitLogicMode register on Chip#" << +cChip->getId() << " to " << cMode << RESET;
                }
                else if(cChip->getFrontEndType() == FrontEndType::SSA) //|| cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    auto cMode = cChip->getReg("SAMPLINGMODE_ALL");
                    fReadoutChipInterface->WriteChipReg(cChip, "SAMPLINGMODE_ALL", cMode);
                    LOG(DEBUG) << BOLDMAGENTA << "Setting HitLogicMode register on Chip#" << +cChip->getId() << " to " << cMode << RESET;
                }
            }
        }
    }
    // charge injection
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                std::string cRegName = "";
                if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2) { cRegName = "CalDAC0"; }
                else if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    cRegName = "Bias_CALDAC";
                }
                else if(cChip->getFrontEndType() == FrontEndType::CBC3)
                {
                    cRegName = "MiscTestPulseCtrl&AnalogMux";
                }
                uint8_t cInjectedCharge = cChip->getReg(cRegName);
                if(cChip->getFrontEndType() == FrontEndType::CBC3) cInjectedCharge = (cInjectedCharge >> 6) & 0x3F;
                fReadoutChipInterface->WriteChipReg(cChip, "InjectedCharge", cInjectedCharge);
                LOG(DEBUG) << BOLDMAGENTA << "Setting Charge injection register on Chip#" << +cChip->getId() << " to " << +cInjectedCharge << RESET;
            }
        }
    }
    // latency
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                uint16_t cLatency = 0;
                if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                {
                    cLatency = cChip->getReg("L1Offset_2_ALL") << 8 | cChip->getReg("L1Offset_1_ALL");
                }
                else if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    cLatency = cChip->getReg("L1-Latency_MSB") << 8 | cChip->getReg("L1-Latency_LSB");
                }
                else if(cChip->getFrontEndType() == FrontEndType::CBC3)
                {
                    auto cRegValueFirst  = cChip->getReg("FeCtrl&TrgLat2");
                    auto cRegValueSecond = cChip->getReg("TriggerLatency1");
                    cLatency             = ((cRegValueFirst & 0x1) << 8) | cRegValueSecond;
                }
                LOG(DEBUG) << BOLDYELLOW << "Setting latency on Chip#" << +cChip->getId() << " to " << cLatency << RESET;
                fReadoutChipInterface->WriteChipReg(cChip, "TriggerLatency", cLatency);
            }
        }
    }
    // configure HIPs
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                uint16_t    cCut = 0;
                std::string cRegName;
                if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                {
                    cRegName = "HipCut_ALL";
                    cCut     = cChip->getReg(cRegName);
                }
                else if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    cRegName = "HIPCUT_ALL";
                    cCut     = cChip->getReg(cRegName);
                }
                else if(cChip->getFrontEndType() == FrontEndType::CBC3)
                {
                    cRegName = "HIP&TestMode";
                    cCut     = cChip->getReg("HIP&TestMode");
                }
                LOG(DEBUG) << BOLDYELLOW << "Setting HIP register on Chip#" << +cChip->getId() << " to " << cCut << RESET;
                fReadoutChipInterface->WriteChipReg(cChip, cRegName, cCut);
            }
        }
    }

    // configure sampling delay
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    std::vector<std::string> cRegNames{"PhaseShiftClock", "ClockDeskewing"};
                    for(auto cRegName: cRegNames)
                    {
                        auto cValueInMemory = cChip->getReg(cRegName);
                        fReadoutChipInterface->WriteChipReg(cChip, cRegName, cValueInMemory);
                    }
                }
                else if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                {
                    std::vector<std::string> cRegNames{"PhaseShift", "ConfDLL"};
                    for(auto cRegName: cRegNames)
                    {
                        auto cValueInMemory = cChip->getReg(cRegName);
                        fReadoutChipInterface->WriteChipReg(cChip, cRegName, cValueInMemory);
                    }
                }
                // To-Do add CBC
                // else if( cChip->getFrontEndType() == FrontEndType::CBC3 )
                // {
                // }
            }
        }
    }

    // make sure MPAs have both modes enabled
    // and that the disabled strips in the SSAs are really disabled
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                std::string cRegName = "";
                if(cChip->getFrontEndType() == FrontEndType::MPA || cChip->getFrontEndType() == FrontEndType::MPA2)
                {
                    fReadoutChipInterface->WriteChipReg(cChip, "ENFLAGS_ALL", 0x0F);
                    LOG(DEBUG) << BOLDMAGENTA << "Setting ENFLAGS_ALL on Chip#" << +cChip->getId() << " to enable both modes.." << RESET;
                }
                if(cChip->getFrontEndType() == FrontEndType::SSA || cChip->getFrontEndType() == FrontEndType::SSA2)
                {
                    for(size_t cIndx = 0; cIndx < NSSACHANNELS; cIndx++)
                    {
                        std::stringstream cRegName;
                        cRegName << "ENFLAGS_S" << +(cIndx + 1);
                        auto cValueInMemory = cChip->getReg(cRegName.str());
                        if((cValueInMemory & 0x1) == 0) // strip is masked
                        {
                            fReadoutChipInterface->WriteChipReg(cChip, cRegName.str(), cValueInMemory);
                        }
                    }
                }
            }
        }
    }

    // make sure all CBC logic registers are re-configured
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            std::vector<std::pair<std::string, uint16_t>> cRegVec;

            for(auto cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() != FrontEndType::CBC3) continue;

                std::vector<std::string> cRegNames{"TestPulsePotNodeSel",
                                                   "MiscTestPulseCtrl&AnalogMux",
                                                   "HIP&TestMode",
                                                   "Pipe&StubInpSel&Ptwidth",
                                                   "CoincWind&Offset34",
                                                   "CoincWind&Offset12",
                                                   "LayerSwap&CluWidth",
                                                   "40MhzClk&Or254"};
                for(auto cRegName: cRegNames)
                {
                    auto cValueInMemory = cChip->getReg(cRegName);
                    cRegVec.push_back({cRegName, cValueInMemory});
                    // fReadoutChipInterface->WriteChipReg(cChip, cRegName, cValueInMemory);
                    // LOG(DEBUG) << BOLDMAGENTA << "Configuring CBC#" << +cChip->getId() << " register " << cRegName << " to 0x" << std::hex << +cValueInMemory << std::dec << RESET;
                }
                // masks
                for(auto cMapItem: cChip->getRegMap())
                {
                    if(cMapItem.first.find("MaskChannel") != std::string::npos)
                    {
                        auto cValueInMemory = cChip->getReg(cMapItem.first);
                        cRegVec.push_back({cMapItem.first, cValueInMemory});
                        // fReadoutChipInterface->WriteChipReg(cChip, cMapItem.first, cValueInMemory);
                        // LOG(DEBUG) << BOLDYELLOW << "Configuring CBC#" << +cChip->getId() << " register " << cMapItem.first << " to 0x" << std::hex << +cValueInMemory << std::dec << RESET;
                    }
                }
                fReadoutChipInterface->WriteChipMultReg(cChip, cRegVec);
            }
        }
    }
    LOG(INFO) << BOLDYELLOW << fMyName << ":UpdateFromRegMap END" << RESET;
}
void OTTool::PrepareForTP(BeBoard* pBoard, uint8_t pTPAmplitude, uint8_t pTPDelay)
{
    // configure trigger
    uint8_t                  cTriggerSource = 6;
    BeBoardRegMap            cRegMap        = pBoard->getBeBoardRegMap();
    std::vector<std::string> cFcmdRegs{
        "test_pulse.en_fast_reset", "test_pulse.delay_after_fast_reset", "test_pulse.delay_after_test_pulse", "test_pulse.delay_before_next_pulse", "triggers_to_accept"};
    std::vector<std::pair<std::string, uint32_t>> cRegVec;
    cRegVec.clear();
    for(size_t cIndx = 0; cIndx < cFcmdRegs.size(); cIndx++)
    {
        std::string cRegName = "fc7_daq_cnfg.fast_command_block." + cFcmdRegs[cIndx];
        cRegVec.push_back({cRegName, cRegMap[cRegName]});
    }
    cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.trigger_source", cTriggerSource});
    cRegVec.push_back({"fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity", 0});
    cRegVec.push_back({"fc7_daq_ctrl.fast_command_block.control.load_config", 0x1});
    fBeBoardInterface->WriteBoardMultReg(pBoard, cRegVec);
    fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_cnfg.tlu_block.tlu_enabled", 0);

    bool cInject = true;
    bool cWith2S = false;
    // inject in one of each CBCs - use group 0
    for(auto cOpticalGroup: *pBoard)
    {
        if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS) continue;
        cWith2S = true;
        for(auto cHybrid: *cOpticalGroup)
        {
            for(auto cChip: *cHybrid)
            {
                cInject = (cChip->getId() == 0);
                fReadoutChipInterface->enableInjection(cChip, cInject);
            }
        }
    }

    // set TP amplitude and delay
    LOG(INFO) << BOLDYELLOW << "Enabling TP with : " << +pTPAmplitude << " injected charge "
              << " delay of " << +pTPDelay << " ns " << RESET;

    // stop triggers
    fBeBoardInterface->Stop(pBoard);
    // send a ReSync
    fBeBoardInterface->ChipReSync(pBoard);

    UpdateFromRegMap(pBoard);
    if(cWith2S) { setSameDacBeBoard(pBoard, "TestPulseDelay", pTPDelay); }

    // inject PS
    if(!cWith2S)
    {
        auto                       cInjectionMaster = findValueInSettings<double>("InjectionMaster", -1);
        Ph2_HwInterface::Injection cInjection;
        // inject one cluster into each MPA-SSA pair
        cInjection.fRow    = 100;
        cInjection.fColumn = 12;
        std::vector<Injection> cInjections;
        cInjections.push_back(cInjection); // 0
        InjectPattern(pBoard, cInjections, cInjectionMaster);
    }
}
