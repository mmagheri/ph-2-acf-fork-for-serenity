/*!
 *
 * \file LinkAlignment.h
 * \brief Link alignment class, automated alignment procedure for CIC-lpGBT-BE
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef OTModuleStartUp_h__
#define OTModuleStartUp_h__

#include "Tool.h"

using namespace Ph2_HwDescription;

class OTModuleStartUp : public Tool
{
  public:
    OTModuleStartUp();
    ~OTModuleStartUp();

    void Initialise();
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;

    void setColdStart(uint8_t pColdStart) { fColdStart = pColdStart; }
    void CheckForResync(Ph2_HwDescription::BeBoard* pBoard);

  protected:
    bool StartUp();

  private:
    void InitializeOT(Ph2_HwDescription::BeBoard* pBoard);
    void Reconfigure(Ph2_HwDescription::BeBoard* pBoard);
    void ModuleStartUpPS(const Ph2_HwDescription::OpticalGroup* pOpticalGroup);
    void ModuleStartUp2S(const Ph2_HwDescription::OpticalGroup* pOpticalGroup);
    bool CicStartUp(const Ph2_HwDescription::OpticalGroup* pOpticalGroup, bool cStartUpSequence = true);

    uint8_t fColdStart{0};
};
#endif
