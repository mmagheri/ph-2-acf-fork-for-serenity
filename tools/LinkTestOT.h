/*!
 *
 * \file LinkAlignment.h
 * \brief Link alignment class, automated alignment procedure for CIC-lpGBT-BE
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef LinkTestOT_h__
#define LinkTestOT_h__

#include "LinkAlignmentOT.h"

using namespace Ph2_HwDescription;

class LinkTestOT : public OTTool
{
  public:
    LinkTestOT();
    ~LinkTestOT();

    void Initialise();
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;
    void CheckLink(const Ph2_HwDescription::OpticalGroup* pGroup, uint8_t pPattern = 0xAA);
    void SetConnectionFile(std::string pFile) { fConnectionFile = pFile; }

  protected:
  private:
    std::map<std::string, std::pair<uint8_t, uint8_t>> fELinkMap2S = {{"CIC_OUT_0_R", std::make_pair(4, 0)},
                                                                      {"CIC_OUT_1_R", std::make_pair(4, 2)},
                                                                      {"CIC_OUT_2_R", std::make_pair(5, 0)},
                                                                      {"CIC_OUT_3_R", std::make_pair(5, 2)},
                                                                      {"CIC_OUT_4_R", std::make_pair(6, 0)},
                                                                      {"CIC_OUT_6_R", std::make_pair(0, 0)},
                                                                      {"CIC_OUT_0_L", std::make_pair(0, 2)},
                                                                      {"CIC_OUT_1_L", std::make_pair(1, 0)},
                                                                      {"CIC_OUT_2_L", std::make_pair(1, 2)},
                                                                      {"CIC_OUT_3_L", std::make_pair(2, 0)},
                                                                      {"CIC_OUT_4_L", std::make_pair(2, 2)},
                                                                      {"CIC_OUT_6_L", std::make_pair(3, 2)}};
    std::map<std::string, uint8_t>                     fuDTCMap    = {{"CIC_OUT_0", 1}, {"CIC_OUT_1", 2}, {"CIC_OUT_2", 3}, {"CIC_OUT_3", 4}, {"CIC_OUT_4", 5}, {"CIC_OUT_5", 6}, {"CIC_OUT_6", 0}};
    std::string                                        fConnectionFile;
};
#endif
