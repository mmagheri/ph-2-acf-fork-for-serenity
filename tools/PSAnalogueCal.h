/*!
 *
 * \file LinkAlignment.h
 * \brief Link alignment class, automated alignment procedure for CIC-lpGBT-BE
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef PSAnalogueCal_h__
#define PSAnalogueCal_h__

#include "../Utils/ContainerRecycleBin.h"
#include "OTTool.h"

/*!
 * \class LatencyScan
 * \brief Class to perform latency and threshold scans
 */
class Occupancy;

namespace Ph2_HwDescription
{
class Chip;
}

using MeasurementMap = std::unordered_map<std::string, std::pair<float, float>>;
class PSAnalogueCal : public OTTool
{
  public:
    PSAnalogueCal();
    ~PSAnalogueCal();

    void Initialise();
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;
    void writeObjects();

  protected:
    void initializeRecycleBin() { fRecycleBin.setDetectorContainer(fDetectorContainer); }
    void cleanContainerMap()
    {
        // for(auto container: fSCurveOccupancyMap) fRecycleBin.free(container.second);
    }

  private:
    void                  CalibrateChipBias(std::string pBiasList);
    size_t                fNMeasurements{5};
    uint32_t              fWait_us{100};
    DetectorDataContainer fInternalADCMeasurements;
    DetectorDataContainer fExternalADCMeasurements;
    DetectorDataContainer fInternalTrimCalValues;
    DetectorDataContainer fExternalTrimCalValues;
    DetectorDataContainer fCalConstants;
    DetectorDataContainer fInternalCalConstants;
    // DetectorDataContainer fBiasTrimDACs;
    void MeasureValue(Ph2_HwDescription::ReadoutChip* pChip, std::string cName);
    void MeasureValues(std::string cName);
    void TrimBias(Ph2_HwDescription::ReadoutChip* pChip, std::string cName);
    void CalibrateInternalADC(Ph2_HwDescription::ReadoutChip* pChip);
    void AdjustVref(Ph2_HwDescription::ReadoutChip* pChip);
    void CalibrateADC(Ph2_HwDescription::ReadoutChip* pChip);

  private:
    // Containers
    ContainerRecycleBin<Occupancy> fRecycleBin;
    void                           ExternalReferenceMeasurement(Ph2_HwDescription::ReadoutChip* pChip, std::string pName);
    void                           InitExternalMeasurement();
    std::vector<std::string>       fMonitorables = {"GND", "VoltageRef", "Bandgap", "AVDD", "PVDD", "DVDD"};
    std::string                    fReference{"GND"};

#ifdef __USE_ROOT__
    // DQMHistogramBeamTestCheck fDQMHistogrammer;
#endif
};
#endif
