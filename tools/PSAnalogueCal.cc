#include "PSAnalogueCal.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

PSAnalogueCal::PSAnalogueCal() : OTTool() {}

PSAnalogueCal::~PSAnalogueCal() {}

// Initialization function
void PSAnalogueCal::Initialise()
{
    Prepare();
    SetName("PSAnalogueCal");
    LOG(INFO) << BOLDYELLOW << "Number of events is " << fNevents << RESET;
    // list of chip registers that can be modified by this tool
    SetChipRegstoPerserve(FrontEndType::SSA2, {"Bias_D5BFEED", "Bias_D5PREAMP", "Bias_D5ALLV", "Bias_D5ALLI", "Bias_D5DAC8", "Bias_D5TDR"});
    initializeRecycleBin();

    // initialise data container
    ContainerFactory::copyAndInitChip<MeasurementMap>(*fDetectorContainer, fInternalADCMeasurements);
    ContainerFactory::copyAndInitChip<MeasurementMap>(*fDetectorContainer, fExternalADCMeasurements);
    ContainerFactory::copyAndInitChip<MeasurementMap>(*fDetectorContainer, fInternalTrimCalValues);
    ContainerFactory::copyAndInitChip<MeasurementMap>(*fDetectorContainer, fExternalTrimCalValues);
    ContainerFactory::copyAndInitChip<MeasurementMap>(*fDetectorContainer, fCalConstants);
    ContainerFactory::copyAndInitChip<std::pair<float, float>>(*fDetectorContainer, fInternalCalConstants);

// measure ground
#if defined(__TCUSB__)
    TC_PSFE cTC_PSFE;
    float   cMeasurement = 0;
    cTC_PSFE.adc_get(TC_PSFE::measurement::GROUND, cMeasurement);
    LOG(INFO) << BOLDYELLOW << "TC GROUND " << cMeasurement << RESET;
#endif
}
void PSAnalogueCal::ExternalReferenceMeasurement(ReadoutChip* pChip, std::string pName)
{
    // // get indices of hybrid + optical group
    // uint8_t cBoardIndex = 0;
    // uint8_t cOpticalGroupIndx = 0;
    // uint8_t cHybridIndx       = 0;
    // uint8_t cBoardId          = pChip->getBeBoardId();
    // for(const auto cBoard: *fDetectorContainer)
    // {
    //     if(cBoard->getId() != cBoardId) continue;
    //     cBoardIndex = cBoard->getIndex();
    //     for(const auto cOpticalGroup: *cBoard)
    //     {
    //         if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
    //         cOpticalGroupIndx = cOpticalGroup->getIndex();
    //         for(const auto cHybrid: *cOpticalGroup)
    //         {
    //             if(pChip->getHybridId() != cHybrid->getId()) continue;
    //             cHybridIndx = cHybrid->getIndex();
    //         } // hybrud loop
    //     }     // OG loop
    // }// board loop
    // if( fDetectorContainer->at(cBoardIndex)->at(cOpticalGroupIndx)->getFrontEndType() != FrontEndType::HYBRIDPS ) return;

    // #if defined(__TCUSB__)
    //     TC_PSFE cTC_PSFE;
    //     float cMeasurement=0;
    //     cTC_PSFE.adc_get(TC_PSFE::measurement::AMUX, cMeasurement);
    //     // first thing to measure
    //     // parameters on the test card
    //     // temperature, hybrid voltage (analogue)

    //     // zero data container
    //     auto& cExternal = fExternalADCMeasurements.at(cBoardIndex);
    //     auto& cExternalOG = cExternal->at(cOpticalGroupIndx);
    //     auto& cExternalH = cExternalOG->at(cHybridIndx);
    //     auto& cExternalC = cExternalH->at(pChip->getIndex());
    //     auto& cExternalM = cExternalC->getSummary<MeasurementMap>();
    //     cExternalM[pName].first = 0; cExternalM[pName].second = 0;
    //     // set all chips to high impedance and enable test pad to route out analogue value
    //     for(const auto cBoard: *fDetectorContainer)
    //     {
    //         for(auto cOpticalReadout: *cBoard)
    //         {
    //             for(auto cHybrid: *cOpticalReadout)
    //             {
    //                 for(auto cReadoutChip: *cHybrid) {
    //                     if( cReadoutChip->getFrontEndType() == FrontEndType::SSA2){
    //                         auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
    //                         auto  cMap = cInterface->GetADCMap();
    //                         cInterface->ReadADC(cReadoutChip, cMap["HighImpedance"]);
    //                         cInterface->ConfigureTestPad(cReadoutChip, 0);
    //                     }
    //                 }//chips
    //             }//hyrbds
    //         }//OGs
    //     }// set all chips to high impedance and disable test pad to route out analogue value

    //     // measure values
    //     std::vector<float> cMeasExt;
    //     for(size_t cIndx = 0; cIndx < fNMeasurements; cIndx++)
    //     {
    //         if( pChip->getFrontEndType() == FrontEndType::SSA2 )
    //         {
    //             auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
    //             auto  cMap = cInterface->GetADCMap();
    //             if( cIndx == 0 )
    //             {
    //                 cInterface->ConfigureTestPad(pChip, 1);
    //                 std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    //             }
    //         }
    //         cTC_PSFE.adc_get(TC_PSFE::measurement::AMUX, cMeasurement);
    //         cMeasExt.push_back(cMeasurement);
    //     }
    //     if( pChip->getFrontEndType() == FrontEndType::SSA2 )
    //     {
    //         auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
    //         auto  cMap = cInterface->GetADCMap();
    //         cInterface->ReadADC(pChip, cMap["HighImpedance"]);
    //         cInterface->ConfigureTestPad(pChip, 0);
    //         std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    //     }//disable test pad + set adc to highZ
    //     cExternalM[pName] = calculateStats(cMeasExt);
    // #endif
}
void PSAnalogueCal::CalibrateADC(ReadoutChip* pChip)
{
    // get indices of hybrid + optical group
    uint8_t cBoardIndex       = 0;
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardId          = pChip->getBeBoardId();
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        cBoardIndex = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    if(fDetectorContainer->at(cBoardIndex)->at(cOpticalGroupIndx)->getFrontEndType() != FrontEndType::HYBRIDPS) return;
    auto& cExternalM = fExternalADCMeasurements.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    auto& cInternalM = fInternalADCMeasurements.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    auto& cCalConst  = fCalConstants.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();

    // use gnd + Vref measurement to update calibration of ADC
    // MeasureValue(pChip,"GND");
    MeasureValue(pChip, "VoltageRef");
    cCalConst["ADC"].first  = cExternalM["GND"].first - cExternalM[fReference].first;                  // offset
    cCalConst["ADC"].second = (cExternalM["VoltageRef"].first - cExternalM[fReference].first) / 4096.; // slope
    // cross-check using bandgap
    MeasureValue(pChip, "Bandgap");
    LOG(INFO) << BOLDYELLOW << "For ROC#" << +pChip->getId() << " GND as measured by TC ADC is " << cExternalM[fReference].first << " External Vref as measured by TC ADC is "
              << cExternalM["VoltageRef"].first - cExternalM[fReference].first << " mV"
              << " ADC offset is " << cCalConst["ADC"].first << " mV"
              << " ADC LSB is " << cCalConst["ADC"].second << " mV"
              << " external measurement of bandgap is " << cExternalM["Bandgap"].first - cExternalM[fReference].first << " mV"
              << " internal measurement of bandgap is " << cInternalM["Bandgap"].first << " ADC units "
              << " internal measurement of ADC offset is " << cInternalM["GND"].first << " ADC units "
              << " after conversion " << (cInternalM["Bandgap"].first - cInternalM["GND"].first) * cCalConst["ADC"].second + cCalConst["ADC"].first << " mV" << RESET;
}
void PSAnalogueCal::AdjustVref(ReadoutChip* pChip)
{
    // get indices of hybrid + optical group
    uint8_t cBoardIndex       = 0;
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardId          = pChip->getBeBoardId();
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        cBoardIndex = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop
    if(fDetectorContainer->at(cBoardIndex)->at(cOpticalGroupIndx)->getFrontEndType() != FrontEndType::HYBRIDPS) return;
    if(pChip->getFrontEndType() == FrontEndType::SSA) return;
    auto  cInterface    = static_cast<SSA2Interface*>(fReadoutChipInterface);
    auto& cExternalM    = fExternalADCMeasurements.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    auto& cExternalTrim = fExternalTrimCalValues.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    // measure the ground for this location on the hybrid
    std::map<std::string, float> cTargetBiasMap = cInterface->GetTargetBiasMap();
    auto                         cTrimDAC       = cInterface->GetBiasTrimMap()["VoltageRef"];
    std::vector<float>           cTrimDacVals   = {0x0, 0xF, 0x1F};
    std::vector<float>           cMeasuredBiasesExt;
    LOG(INFO) << BOLDYELLOW << "SSA#" << +pChip->getId() << "  tune Vref using external ADC on test card " << RESET;
    for(auto cTrimDACVal: cTrimDacVals)
    {
        fReadoutChipInterface->WriteChipReg(pChip, cTrimDAC, cTrimDACVal);
        MeasureValue(pChip, "VoltageRef");
        cMeasuredBiasesExt.push_back(cExternalM["VoltageRef"].first - cExternalM[fReference].first);
    }
    cExternalTrim["VoltageRef"].first  = cMeasuredBiasesExt[0];
    cExternalTrim["VoltageRef"].second = getLeastSquareSlope<float>(cTrimDacVals, cMeasuredBiasesExt);
    auto    cValToSetExt               = ((cTargetBiasMap["VoltageRef"] - cExternalTrim["VoltageRef"].first) / cExternalTrim["VoltageRef"].second);
    uint8_t cTrimVal                   = std::floor(cValToSetExt + 0.5);
    // scan around trim value found
    std::vector<int> cOffsets = {-2, -1, 0, 1, 2};
    std::vector<int> cTrimValues;
    for(auto cOffset: cOffsets) cTrimValues.push_back(cTrimVal + cOffset);
    // find differences
    std::vector<float> cDifferences;
    for(auto cTrimValue: cTrimValues)
    {
        fReadoutChipInterface->WriteChipReg(pChip, cTrimDAC, cTrimValue);
        MeasureValue(pChip, "VoltageRef");
        float cDiffFound = std::fabs(cExternalM["VoltageRef"].first - cExternalM[fReference].first - cTargetBiasMap["VoltageRef"]);
        cDifferences.push_back(cDiffFound);
    }
    auto cDistance = std::distance(cDifferences.begin(), std::min_element(cDifferences.begin(), cDifferences.end()));
    cTrimVal       = cTrimVal + *(cOffsets.begin() + cDistance);
    fReadoutChipInterface->WriteChipReg(pChip, cTrimDAC, cTrimVal);
    MeasureValue(pChip, "VoltageRef");
    float cDiffFound = std::fabs(cExternalM["VoltageRef"].first - cExternalM[fReference].first - cTargetBiasMap["VoltageRef"]);
    LOG(INFO) << BOLDYELLOW << "\t..Best VrefTune found to be  " << +cTrimVal << " to reach a target of " << cTargetBiasMap["VoltageRef"] << "\t..External measurement of Vref is "
              << cExternalM["VoltageRef"].first - cExternalM[fReference].first << " Difference of " << cDiffFound << " mV " << RESET;
}
void PSAnalogueCal::InitExternalMeasurement()
{
#if defined(__TCUSB__)
    for(const auto cBoard: *fDetectorContainer)
    {
        for(const auto cOpticalGroup: *cBoard)
        {
            for(const auto cHybrid: *cOpticalGroup)
            {
                for(const auto cChip: *cHybrid)
                {
                    if(cChip->getFrontEndType() == FrontEndType::SSA2)
                    {
                        auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
                        auto cMap       = cInterface->GetADCMap();
                        cInterface->ReadADC(cChip, cMap["HighImpedance"]);
                        cInterface->ConfigureTestPad(cChip, 0);
                    }
                }
            } // hybrud loop
        }     // OG loop
    }         // board loop
    LOG(INFO) << BOLDYELLOW << "PSAnalogueCal::InitExternalMeasurement Waiting for ADC readings to stabilize ..." << RESET;
    auto startTimeUTC_us   = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    auto currentTimeUTC_us = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    auto cElapsedTime_s    = (float)(currentTimeUTC_us - startTimeUTC_us) * 1e-6;
    do {
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        currentTimeUTC_us    = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        cElapsedTime_s       = (float)(currentTimeUTC_us - startTimeUTC_us) * 1e-6;
        float   cMeasurement = 0;
        TC_PSFE cTC_PSFE;
        cTC_PSFE.adc_get(TC_PSFE::measurement::AMUX, cMeasurement);
    } while(cElapsedTime_s < 1.); // wait for 500 ms
#else
    LOG(WARNING) << BOLDRED << "Attempting to InitExternalMeasurement when no TC USB library has been compiled.." << RESET;
#endif
}
void PSAnalogueCal::MeasureValue(ReadoutChip* pChip, std::string pName)
{
    if(pChip->getFrontEndType() != FrontEndType::SSA2) return;

    // get indices of hybrid + optical group
    uint8_t cBoardIndex       = 0;
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardId          = pChip->getBeBoardId();
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        cBoardIndex = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop

    // zero data container
    auto& cInternal          = fInternalADCMeasurements.at(cBoardIndex);
    auto& cExternal          = fExternalADCMeasurements.at(cBoardIndex);
    auto& cInternalOG        = cInternal->at(cOpticalGroupIndx);
    auto& cExternalOG        = cExternal->at(cOpticalGroupIndx);
    auto& cInternalH         = cInternalOG->at(cHybridIndx);
    auto& cExternalH         = cExternalOG->at(cHybridIndx);
    auto& cInternalC         = cInternalH->at(pChip->getIndex());
    auto& cInternalM         = cInternalC->getSummary<MeasurementMap>();
    auto& cExternalC         = cExternalH->at(pChip->getIndex());
    auto& cExternalM         = cExternalC->getSummary<MeasurementMap>();
    cExternalM[pName].first  = 0;
    cExternalM[pName].second = 0;
    cInternalM[pName].first  = 0;
    cInternalM[pName].second = 0;
    // set all chips to high impedance and disable test pad to route out analogue value
    for(const auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalReadout: *cBoard)
        {
            for(auto cHybrid: *cOpticalReadout)
            {
                for(auto cReadoutChip: *cHybrid)
                {
                    if(cReadoutChip->getFrontEndType() == FrontEndType::SSA2)
                    {
                        auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
                        auto cMap       = cInterface->GetADCMap();
                        cInterface->ReadADC(cReadoutChip, cMap["HighImpedance"]);
                        cInterface->ConfigureTestPad(cReadoutChip, 0);
                    }
                } // chips
            }     // hyrbds
        }         // OGs
    }             // set all chips to high impedance and disable test pad to route out analogue value
    // enable TestPad output for this chip
    if(pChip->getFrontEndType() == FrontEndType::SSA2 && !pName.empty())
    {
        auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
        cInterface->ConfigureTestPad(pChip, 1);
    }
    // measure values
    if(!pName.empty())
        LOG(INFO) << BOLDYELLOW << "External measurement of " << pName << " on SSA#" << +pChip->getId() << RESET;
    else
    {
        pName = "NC";
        LOG(INFO) << BOLDYELLOW << "External measurement when TestPad NC on SSA#" << +pChip->getId() << RESET;
    }
    std::vector<float> cMeasExt, cMeasInt;
    for(size_t cIndx = 0; cIndx < fNMeasurements; cIndx++)
    {
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
        if(pChip->getFrontEndType() == FrontEndType::SSA2)
        {
            auto  cInterface   = static_cast<SSA2Interface*>(fReadoutChipInterface);
            auto  cMap         = cInterface->GetADCMap();
            float cMeasurement = static_cast<SSA2Interface*>(fReadoutChipInterface)->ReadADC(pChip, cMap[pName]);
            cMeasInt.push_back(cMeasurement);
        }
#if defined(__TCUSB__)
        float   cMeasurement = 0;
        TC_PSFE cTC_PSFE;
        cTC_PSFE.adc_get(TC_PSFE::measurement::AMUX, cMeasurement);
        cMeasExt.push_back(cMeasurement);
#endif
    }

    // disable TestPad output for this chip
    if(pChip->getFrontEndType() == FrontEndType::SSA2)
    {
        auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
        auto cMap       = cInterface->GetADCMap();
        cInterface->ReadADC(pChip, cMap["HighImpedance"]);
        cInterface->ConfigureTestPad(pChip, 0);
        std::this_thread::sleep_for(std::chrono::microseconds(fWait_us));
    } // disable test pad + set adc to highZ
    cExternalM[pName] = calculateStats(cMeasExt);
    cInternalM[pName] = calculateStats(cMeasInt);
}
void PSAnalogueCal::MeasureValues(std::string pName)
{
    LOG(INFO) << BOLDYELLOW << "PSAnalogueCal measuring " << pName << " analogue bias" << RESET;
    for(const auto cBoard: *fDetectorContainer)
    {
        for(const auto cOpticalGroup: *cBoard)
        {
            for(const auto cHybrid: *cOpticalGroup)
            {
                for(const auto cChip: *cHybrid) MeasureValue(cChip, pName);
            } // hybrud loop
        }     // OG loop
    }         // board loop
}
void PSAnalogueCal::TrimBias(ReadoutChip* pChip, std::string pName)
{
    // get indices of hybrid + optical group
    uint8_t cBoardIndex       = 0;
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardId          = pChip->getBeBoardId();
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        cBoardIndex = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop

    // zero data container
    auto& cExternalM    = fExternalADCMeasurements.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    auto& cInternalM    = fInternalADCMeasurements.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    auto& cInternalTrim = fInternalTrimCalValues.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    auto& cExternalTrim = fExternalTrimCalValues.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    // auto& cInternalCal = fInternalCalConstants.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<std::pair<float,float>>();

    std::map<std::string, std::string> cBiasTrimMap;
    std::map<std::string, float>       cTargetBiasMap;
    if(pChip->getFrontEndType() == FrontEndType::SSA2)
    {
        auto cInterface = static_cast<SSA2Interface*>(fReadoutChipInterface);
        cBiasTrimMap    = cInterface->GetBiasTrimMap();
        cTargetBiasMap  = cInterface->GetTargetBiasMap();
    }
    auto               cTrimDAC     = cBiasTrimMap[pName];
    std::vector<float> cTrimDacVals = {0x0, 0x1F};
    std::vector<float> cMeasuredBiasesExt;
    std::vector<float> cMeasuredBiasesInt;
    LOG(INFO) << BOLDYELLOW << "Trim DAC " << cTrimDAC << " target bias is " << cTargetBiasMap[pName] << RESET;
    auto cRegValue = fReadoutChipInterface->ReadChipReg(pChip, cTrimDAC);
    // MeasureValue(pChip,pName);
    // LOG(DEBUG) << BOLDYELLOW << "Pre-Trim " << cTrimDAC << " was set to " << cRegValue << "\t... measured value is " <<    cExternalM[pName].first- cExternalM["GND"].first  << RESET;
    for(auto cTrimDACVal: cTrimDacVals)
    {
        fReadoutChipInterface->WriteChipReg(pChip, cTrimDAC, cTrimDACVal);
        cRegValue = fReadoutChipInterface->ReadChipReg(pChip, cTrimDAC);
        MeasureValue(pChip, pName);
        auto cGndCorr = cExternalM[pName].first - cExternalM["GND"].first;
        cMeasuredBiasesExt.push_back(cGndCorr);
        LOG(INFO) << BOLDYELLOW << "\t\t..trim DAC set to " << +cRegValue << " Target is " << cTargetBiasMap[pName] << " Ext " << cExternalM[pName].first << " GND is " << cExternalM["GND"].first
                  << " Corrected value is " << cGndCorr << RESET;
        cGndCorr = (cInternalM[pName].first - cInternalM["GND"].first);
        LOG(DEBUG) << BOLDYELLOW << "\t\t..trim DAC set to " << +cRegValue << " Target is " << cTargetBiasMap[pName] << " Int " << cInternalM[pName].first << " GND is " << cInternalM["GND"].first
                   << " Corrected value is " << cGndCorr << RESET;
        cMeasuredBiasesInt.push_back(cGndCorr);
    }
    // use internal values for trimming..can always do that
    cInternalTrim[pName].first  = cMeasuredBiasesInt[0];
    cInternalTrim[pName].second = getLeastSquareSlope<float>(cTrimDacVals, cMeasuredBiasesInt);
    // use external values for trimming - only works with a PS-FEH TC
    cExternalTrim[pName].first  = cMeasuredBiasesExt[0];
    cExternalTrim[pName].second = getLeastSquareSlope<float>(cTrimDacVals, cMeasuredBiasesExt);
    auto cValToSetExt           = ((cTargetBiasMap[pName] - cExternalTrim[pName].first) / cExternalTrim[pName].second);
    auto cValToSetInt           = ((cTargetBiasMap[pName] - cInternalTrim[pName].first) / cInternalTrim[pName].second);
    auto cValToSetR             = std::floor(cValToSetExt + 0.5);
    LOG(DEBUG) << BOLDYELLOW << "\t.. Using INTERNAL ADC - post calibration - offset of " << cInternalTrim[pName].first << " slope of " << cInternalTrim[pName].second << "[ " << cValToSetInt << " ]"
               << RESET;
    LOG(INFO) << BOLDYELLOW << "\t.. Using EXTERNAL ADC offset of " << cExternalTrim[pName].first << " slope of " << cExternalTrim[pName].second << RESET;
    bool cVerify = true;
    int  cOffset = 0;
    if(cVerify)
    {
        std::vector<int>   cOffsets{-2, -1, 1, 2, 0};
        std::vector<float> cShift;
        LOG(INFO) << BOLDYELLOW << "Post trimming check " << cTrimDAC << " - target is " << cTargetBiasMap[pName] << RESET;
        for(auto cOffset: cOffsets)
        {
            fReadoutChipInterface->WriteChipReg(pChip, cTrimDAC, cValToSetR + cOffset);
            cRegValue = fReadoutChipInterface->ReadChipReg(pChip, cTrimDAC);
            MeasureValue(pChip, pName);
            float cDifference = std::fabs(cExternalM[pName].first - cExternalM["GND"].first - cTargetBiasMap[pName]);
            cShift.push_back(cDifference);
            LOG(INFO) << BOLDYELLOW << "\t... Offset of " << cOffset << " applied to value found "
                      << "\t... measured value is " << cExternalM[pName].first - cExternalM["GND"].first << " ± "
                      << std::sqrt(cExternalM["GND"].second * cExternalM["GND"].second + cExternalM[pName].second * cExternalM[pName].second) << " distance to target is " << cDifference << RESET;
        }
        auto cIterator = std::min_element(cShift.begin(), cShift.end());
        cOffset        = cOffsets[std::distance(cShift.begin(), cIterator)];
    }
    fReadoutChipInterface->WriteChipReg(pChip, cTrimDAC, cValToSetR + cOffset);
    cRegValue = fReadoutChipInterface->ReadChipReg(pChip, cTrimDAC);
    // MeasureValue(pChip,pName);
    // float cDifference = std::fabs(cExternalM[pName].first - cExternalM["GND"].first - cTargetBiasMap[pName]);
    LOG(INFO) << BOLDYELLOW << "Post trimming " << cTrimDAC << " is set to "
              << cRegValue
              // << "\t... measured value is " <<    cExternalM[pName].first - cExternalM["GND"].first
              // << " ± " << std::sqrt(cExternalM["GND"].second*cExternalM["GND"].second + cExternalM[pName].second*cExternalM[pName].second )
              // << " distance to target is " << cDifference
              << RESET;
}
void PSAnalogueCal::CalibrateChipBias(std::string pBiasList)
{
    std::vector<std::string> cBiasNames;
    cBiasNames.clear();
    tokenize(pBiasList, cBiasNames, ",");
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalReadout: *cBoard)
        {
            for(auto cHybrid: *cOpticalReadout)
            {
                for(auto cReadoutChip: *cHybrid)
                {
                    LOG(INFO) << BOLDMAGENTA << "----------------------------------------------------- Calibrating bias DACs on ROC#" << +cReadoutChip->getId()
                              << "-----------------------------------------------------" << RESET;
                    for(auto cBias: cBiasNames) TrimBias(cReadoutChip, cBias);
                } // chips
            }     // hybrids
        }         // OGs
    }             // boards
}
void PSAnalogueCal::CalibrateInternalADC(ReadoutChip* pChip)
{
    // get indices of hybrid + optical group
    uint8_t cBoardIndex       = 0;
    uint8_t cOpticalGroupIndx = 0;
    uint8_t cHybridIndx       = 0;
    uint8_t cBoardId          = pChip->getBeBoardId();
    for(const auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getId() != cBoardId) continue;
        cBoardIndex = cBoard->getIndex();
        for(const auto cOpticalGroup: *cBoard)
        {
            if(pChip->getOpticalGroupId() != cOpticalGroup->getId()) continue;
            cOpticalGroupIndx = cOpticalGroup->getIndex();
            for(const auto cHybrid: *cOpticalGroup)
            {
                if(pChip->getHybridId() != cHybrid->getId()) continue;
                cHybridIndx = cHybrid->getIndex();
            } // hybrud loop
        }     // OG loop
    }         // board loop

    MeasureValue(pChip, "VoltageRef"); // 0.85 V
    MeasureValue(pChip, "Bandgap");    // 0.275 V
    auto& cInternalCal = fInternalCalConstants.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<std::pair<float, float>>();
    auto& cInternalM   = fInternalADCMeasurements.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    // auto& cExternalM = fExternalADCMeasurements.at(cBoardIndex)->at(cOpticalGroupIndx)->at(cHybridIndx)->at(pChip->getIndex())->getSummary<MeasurementMap>();
    // LOG (INFO) << BOLDYELLOW << "VoltageRef set to " << cInternalM["VoltageRef"].first << "\t.." << cExternalM["VoltageRef"].first << RESET;
    // LOG (INFO) << BOLDYELLOW << "Bandgap set to " << cInternalM["Bandgap"].first << "\t.." << cExternalM["Bandgap"].first << RESET;

    std::map<std::string, float> cExpectedMap;
    cExpectedMap["Bandgap"]    = (275. / 850.) * 0xFFF;
    cExpectedMap["VoltageRef"] = (850. / 850.) * 0xFFF;
    cInternalCal.second        = (cExpectedMap["VoltageRef"] - cExpectedMap["Bandgap"]) / (cInternalM["VoltageRef"].first - cInternalM["Bandgap"].first);
    cInternalCal.first         = cInternalM["Bandgap"].first * cInternalCal.second - cExpectedMap["Bandgap"];
    float cInternalBG          = cInternalM["Bandgap"].first * cInternalCal.second - cInternalCal.first;
    float cInternalVref        = cInternalM["VoltageRef"].first * cInternalCal.second - cInternalCal.first;
    LOG(INFO) << BOLDYELLOW << "ADC calibration : gain " << cInternalCal.second << " offset " << cInternalCal.first << RESET;
    LOG(INFO) << BOLDYELLOW << "Internal BG post cal " << cInternalBG << "\t Internal Vref post cal " << cInternalVref << RESET;
}
// calibrate external ADC on test card
// measure ground - offset on the ADC is the measurement with GND
// assume a gain of 1.0
//
// State machine control functions
void PSAnalogueCal::Running()
{
    Initialise();
    InitExternalMeasurement();
    // measure value on test pad when internal mux set to HighZ for all chips
    MeasureValues("HighImpedance");
    MeasureValues("GND");
    MeasureValues("");
    // adjust Vref using external ADC on test card
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalReadout: *cBoard)
        {
            for(auto cHybrid: *cOpticalReadout)
            {
                for(auto cReadoutChip: *cHybrid)
                {
                    LOG(INFO) << BOLDMAGENTA << "----------------------------------------------------- Adjusting Vref on ROC#" << +cReadoutChip->getId()
                              << "-----------------------------------------------------" << RESET;
                    AdjustVref(cReadoutChip);
                    LOG(INFO) << BOLDMAGENTA << "----------------------------------------------------- Calibrating ADC on ROC#" << +cReadoutChip->getId()
                              << "-----------------------------------------------------" << RESET;
                    CalibrateADC(cReadoutChip);
                } // chips
            }     // hybrids
        }         // OGs
    }             // boards

    std::string cBiasList = "BoosterFeedback,PreampBias,VoltageBias,CurrentBias,DAC,TrimDACRange";
    CalibrateChipBias(cBiasList);

    // offsets are the ADC reading with the GND connected
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     // auto& cInternalCal = fInternalCalConstants.at(cBoard->getIndex());
    //     // auto& cInternalMThisBrd = fInternalADCMeasurements.at(cBoard->getIndex());
    //     for(auto cOpticalReadout: *cBoard)
    //     {
    //         // auto& cInternalCalThisOG = cInternalCal->at(cOpticalReadout->getIndex());
    //         // auto& cInternalMThisOG = cInternalMThisBrd->at(cOpticalReadout->getIndex());
    //         for(auto cHybrid: *cOpticalReadout)
    //         {
    //             // auto& cInternalCalThisHybrd = cInternalCalThisOG->at(cHybrid->getIndex());
    //             // auto& cInternalMThisHybrd = cInternalMThisOG->at(cHybrid->getIndex());
    //             for(auto cReadoutChip : *cHybrid )
    //             {
    //                 // auto& cInternalCalThisChip = cInternalCalThisHybrd->at(cReadoutChip->getIndex());
    //                 // auto& cCalADC = cInternalCalThisChip->getSummary<std::pair<float,float>>();
    //                 // auto& cInternalMThisChip = cInternalMThisHybrd->at(cReadoutChip->getIndex());
    //                 // auto& cInternalM = cInternalMThisChip->getSummary<MeasurementMap>();
    //                 // cCalADC.second = cInternalM["GND"].first;
    //                 // cCalADC.first  = 1.0; //assume the gain is exactly 1
    //                 // LOG (INFO) << BOLDYELLOW << "SSA#" << +cReadoutChip->getId()
    //                 //     << " ADC offset " << cCalADC.second
    //                 //     << RESET;
    //                 CalibrateVref(cReadoutChip);
    //             }//chips
    //         }//hybrids
    //     }//OGs
    // }//boards

    // std::string cBiasList="BoosterFeedback,PreampBias,VoltageBias,CurrentBias,DAC,TrimDACRange";
    // CalibrateChipBias(cBiasList);

    // for(auto cBoard: *fDetectorContainer)
    // {
    //     for(auto cOpticalReadout: *cBoard)
    //     {
    //         for(auto cHybrid: *cOpticalReadout)
    //         {
    //             for(auto cReadoutChip : *cHybrid )
    //             {
    //                 LOG(INFO) << BOLDMAGENTA << "----------------------------------------------------- Calibrating ADC on ROC#" << +cReadoutChip->getId()
    //                     << "-----------------------------------------------------" << RESET;
    //                 CalibrateInternalADC(cReadoutChip);
    //             }//chips
    //         }//hybrids
    //     }//OGs
    // }//boards
    // std::vector<float> cInjVals{0,0xFF};
    // float cCalDacRange = 193./0xFF;
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     auto& cInternalMThisBrd = fInternalADCMeasurements.at(cBoard->getIndex());
    //     auto& cExternalMThisBrd = fExternalADCMeasurements.at(cBoard->getIndex());
    //     for(auto cOpticalReadout: *cBoard)
    //     {
    //         auto& cExternalMThisOG = cExternalMThisBrd->at(cOpticalReadout->getIndex());
    //         auto& cInternalMThisOG = cInternalMThisBrd->at(cOpticalReadout->getIndex());
    //         for(auto cHybrid: *cOpticalReadout)
    //         {
    //             auto& cExternalMThisHybrd = cExternalMThisOG->at(cHybrid->getIndex());
    //             auto& cInternalMThisHybrd = cInternalMThisOG->at(cHybrid->getIndex());
    //             for(auto cReadoutChip : *cHybrid )
    //             {
    //                 std::vector<float> cMesurements;
    //                 LOG (INFO) << BOLDYELLOW << "SSA#" << +cReadoutChip->getId() << RESET;
    //                 for( auto cInj : cInjVals)
    //                 {
    //                     fReadoutChipInterface->WriteChipReg(cReadoutChip,"InjectedCharge", cInj);
    //                     MeasureValue(cReadoutChip,"CalLevel");
    //                     auto& cExternalMThisChip = cExternalMThisHybrd->at(cReadoutChip->getIndex());
    //                     auto& cExternalM = cExternalMThisChip->getSummary<MeasurementMap>();
    //                     auto& cInternalMThisChip = cInternalMThisHybrd->at(cReadoutChip->getIndex());
    //                     auto& cInternalM = cInternalMThisChip->getSummary<MeasurementMap>();
    //                     cMesurements.push_back( cInternalM["CalLevel"].first );
    //                     LOG (INFO) << BOLDYELLOW << "\t.. CalLevel DAC = " << cInj
    //                         << " Internal ADC measurement " << cInternalM["CalLevel"].first
    //                         << " External ADC measurement " << cExternalM["CalLevel"].first
    //                         << " External Gnd measurement  " << cExternalM["GND"].first
    //                         << RESET;
    //                 }
    //                 float cADCgain = (1/cCalDacRange) * (cMesurements[0] - cMesurements[1]) * 1/(cInjVals[0] - cInjVals[1] );
    //                 LOG (INFO) << BOLDYELLOW << "ADC gain " << cADCgain << RESET;
    //             }//chips
    //         }//hybrids
    //     }//OGs
    // }//boards

    // std::string cMeasurementList="Bandgap,GND";
    // std::vector<std::string> cAdcInputs;
    // cAdcInputs.clear();
    // tokenize(cMeasurementList, cAdcInputs, ",");
    // for(auto cAdcInput: cAdcInputs){
    //     MeasureValues(cAdcInput);
    // }
    // for(auto cBoard: *fDetectorContainer)
    // {
    //     auto& cExternalMThisBrd = fExternalADCMeasurements.at(cBoard->getIndex());
    //     auto& cInternalMThisBrd = fInternalADCMeasurements.at(cBoard->getIndex());
    //     for(auto cOpticalReadout: *cBoard)
    //     {
    //         auto& cExternalMThisOG = cExternalMThisBrd->at(cOpticalReadout->getIndex());
    //         auto& cInternalMThisOG = cInternalMThisBrd->at(cOpticalReadout->getIndex());
    //         for(auto cHybrid: *cOpticalReadout)
    //         {
    //             auto& cExternalMThisHybrd = cExternalMThisOG->at(cHybrid->getIndex());
    //             auto& cInternalMThisHybrd = cInternalMThisOG->at(cHybrid->getIndex());
    //             for(auto cReadoutChip : *cHybrid )
    //             {
    //                 auto& cExternalMThisChip = cExternalMThisHybrd->at(cReadoutChip->getIndex());
    //                 auto& cInternalMThisChip = cInternalMThisHybrd->at(cReadoutChip->getIndex());
    //                 auto& cExternalM = cExternalMThisChip->getSummary<MeasurementMap>();
    //                 auto& cInternalM = cInternalMThisChip->getSummary<MeasurementMap>();
    //                 for(auto cAdcInput: cAdcInputs){
    //                     LOG (INFO) << BOLDYELLOW << "SSA#" << +cReadoutChip->getId() << "\t.." << cAdcInput
    //                         << " Ext " << cExternalM[cAdcInput].first
    //                         << " Ext Gnd " << cExternalM["GND"].first
    //                         << " Int " << cInternalM[cAdcInput].first
    //                         << RESET;
    //                 }//ADC inputs
    //             }//chips
    //         }//hybrids
    //     }//OGs
    // }//boards
}
void PSAnalogueCal::Stop() {}

void PSAnalogueCal::Pause() {}

void PSAnalogueCal::Resume() {}

void PSAnalogueCal::writeObjects()
{
#ifdef __USE_ROOT__
    this->SaveResults();
    // fDQMHistogrammer.process();
#endif
}