/*!
 *
 * \file LinkAlignment.h
 * \brief Link alignment class, automated alignment procedure for CIC-lpGBT-BE
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef OTTool_h__
#define OTTool_h__

#include "Tool.h"
#include <signal.h>
using namespace Ph2_HwDescription;

struct PrintConfig
{
    uint8_t  fVerbose    = 0;
    uint32_t fPrintEvery = 1;
};

//***************************************************
struct boardData
{
    uint8_t boardId;
    uint8_t opticalGroupId;
    //
    uint32_t cTriggerId;
    uint32_t cBxIdBeBoard;
    uint32_t cEventId;
    //
    uint32_t              cBxId;
    std::vector<uint16_t> L1Id;
    std::vector<uint16_t> BxId;
    //
    // std::vector<uint8_t>  cL1StatusFEs;
    // std::vector<uint8_t>  cL1StatusCIC;
    std::vector<float> glblX;
    std::vector<float> glblY;
    std::vector<float> glblZ;
    std::vector<float> Zunique;
    // candidate track parameters
    std::vector<float> trackSlopes;
    std::vector<float> trackIntercepts;
    std::vector<float> residuals;
    std::vector<float> theta;
    std::vector<float> trackX;
    std::vector<float> trackY;
    std::vector<float> trackZ;
    std::vector<float> stubX;
    std::vector<float> stubY;
    std::vector<float> stubZ;
    std::vector<float> stubZunique;
    std::vector<float> stubBend;
    std::vector<float> stubBxIds;
    float              trackSlopeL;
    float              trackInterceptL;
    std::vector<float> trackYresidualsL;
    float              trackSlopeR;
    float              trackInterceptR;
    std::vector<float> trackYresidualsR;
    float              stubSlope;
    float              firstBxId;
    float              lastBxId;
};
struct feData
{
    std::vector<uint8_t>  HitOccupancy;
    std::vector<uint8_t>  ClusterMultiplicity;
    std::vector<uint8_t>  ClusterSize;
    std::vector<uint8_t>  CicL1Status;
    std::vector<uint8_t>  OrRocL1Status;
    std::vector<uint8_t>  RocL1Status;
    std::vector<uint8_t>  RocHitOccupancy;
    std::vector<uint8_t>  RocClusterMultiplicity;
    std::vector<uint8_t>  GlobalClusterOverflow;
    std::vector<uint8_t>  RocClusterOverflow;
    std::vector<uint16_t> BottomSensorOccupancy;
    std::vector<uint16_t> TopSensorOccupancy;
    std::vector<float>    hitX;
    std::vector<float>    hitY;
    std::vector<float>    hitZ;
};

// // Thread-safe queue
// template <typename T>
// class TSQueue {
// private:
//     // Underlying queue
//     std::queue<T> m_queue;

//     // mutex for thread synchronization
//     std::mutex m_mutex;

//     // Condition variable for signaling
//     std::condition_variable m_cond;

// public:
//     // Pushes an element to the queue
//     void push(T item)
//     {

//         // Acquire lock
//         std::unique_lock<std::mutex> lock(m_mutex);

//         // Add item
//         m_queue.push(item);

//         // Notify one thread that
//         // is waiting
//         m_cond.notify_one();
//     }

//     // Pops an element off the queue
//     T pop()
//     {

//         // acquire lock
//         std::unique_lock<std::mutex> lock(m_mutex);

//         // wait until queue is not empty
//         m_cond.wait(lock,
//                     [this]() { return !m_queue.empty(); });

//         // retrieve item
//         T item = m_queue.front();
//         m_queue.pop();

//         // return item
//         return item;
//     }
//     size_t size()
//     {
//       // acquire lock
//       // std::unique_lock<std::mutex> lock(m_mutex);
//       // acquire lock
//       return m_queue.size();
//     }
// };

class OTTool : public Tool
{
  public:
    OTTool();
    ~OTTool();

    // expect thise to be the same for
    void        Prepare();
    void        Reset();
    void        ConfigurePrintout(PrintConfig pCnfg);
    void        SaveHitDataTree() { fSaveTree = true; }
    void        SetChipRegstoPerserve(FrontEndType pType, std::vector<std::string> pListOfRegs);
    void        SetBrdRegstoPerserve(std::vector<std::string> pListOfRegs);
    void        SetReadoutPause(uint32_t pReadoutPause) { fReadoutPause = pReadoutPause; }
    void        ReadDataFromFile(std::string pRawFileName);
    void        ContinousReadout();
    void        SetName(std::string pName) { fMyName = pName; };
    void        TriggerMonitor(uint32_t pDelta_s = 1);
    static void StopTriggerMonitor(int signum)
    {
        LOG(INFO) << BOLDRED << "Caught Ctrl+C from command line [signum == " << signum << RESET;
        throw Exception("Ctrl+C caught from command line..");
    };
    void InjectPattern(Ph2_HwDescription::BeBoard* pBoard, std::vector<Ph2_HwInterface::Injection> pInjections, int pChipId = 0);
    void SetInjectionType(uint8_t pInjType) { fInjectionType = pInjType; }; // 0-digital, 1 - analogue
    void SetReadoutMode(uint8_t pType) { fReadoutMode = pType; };
    void StubDump();
    void PrepareForTP(Ph2_HwDescription::BeBoard* pBoard, uint8_t pTPAmplitude = 0xF0, uint8_t pTPDelay = 0x00);
    void SetSparsification(Ph2_HwDescription::BeBoard* pBoard);
    void ConfigureStubReadout(Ph2_HwDescription::BeBoard* pBoard);
    void PrepareForUser(Ph2_HwDescription::BeBoard* pBoard, uint8_t pLimitTriggers = 0);
    void ContinousReadoutExt(BeBoard* pBoard, std::thread* cReadoutThread);
    void CheckForResync(Ph2_HwDescription::BeBoard* pBoard);
    void ContinousReadoutTh(uint8_t cBrdId);
    void EventCountingTh(uint8_t cBrdId);
    void CheckForResyncTh(uint8_t cBrdId);

  protected:
    // success or fail
    bool fSuccess{false};
    // chips found on the detector
    uint8_t                 fWithCIC{0};
    uint8_t                 fWithLpGBT{0};
    uint8_t                 fWithMPA{0};
    uint8_t                 fWithSSA{0};
    uint8_t                 fWithCBC{0};
    bool                    fFEsConnected{false};
    std::map<uint8_t, bool> fSparsified;
    // readout related items
    uint32_t fNevents{100};
    uint32_t fEventCountInt{0};
    uint32_t fReadoutPause{0};
    uint32_t fEventCounter{0};
    // waits
    uint32_t fThreadWait{1}; // in us
    // stop trigger monitor
    uint8_t fStopTriggerMonitor{0};
    // type of injection
    uint8_t fInjectionType{0}; // 0-digital,1-analogue
    // type of readout
    uint8_t fReadoutMode{0}; // 0  - from board; 1 -- from .raw
    uint8_t fStopTriggersDone{1};

    void UpdateFromRegMap(Ph2_HwDescription::BeBoard* pBoard);
    void PrintData(Ph2_HwDescription::BeBoard* pBoard);
    void FillTree(Ph2_HwDescription::BeBoard* pBoard);
    void EventPrintout(Ph2_HwDescription::BeBoard* pBoard, Ph2_HwInterface::Event* pEvent);
    void ContinousReadout(Ph2_HwDescription::BeBoard* pBoard);
    void WaitForTriggers(Ph2_HwDescription::BeBoard* pBoard);
    // void ContinousReadoutTh(uint8_t cBrdId);
    void CheckFinishedTh(uint8_t cBrdId);
    void StartReadoutTh(uint8_t cBrdId);
    void CatchStop();
    void StubDump(Ph2_HwDescription::BeBoard* pBoard);

  private:
    // Containers
    DetectorDataContainer fBoardRegContainer;
    // configuration of print-out
    PrintConfig fPrintConfig;
    // name of the tool
    std::string fMyName;
    // Data container
    std::map<uint8_t, std::vector<uint32_t>> fReadoutData;
    std::map<uint8_t, size_t>                fDataPntr;
    std::map<uint8_t, size_t>                fEvenCounter;
    // std::map<uint8_t, TSQueue<uint32_t>> fReadoutQueue;
    std::map<uint8_t, bool> fReadoutValid;
    // list of registers to perserve
    std::vector<std::string>             fBrdRegsToPerserve;
    DetectorDataContainer                fChipRegsToPerserve;
    std::vector<Ph2_HwInterface::Event*> fMyEvents;

    //
    boardData fBoardData;
    feData    fFeData;
    bool      fSaveTree{0};
#ifdef __USE_ROOT__
    TTree* fTree{nullptr};
#endif
};
#endif
