#include "VTRxController.h"
using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

#include "D19cOpticalInterface.h"
#include "DTCOpticalInterface.h"

VTRxController::VTRxController() : OTTool() {}

VTRxController::~VTRxController() {}

// Initialization function
void VTRxController::Initialise()
{
    Prepare();
    SetName("VTRxController");
}

// State machine control functions
void VTRxController::Running()
{
    Initialise();
    Test();
    fSuccess = true;
}
uint8_t VTRxController::RegisterRead(OpticalGroup* pOpticalGroup, uint8_t pAddress)
{
    D19clpGBTInterface* clpGBTInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
    auto                cBoardId        = pOpticalGroup->getBeBoardId();
    auto                cBoardIter      = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    auto                clpGBT          = pOpticalGroup->flpGBT;
#if defined(__EMP__)
    if((*cBoardIter)->getBoardType() != BoardType::D19C)
    {
        DTCFWInterface*      pInterface        = static_cast<DTCFWInterface*>(fBeBoardFWMap.find(cBoardId)->second);
        DTCOpticalInterface* cOpticalInterface = static_cast<DTCOpticalInterface*>(pInterface->getFEConfigurationInterface());
        return cOpticalInterface->VTRxRegisterRead(clpGBT, pAddress);
    }
#endif
    fBeBoardInterface->setBoard(cBoardId);
    D19cFWInterface*      pInterface        = static_cast<D19cFWInterface*>(fBeBoardFWMap.find(cBoardId)->second);
    D19cOpticalInterface* cOpticalInterface = static_cast<D19cOpticalInterface*>(pInterface->getFEConfigurationInterface());

    // clpGBTInterface->ResetI2C(clpGBT, {0, 1, 2});
    // std::this_thread::sleep_for(std::chrono::milliseconds(30));
    clpGBTInterface->WriteChipReg(clpGBT, "I2CM1Config", 8);

    uint8_t cMasterId = 1, cSlaveAddress = 0x50, cNbyte = 1, cFrequency = 2;
    uint8_t cMasterConfig = (cNbyte << 2) | (cFrequency << 0);
    // read from VTRx+ register
    if(!cOpticalInterface->SingleMultiByteWriteI2C(clpGBT, cMasterId, cMasterConfig, cSlaveAddress, pAddress)) return 0;
    auto cReadBackValue = cOpticalInterface->SingleSingleByteReadI2C(clpGBT, cMasterId, cMasterConfig, cSlaveAddress);
    return cReadBackValue;
}
bool VTRxController::RegisterWrite(OpticalGroup* pOpticalGroup, uint8_t pAddress, uint8_t pValue)
{
    D19clpGBTInterface* clpGBTInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
    auto                cBoardId        = pOpticalGroup->getBeBoardId();
    auto                cBoardIter      = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    auto                clpGBT          = pOpticalGroup->flpGBT;
#if defined(__EMP__)
    if((*cBoardIter)->getBoardType() != BoardType::D19C)
    {
        DTCFWInterface*      pInterface        = static_cast<DTCFWInterface*>(fBeBoardFWMap.find(cBoardId)->second);
        DTCOpticalInterface* cOpticalInterface = static_cast<DTCOpticalInterface*>(pInterface->getFEConfigurationInterface());
        return cOpticalInterface->VTRxRegisterWrite(clpGBT, pAddress, pValue);
    }
#endif
    fBeBoardInterface->setBoard(cBoardId);
    D19cFWInterface*      pInterface        = static_cast<D19cFWInterface*>(fBeBoardFWMap.find(cBoardId)->second);
    D19cOpticalInterface* cOpticalInterface = static_cast<D19cOpticalInterface*>(pInterface->getFEConfigurationInterface());

    clpGBTInterface->WriteChipReg(clpGBT, "I2CM1Config", 8);

    uint8_t cMasterId = 1, cSlaveAddress = 0x50, cNbyte = 2, cFrequency = 2;
    uint8_t cMasterConfig = (cNbyte << 2) | (cFrequency << 0);
    // read from VTRx+ register
    uint16_t              cSlaveData = pValue << 8 | pAddress;
    std::vector<uint32_t> cDataToTransmit;
    cDataToTransmit.push_back(cSlaveData << 8 | cSlaveAddress);
    return cOpticalInterface->MultiMultiByteWriteI2C(clpGBT, cMasterId, cMasterConfig, cDataToTransmit);
}

bool VTRxController::Test()
{
    bool cSuccess = true;
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard)
        {
            auto                       cReadBackValue            = RegisterRead(cOpticalGroup, 0x15);
            std::map<uint8_t, uint8_t> cVTRxplusDefaultRegisters = fVTRxplusDefaultRegisters;
            if(cReadBackValue == 0x15)
            {
                cVTRxplusDefaultRegisters = fVTRxplusDefaultRegistersV13;
                LOG(INFO) << BOLDGREEN << "VTRx+ register map for version 1.3 is used!" << RESET;
            }
            else
            {
                LOG(INFO) << BOLDGREEN << "VTRx+ register map for version 1.2 is used!" << RESET;
            }

            auto cMapIterator = cVTRxplusDefaultRegisters.begin();
            do {
                cReadBackValue = RegisterRead(cOpticalGroup, cMapIterator->first);
                LOG(INFO) << BOLDGREEN << "VTRx+ register " << +(cMapIterator->first) << " contains the default value 0x" << std::hex << +cReadBackValue << std::dec << " ." << RESET;
                cMapIterator++;
            } while(cMapIterator != cVTRxplusDefaultRegisters.end());

            uint8_t cRegisterAddress = fLDDMap["BiasCurrent"];
            if(RegisterWrite(cOpticalGroup, cRegisterAddress, 0x2f))
            {
                cReadBackValue = RegisterRead(cOpticalGroup, cRegisterAddress);
                LOG(INFO) << BOLDGREEN << " Successful write to 0x" << std::hex << +cRegisterAddress << " read-back 0x" << +cReadBackValue << std::dec << RESET;
            }
        }
    }
    return cSuccess;
}

void VTRxController::Stop() {}

void VTRxController::Pause() {}

void VTRxController::Resume() {}

void VTRxController::writeObjects() {}