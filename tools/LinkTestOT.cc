#include "LinkTestOT.h"

#include "../HWInterface/D19cDebugFWInterface.h"
#include "../ProductionToolsOT/SFPMonitoringTool.h"
#include "../Utils/CBCChannelGroupHandler.h"
#include "../Utils/ContainerFactory.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

LinkTestOT::LinkTestOT() : OTTool() {}
LinkTestOT::~LinkTestOT() {}

// Initialization function
void LinkTestOT::Initialise()
{
    Prepare();
    SetName("LinkTestOT");
}

void LinkTestOT::CheckLink(const OpticalGroup* pOpticalGroup, uint8_t pPattern)
{
    SFPMonitoringTool   cSPMonitor(fConnectionFile);
    D19clpGBTInterface* clpGBTInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
    if(clpGBTInterface == nullptr) return;

    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    if((*cBoardIter)->isOptical() == 0) return;

    fBeBoardInterface->setBoard(cBoardId);
    D19cFWInterface*      cInterface      = static_cast<D19cFWInterface*>(fBeBoardFWMap.find(cBoardId)->second);
    D19cDebugFWInterface* cDebugInterface = cInterface->getDebugInterface();
    cDebugInterface->SetNIterations(1000);
    auto     cGroups  = clpGBTInterface->getGroups();
    uint32_t cPattern = (pPattern << 24) | (pPattern << 16) | (pPattern << 8) | pPattern;
    for(auto cGroup: cGroups)
    {
        std::vector<std::string>                cModuleLines;
        std::map<uint8_t, std::vector<uint8_t>> cBeMap;
        for(auto cMapItem: fELinkMap2S)
        {
            auto& cELinkPair = cMapItem.second;
            if(cELinkPair.first != cGroup) continue;
            cModuleLines.push_back(cMapItem.first);
            // left or right hybrid
            uint8_t pHybridId = (cMapItem.first.find("R") != std::string::npos) ? 0 : 1;
            pHybridId += 2 * pOpticalGroup->getId();
            auto cTmp    = cMapItem.first.substr(0, cMapItem.first.length() - 2);
            auto cBeLine = fuDTCMap[cTmp];
            if(std::find(cBeMap[pHybridId].begin(), cBeMap[pHybridId].end(), cBeLine) != cBeMap[pHybridId].end()) continue;
            cBeMap[pHybridId].push_back(cBeLine);
        } // get module + be lines connected to this group
        LOG(INFO) << BOLDYELLOW << "E-link Group#" << +cGroup << RESET;
        auto cChannels_perGroup = clpGBTInterface->getChannelsPerGroup(cGroup);
        clpGBTInterface->ConfigureRxPRBS(pOpticalGroup->flpGBT, {cGroup}, cChannels_perGroup, false);
        // LOG(INFO) << BOLDGREEN << "Internal LpGBT pattern generation" << RESET;
        clpGBTInterface->ConfigureRxSource(pOpticalGroup->flpGBT, {cGroup}, lpGBTconstants::PATTERN_CONST);
        clpGBTInterface->ConfigureDPPattern(pOpticalGroup->flpGBT, cPattern);
        auto cValue = cSPMonitor.GetMonitorable("RxPower", "l12", 3);
        LOG(INFO) << BOLDYELLOW << "VTRx+ checkSFP  " << cValue.first << " ± " << cValue.second << RESET;
        // for(auto cModuleLine : cModuleLines ) LOG (INFO) << BOLDYELLOW << "\t.. Module line -- " << cModuleLine << RESET;
        for(auto cMap: cBeMap)
        {
            fBeBoardInterface->WriteBoardReg((*cBoardIter), "fc7_daq_cnfg.physical_interface_block.slvs_debug.hybrid_select", cMap.first);
            fBeBoardInterface->WriteBoardReg((*cBoardIter), "fc7_daq_cnfg.physical_interface_block.slvs_debug.chip_select", 0);
            cDebugInterface->StubDebug(true, 6, false);
            cDebugInterface->L1ADebug(100, false);
            for(auto cLine: cMap.second)
            {
                auto cResult = cDebugInterface->CheckData(cLine, pPattern);
                if(cResult.fErrors.size() == 0)
                    LOG(INFO) << BOLDGREEN << "\t..Hybrid#" << +cMap.first << " SLVS line#" << +cLine << " " << cResult.fErrors.size() << " errors found in " << cResult.fBitsChecked << " bits."
                              << RESET;
                else
                {
                    LOG(INFO) << BOLDRED << "\t..Hybrid#" << +cMap.first << " SLVS line#" << +cLine << " " << cResult.fErrors.size() << " errors found" << cResult.fBitsChecked << " bits." << RESET;
                    size_t cStart      = 0;
                    size_t cWrdCounter = 0;
                    do {
                        for(size_t cWrd = 0; cWrd < 30; cWrd++)
                        {
                            if(cStart >= cResult.fData.length()) continue;

                            std::string cNibble = "";
                            for(size_t cIndx = 0; cIndx < 8; cIndx++)
                            {
                                if(cStart >= cResult.fData.length()) continue;
                                cNibble += cResult.fData[cStart];
                                cStart++;
                            }
                            if(cNibble == "00000000") LOG(INFO) << BOLDRED << "Word#" << +cWrdCounter << "/" << cResult.fData.length() / 8. << " " << cNibble << RESET;
                            cWrdCounter++;
                        }
                    } while(cStart < cResult.fData.length());
                }
            } // SLVS lines in the BE
        }
        clpGBTInterface->ConfigureRxSource(pOpticalGroup->flpGBT, {cGroup}, lpGBTconstants::PATTERN_NORMAL);
        clpGBTInterface->ConfigureDPPattern(pOpticalGroup->flpGBT, 0x00);
    }
}

// State machine control functions
void LinkTestOT::Running()
{
    Initialise();
    for(auto cBoard: *fDetectorContainer)
    {
        for(auto cOpticalGroup: *cBoard) { CheckLink(cOpticalGroup); }
    } // boards
}

void LinkTestOT::Stop() {}

void LinkTestOT::Pause() {}

void LinkTestOT::Resume() {}