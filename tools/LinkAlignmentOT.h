/*!
 *
 * \file LinkAlignment.h
 * \brief Link alignment class, automated alignment procedure for CIC-lpGBT-BE
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef LinkAlignmentOT_h__
#define LinkAlignmentOT_h__

#include "OTTool.h"

using namespace Ph2_HwDescription;

class LinkAlignmentOT : public OTTool
{
  public:
    LinkAlignmentOT();
    ~LinkAlignmentOT();

    void Initialise();
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;

    // get alignment results
    bool    getStatus() const { return fSuccess; }
    uint8_t GetLpGBTDelay(const OpticalGroup* pOpticalGroup) const
    {
        auto cBoardId   = pOpticalGroup->getBeBoardId();
        auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
        return fLpGBTSamplingDelay.at((*cBoardIter)->getIndex())->at(pOpticalGroup->getIndex())->at(0)->getSummary<uint8_t>();
    }
    void CheckLpgbtOutputs(uint8_t pPattern = 0xAA);

  protected:
  private:
    // Alignment parameters
    DetectorDataContainer fLpGBTSamplingDelay;
    bool                  CheckLpgbtOutputs(const Ph2_HwDescription::OpticalGroup* pOpticalGroup, uint8_t pPattern = 0xAA);
    bool                  AlignLpGBTInputs(const Ph2_HwDescription::OpticalGroup* pOpticalGroup);
    bool                  Align();
};
#endif
