#include "OTModuleStartUp.h"
#include "LinkInterface.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

OTModuleStartUp::OTModuleStartUp() : Tool() {}
OTModuleStartUp::~OTModuleStartUp() {}

void OTModuleStartUp::ModuleStartUpPS(const OpticalGroup* pOpticalGroup)
{
    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    LOG(INFO) << BOLDBLUE << "SystemController::ModuleStartUpPS for BeBoard#" << +(*cBoardIter)->getId() << " OpticalGroup#" << +pOpticalGroup->getId() << RESET;

    auto& clpGBT = pOpticalGroup->flpGBT;
    // configure PS ROHs
    if(clpGBT != nullptr)
    {
        const uint8_t cSsaClockDrive = 7;
        const uint8_t cCicClockDrive = 7;

        static_cast<D19clpGBTInterface*>(flpGBTInterface)->ConfigurePSROH(clpGBT);
        const std::vector<uint8_t> cGroupsExamples = {0, 1};
        for(auto cHybrid: *pOpticalGroup)
        {
            // first .. send clock to the SSAs on this hybrid
            uint8_t  cSide        = cHybrid->getId() % 2;
            uint16_t cReadoutRate = static_cast<D19clpGBTInterface*>(flpGBTInterface)->GetRxDataRate(clpGBT, cGroupsExamples[cSide]);
            LOG(INFO) << BOLDMAGENTA << "Readout rate on PS-module (Hybrid# " << +cHybrid->getId() << ") is " << +cReadoutRate << " Mbps" << RESET;

            lpGBTClockConfig cClkCnfg;
            cClkCnfg.fClkFreq         = 4;
            cClkCnfg.fClkDriveStr     = cSsaClockDrive;
            cClkCnfg.fClkInvert       = 1;
            cClkCnfg.fClkPreEmphWidth = 0;
            cClkCnfg.fClkPreEmphMode  = 0; // 3;
            cClkCnfg.fClkPreEmphStr   = 0; // 7;

            LOG(INFO) << BOLDBLUE << "Enabling SSA clock [Side == " << +cSide << "]" << RESET;
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->hybridClock(clpGBT, cClkCnfg, cSide);

            // enable clock to CIC
            cClkCnfg.fClkFreq     = (cReadoutRate == 320) ? 4 : 5;
            cClkCnfg.fClkInvert   = 0;
            cClkCnfg.fClkDriveStr = cCicClockDrive;
            LOG(INFO) << BOLDBLUE << "Enabling CIC clock [Side == " << +cSide << "]" << RESET;
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->cicClock(clpGBT, cClkCnfg, cSide);

            // hold resets
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->ssaReset(clpGBT, true, cSide);
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->mpaReset(clpGBT, true, cSide);
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->cicReset(clpGBT, true, cSide);

            // make sure all SSAs on a module are configured to produce a clock
            // regardless of how many are enabled on this hybrid
            LOG(INFO) << BOLDBLUE << "Resetting SSA" << RESET;
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetSSA(clpGBT, cSide);

            bool cSkipSSA3 = false; // eventually this needs to be set in the xml somewhere
            for(uint8_t cSSAId = 0; cSSAId < 8; cSSAId++)
            {
                if(cSkipSSA3 && cSSAId == 3) continue;

                SSA*    cSSA          = new SSA(cHybrid->getBeBoardId(), cHybrid->getFMCId(), cHybrid->getOpticalGroupId(), cHybrid->getId(), cSSAId, 0, 0, "./settings/SSAFiles/SSA.txt");
                uint8_t cSLVSdriveSSA = cSSA->getReg("SLVS_pad_current");
                cSSA->setOptical(cHybrid->isOptical());
                cSSA->setMasterId(cHybrid->getMasterId());
                LOG(INFO) << BOLDMAGENTA << "SSA " << +cSSAId << " current set to " << +cSLVSdriveSSA << "" << RESET;
                auto cRegItem = cSSA->getRegItem("SLVS_pad_current");
                (fBeBoardInterface->getFirmwareInterface())->SingleRegisterWrite(cSSA, cRegItem, false);
            }

        } // hybrid
    }     // lpGBT part ... resets + clocks
}

void OTModuleStartUp::ModuleStartUp2S(const OpticalGroup* pOpticalGroup)
{
    auto cBoardId   = pOpticalGroup->getBeBoardId();
    auto cBoardIter = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    LOG(DEBUG) << BOLDBLUE << "OTModuleStartUp::ModuleStartUp2S for BeBoard#" << +(*cBoardIter)->getId() << " OpticalGroup#" << +pOpticalGroup->getId() << RESET;

    // configure 2S SEHs
    auto& clpGBT = pOpticalGroup->flpGBT;
    if(clpGBT != nullptr)
    {
        uint8_t                    cHybridClockDrive = 7;
        uint8_t                    cPreEmphMode      = 0; // 3
        uint8_t                    cPreEmphStr       = 0; // 7
        const std::vector<uint8_t> cGroupsExamples   = {0, 1};
        static_cast<D19clpGBTInterface*>(flpGBTInterface)->Configure2SSEH(clpGBT);
        for(auto cHybrid: *pOpticalGroup)
        {
            uint8_t cSide = cHybrid->getId() % 2;
            // work out readout rate
            uint16_t cReadoutRate = flpGBTInterface->GetRxDataRate(clpGBT, cGroupsExamples[cSide]);
            LOG(DEBUG) << BOLDMAGENTA << "Readout rate on 2S-module (Hybrid# " << +cHybrid->getId() << ") is " << +cReadoutRate << " Mbps" << RESET;

            // first .. send clock to the CBCs on this hybrid
            lpGBTClockConfig cClkCnfg;
            cClkCnfg.fClkFreq         = 4;
            cClkCnfg.fClkDriveStr     = cHybridClockDrive;
            cClkCnfg.fClkInvert       = (cSide == 0) ? 1 : 0;
            cClkCnfg.fClkPreEmphWidth = 0;
            cClkCnfg.fClkPreEmphMode  = cPreEmphMode;
            cClkCnfg.fClkPreEmphStr   = cPreEmphStr;
            LOG(DEBUG) << BOLDBLUE << "Enabling Hybrid clock [Side == " << +cSide << "]" << RESET;
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->hybridClock(clpGBT, cClkCnfg, cSide);

            // hold CBC reset
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->cbcReset(clpGBT, true, cSide);
            // auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            // if(cCic == NULL) continue;
            // // Configure CICs on this hybrid
            // // release CIC reset
            // LOG(INFO) << BOLDBLUE << "Resetting CIC" << RESET;
            // static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetCIC(clpGBT, cSide);
        }
    } // lpGBT part ... resets + clocks
}

bool OTModuleStartUp::CicStartUp(const OpticalGroup* pOpticalGroup, bool cStartUpSequence)
{
    auto cBoardId    = pOpticalGroup->getBeBoardId();
    auto cBoardIter  = std::find_if(fDetectorContainer->begin(), fDetectorContainer->end(), [&cBoardId](Ph2_HwDescription::BeBoard* x) { return x->getId() == cBoardId; });
    bool cWith2SFEH  = (*cBoardIter)->getEventType() == EventType::VR2S;
    auto cSparsified = (*cBoardIter)->getSparsification();

    auto& clpGBT   = pOpticalGroup->flpGBT;
    bool  cSuccess = true;
    LOG(INFO) << BOLDGREEN << "####################################################################################" << RESET;
    // LOG(INFO) << BOLDGREEN << "OTModuleStartUp::CicStartUp Link#" << +pOpticalGroup->getId() << RESET;

    LOG(INFO) << BOLDGREEN << "\t...Configuring CIC on Link#" << +pOpticalGroup->getId() << RESET;
    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == NULL) continue;

        // LOG(INFO) << BOLDBLUE << "Configuring CIC" << +(cHybrid->getId() % 2) << " on link " << +cHybrid->getOpticalGroupId() << " on hybrid " << +cHybrid->getId() << RESET;
        fCicInterface->ConfigureChip(cCic);
        fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, false); // make sure all FEs are disabled by default
    }

    for(auto cHybrid: *pOpticalGroup)
    {
        auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
        if(cCic == NULL) continue;

        // LOG(INFO) << BOLDMAGENTA << "OTModuleStartUp::CicStartUp for OpticalGroup#" << +pOpticalGroup->getId() << " CIC on Hybrid#" << +cCic->getHybridId() << RESET;

        // if there is an lpGBT .
        // its configuration overwrites whatever is in the xml
        if(clpGBT != nullptr)
        {
            auto     cChipRate     = static_cast<D19clpGBTInterface*>(flpGBTInterface)->GetChipRate(clpGBT);
            uint16_t cClkFrequency = (cChipRate == 5) ? 320 : 640;
            // LOG (INFO) << BOLDYELLOW << " Configuring CIC clock rate to " << cClkFrequency << RESET;
            cCic->setClockFrequency(cClkFrequency);
        }

        // CIC start-up
        auto cType       = FrontEndType::CBC3;
        auto cHybridIter = std::find_if(cHybrid->begin(), cHybrid->end(), [&cType](Ph2_HwDescription::Chip* x) { return x->getFrontEndType() == cType; });
        bool cIs2S       = cHybridIter != cHybrid->end();
        // 0 --> CBC , 1 --> MPA
        uint8_t cModeSelect = (cIs2S) ? 0 : 1;
        uint8_t cBx0Delay   = (cIs2S) ? 8 : 22;
        // select CIC mode
        cSuccess = fCicInterface->SelectMode(cCic, cModeSelect);
        if(!cSuccess)
        {
            LOG(INFO) << BOLDRED << "FAILED " << BOLDBLUE << " to configure CIC mode.." << RESET;
            throw std::runtime_error(std::string("FAILED to set CIC mode ... something is wrong... .. STOPPING"));
        }
        // LOG(INFO) << BOLDMAGENTA << "CIC configured for " << (cIs2S ? "2S" : "PS") << " readout." << RESET;

        // configure CIC FE enable register
        // first make sure it is set to 0x00
        fCicInterface->EnableFEs(cCic, {0, 1, 2, 3, 4, 5, 6, 7}, false);
        // figure out which Chips are enabled
        std::vector<uint8_t> cChipIds(0);
        for(auto cReadoutChip: *cHybrid)
        {
            // only consider MPAs and CBCs
            if(pOpticalGroup->getFrontEndType() != FrontEndType::HYBRIDPS && (cReadoutChip->getFrontEndType() == FrontEndType::SSA || cReadoutChip->getFrontEndType() == FrontEndType::SSA2)) continue;
            cChipIds.push_back(cReadoutChip->getId() % 8);
        }
        fCicInterface->EnableFEs(cCic, cChipIds, true);

        // make sure data rate is correctly configured
        // only works for CIC2
        if(cCic->getFrontEndType() == FrontEndType::CIC2)
        {
            uint8_t cFeConfigReg  = fCicInterface->ReadChipReg(cCic, "FE_CONFIG");
            auto    cClkFrequency = cCic->getClockFrequency();
            uint8_t cNewValue     = (cFeConfigReg & 0xFD) | ((uint8_t)(cClkFrequency == 640) << 1);
            cSuccess              = fCicInterface->WriteChipReg(cCic, "FE_CONFIG", cNewValue);
        }

        // 2S-FEHs
        // CIC start-up sequence
        uint8_t cClkTerm = 1;
        uint8_t cRxTerm  = 1;
        if(cWith2SFEH)
        {
            cClkTerm = 0;
            cRxTerm  = 1;
        }
        cSuccess = fCicInterface->ConfigureTermination(cCic, cClkTerm, cRxTerm);
        if(cSuccess)
        {
            if(cStartUpSequence)
            {
                // LOG(INFO) << BOLDYELLOW << "Launching CIC start-up sequence.." << RESET;
                cSuccess = fCicInterface->StartUp(cCic, cCic->getDriveStrength(), cCic->getEdgeSelect());
            }
            else
            {
                // LOG(INFO) << BOLDYELLOW << "Not launching CIC start-up sequence.. but will configure drive strength and FCMD edge from xml.." << RESET;
                if(fCicInterface->ConfigureDriveStrength(cCic, cCic->getDriveStrength()))
                    cSuccess = fCicInterface->ConfigureFCMDEdge(cCic, cCic->getEdgeSelect());
                else
                    cSuccess = false;
            }
        }
        else
            throw std::runtime_error(std::string("FAILED to start-up CIC ... something is wrong... .. STOPPING"));

        if(cSuccess)
            cSuccess = fCicInterface->SetSparsification(cCic, cSparsified);
        else
            throw std::runtime_error(std::string("FAILED to set CIC sparsification... .. STOPPING"));

        if(cSuccess)
            cSuccess = fCicInterface->ConfigureStubOutput(cCic);
        else
            throw std::runtime_error(std::string("FAILED to configure CIC stub output... .. STOPPING"));

        if(cSuccess)
            cSuccess = fCicInterface->ManualBx0Alignment(cCic, cBx0Delay);
        else
            throw std::runtime_error(std::string("FAILED to configure CIC Bx0 delay... .. STOPPING"));

        fCicInterface->setInitialized(cCic, 1);

    } // all hybrids connected to this OG
    LOG(INFO) << BOLDGREEN << "####################################################################################" << RESET;
    return cSuccess;
}
void OTModuleStartUp::Reconfigure(BeBoard* pBoard)
{
    LOG(INFO) << BOLDMAGENTA << "OTModuleStartUp::InitializeOT Reconfiguring OT hardware..on BeBoard#" << +pBoard->getId() << RESET;
    auto cInterface = static_cast<D19cFWInterface*>(fBeBoardInterface->getFirmwareInterface());
    cInterface->setConfigured(pBoard, 1);
    LOG(INFO) << BOLDYELLOW << "Forcing Link+Board reset for reconfigure : " << BOLDRED << " OFF " << RESET;
    pBoard->setReset(0);
    pBoard->setLinkReset(0);
}
void OTModuleStartUp::InitializeOT(BeBoard* pBoard)
{
    LOG(INFO) << BOLDMAGENTA << "OTModuleStartUp::InitializeOT Initializing OT hardware..on BeBoard#" << +pBoard->getId() << RESET;
    LOG(INFO) << BOLDYELLOW << "Forcing Link+Board reset during first start-up : " << BOLDGREEN << " ON " << RESET;
    pBoard->setLinkReset(1);
    pBoard->setReset(1);
    fBeBoardInterface->ConfigureBoard(pBoard);
    LOG(INFO) << BOLDYELLOW << "Forcing Link+Board reset after first start-up : " << BOLDRED << " OFF " << RESET;
    pBoard->setLinkReset(0);
    pBoard->setReset(0);

    for(auto cOpticalGroup: *pBoard)
    {
        if(cOpticalGroup->flpGBT == nullptr) continue;

        LOG(INFO) << BOLDBLUE << "Now going to configuring lpGBTs#" << +cOpticalGroup->getId() << " on Board " << int(pBoard->getId()) << RESET;
        D19clpGBTInterface* clpGBTInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
        if(cOpticalGroup->getReset() == 0)
        {
            LOG(INFO) << BOLDYELLOW << "Will not re-configure lpGBT on Link#" << +cOpticalGroup->getId() << RESET;
            continue;
        }

        if(!clpGBTInterface->ConfigureChip(cOpticalGroup->flpGBT))
        {
            LOG(INFO) << BOLDRED << "SOMETHING FUNNY" << RESET;
            continue;
        }
    }

    // module start-up
    // depends on module type
    for(auto cOpticalGroup: *pBoard)
    {
        if(cOpticalGroup->getReset() == 0)
        {
            LOG(INFO) << BOLDYELLOW << "Will not re-configure lpGBT for specific module type.." << RESET;
            continue;
        }

        if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTracker2S)
        {
            LOG(INFO) << BOLDMAGENTA << "Configuring an OuterTracker2S module on Link#" << +cOpticalGroup->getId() << RESET;
            ModuleStartUp2S(cOpticalGroup);
        }
        if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTrackerPS)
        {
            LOG(INFO) << BOLDMAGENTA << "Configuring an OuterTrackerPS module on Link#" << +cOpticalGroup->getId() << RESET;
            ModuleStartUpPS(cOpticalGroup);
        }
    }

    // CIC reset
    for(auto cOpticalGroup: *pBoard)
    {
        auto& clpGBT = cOpticalGroup->flpGBT;
        if(clpGBT == nullptr) continue;

        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            if(cCic == NULL) continue;
            uint8_t cSide = cHybrid->getId() % 2;
            // LOG(INFO) << BOLDYELLOW << "Applying reset to CIC on hybrid" << +cSide << " on Link#" << +cHybrid->getOpticalGroupId() << RESET;
            // apply reset to CIC
            static_cast<D19clpGBTInterface*>(flpGBTInterface)->resetCIC(clpGBT, cSide);
        }
    }

    // CIC start-up
    // in the 2S module case .. one fast command line for the hybrid
    if(pBoard->getBoardType() == BoardType::D19C) { fBeBoardInterface->WriteBoardReg(pBoard, "fc7_daq_cnfg.fast_command_block.hybrid_veto", 0x0000000); }
    for(auto cOpticalGroup: *pBoard)
    {
        bool cSuccess = CicStartUp(cOpticalGroup, true);
        if(!cSuccess)
        {
            LOG(INFO) << BOLDRED << "Failed start-up sequence on OG" << +cOpticalGroup->getId() << RESET;
            throw std::runtime_error(std::string("FAILED to start-up CIC... something is wrong... .. STOPPING"));
        }
    }

    // remove hard reset to ROCs
    for(auto cOpticalGroup: *pBoard)
    {
        auto& clpGBT = cOpticalGroup->flpGBT;
        if(clpGBT == nullptr) continue;
        auto cInterface = static_cast<D19clpGBTInterface*>(flpGBTInterface);
        for(auto cHybrid: *cOpticalGroup)
        {
            auto cSide = cHybrid->getId() % 2;
            // if(cHybrid->getReset() == 0) continue;
            if(cOpticalGroup->getFrontEndType() == FrontEndType::OuterTracker2S)
                cInterface->cbcReset(clpGBT, false, cSide);
            else
            {
                cInterface->ssaReset(clpGBT, false, cSide);
                cInterface->mpaReset(clpGBT, false, cSide);
            }
        }
    }
    fBeBoardInterface->getFirmwareInterface()->setConfigured(pBoard, 1);
}
void OTModuleStartUp::CheckForResync(BeBoard* pBoard)
{
    // check if a resync is needed
    LOG(INFO) << BOLDBLUE << "Checking if a ReSync is needed for Board" << +pBoard->getId() << RESET;
    bool cReSyncNeeded = false;
    for(auto cOpticalGroup: *pBoard)
    {
        for(auto cHybrid: *cOpticalGroup)
        {
            auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
            if(cCic == NULL) continue;
            if(cReSyncNeeded) continue;

            bool cReSyncCIC = fCicInterface->GetResyncRequest(cCic);
            if(cReSyncCIC) LOG(INFO) << BOLDMAGENTA << "\t... CIC" << +cHybrid->getId() << " requires a ReSync" << RESET;

            std::vector<uint8_t> cResyncsCbcs(0);
            for(auto cChip: *cHybrid)
            {
                if(cChip->getFrontEndType() != FrontEndType::CBC3) continue;
                auto cValue = fReadoutChipInterface->ReadChipReg(cChip, "SerialIface&Error");
                if(cValue != 0x00)
                    LOG(INFO) << BOLDMAGENTA << "\t...Link#" << +cOpticalGroup->getId() << " Hybrid#" << +cHybrid->getId() % 2 << " CBC" << +cChip->getId() << " requires a ReSync : Error Register "
                              << std::bitset<8>(cValue) << RESET;

                cResyncsCbcs.push_back(((cValue & 0x1F) != 0x00) ? 1 : 0);
            }
            bool cReSyncCBCs = std::accumulate(cResyncsCbcs.begin(), cResyncsCbcs.end(), 0) > 0;
            cReSyncNeeded    = cReSyncNeeded || cReSyncCBCs || cReSyncCIC;
        }
    }

    if(cReSyncNeeded)
    {
        LOG(INFO) << BOLDMAGENTA << "Sending a ReSync at the end of the OT-module configuration step" << RESET;
        // send a ReSync to all chips before starting
        fBeBoardInterface->ChipReSync(pBoard);
        // check resync request has been cleared
        for(auto cOpticalGroup: *pBoard)
        {
            for(auto cHybrid: *cOpticalGroup)
            {
                auto& cCic = static_cast<OuterTrackerHybrid*>(cHybrid)->fCic;
                if(cCic == NULL) continue;

                if(fCicInterface->GetResyncRequest(cCic))
                {
                    LOG(INFO) << BOLDRED << "ReSync request ofrom CIC" << +cHybrid->getId() << RESET;
                    throw std::runtime_error(std::string("FAILED to clear CIC ReSync request"));
                }
            }
        }
    }
    else
        LOG(INFO) << BOLDMAGENTA << "No ReSync needed after OT-module configuration step" << RESET;
}
bool OTModuleStartUp::StartUp()
{
    LOG(INFO) << BOLDYELLOW << "OTModuleStartUp::StartUp ..." << RESET;
    for(const auto cBoard: *fDetectorContainer)
    {
        if(fColdStart) { InitializeOT(cBoard); }
        else
            Reconfigure(cBoard);
    } // initialize modules + BeBoards
    // configure BeBoards + modules
    ConfigureHw();
    // check for resync
    for(const auto cBoard: *fDetectorContainer)
    {
        CheckForResync(cBoard);
        // fBeBoardInterface->ChipReSync(cBoard);
    }
    // ADC
    for(auto cBoard: *fDetectorContainer)
    {
        for(const auto cOpticalGroup: *cBoard)
        {
            auto& clpGBT = cOpticalGroup->flpGBT;
            if(clpGBT == nullptr) continue;

            flpGBTInterface->GetADCOffset(clpGBT, false);
            flpGBTInterface->GetADCGain(clpGBT, false);
            // flpGBTInterface->SetVrefTune(clpGBT, "ADC2", 0.504);
        }
    }

    // configure package delay from xml node
    for(auto cBoard: *fDetectorContainer)
    {
        if(cBoard->getBoardType() != BoardType::D19C) continue;
        for(const auto cOpticalGroup: *cBoard)
        {
            auto        cCnfg    = cOpticalGroup->getStubCnfg();
            std::string cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link0_link9";
            if(cOpticalGroup->getId() > 9) cRegName = "fc7_daq_cnfg.physical_interface_block.stubs_package_delay_link10_link11";

            LOG(INFO) << BOLDYELLOW << "OTModuleStartUp::StartUp setting stub package delay for Link#" << +cOpticalGroup->getId() << " to " << +cCnfg.first << RESET;
            fBeBoardInterface->WriteBoardReg(cBoard, cRegName, cCnfg.first);
        }
    }
    return true;
}

// State machine control functions
void OTModuleStartUp::Running() { StartUp(); }

void OTModuleStartUp::Stop() {}

void OTModuleStartUp::Pause() {}

void OTModuleStartUp::Resume() {}
