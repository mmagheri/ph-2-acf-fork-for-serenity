/*!
 *
 * \file LinkAlignment.h
 * \brief Link alignment class, automated alignment procedure for CIC-lpGBT-BE
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \date 28 / 06 / 19
 *
 * \Support : sarah.storey@cern.ch
 *
 */

#ifndef VTRxController_h__
#define VTRxController_h__

#include "OTTool.h"

#ifdef __USE_ROOT__
#endif

using namespace Ph2_HwDescription;

class VTRxController : public OTTool
{
  public:
    VTRxController();
    ~VTRxController();

    void Initialise();
    void Running() override;
    void Stop() override;
    void Pause() override;
    void Resume() override;
    // void Reset();
    void writeObjects();

    uint8_t RegisterRead(Ph2_HwDescription::OpticalGroup* pOpticalGroup, std::string cRegName) { return RegisterRead(pOpticalGroup, fLDDMap[cRegName]); }
    bool    RegisterWrite(Ph2_HwDescription::OpticalGroup* pOpticalGroup, std::string cRegName, uint8_t pValue) { return RegisterWrite(pOpticalGroup, fLDDMap[cRegName], pValue); }

  protected:
    std::map<std::string, uint8_t> fLDDMap                      = {{"ControlRegister", 0x04}, {"BiasCurrent", 0x05}, {"ModulationCurrent", 0x06}, {"Emphasis", 0x07}};
    std::map<uint8_t, uint8_t>     fVTRxplusDefaultRegisters    = {{0x00, 0x0f}, {0x01, 0x01}, {0x04, 0x0f}, {0x05, 0x2f}, {0x06, 0x26}, {0x07, 0x00}};
    std::map<uint8_t, uint8_t>     fVTRxplusDefaultRegistersV13 = {{0x00, 0x01}, {0x02, 0x01}, {0x03, 0x30}, {0x04, 0xa0}, {0x05, 0x00}, {0x11, 0x07}};
    std::map<uint8_t, std::string> fI2CStatusMap                = {{4, "TransactionSucess"}, {8, "SDAPulledLow"}, {32, "InvalidCommand"}, {64, "NotACK"}};

  private:
    bool    Test();
    uint8_t RegisterRead(Ph2_HwDescription::OpticalGroup* pOpticalGroup, uint8_t pAddress);
    bool    RegisterWrite(Ph2_HwDescription::OpticalGroup* pOpticalGroup, uint8_t pAddress, uint8_t pValue);
};
#endif
