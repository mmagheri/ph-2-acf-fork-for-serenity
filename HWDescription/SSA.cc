/*!

        \file                   SSA.cc
        \brief                  SSA Description class, config of the SSAs
        \author                 Marc Osherson (copying from Cbc.h)
        \version                1.0
        \date                   31/07/19
        Support :               mail to : oshersonmarc@gmail.com

 */

#include "SSA.h"
#include "../Utils/ChannelGroupHandler.h"
#include "Definition.h"
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string.h>

namespace Ph2_HwDescription
{ // open namespace

SSA::SSA(const FrontEndDescription& pFeDesc, uint8_t pChipId, uint8_t pPartnerId, uint8_t pSSASide, const std::string& filename) : ReadoutChip(pFeDesc, pChipId)
{
    fChipCode         = 3;
    fChipAddress      = 0x20 + pChipId % 8;
    fMaxRegValue      = 255; // 8 bit registers in CBC
    fChipOriginalMask = std::make_shared<ChannelGroup<NSSACHANNELS>>();
    fChipOriginalMask->enableAllChannels();
    fPartnerId = pPartnerId;
    loadfRegMap(filename);
    for(auto& cMapItem: fRegMap)
    {
        if(cMapItem.first.find("_ALL") == std::string::npos) continue;
        LOG(DEBUG) << BOLDYELLOW << "Control register on SSA : " << cMapItem.first << RESET;
        cMapItem.second.fControlReg = 1;
    }
    for(auto& cMapItem: fRegMap)
    {
        if((cMapItem.first.find("_St") != std::string::npos)) continue;
        if((cMapItem.first.find("_S") == std::string::npos)) continue;
        cMapItem.second.fLocalReg = 1;
    }
    setFrontEndType(FrontEndType::SSA);
    this->setNumberOfChannels(NSSACHANNELS);
    this->setClockFrequency(320);
}

SSA::SSA(uint8_t pBeBoardId, uint8_t pFMCId, uint8_t pOpticalGroupId, uint8_t pHybridId, uint8_t pChipId, uint8_t pPartnerId, uint8_t pSSASide, const std::string& filename)
    : ReadoutChip(pBeBoardId, pFMCId, pOpticalGroupId, pHybridId, pChipId)
{
    fChipCode         = 3;
    fChipAddress      = 0x20 + pChipId % 8;
    fMaxRegValue      = 255; // 8 bit registers in CBC
    fChipOriginalMask = std::make_shared<ChannelGroup<NSSACHANNELS>>();
    fChipOriginalMask->enableAllChannels();
    fPartnerId = pPartnerId;
    loadfRegMap(filename);
    for(auto& cMapItem: fRegMap)
    {
        if(cMapItem.first.find("_ALL") == std::string::npos) continue;
        LOG(DEBUG) << BOLDYELLOW << "Control register on SSA : " << cMapItem.first << RESET;
        cMapItem.second.fControlReg = 1;
    }
    for(auto& cMapItem: fRegMap)
    {
        if((cMapItem.first.find("_St") != std::string::npos)) continue;
        if((cMapItem.first.find("_S") == std::string::npos)) continue;
        cMapItem.second.fLocalReg = 1;
    }
    setFrontEndType(FrontEndType::SSA);
    this->setNumberOfChannels(NSSACHANNELS);
    this->setClockFrequency(320);
}

void SSA::loadfRegMap(const std::string& filename)
{ // start loadfRegMap
    std::ifstream file(filename.c_str(), std::ios::in);
    if(file)
    {
        std::string line, fName, fPage_str, fAddress_str, fDefValue_str, fValue_str;
        int         cLineCounter = 0;
        ChipRegItem fRegItem;

        // fhasMaskedChannels = false;
        while(getline(file, line))
        {
            // std::cout<< __PRETTY_FUNCTION__ << " " << line << std::endl;
            if(line.find_first_not_of(" \t") == std::string::npos)
            {
                fCommentMap[cLineCounter] = line;
                cLineCounter++;
                // continue;
            }

            else if(line.at(0) == '#' || line.at(0) == '*' || line.empty())
            {
                // if it is a comment, save the line mapped to the line number so I can later insert it in the same
                // place
                fCommentMap[cLineCounter] = line;
                cLineCounter++;
                // continue;
            }
            else
            {
                std::istringstream input(line);
                input >> fName >> fPage_str >> fAddress_str >> fDefValue_str >> fValue_str;

                fRegItem.fPage     = strtoul(fPage_str.c_str(), 0, 16);
                fRegItem.fAddress  = strtoul(fAddress_str.c_str(), 0, 16);
                fRegItem.fDefValue = strtoul(fDefValue_str.c_str(), 0, 16);
                fRegItem.fValue    = strtoul(fValue_str.c_str(), 0, 16);

                // LOG(INFO) << "CURS " << fRegItem.fAddress - 0x0101;
                if(fRegItem.fPage == 0x00 && fRegItem.fAddress >= 0x0101 && fRegItem.fAddress <= 0x0178)
                { // Register is a Mask
                    if(fRegItem.fValue == 0x0)
                    {
                        // LOG(INFO) << "DISABLE " << fRegItem.fAddress - 0x0101<<std::endl;
                        fChipOriginalMask->disableChannel(fRegItem.fAddress - 0x0101);
                    }
                }
                fRegMap[fName] = fRegItem;
                // std::cout << __PRETTY_FUNCTION__ << +fRegItem.fValue << std::endl;
                cLineCounter++;
            }
        }

        file.close();
    }
    else
    {
        LOG(ERROR) << "The SSA Settings File " << filename << " does not exist!";
        exit(1);
    }

} // end loadfRegMap

void SSA::saveRegMap(const std::string& filename)
{ // start saveRegMap

    std::ofstream file(filename.c_str(), std::ios::out | std::ios::trunc);

    if(file)
    {
        std::set<SSARegPair, RegItemComparer> fSetRegItem;

        for(auto& it: fRegMap) fSetRegItem.insert({it.first, it.second});

        int cLineCounter = 0;

        for(const auto& v: fSetRegItem)
        {
            while(fCommentMap.find(cLineCounter) != std::end(fCommentMap))
            {
                auto cComment = fCommentMap.find(cLineCounter);

                file << cComment->second << std::endl;
                cLineCounter++;
            }

            file << v.first;

            for(int j = 0; j < 48; j++) file << " ";

            file.seekp(-v.first.size(), std::ios_base::cur);

            file << "0x" << std::setfill('0') << std::setw(2) << std::hex << std::uppercase << int(v.second.fPage) << "\t0x" << std::setfill('0') << std::setw(2) << std::hex << std::uppercase
                 << int(v.second.fAddress) << "\t0x" << std::setfill('0') << std::setw(2) << std::hex << std::uppercase << int(v.second.fDefValue) << "\t0x" << std::setfill('0') << std::setw(2)
                 << std::hex << std::uppercase << int(v.second.fValue) << std::endl;

            cLineCounter++;
        }

        file.close();
    }
    else
        LOG(ERROR) << "Error opening file";
} // end saveRegMap

} // namespace Ph2_HwDescription
