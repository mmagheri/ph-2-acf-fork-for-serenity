#include "../miniDAQ/MiddlewareStateMachine.h"
#include "../HWInterface/FC7FpgaConfig.h"
#include "../System/FileParser.h"
#include "../tools/Tool.h"

using namespace Ph2_System;
using namespace Ph2_HwInterface;

MiddlewareStateMachine::MiddlewareStateMachine() {}

MiddlewareStateMachine::~MiddlewareStateMachine()
{
    delete fTheTool;
    fTheTool = nullptr;
}

void MiddlewareStateMachine::initialize()
{
    LOG(INFO) << "Initialized" << RESET;
    return;
}

void MiddlewareStateMachine::configure(const std::string& calibrationName, const std::string& configurationFile)
{
    fTheTool = fCombinedCalibrationFactory.CreateCombinedCalibration(calibrationName);

    LOG(INFO) << BOLDBLUE << "Tool created" << RESET;

    LOG(INFO) << "Configuration file: " << configurationFile << RESET;

    fTheTool->Configure(configurationFile, true, false);

    LOG(INFO) << "Configured" << RESET;

    return;
}

void MiddlewareStateMachine::start(int runNumber)
{
    currentRun_ = runNumber;
    fTheTool->Start(currentRun_);
    LOG(INFO) << "Run " << currentRun_ << " started" << RESET;
    return;
}

void MiddlewareStateMachine::stop()
{
    fTheTool->Stop();
    LOG(INFO) << "Run " << currentRun_ << " stopped" << RESET;
    return;
}

void MiddlewareStateMachine::halt()
{
    try
    {
        stop();
    }
    catch(const std::exception& e)
    {
        LOG(WARNING) << "Could not stop the run, going to call Destroy anyway" << RESET;
    }

    fTheTool->Destroy();
    LOG(INFO) << "Halted" << RESET;
}

void MiddlewareStateMachine::pause()
{
    fTheTool->Pause();
    LOG(INFO) << "Paused" << RESET;
}

void MiddlewareStateMachine::resume()
{
    fTheTool->Resume();
    LOG(INFO) << "Resumed" << RESET;
}

void MiddlewareStateMachine::abort()
{
    fTheTool->Destroy();
    LOG(INFO) << "Aborted" << RESET;
}

MiddlewareStateMachine::Status MiddlewareStateMachine::status() { return fTheTool->GetRunningStatus() ? Status::DONE : Status::RUNNING; }

FC7FpgaConfig MiddlewareStateMachine::getFpgaConfig(const std::string& configurationFile, uint16_t boardId)
{
    FileParser                     theFileParser;
    std::map<uint16_t, RegManager> theRegManagerList = theFileParser.getRegManagerList(configurationFile);

    try
    {
        return FC7FpgaConfig(std::move(theRegManagerList.at(boardId)));
    }
    catch(const std::exception& e)
    {
        std::string errorMessage = "Board with id " + std::to_string(boardId) + " does not exist in file " + configurationFile;
        throw std::runtime_error(errorMessage);
    }
}

std::vector<std::string> MiddlewareStateMachine::getFirmwareList(const std::string& configurationFile, uint16_t boardId)
{
    FC7FpgaConfig theFC7FpgaConfig = getFpgaConfig(configurationFile, boardId);
    return theFC7FpgaConfig.getFpgaConfigList();
}

void MiddlewareStateMachine::deleteFirmwareFromSDcard(const std::string& configurationFile, const std::string& firmwareName, uint16_t boardId)
{
    FC7FpgaConfig theFC7FpgaConfig = getFpgaConfig(configurationFile, boardId);
    theFC7FpgaConfig.deleteFpgaConfig(firmwareName);
    LOG(INFO) << "Firmware image: " << firmwareName << " deleted from SD card";
}

void MiddlewareStateMachine::loadFirmwareInFPGA(const std::string& configurationFile, const std::string& firmwareName, uint16_t boardId)
{
    FC7FpgaConfig theFC7FpgaConfig = getFpgaConfig(configurationFile, boardId);
    theFC7FpgaConfig.jumpToFpgaConfig(firmwareName);
    LOG(INFO) << "Firmware image: " << firmwareName << " loaded on FPGA";
}

void MiddlewareStateMachine::uploadFirmwareOnSDcard(const std::string& configurationFile, const std::string& firmwareName, const std::string& firmwareFile, uint16_t boardId)
{
    FC7FpgaConfig theFC7FpgaConfig = getFpgaConfig(configurationFile, boardId);
    theFC7FpgaConfig.flashProm(firmwareName, firmwareFile);
}

void MiddlewareStateMachine::downloadFirmwareFromSDcard(const std::string& configurationFile, const std::string& firmwareName, const std::string& firmwareFile, uint16_t boardId)
{
    FC7FpgaConfig theFC7FpgaConfig = getFpgaConfig(configurationFile, boardId);
    theFC7FpgaConfig.downloadFpgaConfig(firmwareName, firmwareFile);
}
